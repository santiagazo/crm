<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    public function job()
    {
        return $this->hasOne('Milne\Job');
    }

    public function user()
    {
        return $this->belongsTo('Milne\User');
    }
    
    public function person()
    {
        return $this->belongsTo('Milne\Person', 'person_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo('Milne\Status');
    }

    public function type()
    {
        return $this->belongsTo('Milne\Type');
    }

    public function estimate()
    {
        return $this->belongsTo('Milne\Estimate');
    }

    public function notes()
    {
        return $this->morphMany('Milne\Note', 'noteable');
    }

    public function material_list()
    {
        return $this->morphOne('Milne\MaterialList', 'materialable');
    }
    
    public function media()
    {
        return $this->morphMany('Milne\Media', 'mediable');
    }

    public function pitches()
    {
        return $this->morphMany('Milne\Pitch', 'pitchable');
    }

    public function uc_name()
    {
        return "Bid";
    }

    public function lc_name()
    {
        return "bid";
    }

    public function tax()
    {
        return $this->belongsTo('Milne\Tax');
    }
}
