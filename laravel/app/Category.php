<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'topic',
        'name',
        'slug',
        'price',
        'unit',
        'is_house_product',
        'colors'
    ];

    public function products()
    {
        return $this->hasMany('Milne\Product');
    }

    public function materialLists()
    {
        return $this->belongsToMany('Milne\MaterialList')
        ->withPivot('product_id', 'qty', 'category_price', 'supplier_price', 'color', 'is_house_product')
        ->orderBy('pivot_sort_id','asc');
    }

    public function products_through_pivot()
    {
        return $this->belongsToMany('Milne\Product', 'category_material_list')
        ->withPivot('material_list_id', 'sort_id', 'qty', 'category_price', 'supplier_price', 'color', 'is_house_product')
        ->orderBy('pivot_sort_id','asc');
    }
}
