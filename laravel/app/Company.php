<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name',
        'type_id',
        'slug',
        'tax_amount'
    ];

    public function people()
    {
        return $this->hasMany('Milne\Person');
    }

    public function products()
    {
        return $this->hasMany('Milne\Product');
    }

    public function type()
    {
        return $this->belongsTo('Milne\Type');
    }

    public function material_list()
    {
        return $this->hasMany('Milne\MaterialList');
    }
}
