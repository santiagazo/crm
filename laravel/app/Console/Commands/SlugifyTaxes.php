<?php

namespace Milne\Console\Commands;

use Illuminate\Console\Command;

use Milne\Tax;

class SlugifyTaxes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slugify:taxes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Go through the Taxes Table and slugify all the names.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $taxes = Tax::all();

        foreach ($taxes as $tax) {
            $tax->slug = str_slug($tax->city, '_');
            $tax->save();
        }
    }
}
