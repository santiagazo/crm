<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Estimate extends Model
{
    public function person()
    {
        return $this->belongsTo('Milne\Person');
    }

    public function status()
    {
        return $this->belongsTo('Milne\Status');
    }

    public function bid()
    {
        return $this->hasOne('Milne\Bid');
    }
    
    public function media()
    {
        return $this->morphMany('Milne\Media', 'mediable');
    }

    public function notes()
    {
        return $this->morphMany('Milne\Note', 'noteable');
    }

}
