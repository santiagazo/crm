<?php

namespace Milne\Http\Controllers;

use Illuminate\Http\Request;
use Milne\Job;
use Milne\Bid;
use Milne\Estimate;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function dashboard()
    {
        $estimates = Estimate::DoesntHave('bid')
        ->whereIn('status_id', [16, 17])
        ->orderBy('id', 'desc')
        ->with('person')
        ->limit(10)
        ->get();

        $bids = Bid::whereIn('status_id', [11, 12, 13, 14])
        ->orderBy('created_at', 'asc')
        ->get();

        $jobs = Job::whereBetween('start_at', [Carbon::now()->subMonths(3), Carbon::now()->addMonths(3)])
        ->select('jobs.*', 'bids.accepted_at')
        ->join('bids', 'jobs.bid_id', '=', 'bids.id')
        ->orderBy('bids.accepted_at', 'asc')
        ->with('person')
        ->with('user')
        ->with('bid')
        ->get();

        $brents_jobs = [];
        $anthonys_jobs = [];
        $nathans_jobs = [];

        $brents_revenue = [];
        $anthonys_revenue = [];
        $nathans_revenue = [];

        $total_squares = [];

        foreach($jobs->where('start_at', '>', Carbon::now()->startOfMonth())->where('start_at', '<', Carbon::now()->endOfMonth()) as $job){
            if($job->status_id >= 3){
                
                if($job->type->slug != 'new_construction'){
                    if(strpos($job->user->name, 'Brent') !== false){
                        $brents_jobs[] = $job;
                    }
                    if(strpos($job->user->name, 'Anthony') !== false){
                        $anthonys_jobs[] = $job;
                    }
                    if(strpos($job->user->name, 'Nathan') !== false){
                        $nathans_jobs[] = $job;
                    }
                }

                if(strpos($job->user->name, 'Brent') !== false){
                    $brents_revenue[] = $job->invoice_amount;
                }
                if(strpos($job->user->name, 'Anthony') !== false){
                    $anthonys_revenue[] = $job->invoice_amount;
                }
                if(strpos($job->user->name, 'Nathan') !== false){
                    $nathans_revenue[] = $job->invoice_amount;
                }

                foreach($job->categories->where('unit', 'square') as $category){
                    $total_squares[] = $category->pivot->qty; 
                }
            }
        };

        $top_producers = ['Brent' => count($brents_jobs), 'Anthony' => count($anthonys_jobs), 'Nathan' => count($nathans_jobs)];
        $top_earners = ['Brent' => array_sum($brents_revenue), 'Anthony' => array_sum($anthonys_revenue), 'Nathan' => array_sum($nathans_revenue)];
        $total_jobs = array_sum($top_producers);
        $total_revenue = array_sum($top_earners);

        return view('auth.users.dashboard')
        ->with('jobs', $jobs)
        ->with('bids', $bids)
        ->with('estimates', $estimates)
        ->with('top_producers', $top_producers)
        ->with('top_earners', $top_earners)
        ->with('total_jobs', $total_jobs)
        ->with('total_revenue', $total_revenue)
        ->with('price_per_sq', ($total_squares ? $total_revenue/array_sum($total_squares) : 0));
    }
}
