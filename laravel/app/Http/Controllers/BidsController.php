<?php

namespace Milne\Http\Controllers;

use Milne\Http\Requests\MaterialableRequest;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Milne\MaterialList;
use Milne\Estimate;
use Milne\Template;
use Milne\Category;
use Carbon\Carbon;
use Milne\Company;
use Milne\Product;
use Milne\Person;
use Milne\Status;
use Milne\Type;
use Milne\Tax;
use Milne\Note;
use Milne\Bid;
use Flash;
use Auth;
use DB;

class BidsController extends Controller
{
    public function index(Request $request)
    {
        $user_name = $request->fullname ?: NULL;
        $customer_name = $request->customer_name ?: NULL;
        $address = $request->address ?: NULL;
        $city = $request->city ?: NULL;
        $subdivision = $request->subdivision ?: NULL;
        $lot = $request->lot ?: NULL;
        $status = $request->status ?: NULL;

        $statuses = Status::where('category', 'bid')
        ->pluck('name', 'slug');

        $statuses = array_merge([ NULL => 'All' ], $statuses->toArray());


        $bids = Bid::select('bids.*')->with('user')->with('person');

        if($user_name){
            $bids = $bids->leftJoin('users', 'users.id', '=', 'bids.user_id')
                ->where('users.name', 'LIKE', '%'.$user_name.'%');
        }
        if($customer_name){
            $bids = $bids->leftJoin('people', 'people.id', '=', 'bids.person_id')
                    ->where('people.name', 'LIKE', '%'.$customer_name.'%');
        }
        if($address){
            $bids = $bids->where('address', 'LIKE', '%'.$address.'%');
        }
        if($city){
            $bids = $bids->where('city', 'LIKE', '%'.$city.'%');
        }
        if($subdivision){
            $bids = $bids->where('subdivision', 'LIKE', '%'.$subdivision.'%');
        }
        if($lot){
            $bids = $bids->where('lot', 'LIKE', '%'.$lot.'%');
        }
        if($status){
            $bids = $bids->join('status', 'bids.status_id', '=', 'status.id')
            ->where('status.slug', '=', $status);
        }

        $bids = $bids->orderBy('accepted_at')->paginate(25);

        if($request->ajax()){
            return json_encode($bids);
        }

        return view('auth.bids.index')
        ->with('bids', $bids)
        ->with('user_name', $user_name)
        ->with('customer_name', $customer_name)
        ->with('address', $address)
        ->with('city', $city)
        ->with('subdivision', $subdivision)
        ->with('lot', $lot)
        ->with('statuses', $statuses)
        ->with('status', $status);
    }

    public function create($person_id = NULL, $estimate_id = NULL)
    {
        $status = Status::where('category', 'bid')->pluck('name', 'id');
        $suppliers = Company::join('types', 'types.id', '=', 'companies.type_id')->where('types.name', 'Supplier')->pluck('companies.name', 'companies.id');
        $people = Person::pluck('name', 'id');
        $types = Type::where('category', 'job')->pluck('name', 'id');
        $categories = Category::where('topic', 'product')->pluck('name', 'id');
        $pitches = Type::where('category', 'pitch')->pluck('name', 'id');
        $taxes = Tax::pluck('city', 'slug');
		$taxed_cities = Tax::pluck('city', 'slug');
        
        if($person_id){
            $person = Person::where('id', $person_id)->first();
        }else{
            $person = NULL;
        }

        if($estimate_id){
            $estimate = Estimate::where('id', $estimate_id)->first();
            $estimate->status_id = 18;
            $estimate->save();

            if($estimate->bid && $estimate->bid->material_list){
                Flash::error('There is a bid already created for this estimate. You have been redirected to edit the existing material list.');
                return redirect('/admin/bids/show/'.$estimate->bid->id);
            }elseif($estimate->bid){
                return redirect('/admin/bids/edit/'.$estimate->bid->id);
            }
        }else{
	        $estimate = NULL;
        }

        return view('auth/bids/create')
        ->with('status', $status)
        ->with('person', $person)
        ->with('types', $types)
        ->with('categories', $categories)
        ->with('suppliers', $suppliers)
        ->with('estimate', $estimate)
        ->with('pitches', $pitches)
        ->with('taxes', $taxes)
        ->with('taxed_cities', $taxed_cities);
    }

    public function store(MaterialableRequest $request)
    {
        if($request->estimate_id){
            $estimate = Estimate::findOrFail($request->estimate_id);

            if($estimate->bid){
                Flash::error('There is a bid already created for this. You have been redirected to edit the existing bid.');
                return redirect('admin/bids/edit/'.$estimate->bid->id);
            }

            $estimate->status_id = 18;
            $estimate->save();
        }

        $tax = Tax::where('slug', $request->city)->first();

        $bid = new Bid;
        $bid->user_id = Auth::id();
        $bid->estimate_id = $request->estimate_id ?: NULL;
        $bid->person_id = $request->person_id;
        $bid->status_id = $request->status_id;
        $bid->type_id = $request->type_id;
        $bid->tax_id = $tax->id;
        $bid->address = $request->address;
        $bid->city = str_unslug($request->city);
        $bid->zip = $request->zip;
        $bid->subdivision = $request->subdivision ?: NULL;
        $bid->lot = $request->lot ?: NULL;
        $bid->instructions = $request->instructions;
        $bid->save();

        if($request->note){
            $note = new Note;
            $note->noteable_id = $bid->id;
            $note->noteable_type = 'Milne\Bid';
            $note->user_id = Auth::id();
            $note->content = $request->note;
            $note->save();
        }

        if($request->pitches){
            foreach($request->pitches as $pitch_id){
                $bid->pitches()->create([
                    'pitchable_id' => $bid->id,
                    'pitchable_type' => 'Milne\Bid',
                    'type_id' => $pitch_id,
                ]);
            }
        }

        Flash::success('The bid has been started successfully. Add the needed materials to finish.');

        if($request->add_material_list == "add_material_list"){
            return redirect('admin/material_lists/create/'.$bid->id.'/bid');
        }elseif($request->add_material_list == "add_template_material_list"){
            return redirect('admin/templates/'.$bid->id);
        }

        return redirect('admin/bids/edit/'.$bid->id);
    }

    public function show($id)
    {
        $bid = Bid::where('id', $id)
        ->with('person', 'material_list', 'notes', 'material_list.categories')
        ->first();

        return view('auth/bids/show')
        ->with('bid', $bid);
    }

    public function edit(Request $request, $id=NULL)
    {
        $bid = Bid::findOrFail($id);
        $status = Status::where('category', 'bid')->pluck('name', 'id');
        $suppliers = Company::join('types', 'types.id', '=', 'companies.type_id')->where('types.name', 'Supplier')->pluck('companies.name', 'companies.id');
        $people = Person::pluck('name', 'id');
        $types = Type::where('category', 'job')->pluck('name', 'id');
        $categories = Category::where('topic', 'product')->pluck('name', 'id');
        $person = $bid->person;
        $pitches = Type::where('category', 'pitch')->pluck('name', 'id');
        $selected_pitches = [];
		$taxed_cities = Tax::pluck('city', 'slug');

        foreach($bid->pitches as $pitch){
            $selected_pitches[] = $pitch->type_id;
        }

        return view('auth/bids/edit')
        ->with('bid', $bid)
        ->with('status', $status)
        ->with('person', $person)
        ->with('types', $types)
        ->with('pitches', $pitches)
        ->with('categories', $categories)
        ->with('suppliers', $suppliers)
        ->with('selected_pitches', $selected_pitches)
        ->with('taxed_cities', $taxed_cities);
    }

    public function update(MaterialableRequest $request, $id)
    {
        $tax = Tax::where('slug', $request->city)->first();

        $bid = Bid::where('id', $id)->first();
        $bid->user_id = Auth::id();
        // $bid->estimate_id = $request->estimate_id ?: NULL; estimate_id doesn't get passed in - do not attempt to update?
        $bid->person_id = $request->person_id;
        $bid->status_id = $request->status_id;
        $bid->type_id = $request->type_id;
        $bid->tax_id = $tax->id;
        $bid->address = $request->address;
        $bid->city = str_unslug($request->city);
        $bid->zip = $request->zip;
        $bid->subdivision = $request->subdivision ?: NULL;
        $bid->lot = $request->lot ?: NULL;
        $bid->instructions = $request->instructions;
        $bid->save();

        if($request->note){
            $note = new Note;
            $note->noteable_id = $bid->id;
            $note->noteable_type = 'Milne\Bid';
            $note->user_id = Auth::id();
            $note->content = $request->note;
            $note->save();
        }
        
        foreach($bid->pitches as $pitch){
            $pitch->delete();
        }

        if($request->pitches){
            foreach($request->pitches as $pitch_id){
                $bid->pitches()->create([
                    'pitchable_id' => $bid->id,
                    'pitchable_type' => 'Milne\Bid',
                    'type_id' => $pitch_id,
                ]);
            }
        }

        Flash::success('The bid has been updated successfully.');

        if($request->add_material_list == "add_material_list"){
            return redirect('admin/material_lists/create/'.$bid->id.'/bid');
        }elseif($request->add_material_list == "edit_material_list"){
            return redirect('admin/material_lists/edit/'.$bid->material_list->id.'/bid');
        }elseif ($request->add_material_list == "add_template_material_list"){
            return redirect('admin/templates/'.$bid->id);
        }elseif ($request->add_material_list == "see_bid") {
            return redirect('admin/bids/show/'.$bid->id);
        }

        return redirect('admin/bids/edit/'.$bid->id);
    }
}
