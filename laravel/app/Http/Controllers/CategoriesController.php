<?php

namespace Milne\Http\Controllers;


use Milne\Http\Requests\CategoryRequest;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Milne\Category;
use Milne\Product;
use Milne\Type;
use Config;
use Flash;
use Auth;
use PDF;
use DB;


class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->name ?: NULL;
        $q = $request->q ?: NULL;
        $price = $request->price ?: NULL;
        $unit = $request->unit ? str_slug($request->unit, '_') : NULL;
        $selected_category_ids = $request->selected_category_ids ?: NULL;
        $is_house_product = $request->is_house_product ?: NULL;

        $categories = Category::select('categories.*')->where('categories.topic', 'product');

        if($name){
            $categories = $categories->where('name', 'LIKE', '%'.$name.'%');
        }

        if($price){
            $categories = $categories->where('price', 'LIKE', '%'.$price.'%');
        }

        if($unit){
            $categories = $categories->where('unit', 'LIKE', '%'.$unit.'%');
        }

        if($q){
            $categories = $categories->where('categories.name', 'LIKE', '%'.$q.'%');
        }

        if($selected_category_ids){
            $categories = $categories->whereNotIn('id', $selected_category_ids);
        }

        if($is_house_product){
            $categories = $categories->where('categories.is_house_product', $is_house_product);
        }

        $categories = $categories->orderBy('id')->paginate(25);

        if($request->ajax()){
            return json_encode($categories);
        }

        return view('auth.categories.index')
        ->with('categories', $categories)
        ->with('name', $name)
        ->with('price', $price)
        ->with('unit', $unit)
        ->with('is_house_product', $is_house_product);
    }

    public function create()
    {
        $units = Config::get('units');

        return view('auth.categories.create')
        ->with('units', $units);
    }

    public function store(CategoryRequest $request)
    {
        $category = Category::create([
            'topic' => 'product',
            'name' => $request->name ?: NULL,
            'slug' =>  $request->name ? str_slug($request->name, '_') : NULL,
            'price' => $request->price ?: NULL,
            'unit' => $request->unit ? str_slug($request->unit, '_') : NULL,
            'is_house_product' => $request->is_house_product ?: NULL,
            'colors' => $request->is_house_product && $request->colors ? implode(',', $request->colors) : NULL
        ]);

        Flash::success('The category "'.$category->name.'" has been saved successfully.');

        return redirect('admin/categories/create');
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $units = Config::get('units');
        $colors = $category->colors ? array_combine(array_unique(explode(',', $category->colors)), array_unique(explode(',', $category->colors))) : [];

        return view('auth.categories.edit')
        ->with('category', $category)
        ->with('units', $units)
        ->with('colors', $colors);
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = Category::where('id', $id)->first();
        $category->update([
            'topic' => 'product',
            'name' => $request->name ?: NULL,
            'slug' =>  $request->name ? str_slug($request->name, '_') : NULL,
            'price' => $request->price ?: NULL,
            'unit' => $request->unit ? str_slug($request->unit, '_') : NULL,
            'is_house_product' => $request->is_house_product ?: NULL,
            'colors' => $request->is_house_product && $request->colors ? implode(',', $request->colors) : NULL
        ]);

        Flash::success('The category "'.$category->name.'" has been updated successfully.');

        return redirect('admin/categories');
    }

    public function delete($id)
    {
        $category = Category::where('id', $id)->first();

        if(count($category->products)){
            Flash::error('This category is still tied to products and thus cannot be deleted. Remove it from all products to delete.');
            return redirect('admin/categories/edit/'.$id);
        }

        $category->delete();

        Flash::success('The category has been deleted successfully.');

        return redirect('admin/categories');
    }

}
