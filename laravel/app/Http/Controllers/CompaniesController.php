<?php

namespace Milne\Http\Controllers;


use Milne\Http\Requests\CompanyRequest;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Milne\Company;
use Milne\Product;
use Milne\Type;
use Flash;
use Auth;
use PDF;
use DB;


class CompaniesController extends Controller
{
    public function index(Request $request)
    {
        $type_id = $request->type_id ?: NULL;
        $name = $request->name ?: NULL;
        $tax_amount = $request->tax_amount ?: NULL;
        $select2 = $request->select2 ?: NULL;
        $q = $request->q ?: NULL;
        $contractor = $request->contractor ?: NULL;
        $suppliers = $request->suppliers ?: NULL;

        $companies = Company::select('companies.*')
        ->with('type')
        ->with('products')
        ->with('products.category');

        if($type_id){
            $companies = $companies->leftJoin('types', 'types.id', '=', 'companies.type_id')
                ->where('types.name', 'LIKE', '%'.$type_id.'%');
        }
        if($name){
            $companies = $companies->where('name', 'LIKE', '%'.$name.'%');
        }
        if($name){
            $companies = $companies->where('tax_amount', 'LIKE', '%'.$tax_amount.'%');
        }

        if($q){
            $companies = $companies
                ->where('companies.name', 'LIKE', '%'.$q.'%');
        }
        if($contractor){
            $companies = $companies->where('companies.type_id', 6);
        }
        if($suppliers){
            $companies = $companies->where('companies.type_id', 7);
        }
        if($select2){
            $companies = $companies->orderBy('name')
            ->paginate(25);

            return json_encode($companies);
        }

        $companies = $companies->orderBy('id')->paginate(25);

        if($request->ajax()){
            return response()->json($companies);
        }

        return view('auth.companies.index')
        ->with('tax_amount', $tax_amount)
        ->with('companies', $companies)
        ->with('type_id', $type_id)
        ->with('name', $name);
    }

    public function create()
    {
        $types = Type::where('category', 'company')->pluck('name', 'id');

        return view('auth.companies.create')
        ->with('types', $types);
    }

    public function store(CompanyRequest $request)
    {
        $company = Company::create([
            'name' => $request->name ?: NULL,
            'slug' =>  $request->name ? str_slug($request->name, '_') : NULL,
            'type_id' => $request->type_id ?: NULL,
            'tax_amount' => $request->type_id == 7 && $request->tax_amount ? $request->tax_amount : 0.00
        ]);

        Flash::success('The company has been saved successfully.');

        return redirect('admin/companies');
    }

    public function show(Request $request, $id)
    {
        $company = Company::where('id', $id)
        ->with('type')
        ->with('products')
        ->with('products.category')
        ->first();

        if($request->ajax()){
            return json_encode($company);
        }

        return view('auth.companies.show')
        ->with('company', $company);
    }

    public function edit($id)
    {
        $company = Company::findOrFail($id);
        $companies = Company::pluck('name', 'id');
        $types = Type::where('category', 'company')->pluck('name', 'id');

        return view('auth.companies.edit')
        ->with('companies', $companies)
        ->with('company', $company)
        ->with('types', $types);
    }

    public function update(CompanyRequest $request, $id)
    {
        $company = Company::where('id', $id)->update([
            'name' => $request->name ?: NULL,
            'slug' =>  $request->name ? str_slug($request->name, '_') : NULL,
            'type_id' => $request->type_id ?: NULL,
            'tax_amount' => $request->type_id == 7 && $request->tax_amount ? $request->tax_amount : 0.00
        ]);

        Flash::success('All changes saved successfully.');

        return redirect('admin/companies');
    }

    public function delete($id)
    {
        $company = Company::where('id', $id)->first();

        if(count($company->people) || count($company->products)){
            Flash::error('This company is already tied to people or products and thus cannot be deleted. Remove it from all people and products to delete.');
            return redirect('admin/companies/edit/'.$id);
        }

        $company->delete();

        Flash::success('The company has been deleted successfully.');

        return redirect('admin/companies');
    }

}
