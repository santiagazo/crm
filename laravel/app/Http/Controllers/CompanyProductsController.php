<?php

namespace Milne\Http\Controllers;

use Milne\Http\Requests\ProductRequest;

use Illuminate\Http\Request;
use Milne\Category;
use Carbon\Carbon;
use Milne\Company;
use Milne\Product;
use Milne\Type;
use Config;
use Flash;
use Auth;
use PDF;
use DB;

class CompanyProductsController extends Controller
{    
    public function show($company_id)
    {
    	$company = Company::where('id', $company_id)
    	->with('products')
    	->first();
    	$categories = Category::pluck('name', 'id');
    	$types = Type::where('category', 'taxable')->pluck('name', 'id');
        $units = Config::get('units');

    	return view('auth.company_products.show')
    	->with('company', $company)
    	->with('categories', $categories)
    	->with('types', $types)
    	->with('units', $units);
    }

    public function edit($company_id)
    {
        $company = Company::where('id', $company_id)
        ->with('products')
        ->first();
        $categories = Category::pluck('name', 'id');
        $types = Type::where('category', 'taxable')->pluck('name', 'id');
        $units = Config::get('units');

        return view('auth.company_products.edit')
        ->with('company', $company)
        ->with('categories', $categories)
        ->with('types', $types)
        ->with('units', $units);
    }

    public function update(ProductRequest $request, $company_id)
    {
        $product = Product::where('id', $request->id)->first();
        $product->company_id = $request->company_id ?: NULL;
        $product->category_id = $request->category_id ?: NULL;
        $product->type_id = $request->type_id ?: NULL;
        $product->name = $request->name ?: NULL;
        $product->supplier_price = $request->supplier_price ?: 0;
        $product->unit = $request->unit ? str_slug($request->unit, '_') : NULL;
        $product->description = $request->description ?: NULL;
        $product->colors = $request->colors ? implode(',', $request->colors) : NULL;
        $product->save();

        return json_encode($product->id);
    }

}
