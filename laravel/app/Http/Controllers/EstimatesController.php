<?php

namespace Milne\Http\Controllers;

use Milne\Mail\EstimateRequestConfirmation;
use Milne\Http\Requests\EstimateRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Milne\Libraries\Address;
use Milne\Estimate;
use Milne\Person;
use Milne\Status;
use Milne\Tax;
use Flash;
use Image;
use File;

use GuzzleHttp\Stream\Stream;
use GuzzleHttp\Client;

class EstimatesController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->name ?: NULL;
        $address = $request->address ?: NULL;
        $city = $request->city ?: NULL;
        $phone = $request->phone ?: NULL;
        $email = $request->email ?: NULL;
        $roof_type = $request->roof_type ?: NULL;
        $status = $request->status ?: NULL;

        $statuses = Status::where('category', 'estimate')
        ->pluck('name', 'slug');
        $statuses = array_merge([ NULL => 'All' ], $statuses->toArray());

        $estimates = Estimate::select('estimates.*', 'people.name', 'people.address', 'people.city', 'people.phone', 'people.email')
        ->join('people', 'estimates.person_id', '=', 'people.id');

        if($name){
            $estimates = $estimates->where('people.name', 'LIKE', '%'.$name.'%');
        }
        if($address){
            $estimates = $estimates->where('people.address', 'LIKE', '%'.$address.'%');
        }
        if($city){
            $estimates = $estimates->where('people.city', 'LIKE', '%'.$city.'%');
        }
        if($phone){
            $estimates = $estimates->where('people.phone', 'LIKE', '%'.$phone.'%');
        }
        if($email){
            $estimates = $estimates->where('people.email', 'LIKE', '%'.$email.'%');
        }
        if($roof_type){
            $estimates = $estimates->where('estimates.roof_type', 'LIKE', '%'.$roof_type.'%');
        }
        if($status){
            $estimates = $estimates->join('status', 'estimates.status_id', '=', 'status.id')
            ->where('status.slug', '=', $status);
        }

        $estimates = $estimates->orderBy('estimates.id', 'desc')->paginate(25);

        if($request->ajax()){
            return json_encode($estimates);
        }

        return view('auth.estimates.index')
        ->with('estimates', $estimates)
        ->with('name', $name)
        ->with('address', $address)
        ->with('city', $city)
        ->with('phone', $phone)
        ->with('email', $email)
        ->with('roof_type', $roof_type)
        ->with('status', $status)
        ->with('statuses', $statuses);
    }

    public function create($person_id=NULL)
    {
        $person = Person::where('id', $person_id)->first();
        $people = Person::orderBy('created_at')->pluck('name', 'id');
        $taxed_cities = Tax::pluck('city', 'slug');


        return view('auth.estimates.create')
        ->with('taxed_cities', $taxed_cities)
        ->with('person', $person)
        ->with('people', $people);
    }

    public function store(EstimateRequest $request)
    {
        $person = Person::findOrFail($request->person_id);

        $estimate_type = implode(',', $request->request_type);
        $estimate_type .= $request->other_text ? ' '.$request->other_text : NULL;

        $estimate = new Estimate;
        $estimate->person_id = $person->id;
        $estimate->status_id = 16;
        $estimate->type = $estimate_type;
        $estimate->roof_type = $request->roof_type ?: NULL;
        $estimate->roof_pitch = $request->roof_pitch ?: NULL;
        if($request->project_same_as_billing ){
            $estimate->project_address = $person->address ?: NULL;
            $estimate->project_city = $person->city ?: NULL;
            $estimate->project_zip = $person->zip ?: NULL;
        }else{
            $estimate->project_address = $request->project_address ?: NULL;
            $estimate->project_city = $request->project_city ?: NULL;
            $estimate->project_zip = $request->project_zip ?: NULL;
        }
        $estimate->is_urgent = $request->urgency == 'urgent' ? 1 : NULL;
        $estimate->additional_information = $request->additional_information ?: NULL;
        $estimate->advertisment = $request->advertisment ?: NULL;
        $estimate->referred_by = $request->referred_by ?: NULL;
        $estimate->save();

        Mail::to($person->email)->send(new EstimateRequestConfirmation($person));
        Flash::success('Your estimate has been saved successfully. The client has been sent a confirmation email.');

        return redirect('admin/estimates/show/'.$estimate->id);
    }

    public function show($id)
    {   
        $estimate = Estimate::findOrFail($id);

        return view('auth.estimates.show')
        ->with('estimate', $estimate);
    }

    public function edit($id)
    {
        $estimate = Estimate::findOrFail($id);
        $statuses = Status::where('category', 'estimate')
        ->pluck('name', 'id');

        $query = urlencode($estimate->project_address.' '.$estimate->project_city.', UT '.$estimate->project_zip);

        $address = new Address($query);
        $fullAddress = $address->getFullAddress();

        return view('auth.estimates.edit')
        ->with('estimate', $estimate)
        ->with('statuses', $statuses)
        ->with('fullAddress', $fullAddress);
    }

    public function update(EstimateRequest $request, $id)
    {   
        $estimate = Estimate::findOrFail($id);
        $estimate->status_id = $request->status_id;
        $estimate->update();

        Flash::success('The estimate request has been updated successfully.');

        return redirect('admin/estimates');
    }

    public function satellite(Request $request, $id)
    {
        $estimate = Estimate::where('id', $id)->first();
        $path = 'estimate_images/'.$estimate->id;
        
        if (File::exists(public_path($path.'/satellite.png'))){
        
            return response()->file($path.'/satellite.png');
        
        }else{            

            $query = urlencode($estimate->project_address.' '.$estimate->project_city.' '.$estimate->project_zip);
            $address = new Address($query);
            $coordinates = [
                'latitudes' => $address->getLatitudes(),
                'longitudes' => $address->getLongitudes()
            ];

            $latitude = array_sum($coordinates['latitudes']) / count($coordinates['latitudes']);
            $longitude = array_sum($coordinates['longitudes']) / count($coordinates['longitudes']);

            $client = new Client();
            $response = $client->request('GET', "https://maps.googleapis.com/maps/api/staticmap?center=".$latitude.",".$longitude."&zoom=20&size=400x400&maptype=hybrid");

            if(!File::isDirectory(public_path($path))){
                File::makeDirectory(public_path($path), 0777, true);
            }

            $img = Image::make($response->getBody()->getContents());
            $img = $img->save(public_path($path.'/satellite.png'), 100);

            return response()->file($img);

        }
    }

    public function updateSatellite(Request $request, $id)
    {
        $this->validate($request, [
            'id' => 'required',
            'file' => 'required|mimes:png,jpeg,jpg,gif'
        ]);

        $extension = $request->file ? $request->file->getClientOriginalExtension() : 'NO FILE ATTACHED';
        $name = $request->file ? str_slug(pathinfo($request->file->getClientOriginalName(), PATHINFO_FILENAME), '_').'_'.str_random(5) : str_random(5);

        $path = 'estimate_images/'.$id;

        if(File::exists(public_path($path))){
            File::delete(public_path($path));
        }
        
        if(!File::isDirectory(public_path($path))){
            File::makeDirectory(public_path($path), 0777, true);
        }

        $estimate = Estimate::where('id', $id)->first();
        $estimate->no_satellite_image = 0;
        $estimate->save();

        $img = Image::make($request->file->getRealPath());
        $img = $img->resize(400, 400);
        $img = $img->save(public_path($path.'/satellite.png'), 75);

        return response()->json([
            'message' => 'Success',
            'response' => 'ok',
            'status' => 200
        ]);
    }

    public function deleteSatellite(Request $request, $id)
    {
        $estimate = Estimate::where('id', $id)->first();
        $path = 'estimate_images/'.$estimate->id.'/satellite.png';
        
        if(File::exists(public_path($path))){
            File::delete(public_path($path));
        }

        $estimate->no_satellite_image = 1;
        $estimate->save();

        Flash::success('The satellite image was deleted successfully.');

        return redirect('admin/estimates/edit/'.$id);
    }
}
