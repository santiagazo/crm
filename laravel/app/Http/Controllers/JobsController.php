<?php

namespace Milne\Http\Controllers;

use Milne\Http\Requests\MaterialableRequest;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Milne\Estimate;
use Milne\Template;
use Milne\Category;
use Carbon\Carbon;
use Milne\Company;
use Milne\Product;
use Milne\Person;
use Milne\Status;
use Milne\MaterialList;
use Milne\Note;
use Milne\Type;
use Milne\Job;
use Milne\Tax;
use Milne\Bid;
use Flash;
use Auth;
use DB;

class JobsController extends NotesController
{
    public function index(Request $request)
    {
        $purchase_order = $request->purchase_order ?: NULL;
        $user_name = $request->fullname ?: NULL;
        $customer_name = $request->customer_name ?: NULL;
        $address = $request->address ?: NULL;
        $city = $request->city ?: NULL;
        $subdivision = $request->subdivision ?: NULL;
        $lot = $request->lot ?: NULL;

        $jobs = Job::select('jobs.*')->with('user')->with('person');

        if($purchase_order){
            $jobs = $jobs->where('purchase_order', 'LIKE', '%'.$purchase_order.'%');
        }
        if($user_name){
            $jobs = $jobs->leftJoin('users', 'users.id', '=', 'jobs.user_id')
                ->where('users.name', 'LIKE', '%'.$user_name.'%');
        }
        if($customer_name){
            $jobs = $jobs->leftJoin('people', 'people.id', '=', 'jobs.person_id')
                    ->where('people.name', 'LIKE', '%'.$customer_name.'%');
        }
        if($address){
            $jobs = $jobs->where('address', 'LIKE', '%'.$address.'%');
        }
        if($city){
            $jobs = $jobs->where('city', 'LIKE', '%'.$city.'%');
        }
        if($subdivision){
            $jobs = $jobs->where('subdivision', 'LIKE', '%'.$subdivision.'%');
        }
        if($lot){
            $jobs = $jobs->where('lot', 'LIKE', '%'.$lot.'%');
        }

        $jobs = $jobs->orderBy('purchase_order')->paginate(25);

        if($request->ajax()){
            return json_encode($jobs);
        }

        return view('auth.jobs.index')
        ->with('jobs', $jobs)
        ->with('purchase_order', $purchase_order)
        ->with('user_name', $user_name)
        ->with('customer_name', $customer_name)
        ->with('address', $address)
        ->with('city', $city)
        ->with('subdivision', $subdivision)
        ->with('lot', $lot);

    }

    public function create($bid_id = NULL)
    {
        $status = Status::where('category', 'job')->pluck('name', 'id');
        $suppliers = Company::join('types', 'types.id', '=', 'companies.type_id')->where('types.name', 'Supplier')->pluck('companies.name', 'companies.id');
        $people = Person::pluck('name', 'id');
        $types = Type::where('category', 'job')->pluck('name', 'id');
        $categories = Category::where('topic', 'product')->pluck('name', 'id');
        $pitches = Type::where('category', 'pitch')->pluck('name', 'id');
        $taxed_cities = Tax::pluck('city', 'slug');
        $bid = Bid::where('id', $bid_id)->first();
        $selected_pitches = [];

        if($bid && $bid->job){
            Flash::success("A job already exists for this bid so you've been redirected to edit the existing one.");
            return redirect('admin/jobs/edit/'.$bid->job->id);
        }

        if($bid){
            if(empty($bid->accepted_at)){
                $bid->accepted_at = Carbon::now();
                $bid->save();
            }
            
            foreach($bid->pitches as $pitch){
                $selected_pitches[] = $pitch->type_id;
            }
        }

        return view('auth/jobs/create')
        ->with('status', $status)
        ->with('person', $bid ? $bid->person : NULL)
        ->with('types', $types)
        ->with('categories', $categories)
        ->with('suppliers', $suppliers)
        ->with('bid', $bid)
        ->with('pitches', $pitches)
        ->with('selected_pitches', $selected_pitches)
        ->with('taxed_cities', $taxed_cities);
    }

    public function store(MaterialableRequest $request)
    {
        $tax = Tax::where('slug', $request->city)->first();

        $job = new Job;
        $job->user_id = Auth::id();
        $job->bid_id = $request->bid_id;
        $job->person_id = $request->person_id;
        $job->status_id = $request->status_id;
        $job->type_id = $request->type_id;
        $job->tax_id = $tax ? $tax->id : NULL;
        $job->start_at = $request->start_at ? Carbon::parse($request->start_at) : NULL;
        $job->end_at = $request->end_at ? Carbon::parse($request->end_at) : NULL;
        $job->address = $request->address;
        $job->city = $request->city;
        $job->zip = $request->zip;
        $job->subdivision = $request->subdivision ?: NULL;
        $job->lot = $request->lot ?: NULL;
        $job->instructions = $request->instructions;
        $job->save();

        if($request->note){
            if($request->note){
                $note = new Note;
                $note->noteable_id = $job->id;
                $note->noteable_type = 'Milne\Bid';
                $note->user_id = Auth::id();
                $note->content = $request->note;
                $note->save();
            }
        }

        if($request->pitches){
            foreach($request->pitches as $pitch_id){
                $job->pitches()->create([
                    'pitcheable_id' => $job->id,
                    'pitcheable_type' => 'Milne\Job',
                    'type_id' => $pitch_id,
                ]);
            }
        }

        if($job->bid){
            $bid = Bid::where('id', $job->bid_id)->first();
            if(empty($bid->accepted_at)){
                $bid->accepted_at = Carbon::now();
            }
            $bid->status_id = 14;
            $bid->save();
        }

        Flash::success('The job has been created successfully.');

        if($request->add_material_list){
            return redirect('admin/material_lists/create/'.$job->id.'/job');
        }


        return redirect('admin/jobs/edit/'.$job->id);
    }

    public function show($id)
    {
        $job = Job::where('id', $id)
        ->with('person', 'material_list', 'notes')
        ->first();

        return view('auth/jobs/show')
        ->with('job', $job);
    }

    public function edit(Request $request, $id=NULL)
    {
        $job = Job::findOrFail($id);
        $status = Status::where('category', 'job')->pluck('name', 'id');
        $suppliers = Company::join('types', 'types.id', '=', 'companies.type_id')->where('types.name', 'Supplier')->pluck('companies.name', 'companies.id');
        $people = Person::pluck('name', 'id');
        $types = Type::where('category', 'job')->pluck('name', 'id');
        $categories = Category::where('topic', 'product')->pluck('name', 'id');
        $person = $job->person;
        $pitches = Type::where('category', 'pitch')->pluck('name', 'id');
        $selected_pitches = [];
        $taxed_cities = Tax::pluck('city', 'slug');

        foreach($job->pitches as $pitch){
            $selected_pitches[] = $pitch->type_id;
        }

        if(empty($job->purchase_order)){
            $last_job = Job::where('id', $job->id-1)->first();

            if($last_job){
                $job->purchase_order = $last_job->purchase_order+1;
            }elseif(env('START_PO_NUMBER')){
                $job->purchase_order = env('START_PO_NUMBER');
            }else{
                $job->purchase_order = 1;
            }
            $job->save();
        }

        return view('auth/jobs/edit')
        ->with('job', $job)
        ->with('status', $status)
        ->with('person', $person)
        ->with('types', $types)
        ->with('pitches', $pitches)
        ->with('categories', $categories)
        ->with('suppliers', $suppliers)
        ->with('selected_pitches', $selected_pitches)
        ->with('taxed_cities', $taxed_cities);
    }

    public function update(MaterialableRequest $request, $id)
    {
        $tax = Tax::where('slug', $request->city)->first();

        $job = Job::findOrFail($id);
        $job->user_id = Auth::id();
        $job->person_id = $request->person_id;
        $job->status_id = $request->status_id;
        $job->lost_reason = $request->status_id == 6 ? $request->lost_reason : NULL;
        $job->type_id = $request->type_id;
        $job->tax_id = $tax ? $tax->id : NULL;
        $job->start_at = $request->start_at ? Carbon::parse($request->start_at) : NULL;
        $job->end_at = $request->end_at ? Carbon::parse($request->end_at) : NULL;
        $job->ordered_at = $request->ordered_at ? Carbon::parse($request->ordered_at) : NULL;
        $job->invoiced_at = $request->invoiced_at ? Carbon::parse($request->invoiced_at) : NULL;
        $job->delivery_at = $request->delivery_at ? Carbon::parse($request->delivery_at) : NULL;
        $job->completed_at = $request->completed_at ? Carbon::parse($request->completed_at) : NULL;
        $job->invoiced_at = $request->invoiced_at ? Carbon::parse($request->invoiced_at) : NULL;
        $job->address = $request->address;
        $job->city = $request->city;
        $job->zip = $request->zip;
        $job->subdivision = $request->subdivision ?: NULL;
        $job->lot = $request->lot ?: NULL;
        $job->instructions = $request->instructions;
        $job->update();

        if($request->note){
            $job->notes()->create([
                'user_id' => Auth::id(),
                'content' => $request->note,
            ]);
        }

        foreach($job->pitches as $pitch){
            $pitch->delete();
        }

        if($request->pitches){
            foreach($job->pitches as $pitch){
                $pitch->delete();
            }
            foreach($request->pitches as $pitch_id){
                $job->pitches()->create([
                    'pitcheable_id' => $job->id,
                    'pitcheable_type' => 'Milne\Job',
                    'type_id' => $pitch_id,
                ]);
            }
        }

        $bid = Bid::where('id', $job->bid_id)->first();
        if($bid){
            if(empty($bid->accepted_at)){
                $bid->accepted_at = Carbon::now();
            }
            $bid->status_id = 14;
            $bid->save();
        }

        Flash::success('The job has been updated successfully.');


        if($request->add_material_list == "add_material_list"){
            return redirect('admin/material_lists/create/'.$job->id."/job");
        }elseif($request->add_material_list == "edit_material_list"){
            return redirect('admin/material_lists/edit/'.$job->material_list->id."/job");
        }elseif ($request->add_material_list == "add_template_material_list"){
            return redirect('admin/templates?job_id='.$job->id);
        }elseif ($request->add_material_list == "see_job") {
            return redirect('admin/jobs/show/'.$job->id);
        }

        return redirect('admin/jobs/edit/'.$job->id);
    }

    public function delete(Request $request, $id)
    {
        $job = Job::findOrFail($id);
        
        if($job->material_list){

            Flash('The job cannot be deleted because there is a material list already attached.');

            return redirect('admin/jobs/'.$id);
        }

        $job->delete();

        Flash::success('The job has been deleted successfully.');

        return redirect('admin/jobs');
    }


}
