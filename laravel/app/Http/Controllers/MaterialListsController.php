<?php

namespace Milne\Http\Controllers;

use Illuminate\Http\Request;
use Milne\Job;
use Milne\Bid;
use Milne\MaterialList;
use Milne\Template;
use Milne\Company;
use Auth;
use Flash;

use Milne\Http\Requests\MaterialListRequest;

class MaterialListsController extends Controller
{
    public function create($materialable_id, $materialable_type)
    {
        if($materialable_type == 'bid'){
            $materialable = Bid::findOrFail($materialable_id);

            if($materialable->material_list){
                Flash::error('There is a material list already created for this. You have been redirected to edit the existing material list.');
                return redirect('admin/material_lists/edit/'.$materialable->material_list->id.'/bid');
            }

            return view('auth/material_lists/create_bid')
            ->with('materialable', $materialable);

        }else{
            
            $materialable = Job::findOrFail($materialable_id);
            $material_list = $materialable->bid ? $materialable->bid->material_list : NULL;
            $suppliers = Company::join('types', 'types.id', '=', 'companies.type_id')->where('types.name', 'Supplier')->pluck('companies.name', 'companies.id');

            if($materialable->material_list){
                Flash::error('There is a material list already created for this. You have been redirected to edit the existing material list.');
                return redirect('admin/material_lists/edit/'.$materialable->material_list->id.'/job');
            }

            return view('auth/material_lists/create_job')
            ->with('materialable', $materialable)
            ->with('material_list', $material_list)
            ->with('suppliers', $suppliers);
        }

    }

    public function storeBid(MaterialListRequest $request)
    {
        $bid = Bid::where('id', $request->materialable_id)->first();

        $material_list = new MaterialList;
        $material_list->materialable_id = $request->materialable_id;
        $material_list->materialable_type = 'Milne\Bid';
        $material_list->user_id = Auth::id();
        $material_list->status_id = 7;
        $material_list->save();

        $line_items = json_decode($request->line_items);

        $sort_id = 1;
        foreach($line_items as $item){
            $material_list->categories()->attach([ $item->category_id => [
                    'sort_id' => $sort_id,
                    'qty' => $item->qty,
                    'category_price' => $item->house_price
                ]
            ]);
            $sort_id++;
        }

        if($request->template){
            $template = new Template;
            $template->material_list_id = $material_list->id;
            $template->slug = str_slug($request->template, '_');
            $template->name = $request->template;
            $template->save();
        }

        if($request->is_pre_dry_in || $request->is_six_nail || $request->is_owner_tear_off){
            $bid->is_pre_dry_in = $request->is_pre_dry_in ?: NULL;
            $bid->is_six_nail = $request->is_six_nail ?: NULL;
            $bid->is_owner_tear_off = $request->is_owner_tear_off ?: NULL;
            $bid->save();
        }

        Flash::success('Your material list has been saved successfully.');

        if($request->save_and_view){
            return redirect('admin/bids/show/'.$material_list->materialable->id);
        }

        return redirect('admin/material_lists/edit/'.$material_list->id.'/bid');
    }

    public function storeJob(MaterialListRequest $request)
    {
        $job = Job::where('id', $request->materialable_id)->first();

        if(count($job->material_list)){
            Flash::error('There is a material list already created for this job. You have been redirected to edit the existing material list.');
            return redirect('admin/material_lists/edit/'.$job->material_list->id.'/job');
        }

        $material_list = new MaterialList;
        $material_list->materialable_id = $request->materialable_id;
        $material_list->materialable_type = "Milne\Job";
        $material_list->user_id = Auth::id();
        $material_list->status_id = 7;
        $material_list->company_id = $request->supplier_id;
        $material_list->save();

        $last_job = Job::where('id', $job->id-1)->first();

        if($last_job){
            $job->purchase_order = $last_job->purchase_order+1;
        }elseif(env('START_PO_NUMBER')){
            $job->purchase_order = env('START_PO_NUMBER');
        }else{
            $job->purchase_order = 1;
        }
        $job->is_owner_tear_off = $request->is_owner_tear_off ?: NULL;
        $job->is_six_nail = $request->is_six_nail ?: NULL;
        $job->is_pre_dry_in = $request->is_pre_dry_in ?: NULL;
        $job->save();

        $line_items = json_decode($request->line_items);
        $sort_id = 1;
        foreach($line_items as $item){
            $is_house_product = 0;
            $product_id = NULL;
            if($item->product_id){
                if(is_numeric($item->product_id)){
                    $product_id = $item->product_id;
                }else{
                    $is_house_product = 1;
                }
            }
            $material_list->categories()->attach([ $item->category_id => [
                    'product_id' => $product_id ?: NULL,
                    'sort_id' => $sort_id,
                    'qty' => $item->qty,
                    'category_price' => $item->house_price,
                    'supplier_price' => $item->supplier_subtotal,
                    'color' => $item->color ?: NULL,
                    'is_house_product' => $is_house_product ?: NULL
                ]
            ]);
            $sort_id++;
        }

        if($request->template){
            $template = new Template;
            $template->material_list_id = $material_list->id;
            $template->slug = str_slug($request->template, '_');
            $template->name = $request->template;
            $template->save();
        }

        Flash::success('Your material list has been saved successfully.');

        if($request->save_and_view){
            return redirect('admin/jobs/show/'.$job->id);
        }

        return redirect('admin/people/show/'.$job->person_id);
    }

    public function edit($material_list_id, $materialable_type)
    {
        $material_list = MaterialList::where('id', $material_list_id)
        ->with('categories', 'materialable')
        ->first();

        if($materialable_type == 'bid'){

            return view('auth/material_lists/edit_bid')
            ->with('material_list', $material_list);

        }else{

            $companies = Company::where('type_id', 7)
            ->with('products')
            ->pluck('name', 'id');

            return view('auth/material_lists/edit_job')
            ->with('material_list', $material_list)
            ->with('companies', $companies);
            
        }
    }

    public function updateBid(MaterialListRequest $request, $material_list_id)
    {
        $material_list = MaterialList::where('id', $material_list_id)
            ->where('materialable_type', 'Milne\Bid')
            ->with('materialable')
            ->first();
        $material_list->user_id = Auth::id();
        $material_list->status_id = 7;
        $material_list->save();

        $line_items = json_decode($request->line_items);

        $material_list->categories()->detach();

        $sort_id = 1;
        foreach($line_items as $item){
            $material_list->categories()->attach([ $item->category_id => [
                    'sort_id' => $sort_id,
                    'qty' => $item->qty,
                    'category_price' => $item->house_price
                ]
            ]);
            $sort_id++;
        }

        if($request->template){
            if($material_list->template){
                $template = $material_list->template;
            }else{
                $template = new Template;
            }
            $template->material_list_id = $material_list->id;
            $template->slug = str_slug($request->template, '_');
            $template->name = $request->template;
            $template->save();
        }

        if($request->is_pre_dry_in || $request->is_six_nail || $request->is_owner_tear_off){
            $material_list->materialable->is_pre_dry_in = $request->is_pre_dry_in ?: NULL;
            $material_list->materialable->is_six_nail = $request->is_six_nail ?: NULL;
            $material_list->materialable->is_owner_tear_off = $request->is_owner_tear_off ?: NULL;
            $material_list->materialable->save();
        }

        Flash::success('Your material list has been saved successfully.');

        if($request->update_and_view){
            return redirect('admin/bids/show/'.$material_list->materialable->id);
        }

        return redirect('admin/material_lists/edit/'.$material_list->id.'/bid');
    }

    public function updateJob(MaterialListRequest $request, $material_list_id)
    {
        $material_list = MaterialList::where('id', $material_list_id)->first();
        $material_list->materialable_id = $request->materialable_id;
        $material_list->materialable_type = "Milne\Job";
        $material_list->user_id = Auth::id();
        $material_list->status_id = 7;
        $material_list->company_id = $request->supplier_id;
        $material_list->save();

        $job = Job::where('id', $request->materialable_id)->first();
        $job->is_owner_tear_off = $request->is_owner_tear_off ?: NULL;
        $job->is_six_nail = $request->is_six_nail ?: NULL;
        $job->is_pre_dry_in = $request->is_pre_dry_in ?: NULL;
        $job->save();

        $line_items = json_decode($request->line_items);

        $material_list->categories()->detach();
        $sort_id = 1;

        foreach($line_items as $item){
            $is_house_product = 0;
            $product_id = NULL;
            if($item->product_id){
                if(is_numeric($item->product_id)){
                    $product_id = $item->product_id;
                }else{
                    $is_house_product = 1;
                }
            }
            $material_list->categories()->attach([ $item->category_id => [
                    'product_id' => $product_id ?: NULL,
                    'sort_id' => $sort_id,
                    'qty' => $item->qty,
                    'category_price' => $item->house_price,
                    'supplier_price' => $item->supplier_subtotal,
                    'color' => $item->color ?: NULL,
                    'is_house_product' => $is_house_product ?: NULL
                ]
            ]);
            $sort_id++;
        }

        if($request->template){
            if($material_list->template){
                $template = $material_list->template;
            }else{
                $template = new Template;
            }
            $template->material_list_id = $material_list->id;
            $template->slug = str_slug($request->template, '_');
            $template->name = $request->template;
            $template->save();
        }

        Flash::success('Your material list has been saved successfully.');

        if($request->update_and_view){
            return redirect('admin/jobs/show/'.$material_list->materialable->id);
        }

        return redirect('admin/material_lists/edit/'.$material_list->id.'/job');
    }

}
