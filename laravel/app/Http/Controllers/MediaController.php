<?php

namespace Milne\Http\Controllers;

use Illuminate\Http\Request;
use Milne\Media;
use Image;
use File;
use Response;
use Milne\Estimate;
use Milne\Job;
use Milne\Bid;
use Auth;
use Flash;

class MediaController extends Controller
{
    public function store(Request $request, $id){
        
        $this->validate($request, [
            'id' => 'required|numeric|exists:'.$request->type.'s,id',
            'type' => 'required|max:250',
            'file' => 'mimes:png,jpeg,jpg,gif'
        ]);

        $type = $request->type;

        if($type == 'estimate'){
            $resource = Estimate::findOrFail($id);
            $height = 300;

            if(count($resource->media) >= 10){
                return response()->json([
                        'status'      =>  401,
                        'message'   =>  'You are only allowed to upload 10 images.'
                    ], 401);
            }
        }

        if($type == 'job'){
            $resource = Job::findOrFail($id);
            $height = 800;

            if(count($resource->media) >= 20){
                return response()->json([
                        'status'      =>  401,
                        'message'   =>  'You are only allowed to upload 20 images.'
                    ], 401);
            }
        }

        if($type == 'bid'){
            $resource = Bid::findOrFail($id);
            $height = 800;

            if(count($resource->media) >= 20){
                return response()->json([
                        'status'      =>  401,
                        'message'   =>  'You are only allowed to upload 20 images.'
                    ], 401);
            }
        }



        $extension = $request->file ? $request->file->getClientOriginalExtension() : 'NO FILE ATTACHED';
        $name = $request->file ? str_slug(pathinfo($request->file->getClientOriginalName(), PATHINFO_FILENAME), '_').'_'.str_random(5) : str_random(5);

        $id = $resource->id;
        $filename = $name.'.'.$extension;
        $path = '/'.$type.'_images'.'/'.$resource->id.'/'.str_random(5).'/';

        if(!File::isDirectory(public_path($path))){
            File::makeDirectory(public_path($path), 0777, true);
        }

        $fullFilePath = $path.$filename;

        $img = Image::make($request->file->getRealPath());
        $img = $img->resize($height, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img = $img->save(public_path($fullFilePath), 75);

        $media = new Media;
        $media->mediable_id = $resource->id;
        $media->mediable_type = 'Milne\\'.ucfirst($type);
        $media->user_id = Auth::check() ? Auth::id() : NULL;
        $media->title = $filename;
        $media->slug = str_slug($filename, '_');
        $media->url = $path.'/'.$filename;
        $media->type = $type;
        $media->description = $request->status;
        $media->save();

        Flash::success('Your image has been uploaded successfully.');

        return response()->json($media);
    }

    public function delete(Request $request)
    {
        if(!Auth::user()->hasRole('Admin')){
            return response()->json([
                'status'      =>  403,
                'message'   =>  'You do not have permission to delete this file.'
            ], 403);
        }

        $this->validate($request, [
            'id' => 'required|numeric|exists:media,id',
            'type' => 'required|max:250',
        ]);


        $media = Media::where('id', $request->id)->where('mediable_type', "Milne\\".ucfirst($request->type))->first();

        if(!$media){
            return response()->json([
                'status'      =>  401,
                'message'   =>  'The media/image cannot be found.'
            ], 401);
        }

        File::delete(public_path($media->url));
        $media->delete();

        Flash::success('Your image has been deleted successfully.');

        return json_encode('Media deleted successfully.');
    }

}
