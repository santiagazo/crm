<?php

namespace Milne\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Milne\Note;
use Flash;
use Auth;

class NotesController extends Controller
{
    public function store_note(Request $request)
    {
    	$this->validate($request, [
            'id' => 'required|numeric|exists:'.str_plural($request->type).',id',
            'type' => 'required|max:250',
            'content' => 'required|min:3|max:255'
        ]);

    	$note = new Note;
    	$note->noteable_id = $request->id;
    	$note->noteable_type = 'Milne\\'.ucfirst(str_singular($request->type));
    	$note->user_id = Auth::id();
    	$note->content = $request->content;
    	$note->save();

    	if($request->ajax()){
	    	return response('success', 200);
    	}

    	Flash::success('Your note has been added successfully.');

    	return back();
    }
}
