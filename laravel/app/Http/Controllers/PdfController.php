<?php

namespace Milne\Http\Controllers;

use Milne\Mail\Supplier as EmailSupplier;
use Milne\Mail\Invoice as EmailInvoice;
use Illuminate\Support\Facades\Mail;
use Milne\Http\Requests\EmailRequest;
use Milne\Mail\Bid as EmailBid;
use Milne\Mail\Job as EmailJob;
use Illuminate\Http\Request;
use Milne\Estimate;
use Carbon\Carbon;
use Milne\Job;
use Milne\Bid;
use Flash;
use Auth;
use File;
use PDF;
use App;

class PdfController extends Controller
{
    public function createEstimate($estimate_id)
    {
        $estimate = Estimate::where('id', $estimate_id)
        ->first();

        $path = 'pdfs/estimates/'.$estimate->id.'/estimate.pdf';

        // check to see if file/record already exists
        // and delete if so.
        if(File::exists(public_path($path))){
            File::delete(public_path($path));
        }
        // get the base snappy class
        $base = App::make('snappy.pdf');
        // load up the view with the data
        $pdf = PDF::loadView('pdfs.estimate', ['estimate' => $estimate]);
        // write the pdf's html to the base pdf
        // you just started and save to path

        $base->generateFromHtml($pdf->html, public_path($path));

        return redirect('admin/pdf/estimates/show/'.$estimate_id);
    }

    public function createBid($bid_id)
    {
        $bid = Bid::where('id', $bid_id)
        ->with('material_list')
        ->first();

        $path = 'pdfs/bids/'.$bid->id.'/bid.pdf';

        $bid_amount = [];
        foreach($bid->material_list->categories as $category){
            $bid_amount[] = $category->pivot->category_price*$category->pivot->qty;
        }

        $square_count = [];
        foreach($bid->material_list->categories as $category){
            if($category->unit == 'square'){
                $square_count[] = $category->pivot->qty;
            }
        }

        $labor_per_square_amount = [];
        foreach($bid->material_list->categories as $category){
            if($category->unit == 'labor_square' && strpos($category->name, 'Tear Off') !== false){
                $labor_per_square_amount[] = $category->pivot->category_price;
            }
        }

        $bid->status_id = 11; // pending
        $bid->url = $path;
        $bid->amount = array_sum($bid_amount);
        $bid->square_count = array_sum($square_count);
        $bid->labor_per_square_amount = array_sum($labor_per_square_amount);
        $bid->save();

        // check to see if file/record already exists
        // and delete if so.
        if(File::exists(public_path($path))){
            File::delete(public_path($path));
        }

        // get the bid again so we can send it to the pdf
        // completely eagerloaded with updated bid.
        $bid = Bid::where('id', $bid_id)
        ->with('material_list', 'person', 'notes')
        ->first();

        // get the base snappy class
        $base = App::make('snappy.pdf');
        // load up the view with the data
        $pdf = PDF::loadView('pdfs.bid', ['bid' => $bid]);
        // write the pdf's html to the base pdf
        // you just started and save to path

        $base->generateFromHtml($pdf->html, public_path($path));

        return redirect('admin/pdf/bids/show/'.$bid_id);
    }

    public function createJob($job_id)
    {
        $job = Job::where('id', $job_id)
        ->with('material_list', 'bid')
        ->first();

        $path = 'pdfs/jobs/'.$job->id.'/job.pdf';

        // check to see if file/record already exists
        // and delete if so.
        if(File::exists(public_path($path))){
            File::delete(public_path($path));
        }
        // get the job again so we can send it to the pdf
        // completely eagerloaded with updated bid.
        $job = Job::where('id', $job_id)
        ->with('material_list', 'person', 'notes', 'bid')
        ->first();

        // get the base snappy class
        $base = App::make('snappy.pdf');
        // load up the view with the data
        $pdf = PDF::loadView('pdfs.job', ['job' => $job]);
        // write the pdf's html to the base pdf
        // you just started and save to path

        $base->generateFromHtml($pdf->html, public_path($path));

        return redirect('admin/pdf/jobs/show/'.$job_id);
    }

    public function createSupplier($job_id)
    {
        $job = Job::where('id', $job_id)
        ->with('material_list', 'person', 'notes', 'bid')
        ->first();

        $path = 'pdfs/suppliers/'.$job->id.'/material_list.pdf';

        File::delete(public_path($path));

        // get the base snappy class
        $base = App::make('snappy.pdf');
        // load up the view with the data
        $pdf = PDF::loadView('pdfs.material_list', ['job' => $job]);
        // write the pdf's html to the base pdf
        // you just started and save to path
        $base->generateFromHtml($pdf->html, public_path($path));

        return redirect('admin/pdf/suppliers/show/'.$job_id);
    }

    public function createInvoice($job_id)
    {
        $job = Job::where('id', $job_id)
        ->with('material_list', 'person', 'notes', 'bid')
        ->first();

        $path = 'pdfs/invoices/'.$job->id.'/invoice.pdf';

        $invoice_amount = [];
        foreach($job->material_list->categories as $category){
            $invoice_amount[] = $category->price*$category->pivot->qty;
        }

        $square_count = [];
        foreach($job->material_list->categories as $category){
            if($category->unit == 'square'){
                $square_count[] = $category->pivot->qty;
            }
        }

        $labor_per_square_amount = [];
        foreach($job->material_list->categories as $category){
            if($category->unit == 'labor/square'){
                $labor_per_square_amount[] = $category->pivot->category_price;
            }
        }

        // check to see if file/record already exists
        // and delete if so.
        if($job->invoice_amount || File::exists(public_path($path))){
            File::delete(public_path($path));
        }
        $job->invoice_amount = array_sum($invoice_amount);
        $job->square_count = array_sum($square_count);
        $job->labor_per_square_amount = array_sum($labor_per_square_amount);
        $job->save();

        // get the base snappy class
        $base = App::make('snappy.pdf');
        // load up the view with the data
        $pdf = PDF::loadView('pdfs.invoice', ['job' => $job]);
        // write the pdf's html to the base pdf
        // you just started and save to path

        $base->generateFromHtml($pdf->html, public_path($path));

        return redirect('admin/pdf/invoices/show/'.$job_id);
    }

    public function createPaperEstimate($estimate_id)
    {
        $estimate = Estimate::where('id', $estimate_id)
        ->with('person', 'notes')
        ->first();

        $path = 'pdfs/estimates/'.$estimate->id.'/estimate.pdf';

        // check to see if file/record already exists
        // and delete if so.
        if($estimate || File::exists(public_path($path))){
            File::delete(public_path($path));
        }

        // get the base snappy class
        $base = App::make('snappy.pdf');
        // load up the view with the data
        $pdf = PDF::loadView('pdfs.estimate', ['estimate' => $estimate]);
        // write the pdf's html to the base pdf
        // you just started and save to path
        $base->generateFromHtml($pdf->html, public_path($path));

        return redirect('admin/pdf/estimates/show/'.$estimate_id);
    }

    public function email(EmailRequest $request, $reference_id)
    {
        Flash::success('Your message has been sent successfully.');

        if(request()->segment(3) == 'bids'){

            $bid = Bid::where('id', $reference_id)
            ->with('material_list', 'person', 'notes')
            ->first();

            $bid->sent_at = Carbon::now();
            $bid->save();

            Mail::to($bid->person->email)->send(new EmailBid($bid, $request->subject, $request->content));
            return redirect('admin/pdf/bids/show/'.$reference_id);

        }elseif(request()->segment(3) == 'suppliers'){

            $job = Job::where('id', $reference_id)
            ->with('material_list', 'person', 'notes', 'bid')
            ->first();

            $job->ordered_at = Carbon::now();
            $job->save();

            Mail::to($request->to)->send(new EmailSupplier($job, $request->subject, $request->content));
            return redirect('admin/pdf/suppliers/show/'.$reference_id);

        }elseif(request()->segment(3) == 'jobs'){

            $job = Job::where('id', $reference_id)
            ->with('material_list', 'person', 'notes', 'bid')
            ->first();

            Mail::to($request->to)->send(new EmailJob($job, $request->subject, $request->content));
            return redirect('admin/pdf/jobs/show/'.$reference_id);

        }elseif(request()->segment(3) == 'invoices'){

            $job = Job::where('id', $reference_id)
            ->with('material_list', 'person', 'notes', 'bid')
            ->first();

            $job->invoiced_at = Carbon::now();
            $job->save();

            Mail::to($job->person->email)->send(new EmailInvoice($job, $request->subject, $request->content));
            return redirect('admin/pdf/invoices/show/'.$reference_id);

        }

        return redirect('admin/pdf/bids/show/'.$job_id);
    }

    public function show($reference_id)
    {
        if(request()->segment(3) == 'estimates'){

            $estimate = Estimate::where('id', $reference_id)
            ->with('person')
            ->first();

            return view('pdfs/show_estimate')
            ->with('estimate', $estimate);

        }elseif(request()->segment(3) == 'bids'){

            $bid = Bid::where('id', $reference_id)
            ->with('material_list', 'person', 'notes')
            ->first();

            return view('pdfs/show_bid')
            ->with('bid', $bid);

        }elseif(request()->segment(3) == 'suppliers'){

            $job = Job::where('id', $reference_id)
            ->with('material_list', 'person', 'notes', 'bid')
            ->first();

            $supervisors = [];
            $selected = [];
            $people = $job->material_list->company->people;
            foreach($people as $person){
                if($person->hasRole('Supervisor')){
                    $supervisors[$person->email] = $person->email;
                    $selected[] = $person->email;
                }
            }

            return view('pdfs/show_material_list')
            ->with('job', $job)
            ->with('supervisors', $supervisors)
            ->with('selected', $selected);

        }elseif(request()->segment(3) == 'jobs'){

            $job = Job::where('id', $reference_id)
            ->with('material_list', 'person', 'notes', 'bid')
            ->first();

            return view('pdfs/show_job')
            ->with('job', $job);

        }elseif(request()->segment(3) == 'invoices'){

            $job = Job::where('id', $reference_id)
            ->with('material_list', 'person', 'notes', 'bid')
            ->first();

            return view('pdfs/show_invoice')
            ->with('job', $job);

        }elseif(request()->segment(3) == 'estimates'){
            $estimate = Estimate::where('id', $reference_id)
            ->with('person', 'notes')
            ->first();

            return view('pdfs/show_estimate')
            ->with('estimate', $estimate);
        }
    }

    // File response methods -------------------------------------
    public function estimates($estimate_id)
    {
        $path = 'pdfs/estimates/'.$estimate_id.'/estimate.pdf';
        return response()->file($path);
    }

    public function bids($job_id)
    {
        $path = 'pdfs/bids/'.$job_id.'/bid.pdf';
        return response()->file($path);
    }

    public function jobs($job_id)
    {
        $path = 'pdfs/jobs/'.$job_id.'/job.pdf';
        return response()->file($path);
    }

    public function suppliers($job_id)
    {
        $path = 'pdfs/suppliers/'.$job_id.'/material_list.pdf';
        return response()->file($path);
    }

    public function invoices($job_id)
    {
        $path = 'pdfs/invoices/'.$job_id.'/invoice.pdf';
        return response()->file($path);
    }
    // File response methods -------------------------------------

    // Testing methods -------------------------------------
    public function pdf($reference_id)
    {
        // $bid = Bid::where('id', $reference_id)
        // ->with('material_list')
        // ->first();

        // $job = Job::where('id', $reference_id)
        // ->with('material_list', 'person', 'notes', 'bid')
        // ->first();

        $estimate = Estimate::where('id', $reference_id)
        ->with('person', 'notes')
        ->first();

        // $pdf = PDF::loadView('pdfs.bid', ['bid' => $bid]);
        // $pdf = PDF::loadView('pdfs.material_list', ['job' => $job]);
        // $pdf = PDF::loadView('pdfs.job', ['job' => $job]);
        // $pdf = PDF::loadView('pdfs.invoice', ['job' => $job]);
        $pdf = PDF::loadView('pdfs.estimate', ['estimate' => $estimate]);
        // $pdf = PDF::loadView('pdfs.test');
        return $pdf->inline();
    }

    public function html($reference_id)
    {
        // $bid = Bid::where('id', $reference_id)
        // ->with('material_list')
        // ->first();

        // $job = Job::where('id', $reference_id)
        // ->with('material_list', 'person', 'notes', 'bid')
        // ->first();

        $estimate = Estimate::where('id', $reference_id)
        ->with('person', 'notes')
        ->first();

        // return view('pdfs.bid')
        // return view('pdfs.material_list')
        // return view('pdfs.job')
        // return view('pdfs.invoice')
        return view('pdfs.estimate')
        // ->with('job', $job);
        ->with('estimate', $estimate);
    }
    // Testing methods --------------------------------------
}
