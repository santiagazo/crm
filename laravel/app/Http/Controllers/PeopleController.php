<?php

namespace Milne\Http\Controllers;

use Milne\Http\Requests\PeopleRequest;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Milne\Company;
use Milne\Person;
use Milne\Type;
use Milne\Tax;
use Flash;
use Auth;
use PDF;
use DB;

class PeopleController extends Controller
{
    public function index(Request $request)
    {
        $company_id = $request->company_id ?: NULL;
        $name = $request->name ?: NULL;
        $address = $request->address ?: NULL;
        $city = $request->city ?: NULL;
        $state = $request->state ?: NULL;
        $zip = $request->zip ?: NULL;
        $phone = $request->phone ?: NULL;
        $email = $request->email ?: NULL;
        $roles = $request->roles ?: NULL;
        $q = $request->q ?: NULL;


        $people = Person::select('people.*')->with('company');
        if($company_id){
            $people = $people->leftJoin('companies', 'companies.id', '=', 'people.company_id')
                ->where('companies.name', 'LIKE', '%'.$company_id.'%');
        }
        if($name){
            $people = $people->where('people.name', 'LIKE', '%'.$name.'%');
        }
        if($address){
            $people = $people->where('people.address', 'LIKE', '%'.$address.'%');
        }
        if($city){
            $people = $people->where('people.city', 'LIKE', '%'.$city.'%');
        }
        if($state){
            $people = $people->where('people.state', 'LIKE', '%'.$state.'%');
        }
        if($zip){
            $people = $people->where('people.zip', 'LIKE', '%'.$zip.'%');
        }
        if($phone){
            $people = $people->where('people.phone', 'LIKE', '%'.$phone.'%');
        }
        if($email){
            $people = $people->where('people.email', 'LIKE', '%'.$email.'%');
        }
        if($q){
            $people = $people->leftJoin('companies', 'companies.id', '=', 'people.company_id')
                ->where('companies.name', 'LIKE', '%'.$q.'%')
                ->orWhere('people.name', 'LIKE', '%'.$q.'%');
        }

        $people = $people->paginate(25);

        if($request->ajax()){
            return json_encode($people);
        }

        return view('auth.people.index')
        ->with('people', $people)
        ->with('company_id', $company_id)
        ->with('name', $name)
        ->with('address', $address)
        ->with('city', $city)
        ->with('state', $state)
        ->with('zip', $zip)
        ->with('phone', $phone)
        ->with('roles', $roles)
        ->with('email', $email);
    }

    public function create()
    {
        $roles = Role::whereIn('name', ['Client', 'Supplier', 'Supervisor'])->pluck('name', 'name');
        $taxed_cities = Tax::pluck('city', 'slug');

        return view('auth.people.create')
        ->with('roles', $roles)
        ->with('taxed_cities', $taxed_cities);
    }

    public function store(PeopleRequest $request)
    {
        $person = Person::create([
            'company_id' => $request->company_id ?: NULL,
            'name' => $request->name ?: NULL,
            'address' => $request->address ?: NULL,
            'city' => $request->city ?: NULL,
            'state' => 'UT',
            'zip' => $request->zip ?: NULL,
            'phone' => formatPhone($request->phone) ?: NULL,
            'email' => $request->email ?: NULL
        ]);

        $person->syncRoles($request->role);

        Flash::success('The person has been saved successfully.');

        return redirect('admin/people');
    }

    public function show($id)
    {
        $person = Person::where('id', $id)->with('jobs')->first();
        $companies = Company::where('id', '!=', 1)->pluck('name', 'id');
        $roles = Role::whereIn('name', ['Client', 'Supplier', 'Supervisor'])->pluck('name', 'name');

        $galleries = [];
        foreach($person->estimates as $estimate){
            $galleries[] = '#images_for_estimate_'.$estimate->id;
        }

        $galleries = implode(', ', $galleries);

        return view('auth.people.show')
        ->with('roles', $roles)
        ->with('companies', $companies)
        ->with('person', $person)
        ->with('galleries', $galleries);
    }

    public function edit($id)
    {
        $person = Person::findOrFail($id);
        $companies = Company::where('id', '!=', 1)->pluck('name', 'id');
        $roles = Role::whereIn('name', ['Client', 'Supplier', 'Supervisor'])->pluck('name', 'name');

        return view('auth.people.edit')
        ->with('roles', $roles)
        ->with('companies', $companies)
        ->with('person', $person);
    }

    public function update(PeopleRequest $request, $id)
    {
        $person = Person::where('id', $id)->first();

        $person->update([
            'company_id' => $request->company_id ?: NULL,
            'name' => $request->name ?: NULL,
            'address' => $request->address ?: NULL,
            'city' => $request->city ?: NULL,
            'state' => 'UT',
            'zip' => $request->zip ?: NULL,
            'phone' => formatPhone($request->phone) ?: NULL,
            'email' => $request->email ?: NULL
        ]);

        $person->syncRoles($request->role);

        Flash::success('All changes saved successfully.');

        return redirect('admin/people');
    }

    public function delete($id)
    {
        $person = Person::where('id', $id)->first();

        if(count($person->jobs)){
            Flash::error('This person is already tied to a job and thus cannot be deleted. Remove them from all jobs to delete.');
            return redirect('admin/people/edit/'.$id);
        }

        $person->syncRoles([]);
        $person->delete();

        Flash::success('The person has been deleted successfully.');

        return redirect('admin/people');
    }
}
