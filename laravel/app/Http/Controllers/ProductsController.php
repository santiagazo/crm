<?php

namespace Milne\Http\Controllers;

use Milne\Http\Requests\ProductRequest;

use Illuminate\Http\Request;
use Milne\Category;
use Carbon\Carbon;
use Milne\Company;
use Milne\Product;
use Milne\Type;
use Config;
use Flash;
use Auth;
use PDF;
use DB;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        $company_id = $request->company_id ?: NULL;
        $category_id = $request->category_id ?: NULL;
        $name = $request->name ?: NULL;
        $house_price = $request->house_price ?: NULL;
        $supplier_price = $request->supplier_price ?: NULL;
        $unit = $request->unit ? str_slug($request->unit, '_') : NULL;
        $description = $request->description ?: NULL;
        $colors = $request->colors ?: NULL;
        $id = $request->id ?: NULL;

        $products = Product::select('products.*')
        ->with('company')
        ->with('category');
        
        if($company_id){
            $products = $products->leftJoin('companies', 'companies.id', '=', 'products.company_id')
                ->where('companies.name', 'LIKE', '%'.$company_id.'%');
        }
        if($category_id){
            $products = $products->leftJoin('categories', 'categories.id', '=', 'products.category_id')
                ->where('categories.name', 'LIKE', '%'.$category_id.'%');
        }
        if($name){
            $products = $products->where('products.name', 'LIKE', '%'.$name.'%');
        }
        if($supplier_price){
            $products = $products->where('products.supplier_price', '=', $supplier_price);
        }
        if($unit){
            $products = $products->where('products.unit', 'LIKE', '%'.$unit.'%');
        }
        if($description){
            $products = $products->where('products.description', 'LIKE', '%'.$description.'%');
        }

        $products = $products->orderBy('products.id')->paginate(25);

        if($request->ajax()){
            return json_encode($products);
        }

        return view('auth.products.index')
        ->with('products', $products)
        ->with('company_id', $company_id)
        ->with('category_id', $category_id)
        ->with('name', $name)
        ->with('house_price', $house_price)
        ->with('supplier_price', $supplier_price)
        ->with('unit', $unit)
        ->with('description', $description);
    }

    public function create()
    {
        $types = Type::where('category', 'taxable')->pluck('name', 'id');
        $units = Config::get('units');

        return view('auth.products.create')
        ->with('types', $types)
        ->with('units', $units);
    }

    public function store(ProductRequest $request)
    {
        $product = Product::create([
            'company_id' => $request->company_id ?: NULL,
            'category_id' => $request->category_id ?: NULL,
            'type_id' => $request->type_id ?: NULL,
            'name' => $request->name ?: NULL,
            'supplier_price' => $request->supplier_price ?: 0,
            'unit' => $request->unit ? str_slug($request->unit, '_') : NULL,
            'colors' => $request->colors ? implode(',', $request->colors) : NULL,
            'description' => $request->description ?: NULL,
        ]);

        Flash::success('The product has been saved successfully.');

        return redirect('admin/products');
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $types = Type::where('category', 'taxable')->pluck('name', 'id');
        $units = Config::get('units');
        $colors = array_combine(array_unique(explode(',', $product->colors)), array_unique(explode(',', $product->colors)));

        return view('auth.products.edit')
        ->with('product', $product)
        ->with('types', $types)
        ->with('units', $units)
        ->with('colors', $colors);
    }

    public function update(ProductRequest $request, $id)
    {
        $product = Product::where('id', $id)->update([
            'company_id' => $request->company_id ?: NULL,
            'category_id' => $request->category_id ?: NULL,
            'type_id' => $request->type_id ?: NULL,
            'name' => $request->name ?: NULL,
            'supplier_price' => $request->supplier_price ?: 0,
            'unit' => $request->unit ? str_slug($request->unit, '_') : NULL,
            'description' => $request->description ?: NULL,
            'colors' => $request->colors ? implode(',', $request->colors) : NULL,
        ]);

        Flash::success('All changes saved successfully.');

        return redirect('admin/products');
    }

    public function delete($id)
    {
        $product = Product::where('id', $id)->first();

        if(count($product->material_lists_through_pivot)){
            Flash::error('This product is already tied to a material list and thus cannot be deleted. Remove it from all material lists to delete.');
            return redirect('admin/products/edit/'.$id);
        }

        $product->delete();

        Flash::success('The product has been deleted successfully.');

        return redirect('admin/products');
    }

    public function colors(Request $request, $reference_id)
    { 
        if($request->type == 'category'){
            $reference = Category::where('id', $reference_id)->first();
        }else{
            $reference = Product::where('id', $reference_id)->first();
        }

        $colors = [];
        foreach(explode(',', $reference->colors) as $color){
            if($color){
                $colors[] = [
                    'id' => $color, 
                    'text' => str_unslug($color),
                    'title' => $color
                ];
            }
        }

        return json_encode($colors);
    }
}
