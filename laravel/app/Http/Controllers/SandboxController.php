<?php

namespace Milne\Http\Controllers;

use Illuminate\Http\Request;
use Milne\Person;
use Milne\Product;
use Image;
use File;

class SandboxController extends Controller
{
    public function index(Request $request)
    {

        $company_id = $request->company_id ?: NULL;
        $name = $request->name ?: NULL;
        $address = $request->address ?: NULL;
        $city = $request->city ?: NULL;
        $state = $request->state ?: NULL;
        $zip = $request->zip ?: NULL;
        $phone = $request->phone ?: NULL;
        $email = $request->email ?: NULL;
        $roles = $request->roles ?: NULL;
        $q = $request->q ?: NULL;


        $people = Person::select('people.*')->with('company');
        if($company_id){
            $people = $people->leftJoin('companies', 'companies.id', '=', 'people.company_id')
                ->where('companies.name', 'LIKE', '%'.$company_id.'%');
        }
        if($name){
            $people = $people->where('people.name', 'LIKE', '%'.$name.'%');
        }
        if($address){
            $people = $people->where('people.address', 'LIKE', '%'.$address.'%');
        }
        if($city){
            $people = $people->where('people.city', 'LIKE', '%'.$city.'%');
        }
        if($state){
            $people = $people->where('people.state', 'LIKE', '%'.$state.'%');
        }
        if($zip){
            $people = $people->where('people.zip', 'LIKE', '%'.$zip.'%');
        }
        if($phone){
            $people = $people->where('people.phone', 'LIKE', '%'.$phone.'%');
        }
        if($email){
            $people = $people->where('people.email', 'LIKE', '%'.$email.'%');
        }
        if($q){
            $people = $people->leftJoin('companies', 'companies.id', '=', 'people.company_id')
                ->where('companies.name', 'LIKE', '%'.$company_id.'%')
                ->orWhere('people.name', 'LIKE', '%'.$name.'%')
                ->paginate(25);
            
            return response()->json($people);
        }

        $people = $people->paginate(25);

        if(Request::ajax()){
            return json_encode($people);
        }

        // return view('auth.people.index')
        // ->with('people', $people)
        // ->with('company_id', $company_id)
        // ->with('name', $name)
        // ->with('address', $address)
        // ->with('city', $city)
        // ->with('state', $state)
        // ->with('zip', $zip)
        // ->with('phone', $phone)
        // ->with('roles', $roles)
        // ->with('email', $email);
    }

    public function mail()
    {

        $person = Person::findOrFail(174);
        
        return view('emails.estimateRequestConfirmation')
        ->with('person', $person);
    }

    public function ammon()
    {
        return view('sandbox/ammon');
    }
}
