<?php

namespace Milne\Http\Controllers;

use Illuminate\Http\Request;
use Milne\Location;
use Milne\Testimonial;
use Milne\Person;
use Milne\Rating;
use Milne\Estimate;
use Milne\Job;
use Milne\Bid;
use Milne\Tax;
use Illuminate\Support\Facades\Mail;
use Milne\Mail\Estimate as EmailEstimate;
use Milne\Mail\EstimateRequestConfirmation;
use Milne\Http\Requests\EstimateRequest;
use Flash;
use Carbon\Carbon;
use Milne\Libraries\Address;
use GuzzleHttp\Stream\Stream;
use GuzzleHttp\Client;
use File;
use Image;

class SiteController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site.home');
    }

    public function products()
    {
        return view('site.products');
    }

    public function warranty()
    {
        return view('site.warranty');
    }

    public function outreach()
    {
        return view('site.outreach');
    }

    public function shingle()
    {
        return view('site.shingle');
    }

    public function gallery()
    {
        return view('site.gallery');
    }

    public function testimonials()
    {
        $testimonials = Testimonial::orderBy('person_id')
        ->paginate(5);
        $testimonialsCount = Testimonial::count();
        $ratings = Rating::all();

        $qualityOfWorkmanshipArray = [];
        $cleanlinessArray = [];
        $professionalismArray = [];
        $jobCompletedOnTimeArray = [];
        $priceArray = [];

        foreach($ratings as $rating){
            if($rating->slug == 'quality-of-workmanship' ){
                $qualityOfWorkmanshipArray[] = $rating->number;
            }

            if($rating->slug == 'cleanliness' ){
                $cleanlinessArray[] = $rating->number;
            }

            if($rating->slug == 'professionalism' ){
                $professionalismArray[] = $rating->number;
            }

            if($rating->slug == 'job-completed-on-time' ){
                $jobCompletedOnTimeArray[] = $rating->number;
            }

            if($rating->slug == 'price' ){
                $priceArray[] = $rating->number;
            }
        }

        $qualityOfWorkmanship = array_sum($qualityOfWorkmanshipArray) / count($qualityOfWorkmanshipArray);
        $cleanliness = array_sum($cleanlinessArray) / count($cleanlinessArray);
        $professionalism = array_sum($professionalismArray) / count($professionalismArray);
        $jobCompletedOnTime = array_sum($jobCompletedOnTimeArray) / count($jobCompletedOnTimeArray);
        $price = array_sum($priceArray) / count($priceArray);

        return view('site.testimonials')
        ->with('testimonials', $testimonials)
        ->with('testimonialsCount', $testimonialsCount)
        ->with('qualityOfWorkmanship', $qualityOfWorkmanship)
        ->with('cleanliness', $cleanliness)
        ->with('professionalism', $professionalism)
        ->with('jobCompletedOnTime', $jobCompletedOnTime)
        ->with('price', $price);
    }

    public function storeTestimonials(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
            'email' => 'required|email',
            'phone' => 'required|min:10|max:15',
            'review' => 'required|min:10|max:255',
            'ratings' => 'required',
        ]);

        $person = Person::where('email', $request->email)->first();
        $ratingsArray = [];
        $ratingsRequest = json_decode($request->ratings);
        $i = 0;

        $testimonial = new Testimonial;
        $testimonial->person_id = $person ? $person->id : NULL;
        $testimonial->name = $request->name;
        $testimonial->phone = formatPhone($request->phone);
        $testimonial->email = $request->email;
        $testimonial->comment = $request->review;


        foreach($ratingsRequest as $ratingRequest){

            $name = str_replace('_', ' ', $ratingRequest->name);
            $name = ucwords($name);

            $rating = new Rating;
            $rating->testimonial_id = $testimonial ? $testimonial->id : NULL;
            $rating->name = $name;
            $rating->slug = str_slug($ratingRequest->name, '_');
            $rating->number = $ratingRequest->number;
            $rating->save();

            $ratingsArray[] = $ratingRequest->number;
            $i++;
        }

        $testimonial->stars = $ratingsRequest && $i > 0 ? array_sum($ratingsArray)/$i : NULL;
        $testimonial->save();

        Flash::success('Your review has been saved successfully.');

        return redirect('/testimonials');

    }

    public function about()
    {
        return view('site.about');
    }

    public function contact()
    {
        return view('site.contact');
    }

    public function storeContact(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
            'email' => 'required|email',
            'phone' => 'required|min:10|max:15',
            'message' => 'required|min:10|max:255',
        ]);

        Mail::to('milnebrothersroofing@gmail.com ')->send(new Contact($request->name, $request->email, formatPhone($request->phone), $request->message));

        Flash::success('Your message has been sent successfully.');

        return redirect('contact');
    }

    public function estimate()
    {
        $taxed_cities = Tax::pluck('city', 'slug');
        
        return view('site.estimate')
        ->with('taxed_cities', $taxed_cities);
    }

    public function storeEstimate(EstimateRequest $request)
    {
        $requestedServices = [];
        $name = $request->first_name.' '.$request->last_name;

        $person = Person::where('email', $request->email)->first();

        if(!$person){
            $person = new Person;
        }

        $person->name = $name;
        $person->email = $request->email ?: NULL;
        $person->phone = formatPhone($request->phone) ?: NULL;
        $person->address = $request->address ?: NULL;
        $person->city = $request->city ?: NULL;
        $person->state = 'UT';
        $person->zip = $request->zip ?: NULL;
        $person->save();

        if(!$person->hasRole('Client')){
            $person->assignRole('Client');
        }

        $estimate_type = implode(',', $request->request_type);
        $estimate_type .= $request->other_text ? ' '.$request->other_text : NULL;

        $estimate = new Estimate;
        $estimate->person_id = $person->id;
        $estimate->status_id = 16;
        $estimate->type = $estimate_type;
        $estimate->roof_type = $request->roof_type ?: NULL;
        $estimate->roof_pitch = $request->roof_pitch ?: NULL;
        if($request->billing_same_as_project ){
            $estimate->project_address = $request->address ?: NULL;
            $estimate->project_city = $request->city ?: NULL;
            $estimate->project_zip = $request->zip ?: NULL;
        }else{
            $estimate->project_address = $request->project_address ?: NULL;
            $estimate->project_city = $request->project_city ?: NULL;
            $estimate->project_zip = $request->project_zip ?: NULL;
        }
        $estimate->is_urgent = $request->urgency == 'urgent' ? 1 : NULL;
        $estimate->additional_information = $request->additional_information ?: NULL;
        $estimate->advertisment = $request->advertisment ?: NULL;
        $estimate->referred_by = $request->referred_by ?: NULL;
        $estimate->save();

        Mail::to('milnebrothersroofing@gmail.com')->send(new EmailEstimate($person));
        Mail::to($person->email)->send(new EstimateRequestConfirmation($person));

        if($request->upload_images){
            Flash::success('Your estimate request has been submited successfully. You will be contacted at our earliest convenience. If you would like to upload images you may do that below.');

            return redirect('/estimate/images/'.$estimate->id);
        }

        Flash::success('Your estimate request has been submited successfully. You will be contacted at our earliest convenience.');

        return redirect('/home');
    }

    public function images($estimate_id){

        return view('site.images')
        ->with('estimate_id', $estimate_id);
    }

    public function quote($bid_id, $material_list_id, $person_id, $response)
    {
        $bid = Bid::findOrFail($bid_id);

        if(!$bid || $bid->material_list->id != $material_list_id || $bid->person->id != $person_id){
            abort(403, 'Unauthorized to accept or decline quote.');
        }

        return view('site.quote')
        ->with('bid', $bid)
        ->with('response', $response)
        ->with('bid_id', $bid_id)
        ->with('material_list_id', $material_list_id)
        ->with('person_id', $person_id);
    }

    public function storeQuote(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
            'email' => 'required|email',
            'phone' => 'required|min:10|max:15',
            'bid_id' => 'required|integer',
            'message' => 'max:255',
            'status' => 'required'
        ]);

        $bid = Bid::findOrFail($request->bid_id);

        if(!$bid || $bid->material_list->id != $request->material_list_id || $bid->person->id != $request->person_id){
            abort(403, 'Unauthorized to accept or decline quote.');
        }

        if($request->status == 'accept' && $request->email == $bid->person->email && formatPhone($request->phone) == $bid->person->phone){
            $status = 14;
            $bid->accepted_at = Carbon::now();
            Flash::success("We have successfully received your response. We look forward to working with you. We'll contact you shortly.");
        }elseif($request->status == 'declined'){
            $status = 15;
            Flash::info("We have successfully received your reponse to decline further services. Thank you for following up.");
        }else{
            $status = 12;
            if($request->status == 'accept'){
                $status = 13;
                Flash::error("We have successfully received your response but your email or phone don't match up with what we have in our system. Your response has been changed from \"Accepted\" to \"Conditionally Accepted\". We'll contact you to verify your details or you can resubmit the form if you know the email and phone we have in our system.");
            }
        }

        // Pending = 11
        // Conditionally Accepted = 12
        // Conditionally Accepted Needs Verification = 13
        // accepted = 14
        // Declined = 15

        $bid->status_id = $status;
        $bid->phone = formatPhone($request->phone);
        $bid->email = $request->email;
        $bid->message = $request->message;
        $bid->selected_cash_discount = $request->is_cash_discount ?: NULL;
        $bid->selected_pre_dry_in = $request->is_pre_dry_in ?: NULL;
        $bid->selected_six_nail = $request->is_six_nail ?: NULL;
        $bid->selected_owner_tear_off = $request->is_owner_tear_off ?: NULL;
        $bid->update();

        return redirect('quote/'.$request->bid_id.'/'.$request->material_list_id.'/'.$request->person_id.'/'.$request->status);
    }
}
