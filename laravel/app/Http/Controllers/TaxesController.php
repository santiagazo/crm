<?php

namespace Milne\Http\Controllers;

use Illuminate\Http\Request;
use Milne\Tax;

class TaxesController extends Controller
{
    public function index(Request $request)
    {
    	$taxes = Tax::where('slug', "LIKE", '%'.str_slug($request->q, '_').'%')
    	->paginate(25);

    	return json_encode($taxes);
    }
}


// TO UPDATE UTAH SALES TAX SEE:
// https://tax.utah.gov/sales/rates