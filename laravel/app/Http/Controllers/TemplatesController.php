<?php

namespace Milne\Http\Controllers;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Milne\Template;
use Carbon\Carbon;
use Milne\Company;
use Milne\Person;
use Milne\Status;
use Milne\MaterialList;
use Milne\Type;
use Milne\Bid;
use Flash;
use Auth;
use PDF;
use DB;

class TemplatesController extends Controller
{
    public function index(Request $request, $bid_id = NULL)
    {
        $name = $request->name ?: NULL;
        $house_total = $request->house_total ?: NULL;
        $bid_id = $request->bid_id ?: NULL;

        $templates = Template::select(DB::raw('templates.*, SUM(category_material_list.category_price*category_material_list.qty) as house_total'))
            ->join('material_lists', 'templates.material_list_id', '=', 'material_lists.id')
            ->join('category_material_list', 'category_material_list.material_list_id', '=', 'material_lists.id');

        if($name){
            $templates = $templates->where('templates.name', 'LIKE', '%'.$name.'%');
        }

        $templates = $templates->groupBy('templates.id')
        ->paginate(25);

        if($request->ajax()){
            return json_encode($templates);
        }

        return view('auth.templates.index')
        ->with('templates', $templates)
        ->with('name', $name)
        ->with('house_total', $house_total)
        ->with('bid_id', $bid_id);

    }

    public function show(Request $request, $template_id)
    {
        $bid_id = $request->bid_id ?: NULL;

        if($bid_id){
            $bid = Bid::findOrFail($bid_id);

            if(count($bid->material_list)){
                Flash::error('There is a material list already created for this bid. You have been redirected to edit the existing material list.');
                return redirect('admin/material_lists/edit/'.$bid->material_list->id.'/bid');
            }
        }

        $template = Template::select(DB::raw('templates.*, SUM(category_material_list.category_price*category_material_list.qty) as house_total'))
            ->where('templates.id', $template_id)
            ->join('material_lists', 'templates.material_list_id', '=', 'material_lists.id')
            ->join('category_material_list', 'category_material_list.material_list_id', '=', 'material_lists.id')
            ->groupBy('templates.id')
            ->first();

        return view('auth.templates.show')
        ->with('template', $template)
        ->with('bid_id', $bid_id);
    }

    public function copy(Request $request, $template_id)
    {
        $template = Template::where('id', $template_id)->first();
        $bid_id = $request->bid_id ?: NULL;

        $bid = Bid::findOrFail($bid_id);

        if(count($bid->material_list)){
            Flash::error('There is a material list already created for this bid. You have been redirected to edit the existing material list.');
            return redirect('admin/material_lists/edit/'.$bid->material_list->id.'/bid');
        }

        $materialList = new MaterialList;
        $materialList->materialable_id = $bid_id;
        $materialList->materialable_type = 'Milne\Bid';
        $materialList->user_id = Auth::id();
        $materialList->status_id = 7;
        $materialList->company_id = NULL;
        $materialList->save();

        $categories = $template->material_list->categories;

        foreach($categories as $category){
            $materialList->categories()->attach([ $category->id => [
                    'qty' => $category->pivot->qty,
                    'category_price' => $category->pivot->category_price,
                ]
            ]);
        }

        Flash::success('Your templated material list has been added successfully.');

        return Redirect('admin/material_lists/edit/'.$materialList->id.'/bid');
    }

    public function delete($id)
    {
        $template = Template::findOrFail($id);
        $template->delete();

        Flash::success('The template has been deleted successfully.');

        return Redirect('admin/templates');
    }
}
