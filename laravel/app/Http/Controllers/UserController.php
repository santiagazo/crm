<?php

namespace Milne\Http\Controllers;

use Milne\Http\Requests\UserInviteRequest;
use Illuminate\Http\Request;
use Milne\User;
use Milne\Location;
use Milne\RegistrationReferral;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Milne\Mail\UserInvite;
use Spatie\Permission\Models\Role;
use Flash;


class UserController extends Controller
{
    public function dashboard()
    {
        if(auth()->user()->hasRole('Admin')){
            return redirect('admin/dashboard');
        }else{
            return redirect('employee/dashboard');
        }
    }

    public function index(Request $request)
    {
    	$location_id = $request->location_id ?: NULL;
        $name = $request->name ?: NULL;
        $address = $request->address ?: NULL;
        $city = $request->city ?: NULL;
        $state = $request->state ?: NULL;
        $zip = $request->zip ?: NULL;
        $phone = $request->phone ?: NULL;
        $email = $request->email ?: NULL;
        $roles = $request->roles ?: NULL;

        $users = User::with('location')
        ->with('roles');

        if($location_id){
            $users = $users->leftJoin('companies', 'companies.id', '=', 'users.location_id')
                ->where('companies.name', 'LIKE', '%'.$location_id.'%');
        }
        if($name){
            $users = $users->where('users.name', 'LIKE', '%'.$name.'%');
        }
        if($address){
            $users = $users->where('users.address', 'LIKE', '%'.$address.'%');
        }
        if($city){
            $users = $users->where('users.city', 'LIKE', '%'.$city.'%');
        }
        if($state){
            $users = $users->where('users.state', 'LIKE', '%'.$state.'%');
        }
        if($zip){
            $users = $users->where('users.zip', 'LIKE', '%'.$zip.'%');
        }
        if($phone){
            $users = $users->where('users.phone', 'LIKE', '%'.$phone.'%');
        }
        if($email){
            $users = $users->where('users.email', 'LIKE', '%'.$email.'%');
        }

        $users = $users->orderBy('users.id', 'desc')->paginate(25);

        if($request->ajax()){
            return json_encode($users);
        }

        return view('auth.users.index')
        ->with('users', $users)
        ->with('location_id', $location_id)
        ->with('name', $name)
        ->with('address', $address)
        ->with('city', $city)
        ->with('state', $state)
        ->with('zip', $zip)
        ->with('phone', $phone)
        ->with('roles', $roles)
        ->with('email', $email);
    }

    public function create()
    {
    	return view('auth.users.create');
    }

    public function store(UserInviteRequest $request)
    {
    	$code = str_random(25);

    	$registration_referral = new RegistrationReferral;
    	$registration_referral->code = $code;
    	$registration_referral->role = $request->role;
    	$registration_referral->save();

    	$url = url('register').'?referral_code='.$code;

    	Mail::to($request->email)->send(new UserInvite($request->name, $url));

        Flash::success('An invite has been succeessfully sent to '.$request->email);

    	return redirect('admin/users');
    }

    public function edit(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        $locations = Location::pluck('name', 'id');
        $roles = Role::pluck('name', 'name');

        return view('auth.users.edit')
        ->with('user', $user)
        ->with('locations', $locations)
        ->with('roles', $roles);
    }

    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        $user->name = $request->name;
        $user->location_id = $request->location_id;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->zip = $request->zip;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->save();

        $user->syncRoles($request->role);

        Flash::success($user->name.' has been updated succeessfully.');

        return redirect('admin/users');
    }

    public function delete($id)
    {   
        $user = User::where('id', $id)->delete();
        Flash::success( $user->name.' has been deleted succeessfully.');
        return redirect('admin/users');
    }
}
