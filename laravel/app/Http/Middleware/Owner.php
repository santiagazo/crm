<?php

namespace Milne\Http\Middleware;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Redirect;
use Closure;
use Flash;
use Auth;

class Owner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $is_owner = (bool) Auth::user()->hasRole('Owner');

        if(!$is_owner){
            Flash::error('You are not authorized to access this route.');
            return redirect('/home');
        }

        return $next($request);
    }
}
