<?php

namespace Milne\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EstimateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->person_id){
            return [
                "person_id" => 'required|int',
                "request_type" => 'required|array',
                "urgency" => "required",
                "project_address" => "required|min:3|max:255",
                "project_city" => "required|min:3|max:255",
                "project_zip" => "required|min:5|max:5",
                "additional_information" => 'max:500',
                "advertisment" => "required"
            ];
        }

        if($this->advertisment){
            return [
                "request_type" => 'required|array',
                "urgency" => "required",
                "first_name" => "required|min:2|max:255",
                "last_name" => "required|min:2|max:255",
                "email" => "required|email",
                "phone" => "required|min:10|max:15",
                "project_address" => "required|min:3|max:255",
                "project_city" => "required|min:3|max:255",
                "project_zip" => "required|min:5|max:5",
                "address" => "required|min:3|max:255",
                "city" => "required|min:3|max:255",
                "zip" => "required|min:5|max:5",
                "additional_information" => 'max:500',
                "advertisment" => "required"
            ];
        }

        return [
            'status_id' => 'required|numeric',
        ];
    }
}
