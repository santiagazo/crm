<?php

namespace Milne\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaterialListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'line_items' => 'required',
            'materialable_id' => 'required|int'
        ];
    }
}
