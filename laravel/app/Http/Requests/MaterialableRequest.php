<?php

namespace Milne\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaterialableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            "person_id" => "required|integer",
            "status_id" => "required|integer",
            "type_id" => "required|integer",
            "pitches" => "required|array",
            "accepted_at" => "date",
            "address" => "required|min:5|max:255",
            "city" => "required|min:2|max:255",
            "zip" => "required|min:5|max:5",
            "instructions" => "max:255",
            "lost_reason" => "max:255",
            "note" => "max:500"
        ];

        return $rules;
    }
}
