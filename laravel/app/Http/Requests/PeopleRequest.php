<?php

namespace Milne\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PeopleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "role" => "required",
            "name" => "required|min:2|max:191",
            "address" => "required|min:2|max:191",
            "city" => "required|min:2|max:191",
            "zip" => "required|min:2|max:10",
            "phone" => "required|min:9|max:14",
            "email" => "required|email"
        ];
    }
}
