<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function mediable()
    {
    	return $this->morphTo();
    }
}
