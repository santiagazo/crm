<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'purchase_order',
        'user_id',
        'company_id',
        'person_id',
        'status_id',
        'type_id',
        'bid_amount',
        'invoice_amount',
        'name',
        'subdivision',
        'lot',
        'instructions',
        'start_at',
        'end_at',
        'accepted_at',
        'ordered_at',
        'invoiced_at',
        'completed_at'
    ];


    public function user()
    {
        return $this->belongsTo('Milne\User');
    }

    public function company()
    {
        return $this->belongsTo('Milne\Company');
    }

    public function person()
    {
        return $this->belongsTo('Milne\Person', 'person_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo('Milne\Status');
    }

    public function type()
    {
        return $this->belongsTo('Milne\Type');
    }

    public function tax()
    {
        return $this->belongsTo('Milne\Tax');
    }

    public function estimate()
    {
        return $this->belongsTo('Milne\Estimate');
    }

    public function notes()
    {
        return $this->morphMany('Milne\Note', 'noteable');
    }

    public function media()
    {
        return $this->morphMany('Milne\Media', 'mediable');
    }

    public function material_list()
    {
        return $this->morphOne('Milne\MaterialList', 'materialable');
    }

    public function bid()
    {
        return $this->belongsTo('Milne\Bid');
    }

    public function pitches()
    {
        return $this->morphMany('Milne\Pitch', 'pitchable');
    }

    public function uc_name()
    {
        return "Job";
    }

    public function lc_name()
    {
        return "job";
    }
}
