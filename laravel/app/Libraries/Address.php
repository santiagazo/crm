<?php

namespace Milne\Libraries;

use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Stream\Stream;
use GuzzleHttp\Client;

class Address
{
    private $address;
    private $types;
    private $type;

    public function __construct($query)
    {
        $settings = [
            'headers' => [
                'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Encoding' => 'gzip, deflate',
                'Accept-Language' => 'en-US,en;q=0.5',
                'Session-Control' => 'max-age=0',
                'Connection' => 'keep-alive',
                'DNT' => 1,
                'Upgrade-Insecure-Requests' => 1,
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0'
            ],
            'http_errors' => false,
            //'debug' => true,
            'track_redirects' => true
        ];
        
        $client = new Client($settings);
        
        try {
        
            $response = $client->request('GET', "https://maps.googleapis.com/maps/api/geocode/json?address={$query}");  
        
        }catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            return $responseBodyAsString;
        }

        $json = json_decode($response->getBody(), true);
        $address = $json['results'];

        $this->address = isset($address[0]) ? $address[0] : NULL;
        $this->types =  isset($address[0]) ? $address[0]['types'] : [];
        $this->type = isset($this->types[0]) ? $this->types[0] : NULL;
    }

    public function getFullAddress()
    {
        $FullAddress = isset($this->address['formatted_address']) ? $this->address['formatted_address'] : NULL;
        return $FullAddress ?: NULL;
    }

    public function getStreeNumber()
    {
        if($this->type == 'street_address' || in_array('school', $this->types)){
            // street
            $StreeNumber = isset($this->address['address_components'][0]['long_name']) ? $this->address['address_components'][0]['long_name'] : NULL;
        }elseif($this->type == 'locality'){
            // city
            $StreeNumber = NULL;
        }elseif($this->type == 'postal_code'){
            // zip code
            $StreeNumber = NULL;
        }elseif($this->type == 'administrative_area_level_2'){
            // county
            $StreeNumber = NULL;
        }elseif($this->type == 'administrative_area_level_1'){
            // state
            $StreeNumber = NULL;
        }elseif($this->type == 'country'){
            // country
            $StreeNumber = NULL;
        }elseif($this->type == 'route'){
            // route
            $StreeNumber = NULL;
        }elseif(in_array('premise', $this->types)){
            // neighborhood
            $StreeNumber = NULL;
        }elseif(in_array('park', $this->types)){
            // park
            $StreeNumber = isset($this->address['address_components'][1]['long_name']) ? $this->address['address_components'][1]['long_name'] : NULL;
        }elseif(in_array('point_of_interest', $this->types)){
            // commercial
            $StreeNumber = isset($this->address['address_components'][0]['long_name']) ? $this->address['address_components'][0]['long_name'] : NULL;
        }

        return $StreeNumber ?: NULL;
    }

    public function getStreetName()
    {
        if($this->type == 'street_address'){
            // street
            $StreetName = isset($this->address['address_components'][1]['short_name']) ? $this->address['address_components'][1]['short_name'] : NULL;
        }elseif($this->type == 'locality'){
            // city
            $StreetName = NULL;
        }elseif($this->type == 'postal_code'){
            // zip code
             $StreetName = NULL;
        }elseif($this->type == 'administrative_area_level_2'){
            // county
            $StreetName = NULL;
        }elseif($this->type == 'administrative_area_level_1'){
            // state
            $StreetName = NULL;
        }elseif($this->type == 'country'){
            // country
            $StreetName = NULL;
        }elseif($this->type == 'route'){
            // route
            $StreetName = isset($this->address['address_components'][0]['short_name']) ? $this->address['address_components'][0]['short_name'] : NULL;
        }elseif(in_array('premise', $this->types)){
            // neighborhood
            $StreetName = isset($this->address['address_components'][0]['short_name']) ? $this->address['address_components'][0]['short_name'] : NULL;
        }elseif(in_array('school', $this->types)){
            // school
            $StreetName = isset($this->address['address_components'][1]['short_name']) ? $this->address['address_components'][1]['short_name'] : NULL;
        }elseif(in_array('park', $this->types)){
            // park
            $StreetName = isset($this->address['address_components'][2]['short_name']) ? $this->address['address_components'][2]['short_name'] : NULL;
        }elseif(in_array('point_of_interest', $this->types)){
            // commercial

        }

        return $StreetName ?: NULL;
    }

    public function getFullStreet()
    {
        if($this->type == 'street_address'){
            // street
            $streetNumber = isset($this->address['address_components'][0]['long_name']) ? $this->address['address_components'][0]['long_name'] : NULL;
            $streetName = isset($this->address['address_components'][1]['short_name']) ? $this->address['address_components'][1]['long_name'] : NULL;
            if($streetNumber && $streetName){
                $FullStreet = $streetNumber." ".$streetName;
            }
        }elseif($this->type == 'locality'){
            // city
            $FullStreet = NULL;
        }elseif($this->type == 'postal_code'){
            // zip code
            $FullStreet = NULL;
        }elseif($this->type == 'administrative_area_level_2'){
            // county
            $FullStreet = NULL;
        }elseif($this->type == 'administrative_area_level_1'){
            // state
            $FullStreet = NULL;
        }elseif($this->type == 'country'){
            // country
            $FullStreet = NULL;
        }elseif($this->type == 'route'){
            // route
            $FullStreet = isset($this->address['address_components'][0]['short_name']) ? $this->address['address_components'][0]['long_name'] : NULL;
        }elseif(in_array('premise', $this->types)){
            // neighborhood
            $FullStreet = isset($this->address['address_components'][0]['short_name']) ? $this->address['address_components'][0]['long_name'] : NULL;
        }elseif(in_array('school', $this->types)){
            // school
            $streetNumber = isset($this->address['address_components'][0]['long_name']) ? $this->address['address_components'][0]['long_name'] : NULL;
            $streetName = isset($this->address['address_components'][1]['short_name']) ? $this->address['address_components'][1]['long_name'] : NULL;
            if($streetNumber && $streetName){
                $FullStreet = $streetNumber." ".$streetName;
            }
        }elseif(in_array('park', $this->types)){
            // park
            $streetNumber = isset($this->address['address_components'][1]['long_name']) ? $this->address['address_components'][1]['long_name'] : NULL;
            $streetName = isset($this->address['address_components'][2]['short_name']) ? $this->address['address_components'][2]['long_name'] : NULL;
            if($streetNumber && $streetName){
                $FullStreet = $streetNumber." ".$streetName;
            }
        }elseif(in_array('point_of_interest', $this->types)){
            $streetNumber = isset($this->address['address_components'][0]['long_name']) ? $this->address['address_components'][0]['long_name'] : NULL;
            $streetName = isset($this->address['address_components'][1]['short_name']) ? $this->address['address_components'][1]['long_name'] : NULL;
            if($streetNumber && $streetName){
                $FullStreet = $streetNumber." ".$streetName;
            }
        }

        return $FullStreet ?: NULL;

    }

    public function getCity()
    {
        if($this->type == 'street_address'){
            // street
            $City = isset($this->address['address_components'][2]['long_name']) ? $this->address['address_components'][2]['long_name'] : NULL;
        }elseif($this->type == 'locality'){
            // city
            $City = isset($this->address['address_components'][0]['short_name']) ? $this->address['address_components'][0]['short_name'] : NULL;
        }elseif($this->type == 'postal_code'){
            // zip code
            $City = isset($this->address['address_components'][0]['short_name']) ? $this->address['address_components'][0]['short_name'] : NULL;
        }elseif($this->type == 'administrative_area_level_2'){
            // county
            $City = NULL;
        }elseif($this->type == 'administrative_area_level_1'){
            // state
            $City = NULL;
        }elseif($this->type == 'country'){
            // country
            $City = NULL;
        }elseif($this->type == 'route'){
            // route
            $City = isset($this->address['address_components'][1]['short_name']) ? $this->address['address_components'][1]['short_name'] : NULL;
        }elseif(in_array('premise', $this->types)){
            // neighborhood
            $City = isset($this->address['address_components'][1]['short_name']) ? $this->address['address_components'][1]['short_name'] : NULL;
        }elseif(in_array('school', $this->types)){
            // school
            $City = isset($this->address['address_components'][2]['short_name']) ? $this->address['address_components'][2]['short_name'] : NULL;
        }elseif(in_array('park', $this->types)){
            // park
            $City = isset($this->address['address_components'][3]['short_name']) ? $this->address['address_components'][3]['short_name'] : NULL;
        }elseif(in_array('point_of_interest', $this->types)){
            // commercial
            $City = isset($this->address['address_components'][2]['short_name']) ? $this->address['address_components'][2]['short_name'] : NULL;
        }

        return $City ?: NULL;
    }

    public function getCounty()
    {
        if($this->type == 'street_address'){
            // street
            $County = isset($this->address['address_components'][2]['long_name']) ? $this->address['address_components'][2]['long_name'] : NULL;
        }elseif($this->type == 'locality'){
            // city
            $County = isset($this->address['address_components'][1]['short_name']) ? $this->address['address_components'][1]['short_name'] : NULL;
        }elseif($this->type == 'postal_code'){
            // zip code
            $County = NULL;
        }elseif($this->type == 'administrative_area_level_2'){
            // county
            $County = isset($this->address['address_components'][0]['short_name']) ? $this->address['address_components'][0]['short_name'] : NULL;
        }elseif($this->type == 'administrative_area_level_1'){
            // state
            $County = NULL;
        }elseif($this->type == 'country'){
            // country
            $County = NULL;
        }elseif($this->type == 'route'){
            // route
            $County = isset($this->address['address_components'][2]['short_name']) ? $this->address['address_components'][2]['short_name'] : NULL;
        }elseif(in_array('premise', $this->types)){
            // neighborhood
            $County = isset($this->address['address_components'][2]['short_name']) ? $this->address['address_components'][2]['short_name'] : NULL;
        }elseif(in_array('school', $this->types)){
            // school
            $County = isset($this->address['address_components'][3]['short_name']) ? $this->address['address_components'][3]['short_name'] : NULL;
        }elseif(in_array('park', $this->types)){
            // park
            $County = isset($this->address['address_components'][4]['short_name']) ? $this->address['address_components'][4]['short_name'] : NULL;
        }elseif(in_array('point_of_interest', $this->types)){
            // commercial
            $County = isset($this->address['address_components'][3]['short_name']) ? $this->address['address_components'][3]['short_name'] : NULL;
        }

        return $County ?: NULL;
    }

    public function getStateLong()
    {
        if($this->type == 'street_address'){
            // street
            $StateLong = isset($this->address['address_components'][4]['long_name']) ? $this->address['address_components'][4]['long_name'] : NULL;
        }elseif($this->type == 'locality'){
            // city
            $StateLong = isset($this->address['address_components'][2]['long_name']) ? $this->address['address_components'][2]['long_name'] : NULL;
        }elseif($this->type == 'postal_code'){
            // zip code
            $StateLong = isset($this->address['address_components'][3]['long_name']) ? $this->address['address_components'][3]['long_name'] : NULL;
        }elseif($this->type == 'administrative_area_level_2'){
            // county
            $StateLong = isset($this->address['address_components'][1]['long_name']) ? $this->address['address_components'][1]['long_name'] : NULL;
        }elseif($this->type == 'administrative_area_level_1'){
            // state
            $StateLong = isset($this->address['address_components'][0]['long_name']) ? $this->address['address_components'][0]['long_name'] : NULL;
        }elseif($this->type == 'country'){
            // country
            $StateLong = NULL;
        }elseif($this->type == 'route'){
            // route
            $StateLong = isset($this->address['address_components'][3]['long_name']) ? $this->address['address_components'][3]['long_name'] : NULL;
        }elseif(in_array('premise', $this->types)){
            // neighborhood
            $StateLong = isset($this->address['address_components'][3]['long_name']) ? $this->address['address_components'][3]['long_name'] : NULL;
        }elseif(in_array('school', $this->types)){
            // school
            $StateLong = isset($this->address['address_components'][4]['long_name']) ? $this->address['address_components'][4]['long_name'] : NULL;
        }elseif(in_array('park', $this->types)){
            // park
            $StateLong = isset($this->address['address_components'][5]['long_name']) ? $this->address['address_components'][5]['long_name'] : NULL;
        }elseif(in_array('point_of_interest', $this->types)){
            // commercial
            $StateLong = isset($this->address['address_components'][4]['long_name']) ? $this->address['address_components'][4]['long_name'] : NULL;
        }

        return $StateLong ?: NULL;
    }

    public function getStateShort()
    {

        if($this->type == 'street_address'){
            // street
            $StateShort = isset($this->address['address_components'][4]['short_name']) ? $this->address['address_components'][4]['short_name'] : NULL;
        }elseif($this->type == 'locality'){
            // city
            $StateShort = isset($this->address['address_components'][2]['short_name']) ? $this->address['address_components'][2]['short_name'] : NULL;
        }elseif($this->type == 'postal_code'){
            // zip code
            $StateShort = isset($this->address['address_components'][3]['short_name']) ? $this->address['address_components'][3]['short_name'] : NULL;
        }elseif($this->type == 'administrative_area_level_2'){
            // county
            $StateShort = isset($this->address['address_components'][1]['short_name']) ? $this->address['address_components'][1]['short_name'] : NULL;
        }elseif($this->type == 'administrative_area_level_1'){
            // state
            $StateShort = isset($this->address['address_components'][0]['short_name']) ? $this->address['address_components'][0]['short_name'] : NULL;
        }elseif($this->type == 'country'){
            // country
            $StateShort = NULL;
        }elseif($this->type == 'route'){
            // route
            $StateShort = isset($this->address['address_components'][3]['short_name']) ? $this->address['address_components'][3]['short_name'] : NULL;
        }elseif(in_array('premise', $this->types)){
            // neighborhood
            $StateShort = isset($this->address['address_components'][3]['short_name']) ? $this->address['address_components'][3]['short_name'] : NULL;
        }elseif(in_array('school', $this->types)){
            // school
            $StateShort = isset($this->address['address_components'][4]['short_name']) ? $this->address['address_components'][4]['short_name'] : NULL;
        }elseif(in_array('park', $this->types)){
            // park
            $StateShort = isset($this->address['address_components'][5]['short_name']) ? $this->address['address_components'][5]['short_name'] : NULL;
        }elseif(in_array('point_of_interest', $this->types)){
            // commercial
            $StateShort = isset($this->address['address_components'][4]['short_name']) ? $this->address['address_components'][4]['short_name'] : NULL;
        }

        return $StateShort ?: NULL;
    }

    public function getZip()
    {

        if($this->type == 'street_address'){
            // street
            $Zip = isset($this->address['address_components'][6]['short_name']) ? $this->address['address_components'][6]['short_name'] : NULL;
        }elseif($this->type == 'locality'){
            // city
            $Zip = isset($this->address['address_components'][4]['short_name']) ? $this->address['address_components'][4]['short_name'] : NULL;
        }elseif($this->type == 'postal_code'){
            // zip code
            $Zip = isset($this->address['address_components'][0]['short_name']) ? $this->address['address_components'][0]['short_name'] : NULL;
        }elseif($this->type == 'administrative_area_level_2'){
            // county
            $Zip = NULL;
        }elseif($this->type == 'administrative_area_level_1'){
            // state
            $Zip = NULL;
        }elseif($this->type == 'country'){
            // country
            $Zip = NULL;
        }elseif($this->type == 'route'){
            // route
            $Zip = isset($this->address['address_components'][5]['short_name']) ? $this->address['address_components'][5]['short_name'] : NULL;
        }elseif(in_array('premise', $this->types)){
            // neighborhood
            $Zip = isset($this->address['address_components'][5]['short_name']) ? $this->address['address_components'][5]['short_name'] : NULL;
        }elseif(in_array('school', $this->types)){
            // school
            $Zip = isset($this->address['address_components'][6]['short_name']) ? $this->address['address_components'][6]['short_name'] : NULL;
        }elseif(in_array('park', $this->types)){
            // park
            $Zip = isset($this->address['address_components'][7]['short_name']) ? $this->address['address_components'][7]['short_name'] : NULL;
        }elseif(in_array('point_of_interest', $this->types)){
            // commercial
            $Zip = isset($this->address['address_components'][6]['short_name']) ? $this->address['address_components'][6]['short_name'] : NULL;
        }

        return $Zip ?: NULL;
    }

    public function getLatitude()
    {

        $Latitude = isset($this->address['geometry']['location']['lat']) ? $this->address['geometry']['location']['lat'] : NULL;

        return $Latitude ?: NULL;
    }

    public function getLongitude()
    {

        $Longitude = isset($this->address['geometry']['location']['lng']) ? $this->address['geometry']['location']['lng'] : NULL;

        return $Longitude ?: NULL;
    }

    public function getLatitudes()
    {
        $Latitudes = [];

        if(isset($this->types) && $this->type == 'street_address'){
            // street
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat']+.03 : NULL
            ];
        }elseif(isset($this->types) && $this->type == 'locality'){
            // city
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat'] : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat'] : NULL
            ];
        }elseif(isset($this->types) && $this->type == 'postal_code'){
            // zip code
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat'] : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat'] : NULL
            ];
        }elseif(isset($this->types) && $this->type == 'administrative_area_level_2'){
            // county
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat'] : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat'] : NULL
            ];
        }elseif(isset($this->types) && $this->type == 'administrative_area_level_1'){
            // state
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat'] : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat'] : NULL
            ];
        }elseif(isset($this->types) && $this->type == 'country'){
            // country
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat'] : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat'] : NULL
            ];
        }elseif(isset($this->types) && $this->type == 'route'){
            // route
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat']+.03 : NULL
            ];
        }elseif(isset($this->types) && in_array('premise', $this->types)){
            // neighborhood
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat']+.03 : NULL
            ];
        }elseif(isset($this->types) && in_array('school', $this->types)){
            // school
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat']+.03 : NULL
            ];
        }elseif(isset($this->types) && in_array('park', $this->types)){
            // park
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat']+.03 : NULL
            ];
        }elseif(isset($this->types) && in_array('point_of_interest', $this->types)){
            // commercial
            $Latitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lat']) ? $this->address['geometry']['viewport']['southwest']['lat']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lat']) ? $this->address['geometry']['viewport']['northeast']['lat']+.03 : NULL
            ];
        }

        return $Latitudes ?: NULL;
    }

    public function getLongitudes()
    {
        $Longitudes = [];
        
        if($this->type == 'street_address'){
            // street
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng']+.03 : NULL
            ];
        }elseif($this->type == 'locality'){
            // city
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng'] : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng'] : NULL
            ];
        }elseif($this->type == 'postal_code'){
            // zip code
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng'] : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng'] : NULL
            ];
        }elseif($this->type == 'administrative_area_level_2'){
            // county
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng'] : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng'] : NULL
            ];
        }elseif($this->type == 'administrative_area_level_1'){
            // state
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng'] : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng'] : NULL
            ];
        }elseif($this->type == 'country'){
            // country
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng'] : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng'] : NULL
            ];
        }elseif($this->type == 'route'){
            // route
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng']+.03 : NULL
            ];
        }elseif(in_array('premise', $this->types)){
            // neighborhood
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng']+.03 : NULL
            ];
        }elseif(in_array('school', $this->types)){
            // school
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng']+.03 : NULL
            ];
        }elseif(in_array('park', $this->types)){
            // park
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng']+.03 : NULL
            ];
        }elseif(in_array('point_of_interest', $this->types)){
            // commercial
            $Longitudes = [
                isset($this->address['geometry']['viewport']['southwest']['lng']) ? $this->address['geometry']['viewport']['southwest']['lng']-.03 : NULL,
                isset($this->address['geometry']['viewport']['northeast']['lng']) ? $this->address['geometry']['viewport']['northeast']['lng']+.03 : NULL
            ];
        }

        return $Longitudes ?: NULL;
    }

}