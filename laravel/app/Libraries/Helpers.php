<?php

function formatPrice($price)
{
    setlocale(LC_MONETARY, 'en_US.UTF-8');
    return money_format('%.2n', $price);
}

function str_unslug($string){
    $string = str_replace('_', ' ', $string);
    $string = str_replace('-', ' ', $string);
    return ucwords($string);
}

function formatPhone($phone) {

  // note: making sure we have something
  if(!isset($phone{9})) { return ''; }

  // note: make sure it's url decoded first
  $phone = urldecode($phone);
  
  // note: strip out everything but numbers
  $phoneNumbers = preg_replace("/[^0-9]/", "", $phone);
  
  if(strlen($phoneNumbers) > 11){
    return $phone;
  }

  $phone = $phoneNumbers; 
  
  // note: see if it is a clean number, with no 1 (area codes all start with 2)
  if (strlen($phone) == 10 AND $phone[0] != 1) {
    $phone = '+1'.$phone;
  }
  // note: see if it is a clean number, with 1
  if (strlen($phone) == 11 AND $phone[0] == 1) {
    $phone = '+'.$phone;
  }

  if(preg_match('/^\+?\d?(\d{3})(\d{3})(\d{4})$/', trim($phone),  $matches)) {
      $phone = "(" . $matches[1] . ') ' .$matches[2] . '-' . $matches[3];
      return $phone;
  }

  return $phone;
}
