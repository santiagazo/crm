<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function users()
    {
        return $this->hasMany('Milne\User');
    }

    public function images()
    {
        return $this->morphMany('Milne\Media', 'mediable')
        ->where('type', 'location_images');
    }
}
