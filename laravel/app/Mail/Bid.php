<?php

namespace Milne\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Bid extends Mailable
{
    use Queueable, SerializesModels;

    public $bid;
    public $subject;
    public $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bid, $subject, $content)
    {
        $this->bid = $bid;
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@milnebrothersroofing.com')
        ->subject($this->subject)
                ->view('emails.bid')
                ->attach(public_path('/pdfs/bids/'.$this->bid->id.'/bid.pdf'), [
                    'as' => 'bid.pdf',
                    'mime' => 'application/pdf',
                ]);
    }
}
