<?php

namespace Milne\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Estimate extends Mailable
{
    use Queueable, SerializesModels;

    public $person;
    public $fullAddress;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($person, $fullAddress = NULL)
    {
        $this->person = $person;
        $this->fullAddress = $fullAddress;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $subject = count($this->person->estimates) ? 
        $this->person->name." | ".$this->person->address." ".$this->person->city." ".$this->person->zip." | ".str_limit(str_unslug($this->person->estimates->last()->type), 20) :
        $this->person->name." | ".$this->person->address." ".$this->person->city." ".$this->person->zip;

        $this->from('donotreply@milnebrothersroofing.com')
            ->subject($subject)
            ->view('emails.estimate');
    }
}
