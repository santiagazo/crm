<?php

namespace Milne\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Milne\Job as JobCollection;

class Job extends Mailable
{
    use Queueable, SerializesModels;

    public $job;
    public $subject;
    public $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($job, $subject, $content)
    {
        $this->job = $job;
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@milnebrothersroofing.com')
                ->subject($this->subject)
                ->view('emails.job')
                ->attach(public_path('pdfs/jobs/'.$this->job->id.'/job.pdf'), [
                    'as' => 'job.pdf',
                    'mime' => 'application/pdf',
                ]);
    }
}
