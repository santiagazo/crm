<?php

namespace Milne\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Supplier extends Mailable
{
    use Queueable, SerializesModels;

    public $job;
    public $subject;
    public $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($job, $subject, $content)
    {
        $this->job = $job;
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('milnebrothersroofing@gmail.com')
                ->subject($this->subject)
                ->view('emails.supplier')
                ->attach(public_path('/pdfs/suppliers/'.$this->job->id.'/material_list.pdf'), [
                        'as' => 'material_list.pdf',
                        'mime' => 'application/pdf',
                    ]);
    }
}
