<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class MaterialList extends Model
{
    protected $fillable = [
        'job_id',
        'user_id',
        'status_id'
    ];

    public function materialable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('Milne\User');
    }

    public function status()
    {
        return $this->belongsTo('Milne\Status');
    }

    public function categories()
    {
        return $this->belongsToMany('Milne\Category')
        ->withPivot('product_id', 'sort_id', 'qty', 'category_price', 'supplier_price', 'color', 'is_house_product')
        ->orderBy('pivot_sort_id','asc');
    }

    public function template()
    {
        return $this->hasOne('Milne\Template');
    }

    public function company()
    {
        return $this->belongsTo('Milne\Company');
    }
}
