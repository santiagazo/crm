<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
        'job_id',
        'user_id',
        'content'
    ];

    public function noteable()
    {
        return $this->morphTo();
    }

    public function user()
    {
    	return $this->belongsTo('Milne\User');
    }

}
