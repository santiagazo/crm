<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use DB;

class Person extends Model
{
    use HasRoles;

    protected $guard_name = 'web';

    protected $fillable = [
        'name',
        'address',
        'city',
        'state',
        'zip',
        'phone',
        'email',
    ];

    public function company()
    {
        return $this->belongsTo('Milne\Company');
    }

    public function jobs()
    {
        return $this->hasMany('Milne\Job');
    }

    public function estimates()
    {
        return $this->hasMany('Milne\Estimate');
    }

    public function supervisors()
    {
        return $this->hasRole('Supervisor');
    }
    
    public function notes()
    {
        return $this->morphMany('Milne\Note', 'noteable');
    }

}
