<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Pitch extends Model
{
    protected $fillable = [
        'job_id',
        'type_id'
    ];

    public function pitchable()
    {
        return $this->morphTo();
    }

    public function type()
    {
        return $this->belongsTo('Milne\Type');
    }
}
