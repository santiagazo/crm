<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'company_id',
        'category_id',
        'type_id',
        'name',
        'unit',
        'description',
        'colors',
        'house_price',
        'supplier_price'
    ];

    public function company()
    {
        return $this->belongsTo('Milne\Company');
    }

    public function type()
    {
        return $this->belongsTo('Milne\Type');
    }

    public function material_lists_through_pivot()
    {
        return $this->belongsToMany('Milne\MaterialList', 'category_material_list')
        ->withPivot('material_list_id', 'sort_id', 'qty', 'category_price', 'supplier_price', 'color', 'is_house_product')
        ->orderBy('pivot_sort_id','asc');
    }

    public function category()
    {
        return $this->belongsTo('Milne\Category');
    }
}
