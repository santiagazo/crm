<?php

namespace Milne\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Milne\User;
use Milne\Location;
use Milne\Media;
use Auth;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layouts/site/header'], function($view){
            $location = Location::where('is_main', 1)->first();
            $mainLogo = Media::where('mediable_type', 'Milne\Location')
                ->where('mediable_id', '1')
                ->where('title', 'main-logo')
                ->first();

            $view
            ->with('location', $location)
            ->with('mainLogo', $mainLogo);
        });

        view()->composer(['layouts/site/footer'], function($view) {
            $location = Location::where('is_main', 1)->first();
            $mainLogo = Media::where('mediable_type', 'Milne\Location')
                ->where('mediable_id', '1')
                ->where('title', 'main-logo')
                ->first();

            $view
            ->with('location', $location)
            ->with('mainLogo', $mainLogo);
        });

        view()->composer(['layouts/auth/header'], function($view){
            $authUser = Auth::user();
            $location = Location::where('is_main', 1)->first();
            $mainLogo = Media::where('mediable_type', 'Milne\Location')
                ->where('mediable_id', '1')
                ->where('title', 'main-logo')
                ->first();

            $view
            ->with('authUser', $authUser)
            ->with('location', $location)
            ->with('mainLogo', $mainLogo);
        });

        view()->composer(['layouts/auth/footer'], function($view) {
            $location = Location::where('is_main', 1)->first();
            $mainLogo = Media::where('mediable_type', 'Milne\Location')
                ->where('mediable_id', '1')
                ->where('title', 'main-logo')
                ->first();

            $view
            ->with('location', $location)
            ->with('mainLogo', $mainLogo);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
