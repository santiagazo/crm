<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class RegistrationReferral extends Model
{
    protected $fillable = [
        'code',
        'role'
    ];

}
