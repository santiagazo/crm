<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    protected $fillable = [
        'name',
        'slug'
    ];

    public function jobs()
    {
        return $this->hasMany('Milne\Job');
    }

    public function materialLists()
    {
        return $this->hasMany('Milne\MaterialList');
    }
}
