<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    public function material_list()
    {
        return $this->belongsTo('Milne\MaterialList');
    }

}
