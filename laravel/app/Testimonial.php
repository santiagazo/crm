<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{

    public function person()
    {
    	return $this->belongsTo('Milne\Person');
    }

}
