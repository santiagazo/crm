<?php

namespace Milne;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = [
        'category',
        'name',
        'slug'
    ];

    public function jobs()
    {
        return $this->hasMany('Milne\Job');
    }

    public function products()
    {
        return $this->hasMany('Milne\Product');
    }

    public function companies()
    {
        return $this->hasMany('Milne\Company');
    }
}
