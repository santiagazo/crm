<?php

namespace Milne;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    protected $guard_name = 'web';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
        'city',
        'state',
        'zip',
        'phone',
        'avatar',
        'location_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function jobs()
    {
        return $this->hasMany('Milne\Job');
    }

    public function material_list()
    {
        return $this->hasMany('Milne\MaterialList');
    }

    public function testimonials()
    {
        return $this->hasMany('Milne\Testimonial');
    }

    public function location()
    {
        return $this->belongsTo('Milne\Location');
    }

    public function media()
    {
        return $this->morphMany('App\Media', 'mediable');
    }
}
