<?php

return collect([
    'box' => 'Box',
    'bucket' => 'Bucket',
    'bundle' => 'Bundle',
    'each' => 'Each',
    'foot' => 'Foot',
    'gallon' => 'Gallon',
    'hour' => 'Hour',
    'inch' => 'Inch',
    'labor/square' => 'Labor/Square',
    'linear_foot' => 'Linear Foot',
    'person' => 'Person',
    'piece' => 'Piece',
    'roll' => 'Roll',
    'sheet' => 'Sheet',
    'square' => 'Square',
    'square_foot' => 'Square Foot',
    'square_yard' => 'Square Yard',
    'ton' => 'Ton',
    'yard' => 'Yard'
]);