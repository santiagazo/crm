<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->delete();

        DB::table('companies')->insert([
            'id' => 1,
            'type_id' => 6,
            'name' => 'New Company',
            'slug' => 'new_company'
        ]);

        DB::table('companies')->insert([
            'id' => 2,
            'type_id' => 7,
            'name' => 'Milne Roofing',
            'slug' => 'milne_roofing'
        ]);

        DB::table('companies')->insert([
            'id' => 3,
            'type_id' => 6,
            'name' => 'Ashcroft Homes',
            'slug' => 'ashcroft_homes'
        ]);

        DB::table('companies')->insert([
            'id' => 4,
            'type_id' => 7,
            'name' => 'Roofing Supply Warehouse',
            'slug' => 'roofing_supply_warehouse'
        ]);

        DB::table('companies')->insert([
            'id' => 5,
            'type_id' => 7,
            'name' => 'Tar Paper Co.',
            'slug' => 'tar_paper_co'
        ]);

        DB::table('companies')->insert([
            'id' => 6,
            'type_id' => 6,
            'name' => 'Magleby Construction',
            'slug' => 'magleby_construction'
        ]);

        DB::table('companies')->insert([
            'id' => 7,
            'type_id' => 6,
            'name' => 'Homes-R-us',
            'slug' => 'homes-r-us'
        ]);

        DB::table('companies')->insert([
            'id' => 8,
            'type_id' => 7,
            'name' => 'Local Refuge Station',
            'slug' => 'local_refuge_station'
        ]);
    }
}
