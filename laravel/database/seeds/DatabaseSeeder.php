<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocationsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ModelHasRolesTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(PeopleTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(PersonRoleTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
    }
}
