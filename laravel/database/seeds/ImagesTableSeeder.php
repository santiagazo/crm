<?php

use Illuminate\Database\Seeder;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->delete();

        DB::table('images')->insert([
            'imageable_id' => 1,
         	'imageable_type' => "Milne\Location",
            'sort_id' => 1,
            'author' => '',
            'title' => 'main-logo',
            'caption' => 'main-logo',
            'image' => 'images/logos/milne-main-logo.png',
            'color' => NULL,
            'position' => NULL,
            'repeat' => NULL,
            'scroll_type' => NULL,
            'cover' => NULL,
            'video' => NULL,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

    }
}
