<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->delete();

        DB::table('locations')->insert([
            'id' => '1',
            'is_main' => '1',
            'name' => 'Milne Brothers Roofing',
            'initials' => 'MBR',
            'address1' => '5712 South 800 West',
            'address2' => '',
            'city' => 'Murray',
            'state' => 'UT',
            'zip' => '84123',
            'phone' => '(801) 268-4496',
            'fax' => '(801) 268-4496',
            'email' => 'mbr@milnebrothersroofing.com',
            'url' => 'http://www.milnebrothersroofing.com',
            'short_url' => 'milnebrothersroofing.com'
        ]);
    }
}
