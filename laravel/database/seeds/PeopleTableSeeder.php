<?php

use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->delete();

        DB::table('people')->insert([
            'id' => 1,
            'name' => 'New Person',
            'address' => '',
            'city' => '',
            'zip' => '',
            'phone' => '',
            'email' => '',
        ]);

        DB::table('people')->insert([
            'id' => 2,
            'name' => 'John Doe',
            'address' => '123 Somewhere St.',
            'city' => 'Anywhere',
            'zip' => 'UT',
            'phone' => '801-555-5555',
            'email' => 'jonDoe@example.com'
        ]);

        DB::table('people')->insert([
            'id' => 3,
            'name' => 'Jane Doe',
            'address' => '123 Somewhere St.',
            'city' => 'Anywhere',
            'zip' => 'UT',
            'phone' => '801-555-5555',
            'email' => 'janeDoe@example.com'
        ]);

        DB::table('people')->insert([
            'id' => 4,
            'company_id' => 1,
            'name' => 'Jessie James',
            'address' => '123 Somewhere St.',
            'city' => 'Anywhere',
            'zip' => 'UT',
            'phone' => '801-555-5555',
            'email' => 'jessieJames@example.com'
        ]);

        DB::table('people')->insert([
            'id' => 5,
            'company_id' => 1,
            'name' => 'Luke Skywalker',
            'address' => '123 Somewhere St.',
            'city' => 'Anywhere',
            'zip' => 'UT',
            'phone' => '801-555-5555',
            'email' => 'LukeSkywalker@example.com'
        ]);
    }
}
