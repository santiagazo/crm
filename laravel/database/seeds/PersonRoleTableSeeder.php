<?php

use Illuminate\Database\Seeder;

class PersonRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('person_role')->delete();

        DB::table('person_role')->insert([
            'role_id' => 3,
            'person_id' => 2,
        ]);
        DB::table('person_role')->insert([
            'role_id' => 4,
            'person_id' => 3,
        ]);
        DB::table('person_role')->insert([
            'role_id' => 5,
            'person_id' => 4,
        ]);
        DB::table('person_role')->insert([
            'role_id' => 6,
            'person_id' => 5,
        ]);
        DB::table('person_role')->insert([
            'role_id' => 6,
            'person_id' => 1,
        ]);
    }
}
