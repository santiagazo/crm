<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();

        DB::table('products')->insert([
            'id' => 1,
            'company_id' => 8,
            'type_id' => 5,
            'name' => 'Dumpster - Small',
            'house_price' => '50',
            'supplier_price' =>  '25',
            'unit' => 'Ton',
            'description' => 'Clean up and haul small dumpster/bed of waste to dump.',
            'color' => NULL
        ]);

        DB::table('products')->insert([
            'id' => 2,
            'company_id' => 8,
            'type_id' => 5,
            'name' => 'Dumpster - Medium',
            'house_price' => '100',
            'supplier_price' =>  '50',
            'unit' => 'Ton',
            'description' => 'Clean up and haul medium dumpster of waste to dump.',
            'color' => NULL
        ]);

        DB::table('products')->insert([
            'id' => 3,
            'company_id' => 8,
            'type_id' => 5,
            'name' => 'Dumpster - Large',
            'house_price' => '200',
            'supplier_price' =>  '100',
            'unit' => 'Ton',
            'description' => 'Clean up and haul large dumpster of waste to dump.',
            'color' => NULL
        ]);

        DB::table('products')->insert([
            'id' => 4,
            'company_id' => 2,
            'type_id' => 5,
            'name' => 'Tear Off',
            'house_price' => '800',
            'supplier_price' => '300',
            'unit' => 'Layer',
            'description' => 'Tear-off existing layer(s) of shingles.',
            'color' => NULL
        ]);

        DB::table('products')->insert([
            'id' => 5,
            'company_id' => 5,
            'type_id' => 5,
            'name' => 'Synthetic Felt',
            'house_price' => '800',
            'supplier_price' => '300',
            'unit' => 'Square',
            'description' => 'Felt (Synthetic) CMI SegureGrip 25',
            'color' => 'Black'
        ]);
    }
}
