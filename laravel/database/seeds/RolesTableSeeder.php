<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'Admin',
            'guard_name' => 'web',
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'Owner',
            'guard_name' => 'web',
        ]);
        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'Employee',
            'guard_name' => 'web',
        ]);
        DB::table('roles')->insert([
            'id' => 4,
            'name' => 'Client',
            'guard_name' => 'web',
        ]);
        DB::table('roles')->insert([
            'id' => 5,
            'name' => 'Supplier',
            'guard_name' => 'web',
        ]);
        DB::table('roles')->insert([
            'id' => 6,
            'name' => 'Supervisor',
            'guard_name' => 'web',
        ]);
    }
}
