<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->delete();

        DB::table('status')->insert([
            'id' => 1,
            'category' => 'job',
            'name' => 'Bid',
            'slug' => 'bid'
        ]);

        DB::table('status')->insert([
            'id' => 2,
            'category' => 'job',
            'name' => 'Pending',
            'slug' => 'pending'
        ]);

        DB::table('status')->insert([
            'id' => 3,
            'category' => 'job',
            'name' => 'Complete',
            'slug' => 'complete'
        ]);

        DB::table('status')->insert([
            'id' => 4,
            'category' => 'job',
            'name' => 'Invoiced',
            'slug' => 'invoiced'
        ]);

        DB::table('status')->insert([
            'id' => 5,
            'category' => 'job',
            'name' => 'Paid',
            'slug' => 'paid'
        ]);

        DB::table('status')->insert([
            'id' => 6,
            'category' => 'job',
            'name' => 'Canceled',
            'slug' => 'canceled'
        ]);

        DB::table('status')->insert([
            'id' => 7,
            'category' => 'order',
            'name' => 'Unordered',
            'slug' => 'unordered'
        ]);

        DB::table('status')->insert([
            'id' => 8,
            'category' => 'order',
            'name' => 'Ordered',
            'slug' => 'ordered'
        ]);

        DB::table('status')->insert([
            'id' => 9,
            'category' => 'order',
            'name' => 'Received',
            'slug' => 'received'
        ]);

        DB::table('status')->insert([
            'id' => 10,
            'category' => 'order',
            'name' => 'Canceled',
            'slug' => 'canceled'
        ]);
    }
}
