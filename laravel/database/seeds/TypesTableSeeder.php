<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->delete();

        DB::table('types')->insert([
            'id' => '1',
            'category' => 'job',
            'name' => 'Tear Off',
            'slug' => 'tear_off',
        ]);

        DB::table('types')->insert([
            'id' => '2',
            'category' => 'job',
            'name' => 'New Construction',
            'slug' => 'new_construction',
        ]);

        DB::table('types')->insert([
            'id' => '3',
            'category' => 'job',
            'name' => 'Repair',
            'slug' => 'repair',
        ]);

        DB::table('types')->insert([
            'id' => '4',
            'category' => 'taxable',
            'name' => 'Product',
            'slug' => 'product',
        ]);

        DB::table('types')->insert([
            'id' => '5',
            'category' => 'taxable',
            'name' => 'Service',
            'slug' => 'service',
        ]);

       DB::table('types')->insert([
            'id' => '6',
            'category' => 'company',
            'name' => 'Contractor',
            'slug' => 'Contractor',
        ]);

       DB::table('types')->insert([
            'id' => '7',
            'category' => 'company',
            'name' => 'Supplier',
            'slug' => 'supplier',
        ]);
    }
}
