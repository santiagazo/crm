<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
         	'id' => '1',
            'name' => 'Anthony Milne',
            'email' => 'mbr@milnebrothersroofing.com',
            'password' => bcrypt('password'),
            'remember_token' => 'tcX0KOAFgrVOO2ISkve3JkopOTrR8C4OyAtXrKFVU0QY18BekE7VVDnJoF59',
            'address' => '5712 South 800 West',
            'city' => 'Murray',
            'state' => 'UT',
            'zip' => '84123',
            'phone' => '(801) 268-4496',
            'avatar' => 'https://scontent-sjc2-1.xx.fbcdn.net/v/t1.0-1/p160x160/15178176_1144746565561239_2285090874727675556_n.jpg?oh=fb3f4caea008022d5b245e1da5a89101&oe=59123602',
            'location_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
         	'id' => '2',
            'name' => 'Jay Lara',
            'email' => 'jay@darkcherrysystems.com',
            'password' => bcrypt('greeneyes2'),
            'remember_token' => 'tcX0KOAFgrVOO2ISkve3JkopOTrR8C4OyAtXrKFVU0QY18BekE7VVDnJoR26',
            'address' => '106 S. 610 E.',
            'city' => 'American Fork',
            'state' => 'UT',
            'zip' => '84123',
            'phone' => '(480) 309-1250',
            'avatar' => 'guest/images/users/beaker.jpg',
            'location_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
