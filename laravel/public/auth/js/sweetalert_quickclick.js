/* Author Jay Lara | Dependancies are jquery and sweetalert */

$('body').on('click', '.confirm-link', function(e){
    e.preventDefault();
    console.log('here');
    var text = $(this).data('confirm-text');
    var href = $(this).attr('href');

    console.log(text);
    console.log(href);

    swal({
        title: "Are you sure?",
        text: text,
        type: "warning",
        animation: false,
        showCancelButton: true,
        confirmButtonColor: "#D9534F",
        confirmButtonText: "Proceed",
        closeOnConfirm: true
    },
    function(){
        location.href = href;
    });
});