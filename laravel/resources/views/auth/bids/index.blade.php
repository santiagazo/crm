@extends('layouts.auth.main')

@section('title')
Bids
@endsection

@section('css')
	<link rel="stylesheet" href="">

	<style>
		.form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
table .form-control {
            border: none;
        }
	</style>
@stop

@section('content')

    	<div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ url('admin/bids/create') }}" class="btn btn-sm btn-primary pull-right">Create Bid</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        {!! Form::open(['url' => 'admin/bids', 'method' => 'GET', 'id' => 'bidsForm']) !!}
                                            <th width="120px"></th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('user_name', $user_name, ['placeholder' => 'Project Manager Name', 'class' => 'form-control user-name']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('customer_name', $customer_name, ['placeholder' => "Customer's Name", 'class' => 'form-control customer-name']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('address', $address, ['placeholder' => 'Bid Address', 'class' => 'form-control bid-address']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('city', $city, ['placeholder' => 'Bid City', 'class' => 'form-control bid-city']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('subdivision', $subdivision, ['placeholder' => 'Subdivision', 'class' => 'form-control subdivision']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('lot', $lot, ['placeholder' => 'Lot', 'class' => 'form-control lot']) !!}
                                                </div>
                                            </th>

                                            <th width="125">
                                                <div class="form-group">
                                                    {!! Form::select('status', $statuses, $status, ['class' => 'form-control status_select2']) !!}
                                                </div>
                                            </th>

                                            <div style="display: none">

                                            {!! Form::submit('Submit') !!}
                                            </div>

                                        {!! Form::close() !!}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($bids))
                                        @foreach($bids as $bid)
                                            <tr>
                                                <td>
                                                    @if($bid->material_list)
                                                        @if($bid->status->slug == 'not_sent' && empty($bid->not_sent))
                                                            <a href="{{ url('admin/bids/show/'.$bid->id) }}" class="btn btn-danger btn-xs responsive-button" data-toggle="tooltip" title="Bid Not Sent - View Full Bid">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        @else
                                                            <a href="{{ url('admin/bids/show/'.$bid->id) }}" class="btn btn-info btn-xs responsive-button" data-toggle="tooltip" title="View Full Bid">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        @endif
                                                        <a href="{{ url('admin/bids/edit/'.$bid->id) }}" class="btn btn-success btn-xs responsive-button" data-toggle="tooltip" title="Edit Bid">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <a href="{{ url('admin/material_lists/edit/'.$bid->material_list->id.'/bid') }}" 
                                                            class="btn btn-success btn-xs responsive-button" 
                                                            data-toggle="tooltip" 
                                                            title="Edit Material List">
                                                            <i class="fa fa-list"></i>
                                                        </a>
                                                        @if($bid->accepted_at && $bid->status->slug == 'accepted')
                                                            <a href="{{ url('admin/jobs/create/'.$bid->id) }}" 
                                                                class="btn btn-default btn-xs responsive-button" 
                                                                data-toggle="tooltip" 
                                                                title="Create Job">
                                                                <i class="fa fa-home"></i>
                                                            </a>
                                                        @endif                                                 
                                                    @else
                                                        <a href="{{ url('admin/bids/edit/'.$bid->id) }}" class="btn btn-success btn-xs responsive-button" data-toggle="tooltip" title="Edit Bid">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <a href="{{ url('admin/material_lists/create/'.$bid->id.'/bid') }}" class="btn btn-default btn-xs responsive-button" data-toggle="tooltip" title="Create Material List">
                                                            <i class="fa fa-list"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $bid->user ? $bid->user->name : NULL }}
                                                </td>
                                                <td>
                                                    {{ $bid->person ? $bid->person->name : NULL }}
                                                </td>
                                                <td>
                                                    {{ $bid->address }}
                                                </td>
                                                <td>
                                                    {{ $bid->city }}
                                                </td>
                                                <td>
                                                    {{ $bid->subdivision }}
                                                </td>
                                                <td>
                                                    {{ $bid->lot }}
                                                </td>
                                                <td>
                                                    {{ $bid->status->name }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7">
                                                There are no bids built yet, click Create Bid to get one started.
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {!! $bids->links() !!}
                        </div>
                    </div>

                </div>
            </div>

    	</div>


@stop

@section('js')

	<script>

		$(function(){

            $('.status_select2').select2();

            $('body').on('change', '.status_select2', function(){
                $('#bidsForm').submit();
            });

			$('body').on('keyup', '.user-name, .customer-name, .bid-address, .bid-city, .subdivision, .lot' , function(){
				var input = $(this);
				window.timer=setTimeout(function(){ // setting the delay for each keypress
				 	timedAjaxRequest(input); //runs the ajax request
				},
				500);
				}).keydown(function(){
					clearTimeout(window.timer); //clears out the timeout
			});


			function timedAjaxRequest(input){
				var user_name = $('.user-name').val();
				var customer_name = $('.customer-name').val();
				var bid_address = $('.bid-address').val();
				var bid_city = $('.bid-city').val();
                var subdivision = $('.subdivision').val();
                var lot = $('.lot').val();

				$.ajax({
					url: '{{ url('admin/bids') }}?fullname='+user_name+'&customer_name='+customer_name+'&address='+bid_address+'&city='+bid_city+'&subdivision='+subdivision+'&lot='+lot,
					type: 'GET',
					success: function (response) {
						var response = jQuery.parseJSON(response);
						$('tbody').html('');
						$.each( response.data, function( key, value ) {

                            if(value.subdivision){ var subdivision = value.subdivision; }else{ var subdivision = ""; }
                            if(value.lot){ var lot = value.lot; }else{ var lot = ""; }

							$('tbody').append('<tr>'+
								'<td>'+
									'<a href="{{ url('admin/bids/edit') }}/'+value.id+'" class="btn btn-primary responsive-button">'+
										'<i class="fa fa-pencil"></i>'+
									'</a>'+
								'</td>'+
								'<td>'+value.user.name+'</td>'+
								'<td>'+value.person.name+'</td>'+
								'<td>'+value.address+'</td>'+
                                '<td>'+value.city+'</td>'+
                                '<td>'+subdivision+'</td>'+
                                '<td>'+lot+'</td>'+
							'</tr>')
							$('.responsive-button.right').css({'margin-left': '4px'});
							$('ul.pagination').remove();
						});
					},
					error: function (xhr) {
						console.log(xhr);
					}
				});
			}

		});

	</script>

@stop
