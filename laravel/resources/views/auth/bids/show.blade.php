@extends('layouts.auth.main')

@section('title')
Show Bid
@endsection

@section('css')
    <link rel="stylesheet" href="{{ url('auth/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link href="{{ url('guest/plugins/lightgallery/dist/css/lightgallery.min.css') }}" rel="stylesheet">
    <style>
        .material_list-table{
            margin-bottom: 0px;
        }
        .id-column{
            width: 5%;
        }
        .product-column{
            width: 40%;
        }
        .qty-column{
            width: 10%;
        }
        .supplier-column{
            width: 10%;
        }
        .house-column{
            width: 10%;
        }
        td i.fa{
            margin: 10px;
        }
        .table-bordered>tbody>tr:last-child>td {
            border-bottom: 2px solid #323232;
        }
        .material_list-header{
            background-color: #3c8dbc;
            color: #fff;
        }
        .box-header>.box-tools.template {
            position: absolute;
            right: 10px;
            top: 10px;
        }
        .image-thumbnail{
            margin-bottom: 15px;
            cursor: pointer;
        }
        .table>tfoot>tr>td{
            height: 25px;
            padding: 3px 12px;
        }
        .house_subtotal,
        .supplier_subtotal,
        .house_sub_total,
        .supplier_sub_total,
        .house_total,
        .supplier_total,
        .house_tax,
        .supplier_tax,
        .supplier_sub_difference,
        .supplier_difference{
            text-align: right;
        }

        .house_total,
        .supplier_total,
        .supplier_difference,
        .tfooter-title.total-title{
            font-weight: 700;
        }
        .options{
            float: right;
            margin-top: 5px;
        }
        .w100{
            width:100px;
        }
    </style>
@stop

@section('content')


<section class="content">

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Person</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <h4>{{ $bid->person->name }}</h4>
                    <p>
                        <h5>Billing Address</h5>
                        @php
                            $address = $bid->person ? $bid->person->address : '';
                            $city = $bid->person ? $bid->person->city : '';
                            $zip = $bid->person ? $bid->person->zip: '';
                        @endphp
                        <a href="https://www.google.com/maps/search/?api=1&query={{ urlencode($address.' '.$city.' UT '.$zip) }}" target="about_blank">
                        {{ $bid->person->address }}<br/>
                        {{ $bid->person->city }} UT, {{ $bid->person->zip }}
                    </p>

                    <p>
                        <a href="mailto:{{ $bid->person->email }}">{{ $bid->person->email }}</a>  |  <a href="tel:{{ $bid->person->phone }}">{{ $bid->person->phone }}</a>
                    </p>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ url('admin/people/edit/'.$bid->person->id) }}" class="btn btn-primary btn-sm pull-right">Edit Person</a>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Bid</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>{{ $bid->purchase_order ? 'Purchase Material: '.$bid->purchase_order : 'Bid Id: '.$bid->id }}</h4>
                            <p>
                                <h5>Project Address</h5>
                                @php
                                    $bid_address = $bid->address ?: $bid->person->address;
                                    $bid_city = $bid->city ?: $bid->person->city;
                                    $bid_zip = $bid->zip ?: $bid->person->zip;
                                @endphp
                                <a href="https://www.google.com/maps/search/?api=1&query={{ urlencode($bid_address.' '.$bid_city.' UT '.$bid_zip) }}" target="about_blank">
                                    {{ $bid->address ?: $bid->person->address }}<br/>
                                    {{ $bid->city ?: $bid->person->city }} UT, {{ $bid->zip ?: $bid->person->zip }}
                                </a>
                            </p>
                            @if($bid->subdivision || $bid->lot)
                            <p>
                                {{ $bid->subdivision ? 'Subdivision: '.$bid->subdivision : NULL }}
                                {{ $bid->subdivision && $bid->lot ? ' | ' : NULL }}
                                {{ $bid->lot ? 'Lot: '.$bid->lot : NULL }}
                            </p>
                            @endif
                            <p>Bid Type: {{ str_unslug($bid->type->name) }}</p>
                        </div>
                        <div class="col-md-6">
                            <div class="text-right">{!! $bid->accepted_at ? '<strong>Started On: </strong>'.Carbon\Carbon::parse($bid->accepted_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $bid->ordered_at ? '<strong>Ordered On: </strong>'.Carbon\Carbon::parse($bid->ordered_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $bid->delivery_at ? '<strong>Delivery On: </strong>'.Carbon\Carbon::parse($bid->delivery_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $bid->start_at ? '<strong>Starts On: </strong>'.Carbon\Carbon::parse($bid->start_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $bid->end_at ? '<strong>Ends On: </strong>'.Carbon\Carbon::parse($bid->end_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $bid->invoiced_at ? '<strong>Invoiced On: </strong>'.Carbon\Carbon::parse($bid->invoiced_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $bid->completed_at ? '<strong>Completed On: </strong>'.Carbon\Carbon::parse($bid->completed_at)->format('l M. j, Y') : NULL !!}</div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ url('admin/bids/edit/'.$bid->id) }}" class="btn btn-primary btn-sm pull-right">Edit Bid</a>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $bid->accepted_at ? "Material" : "Bid" }}</h3>
                    <div class="box-tools template pull-right">
                        <strong>{{ $bid->material_list->template ? 'Template: '.$bid->material_list->template->name : 'No Associated Template' }}</strong>
                    </div>
                </div>
                <div class="box-body pb-0">
                    <div class="row">
                        <div class="col-md-12 material_list-holder">
                            <table class="table material_list-table table-bordered">
                                <tbody data-material_list-id='NULL' class="product-holder">
                                    <tr class="material_list-header">
                                        <th class="product-column">Product Category</th>
                                        <th class="qty-column w100"><span class="pull-right"> Qty</span></th>
                                        <th class="price-column w100"><span class="pull-right"> Price</span></th>
                                        <th class="sub-total-column w100"><span class="pull-right"> Sub-Total</span></th>
                                    </tr>
                                    @php
                                        $category_price = [];
                                    @endphp
                                    @foreach($bid->material_list->categories as $category)
                                        <tr class="product_line" data-category-id="">
                                            <td>
                                                {{ $category->name }}
                                            </td>
                                            <td>
                                                <span class="pull-right">{{ $category->pivot->qty }}</span>
                                            </td>
                                            <td>
                                                <span class="pull-right">{{ $category->pivot->category_price }}</span>
                                            </td>
                                            <td>
                                                <span class="pull-right">{{ $category->pivot->category_price*$category->pivot->qty }}</span>
                                            </td>
                                        </tr>

                                        @php
                                            $category_price[] = $category->pivot->category_price*$category->pivot->qty;
                                        @endphp

                                    @endforeach

                                    @php
                                        $category_price = (int) array_sum($category_price);
                                    @endphp
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3">Sub-Total</td>
                                        <td class="house_sub_total">{{ formatPrice($category_price) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Tax</td>
                                        <td class="house_tax">{{ $bid->tax->amount*100 }}%</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="tfooter-title total-title">Total</td>
                                        <td class="house_total">{{ formatPrice($category_price*$bid->tax->amount+$category_price) }}</td>
                                    </tr>
                                </tfoot>

                            </table>

                        </div>
                    </div>
                    <div class="options">
                        <ul class="list-inline">
                            <li><strong>Options: </strong></li>
                            <li class="checkbox">
                                <label>
                                    {{ Form::checkbox('is_pre_dry_in', true, $bid->is_pre_dry_in, ['disabled' => true] ) }} Pre Dry-In
                                </label>
                            </li>
                            <li class="checkbox">
                                <label>
                                    {{ Form::checkbox('is_six_nail', true, $bid->is_six_nail, ['disabled' => true] ) }} Six Nail
                                </label>
                            </li>
                            <li class="checkbox">
                                <label>
                                    {{ Form::checkbox('is_owner_tear_off', true, $bid->is_owner_tear_off, ['disabled' => true] ) }} Owner Tear-Off
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ url('admin/material_lists/edit/'.$bid->material_list->id.'/bid') }}" class="btn btn-primary btn-sm pull-right">Edit Bid Material List</a>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>


    @if(count($bid->notes))
        <div class="row">
            <div class="{{ $bid->estimate ? "col-md-6" : "col-md-12" }}">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Notes</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-footer box-comments">
                                    @foreach($bid->notes as $note)
                                        <div class="box-comment">
                                            <!-- User image -->
                                            <img class="img-circle img-sm" src="{{ url($note->user->avatar ?: 'guest/images/user_empty.png') }}" alt="User Image">

                                            <div class="comment-text">
                                                <span class="username">
                                                    {{ $note->user->name }}
                                                    <span class="text-muted pull-right">{{ Carbon\Carbon::parse($note->created_at)->format('F jS, Y g:ia') }}</span>
                                                </span><!-- /.username -->
                                                {{ $note->content }}
                                            </div>
                                            <!-- /.comment-text -->
                                        </div>
                                        <!-- /.box-comment -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/bids/edit/'.$bid->id) }}" class="btn btn-primary btn-sm pull-right">Add Note</a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            @if(count($bid->media))
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Images</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row" id="images_for_bid">
                                @forelse($bid->media as $image)
                                    <div class="col-sm-3 image-thumbnail" data-src="{{ url($image->url) }}">
                                        <img src="{{ url($image->url) }}" class="img-responsive">
                                    </div>
                                @empty
                                    <div class="col-md-12">
                                        <p>No images uploaded.</p>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            @endif
            @if($bid->estimate)
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Estimate Request</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="callout callout-default">
                                <div class="product-info">
                                    <span class="product-title">Estimate Request {{ $bid->estimate->is_urgent ? '- URGENT' : "" }}</span>
                                    <button class="btn btn-xs btn-default pull-right see-more-btn">See More</button>
                                    <div>
                                        Estimate Types:
                                        @forelse(explode(',', $bid->estimate->type) as $item)
                                            @if(!$loop->last)
                                                {{ str_unslug($item) }},
                                            @else
                                                {{ str_unslug($item) }}
                                            @endif
                                        @empty
                                            Declined to Specify
                                        @endforelse
	                                </div>
	                                <div class="see-more" style="display:none">
	                                    <p>
	                                        Roof Type: {{ str_unslug($bid->estimate->roof_type) }} / Roof Pitch: {{ str_unslug($bid->estimate->roof_pitch) }}
	                                    </p>
	                                    <p>{{ $bid->estimate->additional_information ? "Additional Information: ".$bid->estimate->additional_information : NULL }}</p>
	                                    <p>Source: {{ str_unslug($bid->estimate->advertisment) }} {{ $bid->estimate->referred_by ? " | ".str_unslug($bid->estimate->referred_by) : NULL }}</p>
	                                    <div class="row" id="images_for_estimate">
	                                        @forelse($bid->estimate->media as $image)
	                                            <div class="col-sm-3" data-src="{{ url($image->url) }}">
	                                                <img src="{{ url($image->url) }}" class="img-responsive">
	                                            </div>
	                                        @empty
	                                            <div class="col-md-12">
	                                                <p>No images uploaded.</p>
	                                            </div>
	                                        @endforelse
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- /.box-body -->
	                </div>
	            </div>
            @endif
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="form-group pull-right">
                <a href="{{ url('admin/pdf/bids/create/'.$bid->id) }}" class="btn {{ $bid->accepted_at ? "btn-default" : "btn-primary" }}">View Bid Document</a>
                <a href="{{ url('admin/jobs/create/'.$bid->id) }}" class="btn btn-primary">{{ $bid->accepted_at ? 'Create Job' : 'Accept & Create Job' }}</a>
            </div>
        </div>
    </div>
</section>


@endsection

@section('js')
    <script src="{{ url('auth/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ url('guest/plugins/lightgallery/dist/js/lightgallery-all.min.js') }}"></script>

    <script>

        $(function(){
            $('body').on('click', '.see-more-btn', function(){
                $(this).parent().find('.see-more').slideToggle();
                $(this).text() == "See More" ? $(this).text("See Less") : $(this).text("See More");
            });

            $('#images_for_bid, #images_for_estimate').lightGallery({
                gallery: true,
                download: false,
                onSliderLoad  : function (el) {
                  el.lightGallery();
                }
            });
        });

    </script>

@stop