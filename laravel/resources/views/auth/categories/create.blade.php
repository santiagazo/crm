@extends('layouts.auth.main')

@section('title')
Create Product
@endsection

@section('css')

@stop

@section('content')


    {!! Form::open( ['url' => 'admin/categories'] ) !!}

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create a Product Category</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <!--Text form input-->
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('name', 'Name') !!}
                                    {!! Form::text('name', NULL, ['class' => 'form-control']) !!}
                                    <small>ie: High-Grade Shingles, Mid-Grade Shingles, Standard-Grade Shingles</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('price', 'Price') !!}
                                    {!! Form::text('price', NULL, ['class' => 'form-control']) !!}
                                    <small>Your in-house price to be charged to the customer and bid with.</small>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class='form-group'>
                                    {!! Form::label('unit', 'Unit') !!}
                                    {!! Form::select('unit', $units, NULL, ['class' => 'form-control unit_select2']) !!}
                                    <small>Should be the same as the products in this category.</small>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class='form-group'>
                                    {!! Form::label('is_house_product', 'In House Product') !!}
                                    {!! Form::select('is_house_product', [0 => "No", 1 => "Yes"], 0, ['class' => 'form-control is_house_product_select_2']) !!}
                                    <small>Default to category price.</small>
                                </div>
                            </div>

                            <div class="col-md-12 colors-holder" style="display:none">
                                <div class='form-group'>
                                    {!! Form::label('colors[]', 'In House Product Colors') !!}
                                    {!! Form::select('colors[]', [], NULL, ['class' => 'form-control colors_select2', 'multiple' => true]) !!}
                                    <small>Since it's a in house product list potential colors.</small>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                </div>
            </div>

        </section>

    {!! Form::close() !!}


@stop

@section('js')

    <script>

        $(function(){
            $('.unit_select2').select2();
            $('.is_house_product_select_2').select2({
                minimumResultsForSearch: -1
            });
            $('.colors_select2').select2({
                tags: true
            });

            $('body').on('change', '.is_house_product_select_2', function(){
                $('.colors-holder').slideToggle();
            });
        });

    </script>

@stop
