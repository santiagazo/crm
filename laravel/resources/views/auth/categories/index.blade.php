@extends('layouts.auth.main')

@section('title')
Categories
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
table .form-control {
            border: none;
        }
    </style>
@stop

@section('content')


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ url('admin/categories/create') }}" class="btn btn-primary pull-right">Create Category</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        {!! Form::open(['url' => 'admin/categories', 'method' => 'GET']) !!}
                                            <th width="75px"></th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('name', $name, ['placeholder' => 'Name', 'class' => 'form-control name']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('price', $price, ['placeholder' => 'Price', 'class' => 'form-control price']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('unit', $unit, ['placeholder' => 'Unit', 'class' => 'form-control unit']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::select('is_house_product', ['' => 'All', 0 => 'No', 1 => 'Yes'], NULL, ['placeholder' => 'In House Product', 'class' => 'form-control is_house_product']) !!}
                                                </div>
                                            </th>
                                            <input type="submit" style="visibility: hidden; position: absolute;" />
                                        {!! Form::close() !!}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($categories))
                                        @foreach($categories as $category)
                                            <tr>
                                                <td>
                                                    <a href="{{ url('admin/categories/edit/'.$category->id) }}" class="btn btn-primary responsive-button">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    {{ $category->name }}
                                                </td>
                                                <td>
                                                    {{ $category->price }}
                                                </td>
                                                <td>
                                                    {{ $category->unit }}
                                                </td>
                                                <td>
                                                    {{ $category->is_house_product ? 'Yes' : 'No' }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3">
                                                There are no categories built yet, click Create Product Category to get one started.
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {!! $categories->links() !!}
                        </div>
                    </div>

                </div>
            </div>

        </section>


@stop

@section('js')

    <script>

        $(function(){

            $('body').on('keyup', '.name, .price, .unit', function(){
                window.timer=setTimeout(function(){ // setting the delay for each keypress
                    timedAjaxRequest(); //runs the ajax request
                },
                500);
                }).keydown(function(){
                    clearTimeout(window.timer); //clears out the timeout
            });

            $('body').on('change', '.is_house_product', function(){
                timedAjaxRequest(); //runs the ajax request
            });

            function timedAjaxRequest(){
                var name = $('.name').val();
                var price = $('.price').val();
                var unit = $('.unit').val();
                var is_house_product = $('.is_house_product').val();

                $.ajax({
                    url: '{{ url('admin/categories') }}?name='+name+'&price='+price+'&unit='+unit+'&is_house_product='+is_house_product,
                    type: 'GET',
                    success: function (response) {
                        var categories = jQuery.parseJSON(response);
                        $('tbody').html('');
                        $.each( categories.data, function( key, category ) {
                            var is_house_product = category.is_house_product ? 'Yes' : 'No';
                            $('tbody').append('<tr>'+
                                '<td>'+
                                    '<a href="{{ url('admin/categories/edit') }}/'+category.id+'" class="btn btn-primary responsive-button">'+
                                        '<i class="fa fa-pencil"></i>'+
                                    '</a>'+
                                '</td>'+
                                '<td>'+category.name+'</td>'+
                                '<td>'+category.price+'</td>'+
                                '<td>'+category.unit+'</td>'+
                                '<td>'+is_house_product+'</td>'+
                            '</tr>')
                            $('.responsive-button.right').css({'margin-left': '4px'});
                            $('ul.pagination').remove();
                        });

                    },
                    error: function (xhr) {
                        console.log(xhr);
                    }
                });
            }

        });

    </script>

@stop
