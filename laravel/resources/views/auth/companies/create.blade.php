@extends('layouts.auth.main')

@section('title')
Create Product
@endsection

@section('css')

@stop

@section('content')


    {!! Form::open( ['url' => 'admin/companies'] ) !!}

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create a Company</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <!--Text form input-->
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('name', 'Company Name') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <!--Select form input-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('type_id', 'Company Type') !!}
                                    {!! Form::select('type_id', $types, NULL, ['class' => 'form-control company_type']) !!}
                                </div>
                            </div>

                            <!--Text form input-->
                            <div class="col-md-4 tax_amount">
                                <div class='form-group'>
                                    {!! Form::label('tax_amount', 'Tax Amount') !!}
                                    {!! Form::text('tax_amount', NULL, ['class' => 'form-control']) !!}
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit('Save', ['class' => 'btn btn-default pull-right']) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                </div>
            </div>

        </section>

    {!! Form::close() !!}


@stop

@section('js')

    <script>

        $(function(){

            if($(this).val() == 7){
                $('.tax_amount').fadeIn().find('input').attr('disabled', false);
            }else{
                $('.tax_amount').hide().find('input').attr('disabled', true);
            }

            $('body').on('change', '.company_type', function(){
                if($(this).val() == 7){
                    $('.tax_amount').fadeIn().find('input').attr('disabled', false);
                }else{
                    $('.tax_amount').fadeOut().find('input').attr('disabled', true);
                }
            });

        });

    </script>

@stop
