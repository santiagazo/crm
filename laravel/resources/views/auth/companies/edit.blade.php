@extends('layouts.auth.main')

@section('title')
Edit Company
@endsection

@section('css')

@stop

@section('content')


    {!! Form::open( ['url' => 'admin/companies/'.$company->id] ) !!}

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit a Company</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <!--Text form input-->
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('name', 'Company Name') !!}
                                    {!! Form::text('name', $company->name, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <!--Select form input-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('type_id', 'Company Type') !!}
                                    {!! Form::select('type_id', $types, $company->type_id, ['class' => 'form-control company_type']) !!}
                                </div>
                            </div>

                            <!--Text form input-->
                            <div class="col-md-4 tax_amount" style="{{ $company->type_id != 7 ? 'display:none' : "" }}">
                                <div class='form-group'>
                                    {!! Form::label('tax_amount', 'Tax Amount') !!}
                                    {!! Form::text('tax_amount', $company->tax_amount, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-12">
                                <a href="{{ url('admin/companies/delete/'.$company->id) }}" class="btn btn-danger confirm-link" data-confirm-text="Are you sure you want to delete this company?">Delete</a>
                                {!! Form::submit('Update', ['class' => 'btn btn-default pull-right']) !!}
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                </div>
            </div>

        </section>

    {!! Form::close() !!}


@stop

@section('js')

    <script>

        $(function(){
            $('.company-select2').select2();

            $('body').on('change', '.company_type', function(){
                if($(this).val() == 7){
                    $('.tax_amount').fadeIn().find('input').attr('disabled', false);
                }else{
                    $('.tax_amount').fadeOut().find('input').attr('disabled', true);
                }
            });
        });

    </script>

@stop

