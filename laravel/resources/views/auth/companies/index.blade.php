@extends('layouts.auth.main')

@section('title')
Companies
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
table .form-control {
            border: none;
        }
    </style>
@stop

@section('content')
        <section class="content">
        
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ url('admin/companies/create') }}" class="btn btn-primary pull-right">Create Company</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        {!! Form::open(['url' => 'admin/companies', 'method' => 'GET']) !!}
                                            <th></th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('name', $name, ['placeholder' => 'Name', 'class' => 'form-control name']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('type_id', $type_id, ['placeholder' => 'Company', 'class' => 'form-control type_id']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('tax_amount', $tax_amount, ['placeholder' => 'Tax Amount', 'class' => 'form-control tax_amount']) !!}
                                                </div>
                                            </th>
                                            <input type="submit" style="visibility: hidden; position: absolute;" />
                                        {!! Form::close() !!}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($companies))
                                        @foreach($companies as $company)
                                            <tr>
                                                <td>
                                                    <a href="{{ url('admin/companies/edit/'.$company->id) }}" class="btn btn-primary responsive-button">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    {{ $company->name }}
                                                </td>
                                                <td>
                                                    {{ $company->type->name }}
                                                </td>
                                                <td>
                                                    {{ $company->tax_amount }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3">
                                                There are no companies built yet, click Create Company to get one started.
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {!! $companies->links() !!}
                        </div>
                    </div>

                </div>
            </div>


    </section>

@stop

@section('js')

    <script>

        $(function(){


            $('body').on('keyup', '.name, .type_id, .tax_amount', function(){
                var input = $(this);
                window.timer=setTimeout(function(){ // setting the delay for each keypress
                    timedAjaxRequest(input); //runs the ajax request
                },
                500);
                }).keydown(function(){
                    clearTimeout(window.timer); //clears out the timeout
            });

            function timedAjaxRequest(input){
                var type_id = $('.type_id').val();
                var name = $('.name').val();
                var tax_amount = $('.tax_amount').val();

                $.ajax({
                    url: '{{ url('admin/companies') }}?name='+name+'&type_id='+type_id+'&tax_amount='+tax_amount,
                    type: 'GET',
                    success: function (response) {
                        var companies = jQuery.parseJSON(response);
                        console.log(companies.data);
                        $('tbody').html('');
                        $.each( companies.data, function( key, company ) {
                            $('tbody').append('<tr>'+
                                '<td>'+
                                    '<a href="{{ url('admin/companies/edit') }}/'+company.id+'" class="btn btn-primary responsive-button">'+
                                        '<i class="fa fa-pencil"></i>'+
                                    '</a>'+
                                '</td>'+
                                '<td>'+company.name+'</td>'+
                                '<td>'+company.type.name+'</td>'+
                                '<td>'+company.tax_amount+'</td>'+
                            '</tr>')
                            $('.responsive-button.right').css({'margin-left': '4px'});
                            $('ul.pagination').remove();
                        });

                    },
                    error: function (xhr) {
                        console.log(xhr);
                    }
                });
            }

        });

    </script>

@stop
