@extends('layouts.auth.main')

@section('title')
{{ $company->name }} Products
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
        table .form-control {
            border: none;
        }
        .red-transition {
            background: #dd4b39;
            -webkit-transition:background 1s;
            -moz-transition:background 1s;
            -o-transition:background 1s;
            transition:background 1s
        }
        .grey-transition {
            background: #d2d6de;
            -webkit-transition:background 1s;
            -moz-transition:background 1s;
            -o-transition:background 1s;
            transition:background 1s
        }
        .green-transition {
            background: #398439;
            -webkit-transition:background 1s;
            -moz-transition:background 1s;
            -o-transition:background 1s;
            transition:background 1s
        }
        .white-transision {
            background: #fff;
            -webkit-transition:background 1s;
            -moz-transition:background 1s;
            -o-transition:background 1s;
            transition:background 1s
        }

    </style>
@stop

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary pull-right save_company_products">Save</button>
                                <a href="{{ url('admin/products/create') }}" class="btn btn-default pull-right">Create Product</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Type</th>
                                    <th>Supplier Price</th>
                                    <th>Unit</th>
                                    <th>Description</th>
                                    <th>Colors</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($company->products))
                                    {!! Form::open(['url' => 'admin/products', 'method' => 'GET', 'id' => 'products-form']) !!}
                                        @foreach($company->products as $product)
                                            <tr class="product_row" data-id="{{ $product->id }}" data-last="{{ $loop->last ? "true" : "false" }}">
                                                <td>
                                                    <div class="form-group">
                                                        {!! Form::text('name', $product->name, ['placeholder' => 'Name', 'class' => 'form-control name']) !!}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        {!! Form::select('category_id', $categories, $product->category_id, ['placeholder' => 'Category', 'class' => 'form-control category_id']) !!}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        {!! Form::select('type_id', $types, $product->type_id, ['placeholder' => 'Type', 'class' => 'form-control type_id']) !!}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        {!! Form::text('supplier_price', $product->supplier_price, ['placeholder' => "Supplier Price", 'class' => 'form-control supplier-price']) !!}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        {!! Form::select('unit', $units, $product->unit, ['placeholder' => 'Unit', 'class' => 'form-control unit']) !!}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        {!! Form::text('description', $product->description, ['placeholder' => 'Description', 'class' => 'form-control description']) !!}
                                                    </div>
                                                </td>
                                                <td width="200px">
                                                    <div class="form-group">
                                                        {!! Form::select('colors[]', 
                                                            $product->colors ? array_combine(array_unique(explode(',', $product->colors)), array_unique(explode(',', $product->colors))) : [],
                                                            explode(',', $product->colors), ['placeholder' => 'Colors', 'class' => 'form-control colorsSelect', 'multiple' => true ]) !!}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    {!! Form::close() !!}
                                @else
                                    <tr>
                                        <td colspan="3">
                                            There are no products for this company built yet, click Create Products to get one started.
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button class="btn btn-primary pull-right save_company_products">Save</button>
                    </div>
                </div>

            </div>
        </div>
    </section>
@stop

@section('js')

    <script>

        $(function(){
            $('.colorsSelect').select2({
                tags: true
            });

            var save_complete = false;

            $('body').on('click', '.save_company_products', function(){
                products = [];
                $('.save_company_products').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Saving...');
                $.each($('.product_row'), function(i, row){
                    $(row).addClass('saving')
                        .removeClass('red-transition')
                        .removeClass('green-transition')
                        .removeClass('white-transision')
                        .addClass('grey-transition');
                    var id = $(row).data('id');
                    var name = $(row).find('.name').val();
                    var category_id = $(row).find('.category_id').val();
                    var type_id = $(row).find('.type_id').val();
                    var supplier_price = $(row).find('.supplier-price').val();
                    var unit = $(row).find('.unit').val();
                    var description = $(row).find('.description').val();
                    var colors = $(row).find('.colorsSelect').val();
                    var last_row = $(row).data('last');

                    // console.log('id: '+id, 'name: '+name, 'category_id: '+category_id, 'type_id: '+type_id, 'supplier_price: '+supplier_price, 'unit: '+unit, 'description: '+description, 'colors: '+colors);

                    $.ajax({
                        url: "{{ url('admin/company/products/'.$company->id) }}",
                        type: 'POST',
                        data: {
                                id: id,
                                name: name,
                                company_id: {{ $company->id }},
                                category_id: category_id,
                                type_id: type_id,
                                supplier_price: supplier_price,
                                unit: unit,
                                description: description,
                                colors: colors
                        },
                        success: function (response) {
                            console.log(response);
                            $(row).removeClass('saving').removeClass('grey-transition').addClass('green-transition');
                        },
                        error: function (xhr) {
                            if(xhr.status == 422){
                                $(row).removeClass('grey-transition').addClass('red-transition');
                                var errors = $.parseJSON(xhr.responseText);
                                var messages = '';
                                $.each(errors, function(key, errorMessage){
                                    $.each(errorMessage, function(index, message){
                                        messages += '<li>'+message+'</li>';
                                    });
                                });
                                console.log(messages);
                                jsFlash('error', messages, false);
                            }else{
                                console.log(xhr);
                            }
                        }
                    });
                    if(last_row){
                        save_complete = last_row;
                    }
                }); 
                
                if(save_complete){
                    setTimeout(function() {
                        $('.save_company_products').text('Save');
                        $('tr.product_row:not(.saving)').removeClass('green-transition').addClass('white-transision');

                        if($('tr.product_row.saving').length == 0){
                            jsFlash('success', '<li>All products saved successfully.</li>', false);
                        }
                    }, 1000);
                }

            });

            $('.unit').select2();
            $('.category_id').select2();
            $('.type_id').select2();
        });

    </script>

@stop
