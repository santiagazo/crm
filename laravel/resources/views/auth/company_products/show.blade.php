@extends('layouts.auth.main')

@section('title')
{{ $company->name }} Products
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
table .form-control {
            border: none;
        }
    </style>
@stop

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ url('admin/products/create') }}" class="btn btn-primary pull-right">Create Product</a>
                                <a href="{{ url('admin/company/products/edit/'.$company->id) }}" class="btn btn-primary pull-right">Edit All Products</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Type</th>
                                    <th>Supplier Price</th>
                                    <th>Unit</th>
                                    <th>Description</th>
                                    <th>Colors</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($company->products))
                                    {!! Form::open(['url' => 'admin/products', 'method' => 'GET', 'id' => 'products-form']) !!}
                                        @foreach($company->products as $product)
                                            <tr>
                                                <td>
                                                    {{ $product->name }}
                                                </td>
                                                <td>
                                                    {{ $product->category->name }} 
                                                </td>
                                                <td>
                                                    {{ $product->type->name }}
                                                </td>
                                                <td>
                                                    {{ $product->supplier_price }}
                                                </td>
                                                <td>
                                                    {{ $product->unit }}
                                                </td>
                                                <td>
                                                    {{ $product->description }}
                                                </td>
                                                <td width="200px">
                                                    @foreach(explode(',', $product->colors) as $color)
                                                        <span class="label label-default">{{ $color }}</span>
                                                    @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                    {!! Form::close() !!}
                                @else
                                    <tr>
                                        <td colspan="3">
                                            There are no products for this company built yet, click Create Products to get one started.
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </section>
@stop

@section('js')

    <script>

        $(function(){
            $('.colorsSelect').select2({
                tags: true
            });

            $('.category_id').select2();
            $('.type_id').select2();
        });

    </script>

@stop
