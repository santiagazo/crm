@extends('layouts.auth.main')

@section('title')
Create Estimate Request
@endsection

@section('css')
<link rel="stylesheet" href="{{ url('plugins/dropzone/dist/dropzone.css') }}">
<link href="{{ url('guest/plugins/lightgallery/dist/css/lightgallery.min.css') }}" rel="stylesheet">
@stop

@section('content')

    <section class="content">
        <div class="box box-primary">
            <div class="box-body">
                {!! Form::open( ['url' => 'admin/estimates'] ) !!}
                    <div class="row">
                        @if($person)
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label(NULL, 'Person') !!}
                                    {!! Form::text(NULL, $person->name, ['class' => 'form-control', 'disabled' => true]) !!}
                                    {!! Form::hidden('person_id', $person->id) !!}
                                </div>
                            </div>
                        @else
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('person_id', 'Person') !!}
                                    {!! Form::select('person_id', [], NULL, ['class' => 'form-control person_select']) !!}
                                </div>
                            </div>
                        @endif
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('request_type[]', 'Request Type') !!} <small>(Select all that apply)</small>
                                {!! Form::select('request_type[]', [
                                    'tear_off' => 'Tear Off',
                                    'repair' => 'Repair',
                                    're_roof' => 'Re-Roof (Redo)',
                                    'new_construction' => 'New Construction',
                                    'warranty' => 'Warranty',
                                    'roof_inspection' => 'Roof Inspection',
                                    'flat_roofing_system' => 'Flat Roofing System',
                                    'new_gutters' => 'New Gutters',
                                    'swamp_removal' => 'Swamp Removal',
                                    'tie_in_addition' => 'Tie-In Addition',
                                    'other_estimate' => 'Other'
                                ], NULL, ['multiple' => true, 'class' => 'form-control request_type_select'] ) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('roof_type', "Roof Type") !!}
                                {!! Form::select('roof_type', [
                                        "asphalt" => "Asphalt",
                                        "membrane" => "Membrane",
                                        "metal" => "Metal",
                                        "tar_gravel" => "Tar & Gravel",
                                        "tile" => "Tile",
                                        "dont_know" => "Don't Know"
                                    ], 'asphalt', ['class' => 'roof_type_select']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row other_text" style="display: none;">
                        <div class="col-md-12">
                            <div class='form-group'>
                                {!! Form::label('other_text', 'Other') !!}
                                {!! Form::text('other_text', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('roof_pitch', "Roof Pitch") !!}
                                {!! Form::select('roof_pitch', [
                                    "flat" => "Practically Flat (membrane/gravel)",
                                    "easily_walkable" => "Easily walkable (2/12 to 6/12)",
                                    "bearly_walkable" =>  "Barely walkable (7/12 to 8/12)",
                                    "steep" => "Steep (9/12 and greater)",
                                    "dont_know" => "Don’t Know"
                                ], "easily_walkable", ['class' => 'roof_pitch_select']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('urgency', "Urgency") !!}
                                {!! Form::select('urgency', [
                                    "not_urgent" => "Not Urgent",
                                    "urgent" => "Urgent"
                                ], "not_urgent", ['class' => 'urgency_select']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('advertisment', "How did you find us?") !!}
                                {!! Form::select('advertisment', [
                                    "internet" => "Internet Search",
                                    "referral" => "Referral",
                                    "advertisment" => "Advertisment"
                                ], "internet", ['class' => 'advertisment_select']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row referred_by" style="display: none;">
                        <div class="col-md-12">
                            <div class='form-group'>
                                {!! Form::label('referred_by', 'Referred By') !!}
                                {!! Form::text('referred_by', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    
                    

                    <div class="row">
                        <div class="col-md-12">
                            <small><strong>Billing Address</strong></small>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-0">
                                <label>Address</label>
                                {!! Form::text('address', $person ? $person->address : NULL, ['class' => 'form-control project_same_as_billing', 'disabled' => true]) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-0">
                                <label>City</label>
                                {!! Form::text('city', $person ? $person->city : NULL, ['class' => 'form-control project_same_as_billing', 'disabled' => true]) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mb-0">
                                <label>Zip</label>
                                {!! Form::text('zip', $person ? $person->zip : NULL, ['class' => 'form-control project_same_as_billing', 'disabled' => true]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-0">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('project_same_as_billing', null, true, ['id' => 'project_same_as_billing']) !!}
                                        Project address is the same as Billing
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="project_same_holder" style="display: none;">
                            <div class="col-md-12">
                                <small><strong>Project Address</strong></small>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-0">
                                    <label>Address</label>
                                    {!! Form::text('project_address', NULL, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-0">
                                    <label>City</label>
                                    {!! Form::select('project_city', 
                                        array_merge(['' => 'Select City'], $taxed_cities->toArray()), NULL, 
                                        ['class' => 'form-control city_select2']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-0">
                                    <label>Zip</label>
                                    {!! Form::text('project_zip', NULL, ['class' => 'form-control', 'pattern' => "\d*"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Additional Information</label>
                                        {!! Form::textarea('additional_information', null, ['class' => 'form-control textarea-autoheight', 'rows' => 3]) !!}
                                        <label>Roof conditions, best ways to contact, severity of leak, etc.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary pull-right']) !!}
                    </div>
                        
                        
                    </div>
                {!! Form::close() !!}
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>



    


@stop

@section('js')
    <script src="{{ url('plugins/dropzone/dist/dropzone.js') }}"></script>
    <script src="{{ url('guest/plugins/lightgallery/dist/js/lightgallery-all.min.js') }}"></script>
    <script>

        $(function(){
            $('.request_type_select').select2();
            var city_select = $('.city_select2').select2({ placeholder: "Select City" });
            $('.roof_type_select').select2({ minimumResultsForSearch: -1 });
            $('.roof_pitch_select').select2({ minimumResultsForSearch: -1});
            $('.urgency_select').select2({ minimumResultsForSearch: -1});
            $('.advertisment_select').select2({ minimumResultsForSearch: -1});

            if({{ $person ? 'true' : 'false' }}){
                $('#person_id').attr('disabled', true);
            }else{
                $('#person_id').select2({
                    placeholder: 'Enter a Person',
                    minimumInputLength: 0,
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    ajax: {
                        url: "{{ url('/admin/people') }}",
                        dataType: 'json',
                        delay: 0,
                        data: function (params) {
                          return {
                            q: params.term, // search term
                            page: params.page,
                          }
                        },
                        processResults: function (data, params) {
                          params.page = params.page || 1;
                          return {
                            results: data.data,
                            pagination: {
                              more: (params.page * 30) < data.total_count
                            }
                          };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) { return markup; },
                    allowClear: false,
                    templateResult: formatResult,
                    templateSelection: formatSelection
                });

                function formatResult (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else if(result.company_id){
                        var $result = $('<span><strong>'+result.company.name+'</strong> | '+result.name+' | '+result.email+'</span>');
                    }else{
                        var $result = $('<span>'+result.name+' | '+result.email+'</span>');
                    }

                    return $result;
                };

                function formatSelection (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else{
                        var $result = $('<span>'+result.name+' | '+result.email+'</span>');
                        $('[name=address]').val(result.address);
                        $('[name=city]').val(result.city);
                        $('[name=zip]').val(result.zip);

                        if($('#project_same_as_billing:checked')){
                            $('[name=project_address]').val(result.address);
                            city_select.val(str_slug(result.city).toLowerCase()).trigger('change');
                            $('[name=project_zip]').val(result.zip);
                        }
                    }

                    return $result;
                };

            }

            // Tabbing ---------------------------------
                $('body').on('select2:close', 'select', function () {
                        $(this).focus();
                    }
                );
            // Tabbing ---------------------------------


            function str_unslug(str) {
                var frags = str.split('_');

                for (i=0; i<frags.length; i++) {
                    frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
                }

                return frags.join(' ');
            }

            function str_slug(text)
            {
                return text
                    .toLowerCase()
                    .replace(/ /g,'_')
                    .replace(/[^\w-]+/g,'');
            }

            var select2FocusCount = 0;
            $('.roof_type_select').next('.select2').find('.select2-selection').one('focus', select2Focus).on('blur', function (e) {
                if(select2FocusCount >= 1){
                    $(this).closest('.col-md-6').next().find('input').focus(); // focus the next element when the select2 is closed
                    select2FocusCount = 0;
                }
                select2FocusCount++
            });

            $('.roof_pitch_select').next('.select2').find('.select2-selection').one('focus', select2Focus).on('blur', function (e) {
                if(select2FocusCount >= 1){
                    $(this).closest('.col-md-4').next().find('input').focus(); // focus the next element when the select2 is closed
                    select2FocusCount = 0;
                }
                select2FocusCount++
            });

            $('.city_select2').next('.select2').find('.select2-selection').one('focus', select2Focus).on('blur', function (e) {
                if(select2FocusCount >= 1){
                    $(this).closest('.col-md-4').next().find('input').focus(); // focus the next element when the select2 is closed
                    select2FocusCount = 0;
                }
                select2FocusCount++
            });

            function select2Focus() {
                $(this).closest('.select2').prev('select').select2('open');
            }

            $('body').on('change', '.request_type_select', function(){
                var select_has_other = false;
                $.each($(this).find('option:selected'), function(i, option){
                    console.log($(option).val());
                    if($(option).val() == 'other_estimate'){
                        select_has_other = true;
                    }                
                });

                if(select_has_other){
                    $('.other_text').fadeIn();
                }else{
                    $('.other_text').fadeOut();
                    $('.other_text').val('');
                }
            });

            function populateProjectAddress(){
                if($('#project_same_as_billing:checked')){
                    $('[name=project_address]').val($('[name=address]').val());
                    city_select.val(str_slug($('[name=city]').val())).trigger('change');
                    $('[name=project_zip]').val($('[name=zip]').val());
                }
            }
            populateProjectAddress();

            $('body').on('change', '.project_same_as_billing', function(){
                populateProjectAddress()
            });

            $('body').on('change', '#project_same_as_billing', function(){
                $('.project_same_holder').slideToggle();
                if($('#project_same_as_billing').not(':checked').length){
                    $('[name=project_address]').val('');
                    $('[name=project_city]').val('');
                    $('[name=project_zip]').val('');
                }else{
                    populateProjectAddress()
                }
            });

            $('body').on('change', '.advertisment_select', function(){
                if($(this).val() == 'referral'){
                    $('.referred_by').fadeIn();
                }else{
                    $('.referred_by').fadeOut();
                }
            });

        });

    </script>

@stop

