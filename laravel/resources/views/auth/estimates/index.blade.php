@extends('layouts.auth.main')

@section('title')
Estimate Requests
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
        table .form-control {
            border: none;
        }
    </style>
@stop

@section('content')


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ url('admin/estimates/create') }}" class="btn btn-primary pull-right">Create Estimate</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        {!! Form::open(['url' => 'admin/estimates', 'method' => 'GET', 'id' => 'estimates-form']) !!}
                                            <th></th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('name', $name, ['placeholder' => 'Name', 'class' => 'form-control name']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('address', $address, ['placeholder' => 'Address', 'class' => 'form-control address']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('city', $city, ['placeholder' => "City", 'class' => 'form-control city']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('phone', $phone, ['placeholder' => 'Phone', 'class' => 'form-control phone']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('email', $email, ['placeholder' => 'Email', 'class' => 'form-control email']) !!}
                                                </div>
                                            </th>
                                            {{-- <th>
                                                <div class="form-group">
                                                    {!! Form::text('roof_type', $roof_type, ['placeholder' => 'Roof Type', 'class' => 'form-control roof_type']) !!}
                                                </div>
                                            </th> --}}
                                            <th width="125">
                                                <div class="form-group">
                                                    {!! Form::select('status', $statuses, $status, ['class' => 'form-control status_select2']) !!}
                                                </div>
                                            </th>

                                            <input type="submit" style="visibility: hidden; position: absolute;" />

                                        {!! Form::close() !!}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($estimates))
                                        @foreach($estimates as $estimate)
                                            <tr>
                                                <td width="75px">
                                                    <a href="{{ url('admin/estimates/edit/'.$estimate->id) }}" class="btn btn-primary btn-xs responsive-button" data-toggle="tooltip" title="Edit Estimate">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    {{-- <a href="{{ url('admin/estimates/show/'.$estimate->id) }}" class="btn btn-info btn-xs responsive-button" data-toggle="tooltip" title="View Estimate Request">
                                                        <i class="fa fa-eye"></i>
                                                    </a> --}}
                                                    @if($estimate->bid)
                                                        <a href="{{ url('admin/bids/edit/'.$estimate->bid->id) }}" class="btn btn-success btn-xs responsive-button" data-toggle="tooltip" title="Edit Bid">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @else
                                                        <a href="{{ url('admin/bids/create/'.$estimate->person->id.'/'.$estimate->id) }}" class="btn btn-default btn-xs responsive-button" data-toggle="tooltip" title="Create Bid">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @endif
                                                    
                                                </td>
                                                <td>
                                                    {{ $estimate->person->name }}
                                                </td>
                                                <td>
                                                    {{ $estimate->person->address }}
                                                </td>
                                                <td>
                                                    {{ $estimate->person->city }}
                                                </td>
                                                <td>
                                                    {{ $estimate->person->phone }}
                                                </td>
                                                <td>
                                                    {{ $estimate->person->email }}
                                                </td>
                                                {{-- <td>
                                                    {{ str_unslug($estimate->roof_type) }}
                                                </td> --}}
                                                <td>
                                                    {{ $estimate->status->name }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="9">
                                                There are no estimates requests in those parameters.
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {!! $estimates->links() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>


@stop

@section('js')

    <script>

        $(function(){
            $('.status_select2').select2();

            $('body').on('change', '.status_select2', function(){
                $('#estimates-form').submit();
            });

            $('body').on('keyup', '.name, .address, .city, .phone, .email, .roof_type', function(){
                var input = $(this);
                window.timer=setTimeout(function(){ // setting the delay for each keypress
                    timedAjaxRequest(input); //runs the ajax request
                },
                500);
                }).keydown(function(){
                    clearTimeout(window.timer); //clears out the timeout
            });

            function timedAjaxRequest(input){
                var company_id = $('.company_id').val();
                var name = $('.name').val();
                var address = $('.address').val();
                var city = $('.city').val();
                var phone = $('.phone').val();
                // var roof_type = $('.roof_type').val();

                $.ajax({
                    url: '{{ url('admin/estimates') }}?name='+name+'&address='+address+'&city='+city+'&phone='+phone, //+'&roof_type='+roof_type
                    type: 'GET',
                    success: function (response) {
                        var people = jQuery.parseJSON(response);
                        $('tbody').html('');
                        // persons_roof_type = "";
                        $.each( people.data, function( key, person ) {
                            // $.each( person.estimates, function(key, estimate){
                            //     persons_roof_type = estimate.roof_type; // get the last estimate's roof_type
                            // });
                            $('tbody').append('<tr>'+
                                '<td>'+
                                    '<a href="{{ url('admin/estimates/edit') }}/'+person.id+'" class="btn btn-primary responsive-button">'+
                                        '<i class="fa fa-edit"></i>'+
                                    '</a>'+
                                '</td>'+
                                '<td>'+person.name+'</td>'+
                                '<td>'+person.address+'</td>'+
                                '<td>'+person.city+'</td>'+
                                '<td>'+person.phone+'</td>'+
                                '<td>'+person.email+'</td>'+
                                // '<td>'+persons_roof_type+'</td>'+
                            '</tr>')
                            $('.responsive-button.right').css({'margin-left': '4px'});
                            $('ul.pagination').remove();
                        });

                    },
                    error: function (xhr) {
                        console.log(xhr);
                    }
                });
            }

        });

    </script>

@stop
