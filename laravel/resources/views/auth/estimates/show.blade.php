@extends('layouts.auth.main')

@section('title')
Show Estimate Request
@endsection

@section('css')
<link rel="stylesheet" href="{{ url('plugins/dropzone/dist/dropzone.css') }}">
<link href="{{ url('guest/plugins/lightgallery/dist/css/lightgallery.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Estimate Request</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p>Status: {{ $estimate->status->name }}</p>
                        <p>Submitted: {{ Carbon\Carbon::parse($estimate->created_at)->toFormattedDateString() }}</p>
                        <div class="{{ $estimate->is_urgent ? "" : "product-description" }} clearfix">
                            Estimate Types:
                            {{ str_unslug(implode(', ', explode(',', $estimate->type))) }}
                        </div>
                        <div class="see-more">
                            {{-- <p>
                                Roof Type: {{ str_unslug($estimate->roof_type) }} / Roof Pitch: {{ str_unslug($estimate->roof_pitch) }}
                            </p> --}}
                            <p>
                                <h5>Project Address</h5>
                                @php
                                    $address = $estimate->project_address ?: $person->address;
                                    $city = $estimate->project_city ?: $person->city;
                                    $zip = $estimate->project_zip ?: $person->zip;
                                @endphp
                                <a href="https://www.google.com/maps/search/?api=1&query={{ urlencode($address.' '.$city.' UT '.$zip) }}" target="about_blank">
                                    {{ $address }}<br/>
                                    {{ $city }} UT, {{ $zip }}
                                </a>
                            </p>
                            <p>{{ $estimate->additional_information ? "Additional Information: ".$estimate->additional_information : NULL }}</p>
                            <p>Source: {{ str_unslug($estimate->advertisment) }} {{ $estimate->referred_by ? " | ".str_unslug($estimate->referred_by) : NULL }}</p>
                            <div class="row images-holder" id="images_for_estimate_{{ $estimate->id }}">
                                @forelse($estimate->media->where('user_id', '=', NULL)  as $image)
                                    <div class="col-sm-3" data-src="{{ url($image->url) }}">
                                        <img src="{{ url($image->url) }}" class="img-responsive">
                                    </div>
                                @empty
                                    <div class="col-md-12">
                                        <p>No images uploaded.</p>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ url('admin/bids/create/'.$estimate->person_id.'/'.$estimate->id) }}" class="btn btn-primary btn-sm pull-right">Create Bid</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Person</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p>Name: {{ $estimate->person->name }}</p>
                        <p>Address: 
                            <a href="https://www.google.com/maps/search/?api=1&query={{ urlencode($estimate->person->address.' '.$estimate->person->city.' '.$estimate->person->zip) }}">
                                {{ $estimate->person->address }} {{ $estimate->person->city }}, UT {{ $estimate->person->zip }}
                            </a>
                        </p>
                        <p>Phone: <a href="tel:{{ $estimate->person->phone }}">{{ $estimate->person->phone }}</a></p>
                        <p>Email: <a href="mailto:{{ $estimate->person->email }}">{{ $estimate->person->email }}</a></p>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ url('admin/people/show/'.$estimate->person->id) }}" class="btn btn-sm btn-info pull-right">View Person</a>
                        <a href="{{ url('admin/people/edit/'.$estimate->person->id) }}" class="btn btn-sm btn-primary pull-right">Edit Person</a>
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add an Estimate Note</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @if(count($estimate->notes))
                            <div class="box-footer box-comments">
                                @foreach($estimate->notes as $note)
                                    <div class="box-comment">
                                        <!-- User image -->
                                        <img class="img-circle img-sm" src="{{ url($note->user->avatar ?: 'guest/images/user_empty.png') }}" alt="User Image">

                                        <div class="comment-text">
                                            <span class="username">
                                                {{ $note->user->name }}
                                                <span class="text-muted pull-right">{{ Carbon\Carbon::parse($note->created_at)->format('F jS, Y g:ia') }}</span>
                                            </span><!-- /.username -->
                                            {{ $note->content }}
                                        </div>
                                        <!-- /.comment-text -->
                                    </div>
                                    <!-- /.box-comment -->
                                @endforeach
                            </div>
                        @endif
                        {!! Form::open( ['url' => 'admin/notes', 'id'=>'notes_form'] ) !!}
                            
                            {!! Form::hidden('id', $estimate->id) !!}
                            {!! Form::hidden('type', 'estimate') !!}

                            <div class="form-group">
                                {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => "2"]) !!}
                            </div>
                        
                            {!! Form::submit('Add', ['class' => 'btn btn-primary pull-right']) !!}
                        
                        {!! Form::close() !!}
                        
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ count($estimate->media->where('user_id', '!=', NULL)) ? "Internal" : "Add" }} Images</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row images-holder" id="images_for_job_{{ $estimate->id }}">
                            @forelse($estimate->media->where('user_id', '!=', NULL) as $image)
                                <div class="col-sm-2 image-holder" data-src="{{ url($image->url) }}">
                                    <img src="{{ url($image->url) }}" class="img-responsive" data-id="{{ $image->id }}">
                                    <btn class="btn btn-default btn-xs delete-image"><i class="fa fa-minus" data-toggle="tooltip" title="Delete Image"></i></btn>
                                </div>
                            @empty
                            @endforelse
                        </div>
                        <div id="images_bucket" class="dropzone"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>

@stop

@section('js')
    <script src="{{ url('plugins/dropzone/dist/dropzone.js') }}"></script>
    <script src="{{ url('guest/plugins/lightgallery/dist/js/lightgallery-all.min.js') }}"></script>
    <script>

        $(function(){
            $('.status_select2').select2();
            $('.type_select2').select2();

            $('.images-holder').lightGallery({
                gallery: true,
                download: false,
                onSliderLoad  : function (el) {
                  el.lightGallery();
                }
            });

            // DropZone Start ------------------------------------------
                Dropzone.autoDiscover = false;

                function initDropzone(){

                    var imageDropzone = new Dropzone("#images_bucket", {
                        url: "{{ url('admin/media/'.$estimate->id) }}",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        params: {
                            id: {{ $estimate->id }},
                            type: 'estimate'
                        },
                        addRemoveLinks: false,
                        uploadMultiple: false,
                        maxFilesize: 5, //MB
                        dictDefaultMessage: "Drag and Drop or Click Here to Upload up to 20 Images.",
                    });
                    imageDropzone.on("uploadprogress", function(file) {
                        // console.log(file);
                    });
                    imageDropzone.on("sending", function(file) {
                        // console.log(file);
                    });
                    imageDropzone.on("success", function(file, response){
                        // console.log('success');
                        // console.log(response);
                        // console.log(file);
                        jsFlash('success', 'Your image ('+response.title+') has been uploaded successfully.');
                    });
                    imageDropzone.on("error", function(file, response){
                        console.log('error');
                        // console.log(response.message);
                        // console.log(file);
                        // console.log(file.size);

                        var messages = "";

                        if(file.size >= 2000000){
                            messages += "<li>Please make sure your image size is under 2MB</li>";
                        }

                        if(response.message){
                             messages += "<li>"+response.message+"</li>";
                        }

                        if(response.file){
                            $.each(response.file, function(index, error){
                                messages += '<li>'+error+' ('+file.name+')</li>';
                            });
                        }

                        jsFlash('error', messages)
                    });

                }

                initDropzone();

                // Media Delete Start ----------------------------------

                $('body').on('click', '.delete-image', function(){
                    var image_holder = $(this).closest('.image-holder');
                    var image_id = $(image_holder).find('img').data('id');

                    swal({
                        title: "Are you sure?",
                        text: "Are you sure you want to delete this image?",
                        type: "warning",
                        animation: false,
                        showCancelButton: true,
                        confirmButtonColor: "#D9534F",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: true
                    },
                    function(){
                        $.ajax({
                            url: "{{ url('admin/media/delete?id=') }}"+image_id+"&type=estimate",
                            type: 'GET',
                            success: function (response) {
                                var response = $.parseJSON(response);
                                if (typeof response == "String") {
                                    swal(response);
                                } else {
                                    swal("Deleted!", "You image has been deleted successfully.", "success");
                                }
                                $(image_holder).remove();
                            },
                            error: function (xhr) {
                                if(xhr.code || xhr.status){
                                    var errors = $.parseJSON(xhr.responseText);
                                    var messages = '';
                                    $.each(errors, function(key, errorMessage){
                                        $.each(errorMessage, function(index, message){
                                            messages += '<li>'+message+'</li>';
                                        });
                                    });
                                    console.log(messages);
                                    jsFlash('error', messages, false);
                                }else{
                                    console.log(xhr);
                                }
                            }
                        });
                    });


                });

                // Media Delete End ------------------------------------

            // DropZone End --------------------------------------------
        });

    </script>

@stop

