@extends('layouts.auth.main')

@section('title')
Create Job
@endsection

@section('css')

@stop

@section('content')

<section class="content">
    @if($person)
        <div class="row">
            <div class="col-md-12">
               <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Person</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h4>{{ $person->name }}</h4>
                        <p>
                            <h5>Billing Address</h5>
                            {{ $person->address }}<br/>
                            {{ $person->city }} UT, {{ $person->zip }}
                        </p>

                        <p>
                            <a href="mailto:{{ $person->email }}">{{ $person->email }}</a>  |  <a href="tel:{{ $person->phone }}">{{ $person->phone }}</a>
                        </p>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ url('admin/people/edit/'.$person->id) }}" class="btn btn-primary btn-sm pull-right">Edit Person</a>
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>
        </div>
    @endif

    {!! Form::open( ['url' => 'admin/jobs', 'id' => 'jobForm'] ) !!}

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary color-palette-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-home"></i> Job</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">


                        <div class="row">
                            <!--Select form input-->
                            <div class="col-md-3 col-lg-2">
                                <div class="form-group">
                                    {!! Form::label('person_id', 'Person') !!}
                                    {!! Form::select('person_id', [$person ? $person->id : NULL => $person ? $person->name.' | '.$person->email : NULL], $person ? $person->id : NULL, ["class" => "form-control"]) !!}
                                    @if($person && $person->id)
                                        {!! Form::hidden('person_id', $person->id) !!}
                                    @endif
                                    @if($bid && $bid->id)
                                        {!! Form::hidden('bid_id', $bid->id) !!}
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3 col-lg-2">
                                <div class="form-group">
                                    <label>Job Status</label>
                                    {!! Form::select('status_id', $status, NULL, ['class' => 'form-control', 'id' => 'status_id']) !!}
                                </div>
                            </div>

                            <div class="col-md-3 col-lg-2">
                                <div class="form-group">
                                    <label>Job Type</label>
                                    {!! Form::select('type_id', $types, $bid ? $bid->type->id : 2, ['class' => 'form-control', 'id' => 'type_id']) !!}
                                </div>
                            </div>

                            <!--Select form input-->
                            <div class="col-md-3 col-lg-2">
                                <div class="form-group">
                                    {!! Form::label('pitches[]', 'Pitch') !!}
                                    {!! Form::select('pitches[]', $pitches, $bid ? $selected_pitches : NULL, ['class' => 'form-control pitches_select2', 'multiple' => true]) !!}
                                </div>
                            </div>

                            <div class='col-md-6 col-lg-2'>
                                <div class="form-group">
                                    <label>Start Date</label>
                                    {!! Form::text('start_at', NULL, ['class' => 'form-control', 'id' => 'datetimepicker2']) !!}
                                </div>
                            </div>

                            <div class='col-md-6 col-lg-2'>
                                <div class="form-group">
                                    <label>End Date</label>
                                    {!! Form::text('end_at', NULL, ['class' => 'form-control', 'id' => 'datetimepicker3']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('address', 'Project Address') !!}
                                    {!! Form::text('address', $bid ? $bid->address : NULL, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('city', 'Project City') !!}
                                    {!! Form::select('city', 
                                        $taxed_cities, $bid ? str_slug($bid->city, '_') : NULL, 
                                        ['class' => 'form-control city_select2']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('zip', 'Project Zip') !!}
                                    {!! Form::text('zip', $bid ? $bid->zip : NULL, ['class' => 'form-control', "pattern" => "[0-9]*"]) !!}
                                </div>
                            </div>
                        </div>

                        @if($person && $person->company || !$person)
                            <div class="row" id="is_business" >
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        {!! Form::label('subdivision', 'Subdivision') !!}
                                        {!! Form::text('subdivision', $bid ? $bid->subdivision : NULL, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        {!! Form::label('lot', 'Lot') !!}
                                        {!! Form::text('lot', $bid ? $bid->lot : NULL, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <div class='form-group'>
                                    {!! Form::label('instructions', 'Supplier Instructions') !!}
                                    {!! Form::textarea('instructions', $bid ? $bid->instructions : NULL, ['class' => 'form-control', 'rows' => 1]) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        {!! Form::hidden('add_material_list', NULL, ['id' => 'add_material_list']) !!}

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <div class="row">

        </div>

        <div class="col-md-6">
            <div class="box box-default color-palette-box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-pencil"></i> Add a Note</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::textarea('note', NULL, ['class' => 'form-control notes', 'rows' => 2]) !!}
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button class='btn btn-primary pull-right' id="add_material_list_btn">Save and Add Material List</button>
                    {!! Form::submit('Save', ['class' => 'btn btn-default submit-form pull-right']) !!}
                </div>
            </div>
        </div>

    {!! Form::close() !!}

</section>


@endsection

@section('js')
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>

    <script>

        $(function(){

            $('body').on('click', '#add_material_list_btn' , function(e){
                e.preventDefault();
                $('#add_material_list').val('true');
                $('#jobForm').submit();
            });

            @if($bid && $bid->accepted_at)
                var dateTimePickers = '#datetimepicker2,#datetimepicker6,#datetimepicker3,#datetimepicker4,#datetimepicker5';
            @else
                var dateTimePickers = '#datetimepicker1,#datetimepicker2,#datetimepicker6,#datetimepicker3,#datetimepicker4,#datetimepicker5';
            @endif

            var mousedown = false;
            $(dateTimePickers).on('mousedown', function () {
                mousedown = true;
            });

            $(dateTimePickers).on('focusin', function (e) {
                if(!mousedown) {
                    e.preventDefault();
                    e.stopPropagation();
                }else{
                    $(this).removeAttr('readOnly');
                }
                mousedown = false;
            });

            // DATETIMEPICKERS :::::::::::::::::::::::::::
                var accepted_at = $('#datetimepicker1').datetimepicker({
                    defaultDate: '{{ $bid ? Carbon\Carbon::parse($bid->accepted_at)->toRfc2822String() : NULL }}',
                    format: 'M/D/Y h:mma',
                    useCurrent: false
                });
                var start_at = $('#datetimepicker2').datetimepicker({
                    format: 'M/D/Y h:mma',
                    useCurrent: false
                });
                var end_at = $('#datetimepicker3').datetimepicker({
                    format: 'M/D/Y h:mma',
                    useCurrent: false
                });
            // END DATETIMEPICKERS :::::::::::::::::::::::

            $('.pitches_select2').select2();
            $('.city_select2').select2();

            if({{ $person ? 'true' : 'false' }}){
                $('#person_id').attr('disabled', true);
            }else{
                $('#person_id').select2({
                    placeholder: 'Enter a Person',
                    minimumInputLength: 0,
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    ajax: {
                        url: "{{ url('/admin/people') }}",
                        dataType: 'json',
                        delay: 0,
                        data: function (params) {
                          return {
                            q: params.term, // search term
                            page: params.page,
                          }
                        },
                        processResults: function (data, params) {
                          params.page = params.page || 1;
                          return {
                            results: data.data,
                            pagination: {
                              more: (params.page * 30) < data.total_count
                            }
                          };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) { return markup; },
                    allowClear: false,
                    templateResult: formatResult,
                    templateSelection: formatSelection
                });

                function formatResult (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else if(result.company_id){
                        var $result = $('<span><strong>'+result.company.name+'</strong> | '+result.name+' | '+result.email+'</span>');
                    }else{
                        var $result = $('<span>'+result.name+' | '+result.email+'</span>');
                    }

                    return $result;
                };

                function formatSelection (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else if(result.company_id){
                        var $result = $('<span><strong>'+result.company.name+'</strong> | '+result.name+' | '+result.email+'</span>');
                        $('#is_business').show().find('input').attr('disabled', false);
                    }else{
                        var $result = $('<span>'+result.name+' | '+result.email+'</span>');
                        $('#is_business').hide().find('input').attr('disabled', true);
                    }

                    return $result;
                };

            }

            // Tabbing ---------------------------------
                $('body').on('select2:close', 'select', function () {
                        $(this).focus();
                    }
                );
            // Tabbing ---------------------------------


        });


    </script>

@stop