@extends('layouts.auth.main')

@section('title')
Edit Job
@endsection

@section('css')

@stop

@section('content')

<section class="content">

    @if($person)
        <div class="row">
            <div class="col-md-12">
               <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Person</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h4>{{ $person->name }}</h4>
                        <p>
                            <h5>Billing Address</h5>
                            {{ $person->address }}<br/>
                            {{ $person->city }} UT, {{ $person->zip }}
                        </p>

                        <p>
                            <a href="mailto:{{ $person->email }}">{{ $person->email }}</a>  |  <a href="tel:{{ $person->phone }}">{{ $person->phone }}</a>
                        </p>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ url('admin/people/edit/'.$person->id) }}" class="btn btn-primary btn-sm pull-right">Edit Person</a>
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>
        </div>
    @endif


    {!! Form::open( ['url' => 'admin/jobs/'.$job->id, 'id' => 'jobForm'] ) !!}

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary color-palette-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Job</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <!--Select form input-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('person_id', 'Person') !!}
                                    {!! Form::text('person_id', $person->name.' | '.$person->email, ["class" => "form-control", "disabled" => true]) !!}
                                    @if($person && $person->id)
                                        {!! Form::hidden('person_id', $person->id) !!}
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Job Status</label>
                                    {!! Form::select('status_id', $status, $job->status_id, ['class' => 'form-control', 'id' => 'status_id']) !!}
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Purchase Order</label>
                                    {!! Form::text('purchase_order', $job->purchase_order, ['class' => 'form-control', 'id' => 'purchase_order', 'disabled' => true]) !!}
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Job Type</label>
                                    {!! Form::select('type_id', $types, $job->type_id, ['class' => 'form-control', 'id' => 'type_id']) !!}
                                </div>
                            </div>
                            <!--Select form input-->
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('pitches[]', 'Pitch') !!}
                                    {!! Form::select('pitches[]', $pitches, $selected_pitches, ['class' => 'form-control pitches_select2', 'multiple' => true]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row lost_reason" {{ $job->status_id == 6 ? "" : "style=display:none" }}>
                            <div class="col-md-12">
                                <div class='form-group'>
                                    {!! Form::label('lost_reason', 'Reason for Loss of Job') !!}
                                    {!! Form::text('lost_reason', $job->lost_reason, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class='col-md-2'>
                                <div class="form-group">
                                    <label>Order Date</label>
                                    {!! Form::text('ordered_at', NULL, ['class' => 'form-control', 'id' => 'datetimepicker1']) !!}
                                </div>
                            </div>
                            <div class='col-md-2'>
                                <div class="form-group">
                                    <label>Delivery Date</label>
                                    {!! Form::text('delivery_at', NULL, ['class' => 'form-control', 'id' => 'datetimepicker2']) !!}
                                </div>
                            </div>
                            <div class='col-md-2'>
                                <div class="form-group">
                                    <label>Start Date</label>
                                    {!! Form::text('start_at', NULL, ['class' => 'form-control', 'id' => 'datetimepicker3']) !!}
                                </div>
                            </div>
                            <div class='col-md-2'>
                                <div class="form-group">
                                    <label>End Date</label>
                                    {!! Form::text('end_at', NULL, ['class' => 'form-control', 'id' => 'datetimepicker4']) !!}
                                </div>
                            </div>
                            <div class='col-md-2'>
                                <div class="form-group">
                                    <label>Completed Date</label>
                                    {!! Form::text('completed_at', NULL, ['class' => 'form-control', 'id' => 'datetimepicker5']) !!}
                                </div>
                            </div>
                            <div class='col-md-2'>
                                <div class="form-group">
                                    <label>Invoiced Date</label>
                                    {!! Form::text('invoiced_at', NULL, ['class' => 'form-control', 'id' => 'datetimepicker6', 'disabled' => true]) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('address', 'Project Address') !!}
                                    {!! Form::text('address', $job->project_address ?: $person->address, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('city', 'Project City') !!}
                                    {!! Form::select('city', $taxed_cities, str_slug($job->project_city, '_') ?: str_slug($person->city, '_'), ['class' => 'city_select2 form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('zip', 'Project Zip') !!}
                                    {!! Form::text('zip', $job->project_zip ?: $person->zip, ['class' => 'form-control', "pattern" => "[0-9]*"]) !!}
                                </div>
                            </div>
                        </div>

                        @if($person && $person->company || !$person)
                            <div class="row" id="is_business" >
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        {!! Form::label('subdivision', 'Subdivision') !!}
                                        {!! Form::text('subdivision', $job->subdivision, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        {!! Form::label('lot', 'Lot') !!}
                                        {!! Form::text('lot', $job->lot, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <div class='form-group'>
                                    {!! Form::label('instructions', 'Supplier Instructions') !!}
                                    {!! Form::textarea('instructions', $job->instructions, ['class' => 'form-control', 'rows' => 1]) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        {!! Form::hidden('add_material_list', NULL, ['id' => 'add_material_list']) !!}


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="box box-default color-palette-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add a Note</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::textarea('note', NULL, ['class' => 'form-control notes', 'rows' => 2]) !!}
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            @if(count($job->notes))
                <div class="col-md-6">
                    <div class="box box-info color-palette-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Notes</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-footer box-comments">
                                        @foreach($job->notes as $note)
                                            <div class="box-comment">
                                                <!-- User image -->
                                                <img class="img-circle img-sm" src="{{ url($note->user->avatar ?: 'guest/images/user_empty.png') }}" alt="User Image">

                                                <div class="comment-text">
                                                    <span class="username">
                                                        {{ $note->user->name }}
                                                        <span class="text-muted pull-right">{{ Carbon\Carbon::parse($note->created_at)->format('F jS, Y g:ia') }}</span>
                                                    </span><!-- /.username -->
                                                    {{ $note->content }}
                                                </div>
                                                <!-- /.comment-text -->
                                            </div>
                                            <!-- /.box-comment -->
                                        @endforeach
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            @endif

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    @if(count($job->material_list))
                        <button class='btn btn-info pull-right' id="see_job_btn">Save and View Job</button>
                        <button class='btn btn-primary pull-right' id="edit_material_list_btn">Save and Edit Material List</button>
                    @else
                        <a href="{{ url('admin/jobs/delete/'.$job->id) }}" class="btn btn-danger confirm-link" data-confirm-text="Are you sure you want to delete this job?">Delete</a>
                        <button class='btn btn-primary pull-right' id="add_material_list_btn">Save and Create Material List</button>
                        <button class='btn btn-primary pull-right' id="add_template_btn">Save and Create Material List From Template</button>
                    @endif
                    {!! Form::submit('Save', ['class' => 'btn btn-default pull-right']) !!}
                </div>
            </div>
        </div>

    {!! Form::close() !!}

</section>


@endsection

@section('js')
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>

    <script>
        $(function(){
            
            var mousedown = false;
            $('#datetimepicker1,#datetimepicker2,#datetimepicker3,#datetimepicker4,#datetimepicker5,#datetimepicker6').on('mousedown', function () {
                mousedown = true;
            });

            $('#datetimepicker1,#datetimepicker2,#datetimepicker3,#datetimepicker4,#datetimepicker5,#datetimepicker6').on('focusin', function (e) {
                if(!mousedown) {
                    e.preventDefault();
                    e.stopPropagation();
                }else{
                    $(this).removeAttr('readOnly');
                }
                mousedown = false;
            });

            // DATETIMEPICKERS :::::::::::::::::::::::::::
                var ordered_at = $('#datetimepicker1').datetimepicker({
                    format: 'M/D/Y h:mma',
                    useCurrent: false,
                    @if($job->ordered_at)
                        defaultDate: moment("{{ Carbon\Carbon::parse($job->ordered_at)->toIso8601String() }}"),
                    @endif
                });

                var delivery_at = $('#datetimepicker2').datetimepicker({
                    format: 'M/D/Y h:mma',
                    useCurrent: false,
                    @if($job->delivery_at)
                        defaultDate: moment("{{ Carbon\Carbon::parse($job->delivery_at)->toIso8601String() }}"),
                    @endif
                });

                var start_at = $('#datetimepicker3').datetimepicker({
                    format: 'M/D/Y h:mma',
                    useCurrent: false,
                    @if($job->start_at)
                        defaultDate: moment("{{ Carbon\Carbon::parse($job->start_at)->toIso8601String() }}"),
                    @endif
                });

                var end_at = $('#datetimepicker4').datetimepicker({
                    format: 'M/D/Y h:mma',
                    useCurrent: false,
                    @if($job->end_at)
                        defaultDate: moment("{{ Carbon\Carbon::parse($job->end_at)->toIso8601String() }}"),
                    @endif
                });
                var completed_at = $('#datetimepicker5').datetimepicker({
                    format: 'M/D/Y h:mma',
                    useCurrent: false,
                    @if($job->completed_at)
                        defaultDate: moment("{{ Carbon\Carbon::parse($job->completed_at)->toIso8601String() }}"),
                    @endif
                });
                var invoiced_at = $('#datetimepicker6').datetimepicker({
                    format: 'M/D/Y h:mma',
                    useCurrent: false,
                    @if($job->invoiced_at)
                        defaultDate: moment("{{ Carbon\Carbon::parse($job->invoiced_at->toIso8601String()) }}"),
                    @endif
                });
            // END DATETIMEPICKERS :::::::::::::::::::::::

            $('.pitches_select2').select2();
            $('.city_select2').select2();

            $('body').on('change', '#status_id', function(){
                if($(this).val() == 6){
                    $('.lost_reason').fadeIn();
                }else{
                    $('#lost_reason').val('');
                    $('.lost_reason').fadeOut();
                }
            });

            $('body').on('click', '#add_material_list_btn, #edit_material_list_btn, #see_job_btn, #add_template_btn' , function(e){
                e.preventDefault();
                var this_btn = $(this).attr('id');

                if(this_btn == 'see_job_btn'){
                    $('#add_material_list').val('see_job');
                }else if(this_btn == 'add_material_list_btn'){
                    $('#add_material_list').val('add_material_list');
                }else if(this_btn == 'edit_material_list_btn'){
                    $('#add_material_list').val('edit_material_list');
                }else if(this_btn == 'add_template_btn'){
                    $('#add_material_list').val('add_template_material_list');
                }else{
                    $('#add_material_list').val('add_material_list');
                }

                $('#jobForm').submit();
            });

            if({{ $person ? 'true' : 'false' }}){
                $('#person_id').attr('disabled', true);
            }else{
                $('#person_id').select2({
                    placeholder: 'Enter a Person',
                    minimumInputLength: 0,
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    ajax: {
                        url: "{{ url('/admin/people') }}",
                        dataType: 'json',
                        delay: 0,
                        data: function (params) {
                          return {
                            q: params.term, // search term
                            page: params.page,
                          }
                        },
                        processResults: function (data, params) {
                          params.page = params.page || 1;
                          return {
                            results: data.data,
                            pagination: {
                              more: (params.page * 30) < data.total_count
                            }
                          };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) { return markup; },
                    allowClear: false,
                    templateResult: formatResult,
                    templateSelection: formatSelection
                });

                function formatResult (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else if(result.company_id){
                        var $result = $('<span><strong>'+result.company.name+'</strong> | '+result.name+' | '+result.email+'</span>');
                    }else{
                        var $result = $('<span>'+result.name+' | '+result.email+'</span>');
                    }

                    return $result;
                };

                function formatSelection (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else if(result.company_id){
                        var $result = $('<span><strong>'+result.company.name+'</strong> | '+result.name+' | '+result.email+'</span>');
                        $('#is_business').show().find('input').attr('disabled', false);
                    }else{
                        var $result = $('<span>'+result.name+' | '+result.email+'</span>');
                        $('#is_business').hide().find('input').attr('disabled', true);
                    }

                    return $result;
                };

            }
        });


    </script>

@stop