@extends('layouts.auth.main')

@section('title')
Jobs
@endsection

@section('css')
	<link rel="stylesheet" href="">

	<style>
		.form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
        table .form-control {
            border: none;
        }
	</style>
@stop

@section('content')
    	<div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ url('admin/jobs/create') }}" class="btn btn-primary pull-right">Create Job</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        {!! Form::open(['url' => 'admin/jobs', 'method' => 'GET']) !!}
                                            <th width="100px"></th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('purchase_order', $purchase_order, ['placeholder' => 'Purchase Order', 'class' => 'form-control purchase-order']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('user_name', $user_name, ['placeholder' => 'Project Manager Name', 'class' => 'form-control user-name']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('customer_name', $customer_name, ['placeholder' => "Customer's Name", 'class' => 'form-control customer-name']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('address', $address, ['placeholder' => 'Job Address', 'class' => 'form-control job-address']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('city', $city, ['placeholder' => 'Job City', 'class' => 'form-control job-city']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('subdivision', $subdivision, ['placeholder' => 'Subdivision', 'class' => 'form-control subdivision']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('lot', $lot, ['placeholder' => 'Lot', 'class' => 'form-control lot']) !!}
                                                </div>
                                            </th>

                                            <div style="display: none">

                                            {!! Form::submit('Submit') !!}
                                            </div>

                                        {!! Form::close() !!}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($jobs))
                                        @foreach($jobs as $job)
                                            <tr>
                                                <td>
                                                    <a href="{{ url('admin/jobs/edit/'.$job->id) }}" class="btn btn-success btn-xs responsive-button" data-toggle="tooltip" title="Edit Job">
                                                        <i class="fa fa-home"></i>
                                                    </a>
                                                    @if(count($job->material_list))
                                                        <a href="{{ url('admin/material_lists/edit/'.$job->material_list->id.'/job') }}" 
                                                            class="btn btn-success btn-xs responsive-button" 
                                                            data-toggle="tooltip" 
                                                            title="Edit Materal List">
                                                            <i class="fa fa-list"></i>
                                                        </a>
                                                    @else
                                                        <a href="{{ url('admin/material_lists/create/'.$job->id.'/job') }}" class="btn btn-default btn-xs responsive-button" data-toggle="tooltip" title="Create Materal List">
                                                            <i class="fa fa-list"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $job->purchase_order }}
                                                </td>
                                                <td>
                                                    {{ $job->user ? $job->user->name : NULL }}
                                                </td>
                                                <td>
                                                    {{ $job->person ? $job->person->name : NULL }}
                                                </td>
                                                <td>
                                                    {{ $job->address }}
                                                </td>
                                                <td>
                                                    {{ str_unslug($job->city) }}
                                                </td>
                                                <td>
                                                    {{ $job->subdivision }}
                                                </td>
                                                <td>
                                                    {{ $job->lot }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6">
                                                There are no jobs built yet, click Create Job to get one started.
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {!! $jobs->links() !!}
                        </div>
                    </div>

                </div>
            </div>

    	</div>


@stop

@section('js')

	<script>

		$(function(){


			$('body').on('keyup', '.purchase-order, .user-name, .customer-name, .job-address, .job-city, .subdivision, .lot' , function(){
				var input = $(this);
				window.timer=setTimeout(function(){ // setting the delay for each keypress
				 	timedAjaxRequest(input); //runs the ajax request
				},
				500);
				}).keydown(function(){
					clearTimeout(window.timer); //clears out the timeout
			});


			function timedAjaxRequest(input){
				var purchase_order = $('.purchase-order').val();
				var user_name = $('.user-name').val();
				var customer_name = $('.customer-name').val();
				var job_address = $('.job-address').val();
				var job_city = $('.job-city').val();
                var subdivision = $('.subdivision').val();
                var lot = $('.lot').val();

				$.ajax({
					url: '{{ url('admin/jobs') }}?purchase_order='+purchase_order+'&fullname='+user_name+'&customer_name='+customer_name+'&address='+job_address+'&city='+job_city+'&subdivision='+subdivision+'&lot='+lot,
					type: 'GET',
					success: function (response) {
						var response = jQuery.parseJSON(response);
						$('tbody').html('');
						$.each( response.data, function( key, value ) {

                            if(value.purchase_order){ var purchase_order = value.purchase_order; }else{ var purchase_order = ""; }
                            if(value.subdivision){ var subdivision = value.subdivision; }else{ var subdivision = ""; }
                            if(value.lot){ var lot = value.lot; }else{ var lot = ""; }

							$('tbody').append('<tr>'+
								'<td>'+
									'<a href="{{ url('admin/jobs/edit') }}/'+value.id+'" class="btn btn-primary responsive-button">'+
										'<i class="fa fa-pencil"></i>'+
									'</a>'+
								'</td>'+
								'<td>'+purchase_order+'</td>'+
								'<td>'+value.user.name+'</td>'+
								'<td>'+value.person.name+'</td>'+
								'<td>'+value.address+'</td>'+
                                '<td>'+value.city+'</td>'+
                                '<td>'+subdivision+'</td>'+
                                '<td>'+lot+'</td>'+
							'</tr>')
							$('.responsive-button.right').css({'margin-left': '4px'});
							$('ul.pagination').remove();
						});
					},
					error: function (xhr) {
						console.log(xhr);
					}
				});
			}

		});

	</script>

@stop
