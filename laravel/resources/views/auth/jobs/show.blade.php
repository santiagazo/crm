@extends('layouts.auth.main')

@section('title')
Show Job
@endsection

@section('css')
    <link rel="stylesheet" href="{{ url('auth/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link href="{{ url('guest/plugins/lightgallery/dist/css/lightgallery.min.css') }}" rel="stylesheet">
    <style>
        .material_list-table{
            margin-bottom: 0px;
        }
        .id-column{
            width: 5%;
        }
        .product-column{
            width: 40%;
        }
        .qty-column{
            width: 10%;
        }
        .supplier-column{
            width: 10%;
        }
        .house-column{
            width: 10%;
        }
        td i.fa{
            margin: 10px;
        }
        .table-bordered>tbody>tr:last-child>td {
            border-bottom: 2px solid #323232;
        }
        .material_list-header{
            background-color: #3c8dbc;
            color: #fff;
        }
        .box-header>.box-tools.template {
            position: absolute;
            right: 10px;
            top: 10px;
        }
        .image-thumbnail{
            margin-bottom: 15px;
            cursor: pointer;
        }
        .table>tfoot>tr>td{
            height: 25px;
            padding: 3px 12px;
        }
        .house_subtotal,
        .supplier_subtotal,
        .house_sub_total,
        .supplier_sub_total,
        .house_total,
        .supplier_total,
        .house_tax,
        .supplier_tax,
        .supplier_sub_difference,
        .supplier_difference{
            text-align: right;
        }
        .house_total,
        .supplier_total,
        .supplier_difference,
        .tfooter-title.total-title{
            font-weight: 700;
        }
        .options{
            float: left;
            margin-top: 5px;
        }
        .options .checkbox{
            margin-top: 0px; 
            margin-bottom: 0px; 
        }
        .options .small{
            margin-left: 5px; 
        }
    </style>
@stop

@section('content')


<section class="content">

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Person</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
    

                <!-- /.box-header -->
                <div class="box-body">
                    <h4>{{ $job->person->name }}</h4>
                    <p>
                        <h5>Billing Address</h5>
                        @php
                            $address = $job->person ? $job->person->address : '';
                            $city = $job->person ? $job->person->city : '';
                            $zip = $job->person ? $job->person->zip: '';
                        @endphp
                        <a href="https://www.google.com/maps/search/?api=1&query={{ urlencode($address.' '.$city.' UT '.$zip) }}" target="about_blank">
                        {{ $job->person->address }}<br/>
                        {{ $job->person->city }} UT, {{ $job->person->zip }}
                    </p>

                    <p>
                        <a href="mailto:{{ $job->person->email }}">{{ $job->person->email }}</a>  |  <a href="tel:{{ $job->person->phone }}">{{ $job->person->phone }}</a>
                    </p>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ url('admin/people/edit/'.$job->person->id) }}" class="btn btn-primary btn-sm pull-right">Edit Person</a>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Job</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>{{ $job->purchase_order ? 'Purchase Material: '.$job->purchase_order : 'Job Id: '.$job->id }}</h4>
                            <p>
                                <h5>Project Address</h5>
                                @php
                                    $job_address = $job->address ?: $job->person->address;
                                    $job_city = $job->city ?: $job->person->city;
                                    $job_zip = $job->zip ?: $job->person->zip;
                                @endphp
                                <a href="https://www.google.com/maps/search/?api=1&query={{ urlencode($job_address.' '.$job_city.' UT '.$job_zip) }}" target="about_blank">
                                    {{ $job->address ?: $job->person->address }}<br/>
                                    {{ $job->city ?: $job->person->city }} UT, {{ $job->zip ?: $job->person->zip }}
                                </a>
                            </p>
                            @if($job->subdivision || $job->lot)
                            <p>
                                {{ $job->subdivision ? 'Subdivision: '.$job->subdivision : NULL }}
                                {{ $job->subdivision && $job->lot ? ' | ' : NULL }}
                                {{ $job->lot ? 'Lot: '.$job->lot : NULL }}
                            </p>
                            @endif
                            <p>Job Type: {{ str_unslug($job->type->name) }}</p>
                        </div>
                        <div class="col-md-6">
                            <div class="text-right">{!! $job->accepted_at ? '<strong>Started On: </strong>'.Carbon\Carbon::parse($job->accepted_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $job->ordered_at ? '<strong>Ordered On: </strong>'.Carbon\Carbon::parse($job->ordered_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $job->delivery_at ? '<strong>Delivery On: </strong>'.Carbon\Carbon::parse($job->delivery_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $job->start_at ? '<strong>Starts On: </strong>'.Carbon\Carbon::parse($job->start_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $job->end_at ? '<strong>Ends On: </strong>'.Carbon\Carbon::parse($job->end_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $job->invoiced_at ? '<strong>Invoiced On: </strong>'.Carbon\Carbon::parse($job->invoiced_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $job->completed_at ? '<strong>Completed On: </strong>'.Carbon\Carbon::parse($job->completed_at)->format('l M. j, Y') : NULL !!}</div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ url('admin/jobs/edit/'.$job->id) }}" class="btn btn-primary btn-sm pull-right">Edit Job</a>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Job Material List</h3>
                    <div class="box-tools template pull-right">
                        <strong>{{ $job->material_list->template ? 'Template: '.$job->material_list->template->name : 'No Associated Template' }}</strong>
                    </div>
                </div>
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-12 material_list-holder">
                            <table class="table material_list-table table-bordered">
                                <tbody data-material_list-id='NULL' class="product-holder">
                                    <tr class="material_list-header">
                                        <th class="product-column">Product Category</th>
                                        <th class="qty-column">Qty</th>
                                        <th class="house-column">House Sub-Total</th>
                                        <th class="supplier-column">Supplier Sub-Total</th>
                                        <th class="supplier-select-column">{{ $job->material_list->company ? $job->material_list->company->name : NULL }}</th>
                                        <th class="color-column">Color</th>
                                    </tr>
                                    @php
                                        $category_price = [];
                                        $supplier_price = [];
                                    @endphp
                                    @foreach($job->material_list->categories as $category)
                                        <tr class="product_line" data-category-id="">
                                            <td>
                                                {{ $category->name }}
                                            </td>
                                            <td>
                                                {{ $category->pivot->qty }}
                                            </td>
                                            <td>
                                                {{ number_format($category->pivot->category_price*$category->pivot->qty, 2, '.', '') }}
                                            </td>
                                            <td>
                                                {{ $category->pivot->supplier_price }}
                                            </td>
                                            <td>
                                                @if($category->pivot->product_id)
                                                    {{ $category->products_through_pivot->first()->name }}
                                                @elseif($category->pivot->is_house_product)
                                                    In House Product
                                                @else
                                                    No Products in Category
                                                @endif
                                            </td>
                                            <td>
                                                {{ str_unslug($category->pivot->color) ?: "Colors N/A" }}
                                            </td>
                                        </tr>

                                        @php
                                            $category_price[] = $category->pivot->category_price*$category->pivot->qty;
                                            $supplier_price[] = $category->pivot->supplier_price;
                                        @endphp

                                    @endforeach

                                    @php
                                        $category_price = (int) array_sum($category_price);
                                        $supplier_price = (int) array_sum($supplier_price);
                                    @endphp
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2">Sub-Total</td>
                                        <td class="house_sub_total">{{ formatPrice($category_price) }}</td>
                                        <td class="supplier_sub_total">{{ formatPrice($supplier_price) }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Tax</td>
                                        <td class="house_tax">{{ $job->tax ? $job->tax->amount*100 : 0 }}</td>
                                        <td class="supplier_tax">{{ $job->tax ? $job->tax->amount*100 : 0 }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="tfooter-title total-title">Total</td>
                                        <td class="house_total">{{ $job->tax ? 
                                            formatPrice($category_price*$job->tax->amount+$category_price) : 
                                            formatPrice($category_price) }}</td>
                                        <td class="supplier_total">{{ $job->tax ? 
                                            formatPrice($supplier_price*$job->tax->amount+$supplier_price) : 
                                            formatPrice($supplier_price) }}</td>
                                        <td class="supplier_difference">{{ $job->tax ? 
                                            formatPrice(($category_price-$supplier_price)*($job->tax->amount)+($category_price-$supplier_price)) : 
                                            formatPrice(($category_price-$supplier_price)) }}</td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer ptb-0">
                    <div class="options">
                        <ul class="list-inline">
                            <li><strong>Client Selected: </strong></li>
                            <li class="checkbox">
                                <label>
                                    {{ Form::checkbox('is_cash_discount', true, $job->is_cash_discount, ['disabled' => true]) }} 3% Cash Discount
                                </label>
                            </li>
                            <li class="checkbox">
                                <label>
                                    {{ Form::checkbox('is_pre_dry_in', true, $job->is_pre_dry_in, ['disabled' => true]) }} Pre Dry-In
                                </label>
                            </li>
                            <li class="checkbox">
                                <label>
                                    {{ Form::checkbox('is_six_nail', true, $job->is_six_nail, ['disabled' => true]) }} Six Nail
                                </label>
                            </li>
                            <li class="checkbox">
                                <label>
                                    {{ Form::checkbox('is_owner_tear_off', true, $job->is_owner_tear_off, ['disabled' => true]) }} Owner Tear-Off
                                </label>
                            </li>
                            <div class="small">These options were requested by client when bid was accepted.</div>
                        </ul>
                    </div>
                    <a href="{{ url('admin/material_lists/edit/'.$job->material_list->id.'/job') }}" class="btn btn-primary btn-sm pull-right mt-10">Edit Material List</a>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

    @if(count($job->notes))
        <div class="row">
            <div class="{{ $job->estimate ? "col-md-6" : "col-md-12" }}">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Notes</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-footer box-comments">
                                    @foreach($job->notes as $note)
                                        <div class="box-comment">
                                            <!-- User image -->
                                            <img class="img-circle img-sm" src="{{ url($note->user->avatar ?: 'guest/images/user_empty.png') }}" alt="User Image">

                                            <div class="comment-text">
                                                <span class="username">
                                                    {{ $note->user->name }}
                                                    <span class="text-muted pull-right">{{ Carbon\Carbon::parse($note->created_at)->format('F jS, Y g:ia') }}</span>
                                                </span><!-- /.username -->
                                                {{ $note->content }}
                                            </div>
                                            <!-- /.comment-text -->
                                        </div>
                                        <!-- /.box-comment -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/jobs/edit/'.$job->id) }}" class="btn btn-primary btn-sm pull-right">Add Note</a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            @if(count($job->media))
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Images</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row" id="images_for_job">
                                @forelse($job->media as $image)
                                    <div class="col-sm-3 image-thumbnail" data-src="{{ url($image->url) }}">
                                        <img src="{{ url($image->url) }}" class="img-responsive">
                                    </div>
                                @empty
                                    <div class="col-md-12">
                                        <p>No images uploaded.</p>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            @endif
            @if($job->estimate)
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Estimate Request</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="callout callout-default">
                                <div class="product-info">
                                    <span class="product-title">Estimate Request {{ $job->estimate->is_urgent ? '- URGENT' : "" }}</span>
                                    <button class="btn btn-xs btn-default pull-right see-more-btn">See More</button>
                                    <div>
                                        Estimate Types:
                                        @forelse(json_decode($job->estimate->type) as $item)
                                            @if(!$loop->last)
                                                {{ str_unslug($item) }},
                                            @else
                                                {{ str_unslug($item) }}
                                            @endif
                                        @empty
                                            Declined to Specify
                                        @endforelse
                                    </div>
                                    <div class="see-more" style="display:none">
                                        <p>
                                            Roof Type: {{ str_unslug($job->estimate->roof_type) }} / Roof Pitch: {{ str_unslug($job->estimate->roof_pitch) }}
                                        </p>
                                        <p>{{ $job->estimate->additional_information ? "Additional Information: ".$job->estimate->additional_information : NULL }}</p>
                                        <p>Source: {{ str_unslug($job->estimate->advertisment) }} {{ $job->estimate->referred_by ? " | ".str_unslug($job->estimate->referred_by) : NULL }}</p>
                                        <div class="row" id="images_for_estimate">
                                            @forelse($job->estimate->media as $image)
                                                <div class="col-sm-3" data-src="{{ url($image->url) }}">
                                                    <img src="{{ url($image->url) }}" class="img-responsive">
                                                </div>
                                            @empty
                                                <div class="col-md-12">
                                                    <p>No images uploaded.</p>
                                                </div>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            @endif
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="form-group pull-right">
                @if($job->bid)
                    <a href="{{ url('admin/pdf/bids/create/'.$job->bid->id) }}" class="btn btn-primary">View Bid Document</a>
                @endif
                <a href="{{ url('admin/pdf/suppliers/create/'.$job->id) }}" class="btn btn-primary">View Supplier Document</a>
                <a href="{{ url('admin/pdf/jobs/create/'.$job->id) }}" class="btn btn-primary">View Job Document</a>
                @if($job->completed_at)
                    <a href="{{ url('admin/pdf/invoices/create/'.$job->id) }}" class="btn btn-primary">View Invoice Document</a>
                @endif
            </div>
        </div>
    </div>
</section>


@endsection

@section('js')
    <script src="{{ url('auth/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ url('guest/plugins/lightgallery/dist/js/lightgallery-all.min.js') }}"></script>

    <script>

        $(function(){
            $('body').on('click', '.see-more-btn', function(){
                $(this).parent().find('.see-more').slideToggle();
                $(this).text() == "See More" ? $(this).text("See Less") : $(this).text("See More");
            });

            $('#images_for_job, #images_for_estimate').lightGallery({
                gallery: true,
                download: false,
                onSliderLoad  : function (el) {
                  el.lightGallery();
                }
            });
        });

    </script>

@stop