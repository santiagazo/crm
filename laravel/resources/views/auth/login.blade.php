@extends('layouts.site.main')

@section('title')
Login
@endsection

@section('css')
	<style>
        body, html {
             min-height: 100px;
        }
         .tab-content{
    		width: 300px;
            margin: 210px auto;
         }
    </style>
@endsection

@section('content')
<section class="ptb-60">
    <div class="container">
        <div class="row">

                <div class="tab-content">
                    <form class="tab-pane fade active in" id="top-nav-login" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Email Address" name="email" value="{{ old('email') }}" required autofocus>
                            <i class="form-group__bar"></i>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group"{{ $errors->has('password') ? ' has-error' : '' }}>
                            <input type="password" class="form-control" placeholder="Password" name="password" required>
                            <i class="form-group__bar"></i>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <button class="btn btn-primary btn-block m-t-10 m-b-10">Login</button>

                        <div class="text-center">
                            <a href="#top-nav-forgot-password" data-toggle="tab"><small>Forgot email/password?</small></a>
                        </div>
                    </form>

                    <form class="tab-pane fade forgot-password" id="top-nav-forgot-password" role="form" method="POST" action="{{ url('/password/email') }}">
                        <a href="#top-nav-login" class="top-nav__back" data-toggle="tab"></a>
                        {{ csrf_field() }}
                        <p>We'll send you an email with a reset link faster than you can lay a shingle.</p>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Emaill Address" name="email" value="{{ old('email') }}" required>
                            <i class="form-group__bar"></i>
                        </div>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                        <button class="btn btn-warning btn-block">Reset Password</button>
                    </form>
                </div>

        </div>
    </div>
</section>
@endsection