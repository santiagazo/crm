@extends('layouts.auth.main')

@section('title')
Create Bid Material List
@endsection

@section('css')
    <link rel="stylesheet" href="{{ url('auth/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <style>
        .finalize-bid-btn,
        .add-item-btn{
            margin-top: 10px;
            margin-bottom: 10px;
            margin-right: 10px;
        }
        .material-table{
            margin-bottom: 0;
        }
        .table>tbody>tr>td {
            padding: 0px;
        }
        table .select2-container--default .select2-selection--single,
        table .select2-selection .select2-selection--single {
            border: none;
            border-radius: 0;
            padding: 5px 5px;
            height: 34px;
        }
        .select2-dropdown {
            border: 1px solid #f4f4f4;
        }
        .select2-container--default .select2-search--dropdown .select2-search__field {
            border: 1px solid #f4f4f4;
        }
        table .form-control {
            border: none;
        }
        .id-column{
            width: 5%;
        }
        .product-column{
            width: 70%;
        }
        .qty-column{
            width: 10%;
        }
        .supplier-column{
            width: 10%;
        }
        .house-column{
            width: 10%;
        }
        td i.fa{
            margin: 10px;
        }
        .table>tbody>tr>td,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th {
            padding: 0;
            line-height: 1.42857143;
            vertical-align: top;
        }
        .table-bordered>tbody>tr:last-child>td {
            border-bottom: 2px solid #323232;
        }
        .material-header{
            background-color: #3c8dbc;
            color: #fff;
        }
        .fa-times.text-muted{
            cursor: pointer;
        }
        .supplier_select~.select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 22px;
        }
        table .select2-container--default .select2-selection--single,
        table .select2-selection .select2-selection--single .supplier_select{
            height: 22px;
        }
        .supplier_select~.select2-container--default .select2-selection--single .select2-selection__arrow b {
            margin-top: -4px;
        }
        .date-updates{
            display: block;
            float: right;
        }

        tfoot td .form-control,
        tfoot .tfooter-title{
            height: 25px;
            padding: 3px 12px;
        }
        .house_subtotal,
        .supplier_subtotal,
        .house_sub_total,
        .supplier_sub_total,
        .house_total,
        .supplier_total,
        .house_tax,
        .supplier_tax,
        .supplier_sub_difference,
        .supplier_difference{
            text-align: right;
        }

        .house_total,
        .supplier_total,
        .supplier_difference,
        .tfooter-title.total-title{
            font-weight: 700;
        }
        .options{
            float: right;
            margin-top: 5px;
        }
    </style>
@stop

@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Person</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <h4>{{ $materialable->person->name }}</h4>
                    <p>
                        <h5>Billing Address</h5>
                        {{ $materialable->person->address }}<br/>
                        {{ $materialable->person->city }} UT, {{ $materialable->person->zip }}
                    </p>

                    <p>
                        <a href="mailto:{{ $materialable->person->email }}">{{ $materialable->person->email }}</a>  |  <a href="tel:{{ $materialable->person->phone }}">{{ $materialable->person->phone }}</a>
                    </p>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ url('admin/people/edit/'.$materialable->person->id) }}" class="btn btn-primary btn-sm pull-right">Edit Person</a>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $materialable->uc_name() }}</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>{{ $materialable->purchase_order ? 'Purchase Order: '.$materialable->purchase_order : 'Bid Id: '.$materialable->id }}</h4>
                            <p>
                                <h5>Project Address</h5>
                                {{ $materialable->address ?: $materialable->person->address }}<br/>
                                {{ $materialable->city ?: $materialable->person->city }} UT, {{ $materialable->zip ?: $materialable->person->zip }}
                            </p>
                            @if($materialable->subdivision || $materialable->lot)
                            <p>
                                {{ $materialable->subdivision ? 'Subdivision: '.$materialable->subdivision : NULL }}
                                {{ $materialable->subdivision && $materialable->lot ? ' | ' : NULL }}
                                {{ $materialable->lot ? 'Lot: '.$materialable->lot : NULL }}
                            </p>
                            @endif
                            <p>{{ $materialable->uc_name() }} Type: {{ str_unslug($materialable->type->name) }}</p>
                        </div>
                        <div class="col-md-6">
                            <div class="date-updates">{!! $materialable->accepted_at ? '<strong>Started On: </strong>'.Carbon\Carbon::parse($materialable->accepted_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="date-updates">{!! $materialable->ordered_at ? '<strong>Ordered On: </strong>'.Carbon\Carbon::parse($materialable->ordered_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $materialable->delivery_at ? '<strong>Delivery On: </strong>'.Carbon\Carbon::parse($materialable->delivery_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $materialable->start_at ? '<strong>Starts On: </strong>'.Carbon\Carbon::parse($materialable->start_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $materialable->end_at ? '<strong>Ends On: </strong>'.Carbon\Carbon::parse($materialable->end_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="date-updates">{!! $materialable->invoiced_at ? '<strong>Invoiced On: </strong>'.Carbon\Carbon::parse($materialable->invoiced_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="date-updates">{!! $materialable->completed_at ? '<strong>Completed On: </strong>'.Carbon\Carbon::parse($materialable->completed_at)->format('l M. j, Y') : NULL !!}</div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ url('admin/bids/edit/'.$materialable->id) }}" class="btn btn-primary btn-sm pull-right">Edit {{ $materialable->uc_name() }}</a>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
    </div>

    <div id="add_template"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-list"></i> Bid Material List</h3>
                    <div class="box-tools pull-right">
                        <a href="{{ url('admin/templates?bid_id='.$materialable->id) }}" class="btn btn-sm btn-primary">Create Bid Material List from Template</a>
                        <button type="button" class="btn btn-sm btn-default" id="add_template_btn">Mark as Template</button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-12 material-holder">
                            <table class="table material-table table-bordered">
                                <tbody data-material-id='NULL' class="product-holder">
                                    <tr class="material-header">
                                        <th class="id-column"></th>
                                        <th class="product-column">Product Category</th>
                                        <th class="qty-column">Qty</th>
                                        <th class="house-column">Price</th>
                                        <th nowrap class="house-sub-total">Sub-Total</th>
                                    </tr>
                                    <tr class="product_line" data-category-id="">
                                        <td><i class="fa fa-times text-muted delete_line"></i></td>
                                        <td class="select2container">
                                            {!! Form::select(NULL, [], NULL, ['class' => 'form-control categories_select']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text(NULL, NULL, ['class' => 'form-control product_qty', "pattern" => "[0-9]*"]) !!}
                                        </td>
                                        <td>
                                            {!! Form::text(NULL, NULL, ['class' => 'form-control house_price']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text(NULL, NULL, ['class' => 'form-control house_subtotal']) !!}
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4"><div class="tfooter-title">Sub-Total</div></td>
                                        <td>{!! Form::text(NULL, NULL, ['class' => 'form-control house_sub_total']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><div class="tfooter-title">Tax</div></td>
                                        <td>{!! Form::text(NULL, NULL, ['class' => 'form-control house_tax']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><div class="tfooter-title total-title">Total</div></td>
                                        <td>{!! Form::text(NULL, NULL, ['class' => 'form-control house_total']) !!}</td>
                                    </tr>
                                </tfoot>
                            </table>
                            <button class="btn btn-default btn-sm add-item-btn">Add Item</button>
                            <div class="options">
                                {!! Form::open( ['url' => 'admin/material_lists/bid', 'id' => 'materialListForm'] ) !!}
                                    <ul class="list-inline">
                                        <li><strong>Options: </strong></li>
                                        <li class="checkbox">
                                            <label>
                                                {{ Form::checkbox('is_pre_dry_in', true ) }} Pre Dry-In
                                            </label>
                                        </li>
                                        <li class="checkbox">
                                            <label>
                                                {{ Form::checkbox('is_six_nail', true ) }} Six Nail
                                            </label>
                                        </li>
                                        <li class="checkbox">
                                            <label>
                                                {{ Form::checkbox('is_owner_tear_off', true ) }} Owner Tear-Off
                                            </label>
                                        </li>
                                    </ul>
                                    <div class="hidden materials-form-field">
                                        {!! Form::hidden('line_items', NULL, ['id' => 'line_items']) !!}
                                        {!! Form::hidden('materialable_id', $materialable->id, ['id' => 'materialable_id']) !!}
                                        {!! Form::hidden('template', NULL, ['id' => 'template']) !!}
                                        {!! Form::hidden('save_and_view', false, ['id' => 'save_and_view']) !!}
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ URL::previous() }}" class="btn btn-default">Cancel</a>
            <div class="form-group pull-right">
                {!! Form::submit('Save', ['class' => 'btn btn-default', 'id' => 'submit_btn']) !!}
                <button class="btn btn-primary" id="save_and_view_btn">Save & View</button>
            </div>
        </div>
    </div>
</section>


@endsection

@section('js')
    <script src="{{ url('auth/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

    <script>
        var product_array =[];
        
        function getSelectedCategoryIds(){
            category_ids = [];
            $.each($('tr.product_line'), function(i, row){
                var id = $(row).data('categoryId');
                if(id){
                    category_ids.push(id);
                }
            });

            return category_ids;
        }

        function setTaxPercent(tax = {{ $materialable->tax ? $materialable->tax->amount*100 : '0.00' }}){
            $('.house_tax').val(parseFloat(tax).toFixed(1));
        }
        setTaxPercent();

        function calculatePrices(){
            var house_sub_total = 0;

            $.each($('.product_line'), function(index, product){
                // find the price for house
                var qty = $(product).find('.product_qty').val();
                var price = $(product).find('.house_price').val() ? $(product).find('.house_price').val() : 0;
                var house_subtotal = qty*price;
                $(product).find('.house_subtotal').val(house_subtotal.toFixed(2));
                house_sub_total += parseFloat(house_subtotal.toFixed(2));
            });

            // Set the house_sub_total
            $('.house_sub_total').val(house_sub_total.toFixed(2));

            // Get the tax and set the house_total
            var house_tax = $('.house_tax').val()
            house_tax = house_sub_total*(house_tax/100);
            house_total = house_sub_total+house_tax;
            $('.house_total').val(house_total.toFixed(2));
        }

        function validateAndSubmitForm(){
            var line_items = [];
            var template = $('#template_name').val();
            var validation = "";

            $.each($('.product_line'), function(index, line){
                var category_id = $(line).find('.categories_select').val();
                var qty = $(line).find('.product_qty').val();
                var house_price = $(line).find('.house_price').val();

                line_items.push({
                    "category_id" : category_id,
                    "qty" : qty,
                    "house_price" : house_price
                });

                if(!category_id){
                    validation += '<li>Please confirm there are no empty Product Categories.</li>';
                }
                if(!qty){
                    validation += '<li>Please confirm there are no empty Quantities.</li>';
                }
                if(house_price < 0.01){
                    validation += '<li>Please confirm there are no empty Prices.</li>';
                }
            });

            line_items = JSON.stringify(line_items);

            if(!line_items){
                validation += '<li>Please select at least one product category for this material list.</li>';
            }

            $('#line_items').val(line_items);
            $('#template').val(template);

            if(!validation){
                $('#materialListForm').submit();
            }else{
                jsFlash('error', validation);
            }
        }

        $(function(){
            // Category/Suppliers select2's Start --------------------------------
                function init_category_select2(){
                    $('.categories_select').each(function(i, el){
                        if(!$(this).hasClass('select2-hidden-accessible')){
                            $(this).select2({
                                placeholder: 'Add Product Category',
                                minimumInputLength: 0,
                                allowClear: false,
                                ajax: {
                                    url: "{{ url('admin/categories') }}",
                                    dataType: 'json',
                                    delay: 0,
                                    data: function (params) {
                                        return {
                                            selected_category_ids: getSelectedCategoryIds(),
                                            q: params.term, // search term
                                            page: params.page
                                        }
                                    },
                                    processResults: function (data, params) {
                                        params.page = params.page || 1;
                                        var thisSelect2 = $(this)[0].$element;

                                        return {
                                            results: data.data,
                                            thisSelect: thisSelect2,
                                            pagination: {
                                                more: (params.page * 30) < data.total_count
                                            }
                                        };
                                    },
                                    cache: true
                                },
                                escapeMarkup: function (markup) { return markup; },
                                templateResult: formatResult,
                                templateSelection: formatSelection,
                            });
                        }
                        $(this).focus();
                    });
                }
                init_category_select2();

                function formatResult (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else{
                        var $result = $('<span>' +result.name +' | '+result.price+'/'+result.unit+'</span>');
                    }

                    return $result;
                };

                function formatSelection (result, thisSelect) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else{
                        var $result = $('<span>' +result.name +' | '+result.price+'/'+result.unit+'</span>');
                        $(thisSelect).closest('tr').data('category-id', result.id).find('.house_price').val(result.price);
                        calculatePrices();
                    }
                    return $result;
                };
            // Category select2's End --------------------------------------------

            // Add Product Line Start ----------------------------------
                $('body').on('click', '.add-item-btn', function(e){
                    e.preventDefault();
                    $(this).prev().find('.product-holder').append(
                        '<tr class="product_line" data-category-id="">'+
                            '<td><i class="fa fa-times text-muted delete_line"></i></td>'+
                            '<td class="select2container">'+
                                '{!! Form::select('categories', [], NULL, ['class' => 'form-control categories_select']) !!}'+
                            '</td>'+
                            '<td>'+
                                '{!! Form::text(NULL, NULL, ['class' => 'form-control product_qty', "pattern" => "[0-9]*"]) !!}'+
                            '</td>'+
                            '<td>'+
                                '{!! Form::text(NULL, NULL, ['class' => 'form-control house_price']) !!}'+
                            '</td>'+
                            '<td>'+
                                '{!! Form::text(NULL, NULL, ['class' => 'form-control house_subtotal']) !!}'+
                            '</td>'+
                        '</tr>');
                        init_category_select2();
                });
            // Add Product Line End ------------------------------------

            // changes start --------------------------------------
                $('body').on('change', '.product_qty, .house_price', function(){
                    calculatePrices();
                });

                // if categories_select gets open clear out all options to each product_select.
                $('body').on('change', '.categories_select', function(){
                    $('.product_select').html('');
                    product_array = [];
                    $('.supplier_select').html('');
                });
            // changes end ----------------------------------------

            // Delete Row Start ----------------------------------------
                $('body').on('click', '.delete_line', function(){
                    var product_line = $(this).closest('tr.product_line');
                    swal({
                        title: "Are you sure?",
                        text: "Are you sure you want to remove this line item?",
                        type: "warning",
                        animation: false,
                        showCancelButton: true,
                        confirmButtonColor: "#D9534F",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: true
                    },
                    function(){
                        product_line.remove();
                        calculatePrices();
                    });
                });
            // Delete Row End ------------------------------------------

            // Submit Form ---------------------------------------------
                $('body').on('click', '#submit_btn', function(e){
                    e.preventDefault();
                    validateAndSubmitForm();
                });

                $('body').on('click', '#save_and_view_btn', function(e){
                    e.preventDefault();
                    $('#save_and_view').val(true);
                    validateAndSubmitForm();
                });
            // End Submit Form -----------------------------------------

            // Add Template Start ------------------------------------------
                $('body').on('click', '#add_template_btn', function(){
                $('#add_template').append(
                    '<div class="row">'+    
                        '<div class="col-md-12">'+
                            '<div class="box box-primary">'+
                                '<div class="box-header with-border">'+
                                    '<h3 class="box-title">Template</h3>'+
                                    '<div class="box-tools pull-right">'+
                                        '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="box-body">'+
                                    '<div class="col-md-12">'+
                                        '<div class="form-group">'+
                                            '{!! Form::label(NULL, 'Template Name') !!}'+
                                            '{!! Form::text(NULL, null, ['class' => 'form-control', 'id' => 'template_name', 'placeholder' => 'Create Template Name']) !!}'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
                });
            // Add Template End --------------------------------------------

            // Tabbing Code ------------------------------------------------
                
                $('body').on('select2:close', 'select', function () {
                        $(this).focus();
                    }
                );

                $('body').on('keyup', '.house_subtotal', function(e){
                    if(shiftTab()) {
                       // Focus previous input
                       $('.product_qty:last').focus();
                    } else {
                       // Focus next input
                        $('.add-item-btn').focus();
                    }
                });

                $('body').on('keydown', '.add-item-btn', function(e){
                    if(shiftTab()) {
                       // Focus previous input
                       $('.house_subtotal:last').focus();
                    }
                });

                function shiftTab(evt) {
                    var e = event || evt; // for trans-browser compatibility
                    var pressedKeyCode = e.which || e.keyCode; // for trans-browser compatibility
                    if (pressedKeyCode === 9) {
                        if (e.shiftKey) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

            // End Tabbing Code --------------------------------------------

        });


    </script>

@stop