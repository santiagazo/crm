@extends('layouts.auth.main')

@section('title')
Edit Bid Material List
@endsection

@section('css')
    <link rel="stylesheet" href="{{ url('auth/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/dropzone/dist/dropzone.css') }}">
    <style>
        .finalize-materialable-btn,
        .add-item-btn{
            margin-top: 10px;
            margin-bottom: 10px;
            margin-right: 10px;
        }
        .material_list-table{
            margin-bottom: 0;
        }
        .table>tbody>tr>td {
            padding: 0px;
        }
        table .select2-container--default .select2-selection--single,
        table .select2-selection .select2-selection--single {
            border: none;
            border-radius: 0;
            padding: 5px 5px;
            height: 34px;
        }
        .select2-dropdown {
            border: 1px solid #f4f4f4;
        }
        .select2-container--default .select2-search--dropdown .select2-search__field {
            border: 1px solid #f4f4f4;
        }
        table .form-control {
            border: none;
        }
        .id-column{
            width: 5%;
        }
        .product-column{
            width: 40%;
        }
        .qty-column{
            width: 10%;
        }
        .supplier-column{
            width: 10%;
        }
        .house-column{
            width: 10%;
        }
        td i.fa{
            margin: 10px;
        }
        .table>tbody>tr>td,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th {
            padding: 0;
            line-height: 1.42857143;
            vertical-align: top;
        }
        .table-bordered>tbody>tr:last-child>td {
            border-bottom: 2px solid #323232;
        }
        .material_list-header{
            background-color: #3c8dbc;
            color: #fff;
        }
        .fa-times.text-muted{
            cursor: pointer;
        }
        .supplier_select~.select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 22px;
        }
        table .select2-container--default .select2-selection--single,
        table .select2-selection .select2-selection--single .supplier_select{
            height: 22px;
        }
        .supplier_select~.select2-container--default .select2-selection--single .select2-selection__arrow b {
            margin-top: -4px;
        }
        .images-holder{
            margin-bottom: 15px;
        }
        .delete-image{
            position: absolute;
            top: 5px;
            right: 20px;
        }
        tfoot td .form-control,
        tfoot .tfooter-title{
            height: 25px;
            padding: 3px 12px;
        }
        .house_subtotal,
        .supplier_subtotal,
        .sub_total,
        .supplier_sub_total,
        .total,
        .supplier_total,
        .tax,
        .supplier_tax,
        .supplier_sub_difference,
        .supplier_difference{
            text-align: right;
        }

        .total,
        .supplier_total,
        .supplier_difference,
        .tfooter-title.total-title{
            font-weight: 700;
        }
        .box-header>.box-tools.template {
            position: absolute;
            right: 10px;
            top: 5px;
        }
        .select2-container--default:focus, 
        .select2-selection:focus, 
        .select2-container--default:active, 
        .select2-selection:active {
            border-top: 1px solid #3c8dbc !important;
        }
        .options{
            float: right;
            margin-top: 5px;
        }
        .w100{
            width:100px;
        }
    </style>
@stop

@section('content')
<section class="content">

        <div class="row">
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Person</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h4>{{ $material_list->materialable->person->name }}</h4>
                        <p>
                            <h5>Billing Address</h5>
                            {{ $material_list->materialable->person->address }}<br/>
                            {{ $material_list->materialable->person->city }} UT, {{ $material_list->materialable->person->zip }}
                        </p>

                        <p>
                            <a href="mailto:{{ $material_list->materialable->person->email }}">{{ $material_list->materialable->person->email }}</a>  |  <a href="tel:{{ $material_list->materialable->person->phone }}">{{ $material_list->materialable->person->phone }}</a>
                        </p>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ url('admin/people/edit/'.$material_list->materialable->person->id) }}" class="btn btn-primary btn-sm pull-right">Edit Person</a>
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bid</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>{{ $material_list->materialable->purchase_order ? 'Purchase Material: '.$material_list->materialable->purchase_order : 'Bid Id: '.$material_list->materialable->id }}</h4>
                                <p>
                                    <h5>Project Address</h5>
                                    {{ $material_list->materialable->address ?: $material_list->materialable->person->address }}<br/>
                                    {{ $material_list->materialable->city ?: $material_list->materialable->person->city }} UT, {{ $material_list->materialable->zip ?: $material_list->materialable->person->zip }}
                                </p>
                                @if($material_list->materialable->subdivision || $material_list->materialable->lot)
                                <p>
                                    {{ $material_list->materialable->subdivision ? 'Subdivision: '.$material_list->materialable->subdivision : NULL }}
                                    {{ $material_list->materialable->subdivision && $material_list->materialable->lot ? ' | ' : NULL }}
                                    {{ $material_list->materialable->lot ? 'Lot: '.$material_list->materialable->lot : NULL }}
                                </p>
                                @endif
                                <p>Job Type: {{ str_unslug($material_list->materialable->type->name) }}</p>
                            </div>
                            <div class="col-md-6">
                                <div class="date-updates">{!! $material_list->materialable->accepted_at ? '<strong>Started On: </strong>'.Carbon\Carbon::parse($material_list->materialable->accepted_at)->format('l M. j, Y') : NULL !!}</div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ url('admin/bids/edit/'.$material_list->materialable->id) }}" class="btn btn-primary btn-sm pull-right">Edit Bid</a>
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ count($material_list->materialable->media) ? "" : "Add" }} Images</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row images-holder" id="images_for_materialable_{{ $material_list->materialable->id }}">
                            @forelse($material_list->materialable->media as $image)
                                <div class="col-sm-2 image-holder" data-src="{{ url($image->url) }}">
                                    <img src="{{ url($image->url) }}" class="img-responsive" data-id="{{ $image->id }}">
                                    <btn class="btn btn-default btn-xs delete-image"><i class="fa fa-minus" data-toggle="tooltip" title="Delete Image"></i></btn>
                                </div>
                            @empty
                            @endforelse
                        </div>
                        <div id="images_bucket" class="dropzone"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <div id="add_template"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-list"></i> {{ $material_list->materialable->accepted_at ? "Material" : "Bid" }}</h3>
                        <div class="box-tools template pull-right">
                            @if($material_list->template)
                                <strong>{{ $material_list->template->name }}</strong>
                            @else
                                <button type="button" class="btn btn-sm btn-primary" id="add_template_btn">Mark as Template</button>
                            @endif
                        </div>
                    </div>
                    <div class="box-body pb-0">

                        <div class="row">
                            <div class="col-md-12 material_list-holder">
                                    <table class="table material_list-table table-bordered">
                                        <tbody data-material_list-id='NULL' class="product-holder">
                                            <tr class="material_list-header">
                                                <th class="id-column"></th>
                                                <th class="product-column">Product Category</th>
                                                <th class="qty-column w100"><span class="pull-right">Qty</span></th>
                                                <th class="price-column w100"><span class="pull-right">Price</span></th>
                                                <th class="sub-total-column w100"><span class="pull-right">Sub-Total</span></th>
                                            </tr>
                                            @foreach($material_list->categories as $category)
                                                <tr class="product_line" data-category-id="{{ $category->id }}">
                                                    <td><i class="fa fa-times text-muted delete_line"></i></td>
                                                    <td class="select2container">
                                                        {!! Form::select(NULL, [$category->id => $category->name.' | '.$category->price], $category->id, ['class' => 'form-control categories_select']) !!}
                                                    </td>
                                                    <td class="w100">
                                                        {!! Form::text(NULL, $category->pivot->qty, ['class' => 'form-control product_qty text-right', "pattern" => "[0-9]*"]) !!}
                                                    </td>
                                                    <td class="w100">
                                                        {!! Form::text(NULL, $category->pivot->category_price, ['class' => 'form-control house-price text-right']) !!}
                                                    </td>
                                                    <td class="w100">
                                                        {!! Form::text(NULL, NULL, ['class' => 'form-control house-sub-total text-right']) !!}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4"><div class="tfooter-title">Sub-Total</div></td>
                                                <td>{!! Form::text(NULL, NULL, ['class' => 'form-control sub_total']) !!}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><div class="tfooter-title">Tax</div></td>
                                                <td>{!! Form::text(NULL, NULL, ['class' => 'form-control tax']) !!}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><div class="tfooter-title total-title">Total</div></td>
                                                <td>{!! Form::text(NULL, NULL, ['class' => 'form-control total']) !!}</td>
                                            </tr>
                                        </tfoot>
                                </table>
                                <button class="btn btn-default btn-sm add-item-btn">Add Item</button>
                                <div class="options">
                                    {!! Form::open( ['url' => 'admin/material_lists/bid/'.$material_list->id, 'id' => 'material_listForm'] ) !!}
                                        <ul class="list-inline">
                                            <li><strong>Options: </strong></li>
                                            <li class="checkbox">
                                                <label>
                                                    {{ Form::checkbox('is_pre_dry_in', true, $material_list->materialable->is_pre_dry_in ) }} Pre Dry-In
                                                </label>
                                            </li>
                                            <li class="checkbox">
                                                <label>
                                                    {{ Form::checkbox('is_six_nail', true, $material_list->materialable->is_six_nail ) }} Six Nail
                                                </label>
                                            </li>
                                            <li class="checkbox">
                                                <label>
                                                    {{ Form::checkbox('is_owner_tear_off', true, $material_list->materialable->is_owner_tear_off ) }} Owner Tear-Off
                                                </label>
                                            </li>
                                        </ul>
                                        <div class="hidden material_lists-form-field">
                                            {!! Form::hidden('line_items', NULL, ['id' => 'line_items']) !!}
                                            {!! Form::hidden('materialable_id', $material_list->materialable->id, ['id' => 'materialable_id']) !!}
                                            {!! Form::hidden('template', NULL, ['id' => 'template']) !!}
                                            {!! Form::hidden('update_and_view', false, ['id' => 'update_and_view']) !!}
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <a href="{{ URL::previous() }}" class="btn btn-default">Cancel</a>
                <div class="form-group pull-right">
                    {!! Form::submit('Update', ['class' => 'btn btn-default', 'id' => 'submit_btn']) !!}
                    <button class="btn btn-primary" id="update_and_view_btn">Update & View Bid</button>
                </div>
            </div>
        </div>


</section>


@endsection

@section('js')
    <script src="{{ url('auth/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ url('plugins/dropzone/dist/dropzone.js') }}"></script>

    <script>
        var product_array =[];

        function getSelectedCategoryIds(){
            category_ids = [];
            $.each($('tr.product_line'), function(i, row){
                var id = $(row).data('categoryId');
                if(id){
                    category_ids.push(id);
                }
            });

            return category_ids;
        }

        function setTaxPercent(tax = {{ $material_list->materialable->tax ? $material_list->materialable->tax->amount*100 : "0.00" }}){
            $('.tax').val(parseFloat(tax).toFixed(1));
        }
        setTaxPercent();

        function calculatePrices(){
            var sub_total = 0;

            $.each($('.product_line'), function(index, product){
                // find the price for house
                var qty = $(product).find('.product_qty').val();
                var price = $(product).find('.house-price').val() ? $(product).find('.house-price').val() : 0;
                var house_subtotal = qty*price;
                $(product).find('.house-sub-total').val(house_subtotal.toFixed(2));
                sub_total += parseFloat(house_subtotal.toFixed(2));
            });

            // Set the house_sub_total
            $('.sub_total').val(sub_total.toFixed(2));

            // Get the tax and set the total
            var tax = $('.tax').val()
            tax = sub_total*(tax/100);
            total = sub_total+tax;
            $('.total').val(total.toFixed(2));
        }
        calculatePrices();

        function validateAndSubmitForm(){
            var line_items = [];
            var template = $('#template_name').val();
            var validation = "";

            $.each($('.product_line'), function(index, line){
                var category_id = $(line).find('.categories_select').val();
                var qty = $(line).find('.product_qty').val();
                var house_price = $(line).find('.house-price').val();

                line_items.push({
                    "category_id" : category_id,
                    "qty" : qty,
                    "house_price" : house_price
                });

                if(!category_id){
                    validation += '<li>Please confirm there are no empty Product Categories.</li>';
                }
                if(!qty){
                    validation += '<li>Please confirm there are no empty Quantities.</li>';
                }
                if(house_price < 0.01){
                    validation += '<li>Please confirm there are no empty Prices.</li>';
                }
            });

            line_items = JSON.stringify(line_items);

            if(!line_items){
                validation += '<li>Please select at least one product category for this material list.</li>';
            }

            $('#line_items').val(line_items);
            $('#template').val(template);

            if(!validation){
                $('#material_listForm').submit();
            }else{
                jsFlash('error', validation);
            }
        }

        $(function(){
            // Category/Suppliers select2's Start --------------------------------
                function init_category_select2(){
                    $('.categories_select').each(function(i, el){
                        if(!$(this).hasClass('select2-hidden-accessible')){
                            $(this).select2({
                                placeholder: 'Add Product Category',
                                minimumInputLength: 0,
                                allowClear: false,
                                ajax: {
                                    url: "{{ url('admin/categories') }}",
                                    dataType: 'json',
                                    delay: 0,
                                    data: function (params) {
                                        return {
                                            selected_category_ids: getSelectedCategoryIds(),
                                            q: params.term, // search term
                                            page: params.page
                                        }
                                    },
                                    processResults: function (data, params) {
                                        params.page = params.page || 1;
                                        var thisSelect2 = $(this)[0].$element;

                                        return {
                                            results: data.data,
                                            thisSelect: thisSelect2,
                                            pagination: {
                                                more: (params.page * 30) < data.total_count
                                            }
                                        };
                                    },
                                    cache: true
                                },
                                escapeMarkup: function (markup) { return markup; },
                                templateResult: formatResult,
                                templateSelection: formatSelection,
                            });

                            $(this).focus();
                        }
                    });
                }
                init_category_select2();

                function formatResult (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else{
                        var $result = $('<span>' +result.name +' | '+result.price+'/'+result.unit+'</span>');
                    }

                    return $result;
                };

                function formatSelection (result, thisSelect) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else{
                        var $result = $('<span>' +result.name +' | '+result.price+'/'+result.unit+'</span>');
                        $(thisSelect).closest('tr').data('category-id', result.id).find('.house-price').val(result.price);
                        calculatePrices();
                    }
                    return $result;
                };
            // Category select2's End --------------------------------------------

            // Add Product Line Start ----------------------------------
                $('body').on('click', '.add-item-btn', function(e){
                    e.preventDefault();
                    $(this).prev().find('.product-holder').append(
                        '<tr class="product_line" data-category-id="">'+
                            '<td><i class="fa fa-times text-muted delete_line"></i></td>'+
                            '<td class="select2container">'+
                                '{!! Form::select('categories', [], NULL, ['class' => 'form-control categories_select']) !!}'+
                            '</td>'+
                            '<td>'+
                                '{!! Form::text(NULL, NULL, ['class' => 'form-control product_qty text-right', "pattern" => "[0-9]*"]) !!}'+
                            '</td>'+
                            '<td class="w100">'+
                                '{!! Form::text(NULL, $category->pivot->category_price, ['class' => 'form-control house-price text-right']) !!}'+
                            '</td>'+
                            '<td class="w100">'+
                                '{!! Form::text(NULL, NULL, ['class' => 'form-control house-sub-total text-right']) !!}'+
                            '</td>'+
                        '</tr>');
                        init_category_select2();
                });
            // Add Product Line End ------------------------------------

            // changes start --------------------------------------
                $('body').on('change', '.product_qty, .house-price', function(){
                    calculatePrices();
                });

                // if categories_select gets open clear out all options to each product_select.
                $('body').on('change', '.categories_select', function(){
                    $('.product_select').html('');
                    product_array = [];
                    $('.supplier_select').html('');
                });
            // changes end ----------------------------------------

            // Delete Row Start ----------------------------------------
                $('body').on('click', '.delete_line', function(){
                    var product_line = $(this).closest('tr.product_line');
                    swal({
                        title: "Are you sure?",
                        text: "Are you sure you want to remove this line item?",
                        type: "warning",
                        animation: false,
                        showCancelButton: true,
                        confirmButtonColor: "#D9534F",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: true
                    },
                    function(){
                        product_line.remove();
                        calculatePrices();
                    });
                });
            // Delete Row End ------------------------------------------

            // Submit Form ---------------------------------------------
                $('body').on('click', '#submit_btn', function(e){
                    e.preventDefault();
                    validateAndSubmitForm();
                });

                $('body').on('click', '#update_and_view_btn', function(e){
                    e.preventDefault();
                    $('#update_and_view').val(true);
                    validateAndSubmitForm();
                });
            // End Submit Form -----------------------------------------

            // Add Template Start ------------------------------------------
                $('body').on('click', '#add_template_btn', function(){
                $('#add_template').append(
                    '<div class="row">'+
                        '<div class="col-md-12">'+
                            '<div class="box box-primary">'+
                                '<div class="box-header with-border">'+
                                    '<h3 class="box-title">Template</h3>'+
                                    '<div class="box-tools pull-right">'+
                                        '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="box-body">'+
                                    '<div class="col-md-12">'+
                                        '<div class="form-group">'+
                                            '{!! Form::label(NULL, 'Template Name') !!}'+
                                            '{!! Form::text(NULL, null, ['class' => 'form-control', 'id' => 'template_name', 'placeholder' => 'Create Template Name']) !!}'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
                });
            // Add Template End --------------------------------------------


            // DropZone Start ------------------------------------------
                Dropzone.autoDiscover = false;

                function initDropzone(){

                    var imageDropzone = new Dropzone("#images_bucket", {
                        url: "{{ url('admin/media/'.$material_list->materialable->id) }}",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        params: {
                            id: {{ $material_list->materialable->id }},
                            type: 'bid'
                        },
                        addRemoveLinks: true,
                        uploadMultiple: false,
                        maxFilesize: 5, //MB
                        dictDefaultMessage: "Drag and Drop or Click Here to Upload up to 20 Images.",
                    });
                    imageDropzone.on("uploadprogress", function(file) {
                        // console.log(file);
                    });
                    imageDropzone.on("sending", function(file) {
                        // console.log(file);
                    });
                    imageDropzone.on("success", function(file, response){
                        // console.log('success');
                        // console.log(response);
                        // console.log(file);
                        jsFlash('success', 'Your image ('+response.title+') has been uploaded successfully.');
                    });
                    imageDropzone.on("error", function(file, response){
                        console.log('error');
                        // console.log(response.message);
                        // console.log(file);
                        // console.log(file.size);

                        var messages = "";

                        if(file.size >= 2000000){
                            messages += "<li>Please make sure your image size is under 2MB</li>";
                        }

                        if(response.message){
                            messages += "<li>"+response.message+"</li>";
                        }

                        if(response.file){
                            $.each(response.file, function(index, error){
                                messages += '<li>'+error+' ('+file.name+')</li>';
                            });
                        }

                        jsFlash('error', messages)
                    });

                }

                initDropzone();

                // Media Delete Start ----------------------------------

                    $('body').on('click', '.delete-image', function(){
                        var image_holder = $(this).closest('.image-holder');
                        var image_id = $(image_holder).find('img').data('id');

                        swal({
                            title: "Are you sure?",
                            text: "Are you sure you want to delete this image?",
                            type: "warning",
                            animation: false,
                            showCancelButton: true,
                            confirmButtonColor: "#D9534F",
                            confirmButtonText: "Yes, delete it!",
                            closeOnConfirm: true
                        },
                        function(){
                            $.ajax({
                                url: "{{ url('admin/media/delete?id=') }}"+image_id+"&type=materialable",
                                type: 'GET',
                                success: function (response) {
                                    var response = $.parseJSON(response);
                                    if (typeof response == "String") {
                                        swal(response);
                                    } else {
                                        swal("Deleted!", "You image has been deleted successfully.", "success");
                                    }
                                    $(image_holder).remove();
                                },
                                error: function (xhr) {
                                    if(xhr.code || xhr.status){
                                        var errors = $.parseJSON(xhr.responseText);
                                        var messages = '';
                                        $.each(errors, function(key, errorMessage){
                                            $.each(errorMessage, function(index, message){
                                                messages += '<li>'+message+'</li>';
                                            });
                                        });
                                        console.log(messages);
                                        jsFlash('error', messages, false);
                                    }else{
                                        console.log(xhr);
                                    }
                                }
                            });
                        });


                    });

                // Media Delete End ------------------------------------

            // DropZone End --------------------------------------------

            // Tabbing Code ------------------------------------------------
                $('body').on('select2:close', 'select', function () {
                        $(this).focus();
                    }
                );

                $('body').on('keyup', '.house_subtotal', function(e){
                    if(shiftTab()) {
                       // Focus previous input
                       $('.product_qty:last').focus();
                    } else {
                       // Focus next input
                        $('.add-item-btn').focus();
                    }
                });

                $('body').on('keydown', '.add-item-btn', function(e){
                    if(shiftTab()) {
                       // Focus previous input
                       $('.house_subtotal:last').focus();
                    }
                });

                function shiftTab(evt) {
                    var e = event || evt; // for trans-browser compatibility
                    var pressedKeyCode = e.which || e.keyCode; // for trans-browser compatibility
                    if (pressedKeyCode === 9) {
                        if (e.shiftKey) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

            // End Tabbing Code --------------------------------------------
        });


    </script>

@stop