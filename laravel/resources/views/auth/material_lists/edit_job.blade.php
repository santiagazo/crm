@extends('layouts.auth.main')

@section('title')
Edit Job Material List
@endsection

@section('css')
    <link rel="stylesheet" href="{{ url('auth/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/dropzone/dist/dropzone.css') }}">
    <style>
        .finalize-bid-btn,
        .add-item-btn{
            margin-top: 10px;
            margin-bottom: 10px;
            margin-right: 10px;
        }
        .material_list-table{
            margin-bottom: 0;
        }
        .table>tbody>tr>td {
            padding: 0px;
        }
        table .select2-container--default .select2-selection--single,
        table .select2-selection .select2-selection--single {
            border: none;
            border-radius: 0;
            padding: 5px 5px;
            height: 34px;
        }
        .select2-dropdown {
            border: 1px solid #f4f4f4;
        }
        .select2-container--default .select2-search--dropdown .select2-search__field {
            border: 1px solid #f4f4f4;
        }
        table .form-control {
            border: none;
        }
        .id-column{
            width: 5%;
        }
        .product-column{
            width: 40%;
        }
        .qty-column{
            width: 10%;
        }
        .supplier-column{
            width: 10%;
        }
        .house-column{
            width: 10%;
        }
        td i.fa{
            margin: 10px;
        }
        .table>tbody>tr>td,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th {
            padding: 0;
            line-height: 1.42857143;
            vertical-align: top;
        }
        .table-bordered>tbody>tr:last-child>td {
            border-bottom: 2px solid #323232;
        }
        .material_list-header{
            background-color: #3c8dbc;
            color: #fff;
        }
        .fa-times.text-muted{
            cursor: pointer;
        }
        .supplier_select~.select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 22px;
        }
        table .select2-container--default .select2-selection--single,
        table .select2-selection .select2-selection--single .supplier_select{
            height: 22px;
        }
        .supplier_select~.select2-container--default .select2-selection--single .select2-selection__arrow b {
            margin-top: -4px;
        }
        .images-holder{
            margin-bottom: 15px;
        }
        .delete-image{
            position: absolute;
            top: 5px;
            right: 20px;
        }
        tfoot td .form-control,
        tfoot .tfooter-title{
            height: 25px;
            padding: 3px 12px;
        }
        .house_subtotal,
        .supplier_subtotal,
        .house_sub_total,
        .supplier_sub_total,
        .house_total,
        .supplier_total,
        .house_tax,
        .supplier_tax,
        .supplier_sub_difference,
        .supplier_difference{
            text-align: right;
        }

        .house_total,
        .supplier_total,
        .supplier_difference,
        .tfooter-title.total-title{
            font-weight: 700;
        }
        .options{
            float: right;
            margin-top: 5px;
        }
        .options .checkbox{
            margin-top: 0px; 
            margin-bottom: 0px; 
        }
        .options .small{
            margin-left: 5px; 
        }
    </style>
@stop

@section('content')


<section class="content">

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Person</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <h4>{{ $material_list->materialable->person->name }}</h4>
                    <p>
                        <h5>Billing Address</h5>
                        {{ $material_list->materialable->person->address }}<br/>
                        {{ str_unslug($material_list->materialable->person->city) }} UT, {{ $material_list->materialable->person->zip }}
                    </p>

                    <p>
                        <a href="mailto:{{ $material_list->materialable->person->email }}">{{ $material_list->materialable->person->email }}</a>  |  <a href="tel:{{ $material_list->materialable->person->phone }}">{{ $material_list->materialable->person->phone }}</a>
                    </p>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ url('admin/people/edit/'.$material_list->materialable->person->id) }}" class="btn btn-primary btn-sm pull-right">Edit Person</a>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Job</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Purchase Order: {{ $material_list->materialable->purchase_order }}</h4>
                            <p>
                                <h5>Project Address</h5>
                                {{ $material_list->materialable->address ?: $material_list->materialable->person->address }}<br/>
                                {{ $material_list->materialable->city ? str_unslug($material_list->materialable->city) : str_unslug($material_list->materialable->person->city) }} UT, 
                                {{ $material_list->materialable->zip ?: $material_list->materialable->person->zip }}
                            </p>
                            @if($material_list->materialable->subdivision || $material_list->materialable->lot)
                            <p>
                                {{ $material_list->materialable->subdivision ? 'Subdivision: '.$material_list->materialable->subdivision : NULL }}
                                {{ $material_list->materialable->subdivision && $material_list->materialable->lot ? ' | ' : NULL }}
                                {{ $material_list->materialable->lot ? 'Lot: '.$material_list->materialable->lot : NULL }}
                            </p>
                            @endif
                            <p>Job Type: {{ str_unslug($material_list->materialable->type->name) }}</p>
                        </div>
                        <div class="col-md-6">
                            <div class="date-updates">{!! $material_list->materialable->accepted_at ? '<strong>Started On: </strong>'.Carbon\Carbon::parse($material_list->materialable->accepted_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="date-updates">{!! $material_list->materialable->ordered_at ? '<strong>Ordered On: </strong>'.Carbon\Carbon::parse($material_list->materialable->ordered_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $material_list->materialable->delivery_at ? '<strong>Delivery On: </strong>'.Carbon\Carbon::parse($material_list->materialable->delivery_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $material_list->materialable->start_at ? '<strong>Starts On: </strong>'.Carbon\Carbon::parse($material_list->materialable->start_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="text-right">{!! $material_list->materialable->end_at ? '<strong>Ends On: </strong>'.Carbon\Carbon::parse($material_list->materialable->end_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="date-updates">{!! $material_list->materialable->completed_at ? '<strong>Completed On: </strong>'.Carbon\Carbon::parse($material_list->materialable->completed_at)->format('l M. j, Y') : NULL !!}</div>
                            <div class="date-updates">{!! $material_list->materialable->invoiced_at ? '<strong>Invoiced On: </strong>'.Carbon\Carbon::parse($material_list->materialable->invoiced_at)->format('l M. j, Y') : NULL !!}</div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ url('admin/jobs/edit/'.$material_list->materialable->id) }}" class="btn btn-primary btn-sm pull-right">Edit Job</a>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ count($material_list->materialable->media) ? "" : "Add" }} Images</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row images-holder" id="images_for_job_{{ $material_list->materialable->id }}">
                        @forelse($material_list->materialable->media as $image)
                            <div class="col-sm-2 image-holder" data-src="{{ url($image->url) }}">
                                <img src="{{ url($image->url) }}" class="img-responsive" data-id="{{ $image->id }}">
                                <btn class="btn btn-default btn-xs delete-image"><i class="fa fa-minus" data-toggle="tooltip" title="Delete Image"></i></btn>
                            </div>
                        @empty
                        @endforelse
                    </div>
                    <div id="images_bucket" class="dropzone"></div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>




    <div id="add_template"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-list"></i> Job Material List</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-sm btn-primary" id="add_template_btn">Mark as Template</button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-12 material_list-holder">
                            <table class="table material_list-table table-bordered">
                                <tbody data-material_list-id='NULL' class="product-holder">
                                    <tr class="material_list-header">
                                        <th class="id-column"></th>
                                        <th class="product-column">Product Category</th>
                                        <th class="qty-column">Qty</th>
                                        <th class="house-column">House Price</th>
                                        <th nowrap class="house-subtotal-column">House Subtotal</th>
                                        <th nowrap class="supplier-column">Supplier Subtotal</th>
                                        <th class="supplier-select-column">{!! Form::select(NULL, [], NULL,
                                            ['class' => 'form-control supplier_select']) !!}</th>
                                        <th class="supplier-color-column">Color</th>
                                    </tr>
                                    @foreach($material_list->categories as $category)
                                        @php
                                            
                                            $selected_product_or_category_id = NULL;
                                            
                                            if($category->pivot->product_id){
                                                $selected_product_or_category_id = $category->pivot->product_id;
                                            }elseif($category->is_house_product){
                                                $selected_product_or_category_id = 'in_house_product|'.$category->id;
                                            }elseif($category->pivot->is_house_product){
                                                $selected_product_or_category_id = 'in_house_product|'.$category->id;
                                            }

                                        @endphp
                                        <tr class="product_line" data-category-id="{{ $category->id }}">
                                            <td><i class="fa fa-times text-muted delete_line"></i></td>
                                            <td class="select2container">
                                                {!! Form::select(NULL, [$category->id => $category->name.' | '.$category->price], $category->id, ['class' => 'form-control categories_select']) !!}
                                            </td>
                                            <td>
                                                {!! Form::text(NULL, $category->pivot->qty, ['class' => 'form-control product_qty', "pattern" => "[0-9]*"]) !!}
                                            </td>
                                            <td>
                                                {!! Form::text(NULL, $category->pivot->category_price, ['class' => 'form-control house_price']) !!}
                                            </td>
                                            <td>
                                                {!! Form::text(NULL, $category->pivot->category_price, ['class' => 'form-control house_subtotal']) !!}
                                            </td>
                                            <td>
                                                {!! Form::text(NULL, $category->pivot->supplier_price ?: NULL, ['class' => 'form-control supplier_subtotal']) !!}
                                            </td>
                                            <td>
                                                {!! Form::select(NULL, [], NULL,
                                                    ['class' => 'form-control product_select', 'data-product-id' => $selected_product_or_category_id ?: NULL]) !!}
                                            </td>
                                            <td>
                                                {!! Form::select(NULL, [], NULL, ['class' => 'form-control color_select', 'data-color-id' => $category->pivot->color ?: NULL]) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4"><div class="tfooter-title">Sub-Total</div></td>
                                        <td>{!! Form::text(NULL, NULL, ['class' => 'form-control house_sub_total']) !!}</td>
                                        <td>{!! Form::text(NULL, NULL, ['class' => 'form-control supplier_sub_total']) !!}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><div class="tfooter-title">Tax</div></td>
                                        <td>{!! Form::text(NULL, NULL, ['class' => 'form-control house_tax']) !!}</td>
                                        <td>{!! Form::text(NULL, NULL, ['class' => 'form-control supplier_tax']) !!}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><div class="tfooter-title total-title">Total</div></td>
                                        <td>{!! Form::text(NULL, NULL, ['class' => 'form-control house_total']) !!}</td>
                                        <td>{!! Form::text(NULL, NULL, ['class' => 'form-control supplier_total']) !!}</td>
                                        <td>{!! Form::text(NULL, NULL, ['class' => 'form-control supplier_difference']) !!}</td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                            <button class="btn btn-default btn-sm add-item-btn">Add Item</button>
                            <div class="options">
                            {!! Form::open( ['url' => 'admin/material_lists/job/'.$material_list->id, 'id' => 'material_listForm'] ) !!}
                                <ul class="list-inline">
                                    <li><strong>Client Selected: </strong></li>
                                    <li class="checkbox">
                                        <label>
                                            {{ Form::checkbox('is_cash_discount', true, $material_list->materialable->is_cash_discount) }} 3% Cash Discount
                                        </label>
                                    </li>
                                    <li class="checkbox">
                                        <label>
                                            {{ Form::checkbox('is_pre_dry_in', true, $material_list->materialable->is_pre_dry_in) }} Pre Dry-In
                                        </label>
                                    </li>
                                    <li class="checkbox">
                                        <label>
                                            {{ Form::checkbox('is_six_nail', true, $material_list->materialable->is_six_nail) }} Six Nail
                                        </label>
                                    </li>
                                    <li class="checkbox">
                                        <label>
                                            {{ Form::checkbox('is_owner_tear_off', true, $material_list->materialable->is_owner_tear_off) }} Owner Tear-Off
                                        </label>
                                    </li>
                                    <div class="small">Change options above to show or hide on invoice</div>
                                </ul>
                                <div class="hidden material_lists-form-field">
                                    {!! Form::hidden('line_items', NULL, ['id' => 'line_items']) !!}
                                    {!! Form::hidden('supplier_id', NULL, ['id' => 'supplier_id']) !!}
                                    {!! Form::hidden('materialable_id', $material_list->materialable->id, ['id' => 'job_id']) !!}
                                    {!! Form::hidden('template', NULL, ['id' => 'template']) !!}
                                    {!! Form::hidden('update_and_view', false, ['id' => 'update_and_view']) !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            <a href="{{ URL::previous() }}" class="btn btn-default">Cancel</a>
            <div class="form-group pull-right">
                {!! Form::submit('Update', ['class' => 'btn btn-default', 'id' => 'submit_btn']) !!}
                <button class="btn btn-primary" id="update_and_view_btn">Update & View Job</button>
            </div>
        </div>
    </div>

</section>


@endsection

@section('js')
    <script src="{{ url('auth/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ url('plugins/dropzone/dist/dropzone.js') }}"></script>

    <script>
        var product_array =[];

        function getSelectedCategoryIds(){
            category_ids = [];
            $.each($('tr.product_line'), function(i, row){
                var id = $(row).data('categoryId');
                if(id){
                    category_ids.push(id);
                }
            });

            return category_ids;
        }

        function setTaxPercent(tax = {{ $material_list->materialable->tax ? $material_list->materialable->tax->amount*100 : '0.00' }}){
            $('.house_tax').val(parseFloat(tax).toFixed(1));
            $('.supplier_tax').val(parseFloat(tax).toFixed(1));
        }
        setTaxPercent();

        function calculatePrices(){
            var house_sub_total = 0;
            var supplier_sub_total = 0;

            $.each($('.product_line'), function(index, product){
                // find the price for house
                var qty = $(product).find('.product_qty').val();
                var price = $(product).find('.house_price').val() ? $(product).find('.house_price').val() : 0;
                var house_subtotal = qty*price;
                $(product).find('.house_subtotal').val(house_subtotal.toFixed(2));
                house_sub_total += parseFloat(house_subtotal.toFixed(2));

                // find the price for supplier
                var supplier_price = $(product).find('.product_select').val();
                var supplier_subtotal = qty*supplier_price;
                $(product).find('.supplier_subtotal').val(supplier_subtotal.toFixed(2));
                supplier_sub_total += parseFloat(supplier_subtotal.toFixed(2));
            });

            // Set the house_sub_total
            $('.house_sub_total').val(house_sub_total.toFixed(2));

            // Set the supplier_sub_total
            $('.supplier_sub_total').val(supplier_sub_total.toFixed(2));

            // Get the tax and set the house_total
            var house_tax = $('.house_tax').val()
            house_tax = house_sub_total*(house_tax/100);
            house_total = house_sub_total+house_tax;
            $('.house_total').val(house_total.toFixed(2));

            // Get the tax and set the supplier_total
            var supplier_tax = $('.supplier_tax').val()
            supplier_tax = supplier_sub_total*(supplier_tax/100);
            supplier_total = supplier_sub_total+supplier_tax;
            $('.supplier_total').val(supplier_total.toFixed(2));

            // Get the total_differencew and set it to supplier_difference
            var total_difference = parseInt(house_total) - parseInt(supplier_total);
            $('.supplier_difference').val(total_difference.toFixed(2));
        }

        function createProductSelects(products){
            $.each($('.product_line'), function(index, line){ // loop through each line item
                var in_house_product_count = 0; // we can have up to one in_house_product / line.
                $.each(products, function(i, product){ // loop through each product
                    if($(line).data('categoryId') == product.category_id){ // if the line item category matches the product category
                        if($.inArray(product.id, product_array) == -1){ // if we haven't already listed the product in the select
                            if(product.category.is_house_product && in_house_product_count < 1){ // if the products category is an in house product default then list it first.
                                $(line).find('.product_select').append(
                                '<option data-product-id="in_house_product|'+product.category.id+'" value="'+product.category.price+'">In House Product | '+product.category.price+'</option>'); // add the category pricing it to the select as the first item.
                                in_house_product_count++
                            }
                            $(line).find('.product_select').append(
                            '<option data-product-id="'+product.id+'" value="'+product.supplier_price+'">'+product.name+' | '+product.supplier_price+'/'+product.unit+'</option>'); // add it to the select
                            product_array.push(product.id); // make sure we can check for the new item in the product_array.
                            // Add an In House Product to each select if it's not already the default.
                            if(!product.category.is_house_product && in_house_product_count < 1){
                                $(line).find('.product_select').append(
                                '<option data-product-id="in_house_product|'+product.category.id+'" value="'+product.category.price+'">In House Product | '+product.category.price+'</option>'); 
                                in_house_product_count++
                            }
                        }
                    }
                });
            });

            // add the category price if there is no product price available.
            $.each($('.product_line'), function(index, line){ // loop through each line item
                if($(line).find('.product_select').find('option').length == 0){ // figure out if it has any options
                    var price = $(line).find('.house_price').val(); // since it doesn't get the price and
                    if(price){
                        $(line).find('.product_select').append('<option data-product-id="0" value="'+price+'">Category Price | '+price+'</option>'); // add it to the select
                    }
                }
                var this_lines_selected_option = $(line).find('.product_select').attr('data-product-id');
                if(this_lines_selected_option){
                    $(line).find('.product_select option[data-product-id="'+this_lines_selected_option+'"]').prop('selected', true);
                }
                init_color_select2($(line).find('.product_select'));
            });

            $('.product_select').select2();
            setProductIdOnSelects();
            calculatePrices();
        }

        function setProductIdOnSelects(){
            // get the product_id of the newly selected product options and add it to the
            // data-product-id of the select for later reference (on submit).
            $.each($('.product_line'), function(index, line){
                var select = $(line).find('.product_select');
                if($(select).hasClass('select2-hidden-accessible')){
                    var select = $(select);
                    var selected = $(select).select2("data");
                    if(select.data('productId')){
                        // EDIT PAGE ONLY - check to see if we've already added a data-product-id to the select from php
                        // and write it here so we don't overwrite it with 0 or anything like that.
                        var product_id = select.data('productId')
                    }else if(selected[0] && selected[0].element.dataset.productId){
                        var product_id = selected[0].element.dataset.productId;
                    }else if(selected[0]){
                        var product_id = selected[0].id;
                    }else{
                        var product_id = 0;
                    }
                    $(select).data('product-id', product_id);
                }
            });
        }

        function validateAndSubmitForm(){
            var line_items = [];
            var supplier_id = $('.supplier_select').val();
            var template = $('#template_name').val();
            var validation = "";

            $.each($('.product_line'), function(index, line){
                var category_id = $(line).find('.categories_select').val();
                var qty = $(line).find('.product_qty').val();
                var house_price = $(line).find('.house_price').val();
                var supplier_subtotal = $(line).find('.supplier_subtotal').val();
                var select_data = $(line).find('.product_select').select2("data");
                var product_id = select_data[0].element.dataset.productId ? select_data[0].element.dataset.productId : select_data[0].id;
                var product_color = $(line).find('.color_select').val();

                line_items.push({
                    "category_id" : category_id,
                    "qty" : qty,
                    "house_price" : house_price,
                    "supplier_subtotal" : supplier_subtotal,
                    "product_id" : product_id,
                    "color" : product_color
                });

                if(!category_id){
                    validation += '<li>Please confirm there are no empty Product Categories.</li>';
                }
                if(!qty){
                    validation += '<li>Please confirm there are no empty Quantities.</li>';
                }
                if(house_price == 0.00){
                    validation += '<li>Please confirm there are no empty House Prices.</li>';
                }
                if(supplier_subtotal == 0.00){
                    validation += '<li>Please confirm there are no empty Supplier Prices.</li>';
                }
            });

            line_items = JSON.stringify(line_items);

            if(!supplier_id){
                validation += '<li>A supplier must be selected.</li>';
            }

            if(!line_items){
                validation += "<li>Please select at least one product category for this job's material list.</li>";
            }

            $('#line_items').val(line_items);
            $('#supplier_id').val(supplier_id);
            $('#template').val(template);

            if(!validation){
                $('#material_listForm').submit();
            }else{
                jsFlash('error', validation);
            }
        }

        // Colors select2 Start ----------------------------------------------
            function init_color_select2(product_select){
                var in_house_product = [];
                var category_id = '';
                if($(product_select).hasClass('select2-hidden-accessible')){
                    var selected = $(product_select).select2("data");
                    if(selected[0] && selected[0].element.dataset.productId.search('in_house_product') !== -1){
                        // this is a in_house_product
                        in_house_product = selected[0].element.dataset.productId.split("|");
                        var category_id = in_house_product[1];
                    }else if(selected[0] && selected[0].element.dataset.productId){
                        var product_id = selected[0].element.dataset.productId;
                    }else if(selected[0]){
                        // get the id on an item coming from db;
                        var product_id = selected[0].id;
                    }else{
                        var product_id = 0;
                    }
                }
                var color_select = $(product_select).closest('tr.product_line').find('.color_select');
                var color_array = [];

                if(product_id == 0 && !category_id || typeof product_id == "undefined" && !category_id){ // check to see if there is a product id && no category id (in_house_product).
                    // the product values that aren't id's but instead prices.
                    $(color_select).html('<option value="">Colors N/A</option>');
                    color_select.select2();
                    return;
                }

                if(product_id && !category_id){
                    var url = "{{ url('admin/products/colors') }}/"+product_id;
                }else{
                    var url = "{{ url('admin/products/colors') }}/"+category_id+"?type=category";
                }

                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (response) {
                        var response = $.parseJSON(response);
                        if (typeof response == "object") {
                            if(response.length){
                                $(color_select).html('');
                                var selected_color = $(color_select).data('colorId') ? $(color_select).data('colorId') : 0;
                                $.each(response, function(i, color){
                                    if($.inArray(color.id, color_array) == -1){ // if we haven't already listed the color in the select
                                        if(selected_color != color.id){
                                            $(color_select).append('<option value="'+color.id+'">'+color.text+'</option>'); // then go ahead an list it and
                                        }else{
                                            $(color_select).append('<option value="'+color.id+'" selected>'+color.text+'</option>'); // then go ahead an list it and
                                        }
                                        color_array.push(color.id); // make sure we can check for the new item in the color_array.
                                    }
                                });
                                $(color_select).select2(); // initiate the select

                            }else{
                                $(color_select).html('<option value="">Colors N/A</option>');
                                color_select.select2();
                            }
                        }

                    },
                    error: function (xhr) {
                        if(xhr.status == 422){
                            var errors = $.parseJSON(xhr.responseText);
                            var messages;
                            $.each(errors, function(key, errorMessage){
                                $.each(errorMessage, function(index, message){
                                    messages += '<li>'+message+'</li>';
                                });
                            });
                            jsFlash('error', messages, false);
                        }else{
                            console.log(xhr);
                        }
                    }
                });
            }
        // Colors select2 End ------------------------------------------------

        $(function(){
            // Category/Suppliers select2's Start --------------------------------
                function init_category_select2(){
                    $('.categories_select').each(function(i, el){
                        if(!$(this).hasClass('select2-hidden-accessible')){
                            $(this).select2({
                                placeholder: 'Add Product Category',
                                minimumInputLength: 0,
                                allowClear: false,
                                ajax: {
                                    url: "{{ url('admin/categories') }}",
                                    dataType: 'json',
                                    delay: 0,
                                    data: function (params) {
                                        return {
                                            selected_category_ids: getSelectedCategoryIds(),
                                            q: params.term, // search term
                                            page: params.page
                                        }
                                    },
                                    processResults: function (data, params) {
                                        params.page = params.page || 1;
                                        return {
                                            results: data.data,
                                            pagination: {
                                                more: (params.page * 30) < data.total_count
                                            }
                                        };
                                    },
                                    cache: true
                                },
                                escapeMarkup: function (markup) { return markup; },
                                templateResult: formatResult,
                                templateSelection: formatSelection,
                            });
                        }
                        $(this).focus();
                    });
                }

                function formatResult (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else{
                        var $result = $('<span>' +result.name +' | '+result.price+'/'+result.unit+'</span>');
                    }

                    return $result;
                };

                function formatSelection (result, thisSelect) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else{
                        var $result = $('<span>' +result.name +' | '+result.price+'/'+result.unit+'</span>');
                        $(thisSelect).closest('tr').data('category-id', result.id).find('.house_price').val(result.price);

                        calculatePrices();
                    }
                    return $result;
                };
            // Category select2's End --------------------------------------------

            // Suppliers select2's Start -----------------------------------------
                function init_supplier_select2(){
                    var url = "{{ url('admin/companies') }}";

                    $('.supplier_select').select2({
                        placeholder: 'Suppliers',
                        minimumInputLength: 0,
                        allowClear: false,
                        initSelection: function (element, callback)
                        {
                            var name = '{{ $material_list->company ? $material_list->company->name : NULL }}';
                            if (name !== "" && name !== 0){
                                $.ajax(url,
                                    {
                                        data: {q: name},
                                        dataType: "json"
                                    }).done(function (data){
                                        $.each(data.data, function (i, value){                    
                                            $('.supplier_select').append('<option value='+value.id+' selected>'+value.name+'</option>');                    
                                            callback(value);
                                        });
                                    ;
                                });
                            }
                        },
                        ajax: {
                            url: url,
                            dataType: 'json',
                            delay: 0,
                            data: function (params) {
                                return {
                                    q: params.term, // search term
                                    suppliers: 'true',
                                    page: params.page
                                }
                            },
                            processResults: function (data, params) {
                                params.page = params.page || 1;
                                // if supplier_select gets open clear out all options to each select
                                $('.product_select').html('');
                                // and reset the product_array.
                                product_array = [];
                                return {
                                    results: data.data,
                                    pagination: {
                                        more: (params.page * 30) < data.total_count
                                    }
                                };
                            },
                            cache: true
                        },
                        escapeMarkup: function (markup) { return markup; },
                        templateResult: formatSupplierResult,
                        templateSelection: formatSupplierSelection,
                    });
                }
                init_supplier_select2();

                function formatSupplierResult (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else{
                        var $result = $('<span>' +result.name +'</span>');
                    }

                    return $result;
                };

                function formatSupplierSelection (result) {
                    if(result.text){
                        var $result = $('<span>' +result.text +'</span>');
                    }else{
                        var $result = $('<span>' +result.name +'</span>');
                    }
                    createProductSelects(result.products);

                    return $result;
                };

                // When working with the edit page something unique has to happen...
                    // 1st we use initSelection (4.0.3 mod - this is not 4.0 native code) to populate ONLY the selected option.
                    // 2nd at this point select2 will ONLY return the ID, TEXT, and TITLE for the selected option bc they're already given in the initSelection populated list.
                    // 3rd we destroy that list by clearing all options and allow the select2 to build the list dynamically so that it grabs the full company object with product relations.
                var supplier_select_clicks = 0;
                $('body').on('click', '.supplier-select-column .select2-container, .select2-selection__clear, .select2-selection__arrow', function(){
                    if(supplier_select_clicks < 1){
                        $('.supplier_select').html('');
                        supplier_select_clicks++;
                    }
                });
            // Suppliers select2's End -------------------------------------------

            // Add Product Line Start ----------------------------------
                $('body').on('click', '.add-item-btn', function(e){
                    e.preventDefault();
                    $(this).prev().find('.product-holder').append(
                        '<tr class="product_line" data-category-id="">'+
                            '<td><i class="fa fa-times text-muted delete_line"></i></td>'+
                            '<td class="select2container">'+
                                '{!! Form::select('categories', [], NULL, ['class' => 'form-control categories_select']) !!}'+
                            '</td>'+
                            '<td>'+
                                '{!! Form::text(NULL, NULL, ['class' => 'form-control product_qty', "pattern" => "[0-9]*"]) !!}'+
                            '</td>'+
                            '<td>'+
                                '{!! Form::text(NULL, NULL, ['class' => 'form-control house_price']) !!}'+
                            '</td>'+
                            '<td>'+
                                '{!! Form::text(NULL, NULL, ['class' => 'form-control house_subtotal']) !!}'+
                            '</td>'+
                            '<td>'+
                                '{!! Form::text(NULL, NULL, ['class' => 'form-control supplier_subtotal']) !!}'+
                            '</td>'+
                            '<td>'+
                                '{!! Form::select(NULL, [], NULL, ['class' => 'form-control product_select']) !!}'+
                            '</td>'+
                            '<td>'+
                                '{!! Form::select(NULL, [], NULL, ['class' => 'form-control color_select']) !!}'+
                            '</td>'+
                        '</tr>');
                        init_category_select2();
                });
            // Add Product Line End ------------------------------------

            // Changes start --------------------------------------
                $('body').on('change', '.product_qty, .product_select', function(){
                    calculatePrices();
                    setProductIdOnSelects();
                });

                $('body').on('change', '.product_select', function(){
                    init_color_select2($(this));
                });

                // if categories_select gets open clear out all options to each product_select.
                $('body').on('change', '.categories_select', function(){
                    $('.product_select').html('');
                    product_array = [];
                    $('.supplier_select').html('');
                });

                $('body').on('change', '.house_price', function(){
                    calculatePrices();
                });
            // Changes end ----------------------------------------

            // Delete Row Start ----------------------------------------
                $('body').on('click', '.delete_line', function(){
                    var product_line = $(this).closest('tr.product_line');
                    swal({
                        title: "Are you sure?",
                        text: "Are you sure you want to remove this line item?",
                        type: "warning",
                        animation: false,
                        showCancelButton: true,
                        confirmButtonColor: "#D9534F",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: true
                    },
                    function(){
                        product_line.remove();
                        calculatePrices();
                    });
                });
            // Delete Row End ------------------------------------------

            // Submit Form ---------------------------------------------
                $('body').on('click', '#submit_btn', function(e){
                    e.preventDefault();
                    validateAndSubmitForm();
                });

                $('body').on('click', '#update_and_view_btn', function(e){
                    e.preventDefault();
                    $('#update_and_view').val(true);
                    validateAndSubmitForm();
                });
            // End Submit Form -----------------------------------------

            // Add Template Start
                $('body').on('click', '#add_template_btn', function(){
                $('#add_template').append(
                    '<div class="row">'+
                        '<div class="col-md-12">'+
                            '<div class="box box-primary">'+
                                '<div class="box-header with-border">'+
                                    '<h3 class="box-title">Template</h3>'+
                                    '<div class="box-tools pull-right">'+
                                        '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="box-body">'+
                                    '<div class="col-md-12">'+
                                        '<div class="form-group">'+
                                            '{!! Form::label(NULL, 'Template Name') !!}'+
                                            '{!! Form::text(NULL, $material_list->template ? $material_list->template->name : NULL, ['class' => 'form-control', 'id' => 'template_name', 'placeholder' => 'Enter Template Name ']) !!}'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
                });
            // Add Template End

            // DropZone Start ------------------------------------------
                Dropzone.autoDiscover = false;

                function initDropzone(){

                    var imageDropzone = new Dropzone("#images_bucket", {
                        url: "{{ url('admin/media/'.$material_list->materialable->id) }}",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        params: {
                            id: {{ $material_list->materialable->id }},
                            type: 'job'
                        },
                        addRemoveLinks: true,
                        uploadMultiple: false,
                        maxFilesize: 5, //MB
                        dictDefaultMessage: "Drag and Drop or Click Here to Upload up to 20 Images.",
                    });
                    imageDropzone.on("uploadprogress", function(file) {
                        // console.log(file);
                    });
                    imageDropzone.on("sending", function(file) {
                        // console.log(file);
                    });
                    imageDropzone.on("success", function(file, response){
                        // console.log('success');
                        // console.log(response);
                        // console.log(file);
                        jsFlash('success', 'Your image ('+response.title+') has been uploaded successfully.');
                    });
                    imageDropzone.on("error", function(file, response){
                        console.log('error');
                        // console.log(response.message);
                        // console.log(file);
                        // console.log(file.size);

                        var messages = "";

                        if(file.size >= 2000000){
                            messages += "<li>Please make sure your image size is under 2MB</li>";
                        }

                        if(response.message){
                             messages += "<li>"+response.message+"</li>";
                        }

                        if(response.file){
                            $.each(response.file, function(index, error){
                                messages += '<li>'+error+' ('+file.name+')</li>';
                            });
                        }

                        jsFlash('error', messages)
                    });

                }

                initDropzone();

                // Media Delete Start ----------------------------------

                $('body').on('click', '.delete-image', function(){
                    var image_holder = $(this).closest('.image-holder');
                    var image_id = $(image_holder).find('img').data('id');

                    swal({
                        title: "Are you sure?",
                        text: "Are you sure you want to delete this image?",
                        type: "warning",
                        animation: false,
                        showCancelButton: true,
                        confirmButtonColor: "#D9534F",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: true
                    },
                    function(){
                        $.ajax({
                            url: "{{ url('admin/media/delete?id=') }}"+image_id+"&type=job",
                            type: 'GET',
                            success: function (response) {
                                var response = $.parseJSON(response);
                                if (typeof response == "String") {
                                    swal(response);
                                } else {
                                    swal("Deleted!", "You image has been deleted successfully.", "success");
                                }
                                $(image_holder).remove();
                            },
                            error: function (xhr) {
                                if(xhr.code || xhr.status){
                                    var errors = $.parseJSON(xhr.responseText);
                                    var messages = '';
                                    $.each(errors, function(key, errorMessage){
                                        $.each(errorMessage, function(index, message){
                                            messages += '<li>'+message+'</li>';
                                        });
                                    });
                                    console.log(messages);
                                    jsFlash('error', messages, false);
                                }else{
                                    console.log(xhr);
                                }
                            }
                        });
                    });


                });

                // Media Delete End ------------------------------------

            // DropZone End --------------------------------------------

            // Tabbing Code ------------------------------------------------
                
                $('body').on('select2:close', 'select', function () {
                        $(this).focus();
                    }
                );

                $('body').on('keyup', '.house_sub_total', function(e){
                    if(!shiftTab()) {
                       // Focus next input
                        $('.add-item-btn').focus();
                    }
                });

                $('body').on('keydown', '.add-item-btn', function(e){
                    if(shiftTab()) {
                       // Focus previous input
                       setTimeout(function() {
                            $('.color_select:last').focus();
                        }, 10);
                    }
                });

                function shiftTab(evt) {
                    var e = event || evt; // for trans-browser compatibility
                    var pressedKeyCode = e.which || e.keyCode; // for trans-browser compatibility
                    if (pressedKeyCode === 9) {
                        if (e.shiftKey) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }


                $('.category_select').select2().on("select2:close", function (e) {
                    $('.product_qty').focus(); // focus the next element when the select2 is closed
                });

                $('body').on('.select2container', '.category_select', function () {
                    $('.category_select').select2('open');   // open
                });

            // End Tabbing Code --------------------------------------------
        });


    </script>

@stop