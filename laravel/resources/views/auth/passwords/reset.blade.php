@extends('layouts.site.main')

@section('css')
    <style>
        body, html {
             min-height: 100px;
        }
         .tab-content{
            width: 300px;
            margin: 210px auto;
         }
    </style>
@endsection

@section('content')
<section class="ptb-60">
    <div class="container">
        <div class="row">

                <div class="tab-content">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Email Address" name="email" value="{{ old('email') }}" required autofocus>
                            <i class="form-group__bar"></i>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group"{{ $errors->has('password') ? ' has-error' : '' }}>
                            <input type="password" class="form-control" placeholder="Password" name="password" required>
                            <i class="form-group__bar"></i>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group"{{ $errors->has('password_confirmation') ? ' has-error' : '' }}>
                            <input type="password" class="form-control" placeholder="Password Confirmation" name="password_confirmation" required>
                            <i class="form-group__bar"></i>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

        </div>
    </div>
</section>
@endsection