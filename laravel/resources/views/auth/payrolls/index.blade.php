@extends('layouts.auth.main')

@section('title')
People
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .background-roof{
            background: url({{ url('guest/images/patterns/roof-shingles-dark.png') }}) no-repeat center center fixed; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        .page-center{
            padding: 375px 0;
        }
        .text-white{
            color: #fff !important;
        }
    </style>
@stop

@section('content')


        <div class="container-fluid">
            <div class="row background-roof">
                <div class="col-md-12 text-center page-center">
                    <h2 class="text-white">Payroll Coming Soon.</h2>
                </div>
            </div>
        </div>


@stop

@section('js')

@stop
