@extends('layouts.auth.main')

@section('title')
Create Person
@endsection

@section('css')

@stop

@section('content')


    {!! Form::open( ['url' => 'admin/people'] ) !!}

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create a Person</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                                <!--Text form input-->
                                <div class="col-md-4">
                                    <div class='form-group'>
                                        {!! Form::label('name', "Person's Name") !!}
                                        {!! Form::text('name', NULL, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                 <!--Select Lists() form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('company_id', 'Company') !!}
                                        {!! Form::select('company_id', [], NULL, ['class' => 'form-control company-select2']) !!}
                                    </div>
                                </div>

                                <!--Select form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('role', 'Role') !!}
                                        {!! Form::select('role', $roles, NULL, ['class' => 'form-control']) !!}
                                    </div>
                                </div>


                                <!--Select form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('address', 'Address') !!}
                                        {!! Form::text('address', NULL, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <!--Select form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('city', 'City') !!}
                                        {!! Form::select('city', array_merge(['' => 'Select City'], $taxed_cities->toArray()), NULL, ['class' => 'form-control city_select2']) !!}
                                    </div>
                                </div>

                                <!--Text form input-->
                                <div class="col-md-4">
                                    <div class='form-group'>
                                        {!! Form::label('zip', 'Zip') !!}
                                        {!! Form::text('zip', NULL, ['class' => 'form-control']) !!}
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('phone', 'Phone') !!}
                                        {!! Form::text('phone', null, ['class' => 'form-control', 'data-inputmask' => "'mask': '(999) 999-9999'", 'data-mask']) !!}
                                    </div>
                                </div>

                                <!--Textarea form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('email', 'Email') !!}
                                        {!! Form::text('email', NULL, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                </div>
            </div>

        </section>

    {!! Form::close() !!}


@stop

@section('js')
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>

        $(function(){
            $("[data-mask]").inputmask();

            $('.city_select2').select2({
                placeholder: "Select City"
            });

            $('.company-select2').select2({
                placeholder: 'Enter a Company Name',
                minimumInputLength: 0,
                escapeMarkup: function (markup) {
                    return markup;
                },
                ajax: {
                    url: "{{ url('/admin/companies') }}",
                    dataType: 'json',
                    delay: 0,
                    data: function (params) {
                      return {
                        q: params.term, // search term
                        page: params.page,
                        select2: true
                      }
                    },
                    processResults: function (data, params) {
                      params.page = params.page || 1;
                      return {
                        results: data.data,
                        pagination: {
                          more: (params.page * 30) < data.total_count
                        }
                      };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; },
                allowClear: true,
                templateResult: formatResult,
                templateSelection: formatSelection
            });

            function formatResult (result) {
                if(result.name){
                    var $result = $('<span>' +result.name +'</span>');
                }else{
                    var $result = $('<span>' +result.text +'</span>');
                }
                return $result;
            };

            function formatSelection (result) {
                if(result.name){
                    var $result = $('<span>' +result.name +'</span>');
                }else{
                    var $result = $('<span>' +result.text +'</span>');
                }
                return $result;
            };
        });

    </script>

@stop
