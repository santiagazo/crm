@extends('layouts.auth.main')

@section('title')
Edit Person
@endsection

@section('css')

@stop

@section('content')


    {!! Form::open( ['url' => 'admin/people/'.$person->id] ) !!}

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit a Person</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                                <!--Text form input-->
                                <div class="col-md-4">
                                    <div class='form-group'>
                                        {!! Form::label('name', "Person's Name") !!}
                                        {!! Form::text('name', $person->name, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                 <!--Select Lists() form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('company_id', 'Company') !!}
                                        {!! Form::select('company_id', $companies, $person->company ? $person->company->id : NULL, ['class' => 'form-control company-select2']) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('role', 'Role') !!}
                                        {!! Form::select('role', $roles, count($person->roles) ? $person->roles->first()->name : 'Client', ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <!--Select form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('address', 'Address') !!}
                                        {!! Form::text('address', $person->address, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <!--Select form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('city', 'City') !!}
                                        {!! Form::text('city', $person->city, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <!--Text form input-->
                                <div class="col-md-4">
                                    <div class='form-group'>
                                        {!! Form::label('zip', 'Zip') !!}
                                        {!! Form::text('zip', $person->zip, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('phone', 'Phone') !!}
                                        {!! Form::text('phone', $person->phone, ['class' => 'form-control', 'data-inputmask' => "'mask': '(999) 999-9999'", 'data-mask']) !!}
                                    </div>
                                </div>

                                <!--Textarea form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('email', 'Email') !!}
                                        {!! Form::text('email', $person->email, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <a href="{{ url('admin/people/delete/'.$person->id) }}" class="btn btn-danger confirm-link" data-confirm-text="Are you sure you want to delete this person?">Delete</a>
                                    {!! Form::submit('update', ['class' => 'btn btn-default pull-right']) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                </div>
            </div>

        </section>

    {!! Form::close() !!}


@stop

@section('js')
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>

        $(function(){
            $("[data-mask]").inputmask();

            $('.company-select2').select2();
        });

    </script>

@stop
