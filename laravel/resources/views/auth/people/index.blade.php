@extends('layouts.auth.main')

@section('title')
People
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
        table .form-control {
            border: none;
        }
    </style>
@stop

@section('content')


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ url('admin/people/create') }}" class="btn btn-primary pull-right">Create Person</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        {!! Form::open(['url' => 'admin/people', 'method' => 'GET', 'id' => 'people-form']) !!}
                                            <th width="120px"></th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('company_id', $company_id, ['placeholder' => 'Company', 'class' => 'form-control company_id']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('name', $name, ['placeholder' => 'Name', 'class' => 'form-control name']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('address', $address, ['placeholder' => 'Address', 'class' => 'form-control address']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('city', $city, ['placeholder' => "City", 'class' => 'form-control city']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('zip', $zip, ['placeholder' => 'Zip', 'class' => 'form-control zip']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('phone', $phone, ['placeholder' => 'Phone', 'class' => 'form-control phone']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('email', $email, ['placeholder' => 'Email', 'class' => 'form-control email']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('roles', $roles, ['placeholder' => 'Roles', 'class' => 'form-control roles']) !!}
                                                </div>
                                            </th>

                                            <input type="submit" style="visibility: hidden; position: absolute;" />

                                        {!! Form::close() !!}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($people))
                                        @foreach($people as $person)
                                            <tr>
                                                <td>
                                                    <a href="{{ url('admin/people/show/'.$person->id) }}" class="btn btn-primary btn-xs responsive-button" data-toggle="tooltip" title="Show Person">
                                                        <i class="fa fa-user"></i>
                                                    </a>
                                                    <a href="{{ url('admin/people/edit/'.$person->id) }}" class="btn btn-primary btn-xs responsive-button" data-toggle="tooltip" title="Edit Person">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    @if(count($person->jobs))
                                                        <a href="{{ url('admin/jobs/edit/'.$person->jobs->last()->id) }}" class="btn btn-success btn-xs responsive-button" data-toggle="tooltip" title="Edit Job">
                                                            <i class="fa fa-home"></i>
                                                        </a>
                                                    @else
                                                        <a href="{{ url('admin/people/show/'.$person->id) }}" class="btn btn-default btn-xs responsive-button" data-toggle="tooltip" title="View Estimate Requests">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <a href="{{ url('admin/bids/create/'.$person->id) }}" class="btn btn-default btn-xs responsive-button" data-toggle="tooltip" title="Create New Bid">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $person->company ? $person->company->name : NULL }}
                                                </td>
                                                <td>
                                                    {{ $person->name }}
                                                </td>
                                                <td>
                                                    {{ $person->address }}
                                                </td>
                                                <td>
                                                    {{ $person->city }}
                                                </td>
                                                <td>
                                                    {{ $person->zip }}
                                                </td>
                                                <td>
                                                    {{ $person->phone }}
                                                </td>
                                                <td>
                                                    {{ $person->email }}
                                                </td>
                                                <td>
                                                    @foreach($person->roles as $role)
                                                        <span class="label label-sm label-primary">{{ $role->name }}</span>
                                                    @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8">
                                                There are no people built yet, click Create Person to get one started.
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {!! $people->links() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>


@stop

@section('js')

    <script>

        $(function(){
            $('body').on('keyup', '.company_id, .name, .address, .city, .state, .zip, .phone, .email', function(){
                var input = $(this);
                window.timer=setTimeout(function(){ // setting the delay for each keypress
                    timedAjaxRequest(input); //runs the ajax request
                },
                500);
                }).keydown(function(){
                    clearTimeout(window.timer); //clears out the timeout
            });

            function timedAjaxRequest(input){
                var company_id = $('.company_id').val();
                var name = $('.name').val();
                var address = $('.address').val();
                var city = $('.city').val();
                var state = ''; // $('.state').val();
                var zip = $('.zip').val();
                var phone = $('.phone').val();

                $.ajax({
                    url: '{{ url('admin/people') }}?company_id='+company_id+'&name='+name+'&address='+address+'&city='+city+'&state='+state+'&zip='+zip+'&phone='+phone,
                    type: 'GET',
                    success: function (response) {
                        var people = jQuery.parseJSON(response);
                        console.log(people.data);
                        $('tbody').html('');
                        $.each( people.data, function( key, person ) {
                            var company = person.company ? person.company.name : '';
                            $('tbody').append('<tr>'+
                                '<td>'+
                                    '<a href="{{ url('admin/people/edit') }}/'+person.id+'" class="btn btn-primary responsive-button">'+
                                        '<i class="fa fa-edit"></i>'+
                                    '</a>'+
                                '</td>'+
                                '<td>'+company+'</td>'+
                                '<td>'+person.name+'</td>'+
                                '<td>'+person.address+'</td>'+
                                '<td>'+person.city+'</td>'+
                                // '<td>'+person.state+'</td>'+
                                '<td>'+person.zip+'</td>'+
                                '<td>'+person.phone+'</td>'+
                                '<td>'+person.email+'</td>'+
                            '</tr>')
                            $('.responsive-button.right').css({'margin-left': '4px'});
                            $('ul.pagination').remove();
                        });

                    },
                    error: function (xhr) {
                        console.log(xhr);
                    }
                });
            }

        });

    </script>

@stop
