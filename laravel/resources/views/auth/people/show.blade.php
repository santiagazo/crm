@extends('layouts.auth.main')

@section('title')
Show Person
@endsection

@section('css')
<link href="{{ url('guest/plugins/lightgallery/dist/css/lightgallery.min.css') }}" rel="stylesheet">
<style>
    .products-list .item{
        border-left: 4px solid #e7e7e7;
    }
    .products-list .product-info {
        margin-left: 20px;
    }
    .callout a,
    .callout a:hover {
        text-decoration: none;
        color: #3c8dbc;
    }
    .bg-red, 
    .callout.callout-danger, 
    .alert-danger, 
    .alert-error, 
    .label-danger, 
    .modal-danger .modal-body {
        background-color: #dd4b3933 !important;
        color: #585858 !important;
    }
    .clearfix{
        clear: both;
    }
    .ptb-12 {
        padding-top: 12px;
        padding-bottom: 12px;
    }
    .btn-block-left {
        width: calc(100% - 24px);
    }
</style>

@stop

@section('content')

    <section class="content">
        {{-- <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            @forelse($person->estimates as $estimate)
                                <div class="col-md-2">
                                    <a href="{{ url('admin/estimates/edit/'.$estimate->id) }}" class="btn btn-large btn-success btn-block-left pull-left">Estimate Request</a>
                                    <span class="label label-default pull-right ptb-12"><i class="fa fa-arrow-right"></i></span>
                                </div>
                                @if($estimate->bid)
                                    <div class="col-md-2">
                                        <a href="{{ url('admin/bids/'.$estimate->bid->id) }}" class="btn btn-large btn-success btn-block-left pull-left">Bid</a>
                                        <span class="label label-default pull-right ptb-12"><i class="fa fa-arrow-right"></i></span>
                                    </div>
                                @else
                                    <div class="col-md-2">
                                        <a href="{{ url('admin/bids') }}" class="btn btn-large btn-default btn-block-left pull-left">Awaiting Bid</a>
                                        <span class="label label-default pull-right ptb-12"><i class="fa fa-arrow-right"></i></span>
                                    </div>
                                @endif
                                @if($estimate->bid && $estimate->bid->job)
                                    <div class="col-md-2">
                                        <a href="{{ url('admin/jobs/'.$estimate->bid->job->id) }}" class="btn btn-large btn-success btn-block-left pull-left">Job</a>
                                        <span class="label label-default pull-right ptb-12"><i class="fa fa-arrow-right"></i></span>
                                    </div>
                                @elseif($estimate->bid)
                                    <div class="col-md-2">
                                        <a href="{{ url('admin/jobs/'.$estimate->bid->id) }}" class="btn btn-large btn-default btn-block-left pull-left">Awaiting Bid Approval</a>
                                        <span class="label label-default pull-right ptb-12"><i class="fa fa-arrow-right"></i></span>
                                    </div>
                                @else
                                    <div class="col-md-2">
                                        <a href="{{ url('admin/jobs/create') }}" class="btn btn-large btn-default btn-block-left pull-left">Create Job</a>
                                        <span class="label label-default pull-right ptb-12"><i class="fa fa-arrow-right"></i></span>
                                    </div>
                                @endif
                                @if($estimate->bid && $estimate->bid->job && $estimate->bid->job->media)
                                    <div class="col-md-2">
                                        <a href="{{ url('admin/jobs/'.$estimate->bid->job->id) }}" class="btn btn-large btn-success btn-block-left pull-left">Invoice</a>
                                        <span class="label label-default pull-right ptb-12"><i class="fa fa-arrow-right"></i></span>
                                    </div>
                                @elseif($estimate->bid)
                                    <div class="col-md-2">
                                        <a href="{{ url('admin/bids/'.$estimate->bid->id) }}" class="btn btn-large btn-default btn-block-left pull-left">Awaiting Invoicing</a>
                                        <span class="label label-default pull-right ptb-12"><i class="fa fa-arrow-right"></i></span>
                                    </div>
                                @else
                                    <div class="col-md-2">
                                        <a href="{{ url('admin/bids/create') }}" class="btn btn-large btn-default btn-block-left pull-left">Awaiting Invoicing</a>
                                        <span class="label label-default pull-right ptb-12"><i class="fa fa-arrow-right"></i></span>
                                    </div>
                                @endif
                            @empty
                                <div class="col-md-2">
                                    <a href="{{ url('admin/estimates') }}" class="btn btn-large btn-default btn-block-left pull-left">Awaiting Request</a>
                                    <span class="label label-default pull-right ptb-12"><i class="fa fa-arrow-right"></i></span>
                                </div>
                            @endforelse
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Person</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h4>{{ $person->name }}</h4>
                        <p>
                            <h5>Billing Address</h5>
                            @php
                                $address = $person ? $person->address : '';
                                $city = $person ? $person->city : '';
                                $zip = $person ? $person->zip: '';
                            @endphp
                            <a href="https://www.google.com/maps/search/?api=1&query={{ urlencode($address.' '.$city.' UT '.$zip) }}" target="about_blank">
                                {{ $person->address }}<br/>
                                {{ $person->city }} UT, {{ $person->zip }}
                            </a>
                        </p>

                        <p>
                            <a href="mailto:{{ $person->email }}">{{ $person->email }}</a>  |  <a href="tel:{{ $person->phone }}">{{ $person->phone }}</a>
                        </p>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ url('admin/estimates/create/'.$person->id) }}" class="btn btn-primary btn-sm pull-right">Create an Estimate Request</a>
                        <a href="{{ url('admin/people/edit/'.$person->id) }}" class="btn btn-default btn-sm pull-right">Edit Person</a>
                    </div>
                    <!-- /.box-footer-->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add a Note about {{ $person->name }}</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @if(count($person->notes))
                            <div class="box-footer box-comments">
                                @foreach($person->notes as $note)
                                    <div class="box-comment">
                                        <!-- User image -->
                                        <img class="img-circle img-sm" src="{{ url($note->user->avatar ?: 'guest/images/user_empty.png') }}" alt="User Image">

                                        <div class="comment-text">
                                            <span class="username">
                                                {{ $note->user->name }}
                                                <span class="text-muted pull-right">{{ Carbon\Carbon::parse($note->created_at)->format('F jS, Y g:ia') }}</span>
                                            </span><!-- /.username -->
                                            {{ $note->content }}
                                        </div>
                                        <!-- /.comment-text -->
                                    </div>
                                    <!-- /.box-comment -->
                                @endforeach
                            </div>
                        @endif
                        {!! Form::open( ['url' => 'admin/notes', 'id'=>'notes_form'] ) !!}
                            
                            {!! Form::hidden('id', $person->id) !!}
                            {!! Form::hidden('type', 'person') !!}

                            <div class="form-group">
                                {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => "2"]) !!}
                            </div>
                        
                            {!! Form::submit('Add', ['class' => 'btn btn-primary pull-right']) !!}
                        
                        {!! Form::close() !!}
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Jobs</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            @forelse($person->jobs as $job)
                                <li class="callout callout-default">
                                    <div class="product-info">
                                        <a href="{{ url('admin/jobs/edit/'.$job->id) }}" class="product-title">{{ $job->purchase_order ? 'PO: '.$job->purchase_order : 'Bid No: '.$job->id }}
                                        <span class="btn btn-xs btn-primary pull-right">Edit Job</span></a>
                                        <span class="product-description">
                                            {!! $job->name ? $job->name.'<br/>' : NULL !!}
                                            {{ $job->address ?: $job->person->address }} {{ $job->city ?: $job->person->city }}, UT {{ $job->zip ?: $job->person->zip }}<br/>
                                            {{ $job->subdivision ? 'Subdivision: '.$job->subdivision : NULL }}  {{ $job->lot ? 'Lot: '.$job->lot : NULL }}
                                            {!! $job->instructions ? $job->instruction.'<br/>' : NULL !!}
                                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->
                            @empty
                                <p>There are no jobs listed for this person yet. Click Create Job to get started.</p>
                            @endforelse
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>

    </section>

@stop

@section('js')
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ url('guest/plugins/lightgallery/dist/js/lightgallery-all.min.js') }}"></script>
    <script>

        $(function(){
            $("[data-mask]").inputmask();

            $('.company-select2').select2();

            $('body').on('click', '.see-more-btn', function(){
                $(this).parent().find('.see-more').slideToggle();
                $(this).text() == "See More" ? $(this).text("See Less") : $(this).text("See More");
            });

            $('{{ $galleries }}').lightGallery({
                gallery: true,
                download: false,
                onSliderLoad  : function (el) {
                  el.lightGallery();
                }
            });
        });

    </script>

@stop
