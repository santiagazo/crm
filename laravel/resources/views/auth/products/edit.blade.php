@extends('layouts.auth.main')

@section('title')
Edit Product
@endsection

@section('css')

@stop

@section('content')


    {!! Form::open( ['url' => 'admin/products/'.$product->id] ) !!}

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit a Product</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <!--Select Lists() form input-->
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {!! Form::label('company_id', 'Company') !!}
                                    {!! Form::select('company_id', [$product->company->id => $product->company->name], $product->company->id, ['class' => 'form-control company-select2']) !!}
                                </div>
                            </div>

                            <!--Select Lists() form input-->
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {!! Form::label('category_id', 'Category') !!}
                                    {!! Form::select('category_id', [$product->category->id => $product->category->name], $product->category->id, ['class' => 'form-control category-select2']) !!}
                                </div>
                            </div>

                            <!--Text form input-->
                            <div class="col-lg-4">
                                <div class='form-group'>
                                    {!! Form::label('name', 'Product Name') !!}
                                    {!! Form::text('name', $product->name, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <!--Select form input-->
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {!! Form::label('unit', 'Unit') !!}
                                    {!! Form::select('unit', $units, $product->unit, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <!--Select form input-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('type_id', 'Tax Type') !!}
                                    {!! Form::select('type_id', $types, $product->type_id, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <!--Text form input-->
                            <div class="col-md-4">
                                <div class='form-group'>
                                    {!! Form::label('supplier_price', 'Supplier Price') !!}
                                    {!! Form::text('supplier_price', $product->supplier_price, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <!--Select form input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('colors[]', 'Colors') !!}
                                    {!! Form::select('colors[]', $colors, explode(',',$product->colors), ['class' => 'form-control color_select2', 'multiple' => true]) !!}
                                </div>
                            </div>

                            <!--Textarea form input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('description', 'Description') !!}
                                    {!! Form::textarea('description', $product->description, ['class' => 'form-control', 'rows' => 3]) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-12">
                                <a href="{{ url('admin/products/delete/'.$product->id) }}" class="btn btn-danger confirm-link" data-confirm-text="Are you sure you want to delete this product?">Delete</a>
                                {!! Form::submit('Update', ['class' => 'btn btn-default pull-right']) !!}
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                </div>
            </div>

        </section>

    {!! Form::close() !!}


@stop

@section('js')

    <script>

        $(function(){
            $('.company-select2').select2({
                placeholder: 'Enter a Company Name',
                minimumInputLength: 0,
                escapeMarkup: function (markup) {
                    return markup;
                },
                ajax: {
                    url: "{{ url('/admin/companies') }}",
                    dataType: 'json',
                    delay: 0,
                    data: function (params) {
                      return {
                        q: params.term, // search term
                        page: params.page,
                        suppliers: true,
                        select2: true,
                      }
                    },
                    processResults: function (data, params) {
                      params.page = params.page || 1;
                      console.log(data.data);
                      return {
                        results: data.data,
                        pagination: {
                          more: (params.page * 30) < data.total_count
                        }
                      };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; },
                allowClear: false,
                templateResult: formatResult,
                templateSelection: formatSelection
            });

            $('.category-select2').select2({
                placeholder: 'Enter a Category Name',
                minimumInputLength: 0,
                escapeMarkup: function (markup) {
                    return markup;
                },
                ajax: {
                    url: "{{ url('/admin/categories') }}",
                    dataType: 'json',
                    delay: 0,
                    data: function (params) {
                      return {
                        q: params.term, // search term
                        page: params.page,
                        suppliers: true,
                        select2: true,
                      }
                    },
                    processResults: function (data, params) {
                      params.page = params.page || 1;
                      console.log(data.data);
                      return {
                        results: data.data,
                        pagination: {
                          more: (params.page * 30) < data.total_count
                        }
                      };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; },
                allowClear: false,
                templateResult: formatResult,
                templateSelection: formatSelection
            });

            function formatResult (result) {
                if(result.name){
                    var $result = $('<span>' +result.name +'</span>');
                }else{
                    var $result = $('<span>' +result.text +'</span>');
                }
                return $result;
            };

            function formatSelection (result) {
                if(result.name){
                    var $result = $('<span>' +result.name +'</span>');
                }else{
                    var $result = $('<span>' +result.text +'</span>');
                }
                return $result;
            };

            $('.color_select2').select2({
                tags: true
            });
        });

    </script>

@stop

