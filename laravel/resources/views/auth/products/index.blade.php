@extends('layouts.auth.main')

@section('title')
Products
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
table .form-control {
            border: none;
        }
    </style>
@stop

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ url('admin/products/create') }}" class="btn btn-primary pull-right">Create Product</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    {!! Form::open(['url' => 'admin/products', 'method' => 'GET', 'id' => 'products-form']) !!}
                                        <th></th>
                                        <th>
                                            <div class="form-group">
                                                {!! Form::text('company_id', $company_id, ['placeholder' => 'Company', 'class' => 'form-control company_id']) !!}
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                {!! Form::text('category_id', $category_id, ['placeholder' => 'Category', 'class' => 'form-control category_id']) !!}
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                {!! Form::text('name', $name, ['placeholder' => 'Name', 'class' => 'form-control name']) !!}
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                {!! Form::text('supplier_price', $supplier_price, ['placeholder' => "Supplier Price", 'class' => 'form-control supplier-price']) !!}
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                {!! Form::text('unit', $unit, ['placeholder' => 'Unit', 'class' => 'form-control unit']) !!}
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                {!! Form::text('description', $description, ['placeholder' => 'Description', 'class' => 'form-control description']) !!}
                                            </div>
                                        </th>

                                        <input type="submit" style="visibility: hidden; position: absolute;" />

                                    {!! Form::close() !!}
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($products))
                                    @foreach($products as $product)
                                        <tr>
                                            <td>
                                                <a href="{{ url('admin/products/edit/'.$product->id) }}" class="btn btn-primary responsive-button">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ url('admin/company/products/show/'.$product->company->id) }}">{{ $product->company->name }}</a>
                                            </td>
                                            <td>
                                                {{ $product->category->name }}
                                            </td>
                                            <td>
                                                {{ $product->name }}
                                            </td>
                                            <td>
                                                {{ $product->supplier_price }}
                                            </td>
                                            <td>
                                                {{ $product->unit }}
                                            </td>
                                            <td>
                                                {{ $product->description }}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">
                                            There are no products built yet, click Create Products to get one started.
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        {!! $products->links() !!}
                    </div>
                </div>

            </div>
        </div>
    </section>
@stop

@section('js')

    <script>

        $(function(){
            $('body').on('keyup', '.company_id, .category_id, .name, .house-price, .supplier-price, .unit, .description', function(){
                var input = $(this);
                window.timer=setTimeout(function(){ // setting the delay for each keypress
                    timedAjaxRequest(input); //runs the ajax request
                },
                500);
                }).keydown(function(){
                    clearTimeout(window.timer); //clears out the timeout
            });

            function timedAjaxRequest(input){
                var company_id = $('.company_id').val();
                var category_id = $('.category_id').val();
                var name = $('.name').val();
                var house_price = $('.house-price').val();
                var supplier_price = $('.supplier-price').val();
                var unit = $('.unit').val();
                var description = $('.description').val();

                $.ajax({
                    url: '{{ url('admin/products') }}?company_id='+company_id+'&category_id='+category_id+'&name='+name+'&house_price='+house_price+'&supplier_price='+supplier_price+'&unit='+unit+'&description='+description,
                    type: 'GET',
                    success: function (response) {
                        var products = jQuery.parseJSON(response);
                        console.log(products.data);
                        $('tbody').html('');
                        $.each( products.data, function( key, product ) {
                            $('tbody').append('<tr>'+
                                '<td>'+
                                    '<a href="{{ url('admin/products/edit') }}/'+product.id+'" class="btn btn-primary responsive-button">'+
                                        '<i class="fa fa-edit"></i>'+
                                    '</a>'+
                                '</td>'+
                                '<td><a href="{{ url('admin/company/products/show') }}/'+product.company_id+'">'+product.company.name+'</a></td>'+
                                '<td>'+product.category.name+'</td>'+
                                '<td>'+product.name+'</td>'+
                                '<td>'+product.supplier_price+'</td>'+
                                '<td>'+product.unit+'</td>'+
                                '<td>'+product.description+'</td>'+
                            '</tr>')
                            $('.responsive-button.right').css({'margin-left': '4px'});
                            $('ul.pagination').remove();
                        });

                    },
                    error: function (xhr) {
                        console.log(xhr);
                    }
                });
            }

        });

    </script>

@stop
