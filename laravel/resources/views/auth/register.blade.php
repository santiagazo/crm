@extends('layouts.site.main')

@section('css')
	<style>
        body, html {
             min-height: 100px;
        }
         .tab-content{
    		width: 300px;
            margin: 150px auto;
         }
    </style>
@endsection

@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="tab-content">
                <form class="tab-pane fade active in" role="form" method="POST" action="{{ url('/register') }}">
                	{{ csrf_field() }}

					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}" required autofocus>
                        <i class="form-group__bar"></i>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="Email Address" name="email" value="{{ old('email') }}" required>
                        <i class="form-group__bar"></i>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" placeholder="Password" name="password" required>
                        <i class="form-group__bar"></i>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input id="password-confirm" type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required>
                        <i class="form-group__bar"></i>
                    </div>

                    {!! Form::hidden('referral_code', $referral_code ?: old('referral_code')) !!}

                    <p><small>By Signing up with Milne Brothers Roofing, you're agreeing to our <a href="">terms and conditions</a>.</small></p>

                    <button class="btn btn-primary btn-block m-t-10 m-b-10">Register</button>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
