@extends('layouts.auth.main')

@section('title')
Templates
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
        table .form-control {
            border: none;
        }
    </style>
@stop

@section('content')


        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                            <div class="box-header with-border">
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(!$bid_id)
                                            <a href="{{ url('admin/jobs/create') }}" class="btn btn-primary btn-sm pull-right">Create Job</a>
                                        @endif
                                        @if($bid_id && count(!$templates))
                                            <a href="{{ url('admin/material_lists/create/'.$bid_id.'/bid') }}" class="btn btn-primary btn-sm pull-right">Continue with out a template</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        {!! Form::open(['url' => 'admin/templates', 'method' => 'GET', 'id' => 'templates-form']) !!}
                                            <th width="33px"></th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('name', $name, ['placeholder' => 'Name', 'class' => 'form-control name']) !!}
                                                </div>
                                            </th>

                                            <input type="submit" style="visibility: hidden; position: absolute;" />

                                        {!! Form::close() !!}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($templates))
                                        @foreach($templates as $template)
                                            <tr>
                                                <td>
                                                    <a href="{{ url('admin/templates/show/'.$template->id.'?bid_id='.$bid_id) }}" class="btn btn-primary responsive-button">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    {{ $template->name }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3">
                                                There are no templates built yet.
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {!! $templates->links() !!}
                        </div>
                    </div>

                </div>
            </div>
        </section>

@stop

@section('js')

    <script>

        $(function(){

            $('body').on('keyup', '.name', function(){
                var input = $(this);
                window.timer=setTimeout(function(){ // setting the delay for each keypress
                    timedAjaxRequest(input); //runs the ajax request
                },
                500);
                }).keydown(function(){
                    clearTimeout(window.timer); //clears out the timeout
            });

            function timedAjaxRequest(input){
                var name = $('.name').val();

                $.ajax({
                    url: '{{ url('admin/templates') }}?name='+name,
                    type: 'GET',
                    success: function (response) {
                        var templates = jQuery.parseJSON(response);
                        console.log(templates.data);
                        $('tbody').html('');
                        $.each( templates.data, function( key, template ) {
                            $('tbody').append('<tr>'+
                                '<td>'+
                                    '<a href="{{ url('admin/templates/show') }}/'+template.id+'" class="btn btn-primary responsive-button">'+
                                        '<i class="fa fa-eye"></i>'+
                                    '</a>'+
                                '</td>'+
                                '<td>'+template.name+'</td>'+
                            '</tr>')
                            $('.responsive-button.right').css({'margin-left': '4px'});
                            $('ul.pagination').remove();
                        });

                    },
                    error: function (xhr) {
                        console.log(xhr);
                    }
                });
            }

        });

    </script>

@stop
