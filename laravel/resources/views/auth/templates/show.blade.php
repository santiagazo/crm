@extends('layouts.auth.main')

@section('title')
View Template
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .table-bordered>tfoot>tr>th{
            border-top: 2px #d0d0d0 solid;
        }
    </style>
@stop

@section('content')


    <section class="content">

        <div class="row">
            <div class="col-lg-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $template->name }}</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Qty</th>
                                    <th>Category Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($template->material_list->categories as $category)
                                            <tr>
                                                <td>{{ $category->name }} | {{ $category->price }}/{{ str_unslug($category->unit) }}</td>
                                                <td>{{ $category->pivot->qty }}</td>
                                                <td>{{ formatPrice($category->pivot->category_price) }}</td>
                                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ url('admin/templates/delete/'.$template->id) }}" class="btn btn-danger">Delete Template</a>
                        @if($bid_id)
                            <a href="{{ url('admin/templates/copy/'.$template->id.'/?bid_id='.$bid_id) }}" class="btn btn-primary pull-right">Create New Material Sheet From This Template</a>
                        @endif
                    </div>
                    <!-- /.box-footer-->
                </div>

            </div>
        </div>


    </section>


@stop

@section('js')

    <script>

        $(function(){
        });

    </script>

@stop
