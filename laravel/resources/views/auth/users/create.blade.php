@extends('layouts.auth.main')

@section('title')
Invite New User
@endsection

@section('css')
@stop

@section('content')
<!-- Main content -->
<section class="content">
	
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
		        {!! Form::open( ['url' => 'admin/users'] ) !!}
			    <div class="box-body">

					<div class="col-md-12">
						<p>
				    		Users must be issued an invite to their email. The email link will have a unique one-time use code that will allow them to register. Please designate which role you want them to play
				    		in the company. Anyone designated as Owner will always recieve an Admin role as well. Once they register you will be able to edit their user and adjust any roles you wish.
						</p>
					</div>

		        	<!--Text form input-->
		        	<div class="col-md-5">
		        		<div class='form-group'>
		        			{!! Form::label('name', 'Name') !!}
		        			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		        		</div>
		        	</div>
		        
		        	<!--Text form input-->
		        	<div class="col-md-5">
		        		<div class='form-group'>
		        			{!! Form::label('email', 'Email') !!}
		        			{!! Form::text('email', null, ['class' => 'form-control']) !!}
		        		</div>
		        	</div>

		        	<!--Select form input-->
		        	<div class="col-md-2">
		        		<div class="form-group">
		        			{!! Form::label('role', 'Role') !!}
		        			{!! Form::select('role', ['Employee' => 'Employee', 'Admin' => 'Admin', 'Owner' => 'Owner'], 'Employee', ['class' => 'form-control']) !!}
		        		</div>
		        	</div>

			    </div>
			    <!-- /.box-body -->
			    <div class="box-footer">
			        <!--Submit form input-->
		        	<div class="col-md-12">
		        		<div class="form-group">
		        			{!! Form::submit('Submit', ['class' => 'btn btn-primary btn-sm pull-right']) !!}
		        		</div>
		        	</div>
			    </div>
		        {!! Form::close() !!}
			    <!-- /.box-body -->
			</div>
		</div>
	</div>
	
</section>
<!-- /.content -->

@endsection

