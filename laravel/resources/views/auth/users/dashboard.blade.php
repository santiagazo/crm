@extends('layouts.auth.main')

@section('title')
Dashboard
@endsection

@section('css')
	  <!-- fullCalendar -->
  <link rel="stylesheet" href="{{ url('auth/plugins/fullcalendar/fullcalendar.min.css') }}">
  <link rel="stylesheet" href="{{ url('auth/plugins/fullcalendar/fullcalendar.print.css') }}" media="print">

	<style>
		.clickable-row{
			cursor: pointer;
		}
		.box-fixed-height{
			max-height: 300px;
			overflow-y: auto;
		}
	</style>
@stop

@section('content')
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-8 col-lg-6">
			<div class="box box-primary">
				<div class="box-body no-padding">
					<!-- THE CALENDAR -->
					<div id="calendar"></div>
				</div>
			</div>
			<div class="row">
				@foreach($top_producers as $name => $job_count)
					<div class="col-sm-4">
				        <div class="small-box bg-primary">
				            <div class="inner">
				                <h3>{{ $job_count }}</h3>
				                <p>{{ $name }}</p>
				            </div>
				            <div class="icon">
				                <i class="ion ion-home"></i>
				            </div>
				            <a href="{{ url('/jobs') }}" class="small-box-footer">
				                Jobs Completed ({{ Carbon\Carbon::now()->format('M') }})
				            </a>
				        </div>
					</div>
				@endforeach
			</div>

			<div class="row">
				@foreach($top_earners as $name => $revenue_sum)
					<div class="col-sm-4">
				        <div class="small-box bg-info">
				            <div class="inner">
				                <h3><sup style="font-size: 20px">$</sup>{{ $revenue_sum }}</h3>
				                <p>{{ $name }}</p>
				            </div>
				            <div class="icon">
				                <i class="ion ion-social-usd"></i>
				            </div>
				            <a href="{{ url('/jobs') }}" class="small-box-footer">
				                Revenue Made ({{ Carbon\Carbon::now()->format('M') }})
				            </a>
				        </div>
					</div>
				@endforeach
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="small-box bg-success">
			            <div class="inner">
			                <h3>{{ $total_jobs }}</h3>
							<p>Total Jobs Completed</p>
			            </div>
			            <div class="icon">
			                <i class="ion ion-home"></i>
			            </div>
			            <a href="{{ url('/jobs') }}" class="small-box-footer">
			                {{ Carbon\Carbon::now()->format('F') }}
			            </a>
			        </div>
				</div>
				<div class="col-sm-4">
					<div class="small-box bg-success">
			            <div class="inner">
			                <h3><sup style="font-size: 20px">$</sup>{{ $total_revenue }}</h3>
			                <p>Total Revenue</p>
			            </div>
			            <div class="icon">
			                <i class="ion ion-social-usd"></i>
			            </div>
			            <a href="{{ url('/jobs') }}" class="small-box-footer">
			               {{ Carbon\Carbon::now()->format('F') }}
			            </a>
			        </div>
				</div>
				<div class="col-sm-4">
					<div class="small-box bg-success">
			            <div class="inner">
			                <h3><sup style="font-size: 20px">$</sup>{{ $price_per_sq }}</h3>
			                <p>Price Per Square</p>
			            </div>
			            <div class="icon">
			                <i class="ion ion-ios-albums"></i>
			            </div>
			            <a href="{{ url('/jobs') }}" class="small-box-footer">
			                {{ Carbon\Carbon::now()->format('F') }}
			            </a>
			        </div>
				</div>
			</div>
			
			
		</div>

		<div class="col-md-4 col-lg-6">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Pending Estimate Requests</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body box-fixed-height">
					<div class="table-responsive">
						<table class="table table-striped no-margin">
							<thead>
								<tr>
									<th>Name</th>
									<th>City</th>
									<th>Type</th>
									<th>Pitch</th>
									<th>Urgency</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								@forelse($estimates as $estimate)
									<tr class="clickable-row" data-href="{{ url('admin/estimates/edit/'.$estimate->id) }}">
										<td>{{ $estimate->person->name }}</td>
										<td>{{ $estimate->project_city }}</td>
										<td>
											@foreach( (array)json_decode($estimate->type) as $need )
												@if(!$loop->first)
												 / 
												@endif 
											{{ str_unslug($need) }}
											@endforeach
										</td>
										<td>{{ str_unslug($estimate->roof_pitch) }}</td>
										<td>{!! $estimate->is_urgent ? '<span class="label label-danger">Urgent</span>' : '<span class="label label-default">Not Urgent</span>' !!}</td>
										<td>{!! $estimate->status->slug == 'open' ? '<span class="label label-warning">Open</span>' : '<span class="label label-success">Contacted</span>' !!}</td>
									</tr>
								@empty
									<tr colspan="5">
										<p>There are no pending estimates at this time.</p>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer clearfix">
					<a href="{{ url('/admin/estimates') }}" class="btn btn-xs btn-default pull-right">See More</a>
				</div>
				<!-- /.box-footer -->
			</div>
			<!-- /.box -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">{{ explode(' ', auth()->user()->name)[0] }}'s Pending Bids</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body box-fixed-height">
					<div class="table-responsive">
						<table class="table table-striped no-margin">
							<thead>
								<tr>
									<th>Name</th>
									<th>City</th>
									<th>Type</th>
									<th>Bid Amount</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								@forelse($bids as $bid)
									<tr class="clickable-row" data-href="{{ url('admin/bids/edit/'.$bid->id) }}">
										<td>{{ $bid->person ? $bid->person->name : NULL }}</td>
										<td>{{ $bid->person ? $bid->city : NULL }}</td>
										<td>{{ $bid->type->name }}</td>
										<td>{{ $bid->amount ?: NULL }}</td>
										@if(empty($bid->sent_at) && $bid->status->slug == 'not_sent')
											<td><span class="label label-danger">{{ $bid->status->name }}</span></td>
										@elseif($bid->status->slug == 'accepted')
											<td><span class="label label-success">{{ $bid->status->name }}</span></td>
										@elseif($bid->status->slug == 'pending')
											<td><span class="label label-primary">{{ $bid->status->name }}</span></td>
										@elseif($bid->status->slug == 'conditionally_accetped_verify')
											<td><span class="label label-yellow">{{ $bid->status->name }}</span></td>
										@elseif($bid->status->slug == 'conditionally_accetped')
											<td><span class="label label-orange">{{ $bid->status->name }}</span></td>
										@else
											<td><span class="label label-danger">{{ $bid->status->name }}</span></td>
										@endif
									</tr>
								@empty
									<tr colspan="5">
										<p>There are no pending bids at this time.</p>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer clearfix">
					<a href="{{ url('/admin/bids') }}" class="btn btn-xs btn-default pull-right">See More</a>
				</div>
				<!-- /.box-footer -->
			</div>
			<!-- /.box -->
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">Job Queue</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body box-fixed-height">
					<div class="table-responsive">
						<table class="table table-striped no-margin">
							<thead>
								<tr>
									<th>Name</th>
									<th>City</th>
									<th>Type</th>
									<th>Bid Amount</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								@forelse($jobs->where('start_at', '>', Carbon\Carbon::now()->startOfDay())->where('status_id', 2) as $job)
									<tr class="clickable-row" data-href="{{ url('admin/jobs/edit/'.$job->id) }}">
										<td>{{ $job->person ? $job->person->name : NULL }}</td>
										<td>{{ $job->person ? $job->project_city : NULL }}</td>
										<td>{{ $job->type->name }}</td>
										<td>{{ $job->bid ? $job->bid->amount : NULL }}</td>
										<td>{!! $job->status->slug == 'pending' ? '<span class="label label-success">Pending</span>' : '<span class="label label-default">'.$job->status->name.'</span>' !!}</td>
									</tr>
								@empty
									<tr colspan="5">
										<p>There are no pending jobs at this time.</p>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer clearfix">
					<a href="{{ url('/admin/jobs') }}" class="btn btn-xs btn-default pull-right">See More</a>
				</div>
				<!-- /.box-footer -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
<!-- /.content -->

@endsection

@section('js')
	<!-- fullCalendar -->
	<script src="{{ url('auth/plugins/moment/min/moment.min.js') }}"></script>
	<script src="{{ url('auth/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
	<script>

		$(function(){
			$('body').on('click', '.clickable-row', function(){
				var href = $(this).data('href');
				location.href = href;
			});

			/* initialize the calendar
			-----------------------------------------------------------------*/
			$('#calendar').fullCalendar({
				header    : {
					left  : 'prev,next today',
					center: 'title',
					right : 'month,agendaWeek,agendaDay'
				},
				buttonText: {
					today: 'today',
					month: 'month',
					week : 'week',
					day  : 'day'
				},
				//Random default events
				events    : [
					@foreach($jobs->where('start_at', '!=', NULL) as $job)
						{
							title: '{{ $job->person->name }}',
							start: '{{ Carbon\Carbon::parse($job->start_at)->toRfc2822String() }}',
							end  : '{{ $job->end_at ? Carbon\Carbon::parse($job->end_at)->toRfc2822String() : Carbon\Carbon::parse($job->start_at)->addHour()->toRfc2822String() }}',
							url  : '{{ url('/admin/jobs/show/'.$job->id) }}',
							@if($job->ordered_at == NULL)

								backgroundColor: '#f39c12', //yellow
								borderColor    : '#f39c12', //yellow
								description    : 'Job shows no materials ordered yet.'

							@elseif($job->start_at < $job->delivery_at || $job->delivery_at == NULL)

								backgroundColor: '#f56954', //red
								borderColor    : '#f56954', //red
								description    : 'Job has NO specified delivery date or job delivery date is before job schedule date.'

							@elseif($job->accepted_at == NULL)

								backgroundColor: '#00c0ef', //aqua
								borderColor    : '#00c0ef', //aqua
								description    : 'Job has no specified accepted date.'

							@elseif($job->start_at < $job->delivery_at || $job->delivery_at == NULL)

								backgroundColor: '#f56954', //red
								borderColor    : '#f56954', //red
								description    : 'job has NO specified delivery date or job delivery date is before job schedule date.'
								
							@else

								backgroundColor: '#00a65a', //green
								borderColor    : '#00a65a', //green
								description    : '{{ $job->person->name }} | {{ $job->address }} {{ $job->city }}, {{ $job->zip }}'
							
							@endif
						},
					@endforeach
				],
				eventAfterRender: function(event, element) {
			        $(element).tooltip({
			            title: event.description,
			            container: "body"
			        });
			    },
				editable  : false,
				droppable : false, // this allows things to be dropped onto the calendar !!!
			});
		});

	</script>

@stop