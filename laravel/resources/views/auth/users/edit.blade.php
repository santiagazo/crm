@extends('layouts.auth.main')

@section('title')
Edit User
@endsection

@section('css')

@stop

@section('content')


    {!! Form::open( ['url' => 'admin/users/'.$user->id] ) !!}

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit a User</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                                <!--Text form input-->
                                <div class="col-md-4">
                                    <div class='form-group'>
                                        {!! Form::label('name', "User's Name") !!}
                                        {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                 <!--Select Lists() form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('location_id', 'Location') !!}
                                        {!! Form::select('location_id', $locations, $user->location ? $user->location->id : NULL, ['class' => 'form-control company-select2']) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('role', 'Role') !!}
                                        {!! Form::select('role', $roles, count($user->roles) ? $user->roles->first()->name : 'Employee', ['class' => 'form-control role-select2']) !!}
                                    </div>
                                </div>

                                <!--Select form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('address', 'Address') !!}
                                        {!! Form::text('address', $user->address, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <!--Select form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('city', 'City') !!}
                                        {!! Form::text('city', $user->city, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <!--Text form input-->
                                <div class="col-md-4">
                                    <div class='form-group'>
                                        {!! Form::label('zip', 'Zip') !!}
                                        {!! Form::text('zip', $user->zip, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('phone', 'Phone') !!}
                                        {!! Form::text('phone', $user->phone, ['class' => 'form-control', 'data-inputmask' => "'mask': '(999) 999-9999'", 'data-mask']) !!}
                                    </div>
                                </div>

                                <!--Textarea form input-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('email', 'Email') !!}
                                        {!! Form::text('email', $user->email, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <a href="{{ url('admin/users/delete/'.$user->id) }}" class="btn btn-danger confirm-link" data-confirm-text="Are you sure you want to delete this user?">Delete</a>
                                    {!! Form::submit('update', ['class' => 'btn btn-default pull-right']) !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                </div>
            </div>

        </section>

    {!! Form::close() !!}


@stop

@section('js')
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>

        $(function(){
            $("[data-mask]").inputmask();
            $('.role-select2').select2();
            $('.company-select2').select2();
        });

    </script>

@stop
