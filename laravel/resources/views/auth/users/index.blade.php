@extends('layouts.auth.main')

@section('title')
Users
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .form-group {
             margin-bottom: 0px ;
        }
        .table>tbody>tr>th {
            padding: 0px;
        }
        table .form-control {
            border: none;
        }
        .label{
        	margin-right: 5px;
        }
    </style>
@stop

@section('content')

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ url('admin/users/create') }}" class="btn btn-primary btn-sm pull-right">Invite New User</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        {!! Form::open(['url' => 'admin/users', 'method' => 'GET', 'id' => 'users-form']) !!}
                                            <th width="40px"></th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('location_id', $location_id, ['placeholder' => 'Location', 'class' => 'form-control location_id']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('name', $name, ['placeholder' => 'Name', 'class' => 'form-control name']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('address', $address, ['placeholder' => 'Address', 'class' => 'form-control address']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('city', $city, ['placeholder' => "City", 'class' => 'form-control city']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('zip', $zip, ['placeholder' => 'Zip', 'class' => 'form-control zip']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('phone', $phone, ['placeholder' => 'Phone', 'class' => 'form-control phone']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('email', $email, ['placeholder' => 'Email', 'class' => 'form-control email']) !!}
                                                </div>
                                            </th>
                                            <th>
                                                <div class="form-group">
                                                    {!! Form::text('roles', $roles, ['placeholder' => 'Roles', 'class' => 'form-control roles']) !!}
                                                </div>
                                            </th>

                                            <input type="submit" style="visibility: hidden; position: absolute;" />

                                        {!! Form::close() !!}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($users))
                                        @foreach($users as $user)
                                            <tr>
                                                <td>
                                                    <a href="{{ url('admin/users/edit/'.$user->id) }}" class="btn btn-primary btn-xs responsive-button" data-toggle="tooltip" title="Edit Person">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    {{ $user->location ? $user->location->name : NULL }}
                                                </td>
                                                <td>
                                                    {{ $user->name }}
                                                </td>
                                                <td>
                                                    {{ $user->address }}
                                                </td>
                                                <td>
                                                    {{ $user->city }}
                                                </td>
                                                <td>
                                                    {{ $user->zip }}
                                                </td>
                                                <td>
                                                    {{ $user->phone }}
                                                </td>
                                                <td>
                                                    {{ $user->email }}
                                                </td>
                                                <td>
                                                    @foreach($user->roles as $role)
                                                        <span class="label label-sm label-primary">{{ $role->name }}</span>
                                                    @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8">
                                                There are no people built yet, click Create Person to get one started.
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {!! $users->links() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>


@stop

@section('js')

    <script>

        $(function(){
            $('body').on('keyup', '.location_id, .name, .address, .city, .state, .zip, .phone, .email', function(){
                var input = $(this);
                window.timer=setTimeout(function(){ // setting the delay for each keypress
                    timedAjaxRequest(input); //runs the ajax request
                },
                500);
                }).keydown(function(){
                    clearTimeout(window.timer); //clears out the timeout
            });

            function timedAjaxRequest(input){
                var location_id = $('.location_id').val();
                var name = $('.name').val();
                var address = $('.address').val();
                var city = $('.city').val();
                var state = ''; // $('.state').val();
                var zip = $('.zip').val();
                var phone = $('.phone').val();

                $.ajax({
                    url: '{{ url('admin/users') }}?location_id='+location_id+'&name='+name+'&address='+address+'&city='+city+'&state='+state+'&zip='+zip+'&phone='+phone,
                    type: 'GET',
                    success: function (response) {
                        var users = jQuery.parseJSON(response);
                        console.log(users.data);
                        $('tbody').html('');
                        $.each( users.data, function( key, user ) {
                            var location = user.location ? user.location.name : '';
                            var roles = '';
                            $.each(user.roles, function(i, role){
                            	roles += '<span class="label label-sm label-primary">'+role.name+'</span>';
                            });
                            $('tbody').append('<tr>'+
                                '<td>'+
                                    '<a href="{{ url('admin/users/edit') }}/'+user.id+'" class="btn btn-primary responsive-button">'+
                                        '<i class="fa fa-edit"></i>'+
                                    '</a>'+
                                '</td>'+
                                '<td>'+location+'</td>'+
                                '<td>'+user.name+'</td>'+
                                '<td>'+user.address+'</td>'+
                                '<td>'+user.city+'</td>'+
                                '<td>'+user.zip+'</td>'+
                                '<td>'+user.phone+'</td>'+
                                '<td>'+user.email+'</td>'+
                                '<td>'+roles+'</td>'+
                            '</tr>')
                            $('.responsive-button.right').css({'margin-left': '4px'});
                            $('ul.pagination').remove();
                        });

                    },
                    error: function (xhr) {
                        console.log(xhr);
                    }
                });
            }

        });

    </script>

@stop
