<!DOCTYPE html>
<html lang="en">
<!--[if IE 9 ]><html lang="en" class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Milne Brothers Roofing</title>

        <!-- Vendors -->

        <!-- Bootstrap -->
        <link href="{{ url('guest/plugins/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Material design colors -->
        <link href="{{ url('guest/plugins/material-design-iconic-font/dist/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- CSS animations -->
        <link rel="stylesheet" href="{{ url('guest/plugins/animate.css/animate.min.css') }}">
        <!-- Select2 - Custom Selects -->
        <link rel="stylesheet" href="{{ url('guest/plugins/select2/dist/css/select2.min.css') }}">
        <!-- Slick Carousel -->
        <link rel="stylesheet" href="{{ url('guest/plugins/slick-carousel/slick/slick.css') }}">
        <!-- NoUiSlider - Input Slider -->
        <link rel="stylesheet" href="{{ url('guest/plugins/nouislider/distribute/nouislider.min.css') }}">
        <!-- Site -->
        <link rel="stylesheet" href="{{ url('guest/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
        <link rel="stylesheet" href="{{ url('guest/plugins/select2/dist/css/select2.min.css') }}">
        <link rel="stylesheet" href="{{ url('guest/css/app_1.min.css') }}">
        <link rel="stylesheet" href="{{ url('guest/css/app_2.min.css') }}">

        <style>

            .four-zero {
                background-color: #0058A8;
            }

        </style>
    </head>

    <body>
        <section id="main" class="four-zero">
            <div class="four-zero__content">
                <h1>403</h1>
                <p>Unauthorized.</p>

                <div class="four-zero__links">
                    <a href="{{ url('/') }}">Home page</a>
                    <a href="{{ URL::previous() }}">Previous page</a>
                </div>
            </div>
        </section>

        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1>Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="ie-warning__inner">
                    <ul class="ie-warning__download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>
        <![endif]-->

        <!-- Javascript -->

        <!-- jQuery -->
        <script src="{{ url('guest/plugins/jquery/dist/jquery.min.js') }}"></script>

        <!-- Bootstrap -->
        <script src="{{ url('guest/plugins/bootstrap/dist/js/bootstrap.min.js') }}"></script>

        <!-- Waves button ripple effects -->
        <script src="{{ url('guest/plugins/Waves/dist/waves.min.js') }}"></script>

        <!-- Select 2 - Custom Selects -->
        <script src="{{ url('guest/plugins/select2/dist/js/select2.full.min.js') }}"></script>

        <!-- Slick Carousel - Custom Alerts -->
        <script src="{{ url('guest/plugins/slick-carousel/slick/slick.min.js') }}"></script>

        <!-- NoUiSlider -->
        <script src="{{ url('guest/plugins/nouislider/distribute/nouislider.min.js') }}"></script>

        <!-- Autosize -->
        <script src="{{ url('guest/plugins/autosize/dist/autosize.js') }}"></script>

        <!-- Forms -->
        <script src='{{ url('guest/plugins/select2/dist/js/select2.full.min.js') }}'></script>
        <script src='{{ url('guest/plugins/moment/min/moment-with-locales.min.js') }}'></script>
        <script src='{{ url('guest/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}'></script>

        <!-- IE9 Placeholder -->
        <!--[if IE 9 ]>
            <script src="{{ url('plugins/jquery-placeholder/jquery.placeholder.min.js') }}"></script>
        <![endif]-->

        <!-- Site functions and actions -->
        <script src="{{ url('js/app.min.js') }}"></script>

    </body>
</html>