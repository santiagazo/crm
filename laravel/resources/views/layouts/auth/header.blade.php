
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="google-site-verification" content="LojR0_-pHUNKi5O08Q7Na3hNc0AoG5tncCMMnk-9oRg" />
  
  <title>@yield('title') | {{ $location->name }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ url('auth/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ url('auth/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('auth/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ url('auth/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ url('auth/plugins/select2/select2.min.css') }}">
  <link rel="stylesheet" href="{{ url('auth/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">

  <link rel="stylesheet" href="{{ url('auth/plugins/sweetalert/dist/sweetalert.css') }}">
  <link rel="stylesheet" href="{{ url('auth/plugins/sweetalert/themes/google/google.css') }}">

  <link rel="stylesheet" href="{{ url('auth/css/custom.css') }}">

  @yield('css')

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="{{ url('home') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">{{ $location->initials }}</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">{{ $location->name }}</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ url($authUser->avatar ?: 'guest/images/user_empty.png') }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ $authUser->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ url($authUser->avatar ?: 'guest/images/user_empty.png') }}" class="img-circle" alt="User Image">

                <p>
                  {{ $authUser->name }} - {{ $authUser->roles->first()->name }}
                  <small>Member since {{ Carbon\Carbon::parse($authUser->created_at)->format('M Y') }}</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ $authUser->id }}" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                          onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                          Logout
                      </a>

                      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ url($authUser->avatar ?: 'guest/images/user_empty.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ $authUser->name }}</p>
        </div>
      </div>
      <!-- search form -->
      {{-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> --}}
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{{ Request::is('dashboard') || Request::is('dashboard/*') ? 'active' : '' }}">
          <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="{{ Request::is('admin/estimates') || Request::is('admin/estimates/*') ? 'active' : '' }}">
          <a href="{{ url('/admin/estimates') }}">
            <i class="fa fa-comment"></i> <span>Estimate Requests</span>
          </a>
        </li>
        <li class="{{ Request::is('admin/bids') || Request::is('admin/bids/*') ? 'active' : '' }}">
          <a href="{{ url('/admin/bids') }}">
            <i class="fa fa-pencil"></i> <span>Bids</span>
          </a>
        </li>
        <li class="{{ Request::is('admin/jobs') || Request::is('admin/jobs/*') ? 'active' : '' }}">
          <a href="{{ url('/admin/jobs') }}">
            <i class="fa fa-home"></i> <span>Jobs</span>
          </a>
        </li>
        <li class="{{ Request::is('admin/people') || Request::is('admin/people/*') ? 'active' : '' }}">
          <a href="{{ url('/admin/people') }}">
            <i class="fa fa-user"></i> <span>People</span>
          </a>
        </li>
        <li class="{{ Request::is('admin/templates') || Request::is('admin/templates/*') ? 'active' : '' }}">
          <a href="{{ url('/admin/templates') }}">
            <i class="fa fa-clipboard"></i> <span>Templates</span>
          </a>
        </li>
        <li class="{{ Request::is('admin/categories') || Request::is('admin/categories/*')  ? 'active' : '' }}">
          <a href="{{ url('/admin/categories') }}">
            <i class="fa fa-list"></i> <span>Product Categories</span>
          </a>
        </li>
        <li class="{{ Request::is('admin/products') || Request::is('admin/products/*') ? 'active' : '' }}">
          <a href="{{ url('/admin/products') }}">
            <i class="fa fa-shield"></i> <span>Products</span>
          </a>
        </li>
        <li class="{{ Request::is('admin/companies') || Request::is('admin/companies/*') ? 'active' : '' }}">
          <a href="{{ url('/admin/companies') }}">
            <i class="fa fa-building"></i> <span>Companies</span>
          </a>
        </li>
        <li class="{{ Request::is('admin/payroll') || Request::is('admin/payroll/*') ? 'active' : '' }}">
          <a href="{{ url('/admin/payroll') }}">
            <i class="fa fa-address-card"></i> <span>Payroll</span>
          </a>
        </li>
        <li class="{{ Request::is('admin/users') || Request::is('admin/users/*') ? 'active' : '' }}">
          <a href="{{ url('/admin/users') }}">
            <i class="fa fa-users"></i> <span>Users</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1 class="h1-title">@yield('title')</h1>
          @include('errors.flash')
        </div>
      </div>
    </div>
    
    