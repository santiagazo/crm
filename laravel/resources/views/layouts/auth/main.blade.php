@include('layouts/auth/header')

<!-- - Content - -->
@yield('content')
<!-- - End Content - -->

@include('layouts/auth/footer')