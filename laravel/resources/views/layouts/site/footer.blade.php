		<footer id="footer">
            <div class="footer__bottom">
                <div class="container">

                    <div class="row">
                        <ul class="list-inline">
                            <li>
                                <span class="footer__copyright">© Milne Brothers Roofing</span>
                            </li>
                            <li>
                                <a href="{{ url('/about') }}">About Us</a>
                            </li>
                            <li>
                                <a href="{{ url('/warranty') }}">Warranty</a>
                            </li>
                            <li>
                                <a href="{{ url('/contact') }}">Careers</a>
                            </li>
                            <li>
                                <a href="{{ url('/contact') }}">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="footer__to-top" data-rmd-action="scroll-to" data-rmd-target="html">
                    <i class="zmdi zmdi-chevron-up"></i>
                </div>
            </div>
        </footer>

        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1>Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="ie-warning__inner">
                    <ul class="ie-warning__download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="images/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="images/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="images/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="images/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="images/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>
        <![endif]-->

        <!-- Javascript -->

        <!-- jQuery -->
        <script src="{{ url('guest/plugins/jquery/dist/jquery.min.js') }}"></script>

        <!-- Bootstrap -->
        <script src="{{ url('guest/plugins/bootstrap/dist/js/bootstrap.min.js') }}"></script>

        <!-- Waves button ripple effects -->
        <script src="{{ url('guest/plugins/Waves/dist/waves.min.js') }}"></script>

        <!-- Slick Carousel - Custom Alerts -->
        <script src="{{ url('guest/plugins/slick-carousel/slick/slick.min.js') }}"></script>

        <!-- NoUiSlider -->
        <script src="{{ url('guest/plugins/nouislider/distribute/nouislider.min.js') }}"></script>

        <!-- Autosize -->
        <script src="{{ url('guest/plugins/autosize/dist/autosize.js') }}"></script>

		<!-- Forms -->
        <script src='{{ url('guest/plugins/select2/dist/js/select2.full.min.js') }}'></script>
    	<script src='{{ url('guest/plugins/moment/min/moment-with-locales.min.js') }}'></script>
    	<script src='{{ url('guest/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}'></script>

        <!-- IE9 Placeholder -->
        <!--[if IE 9 ]>
            <script src="{{ url('plugins/jquery-placeholder/jquery.placeholder.min.js') }}"></script>
        <![endif]-->

        <!-- Site functions and actions -->
        <script src="{{ url('js/app.min.js') }}"></script>

        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $(function(){
                changeHeader();
                $( window ).bind( "resize", changeHeader );

                function changeHeader() {
                    if($('header').width() > 991){
                        $('header').attr('id', 'header-alt')
                    }else{
                        $('header').attr('id', 'header')
                    }
                }
            });
        </script>

        @yield('js')
        @yield('jsFlash')
    </body>
</html>
