<!DOCTYPE html>
<html lang="en">
<!--[if IE 9 ]><html lang="en" class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta author='Jay Lara - darkcherrysystems.com'>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="google-site-verification" content="LojR0_-pHUNKi5O08Q7Na3hNc0AoG5tncCMMnk-9oRg" />

        <title>@yield('title') | {{ $location->name }}</title>

        <!-- Vendors -->

        <!-- Bootstrap -->
		<link href="{{ url('guest/plugins/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Material design colors -->
        <link href="{{ url('guest/plugins/material-design-iconic-font/dist/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- CSS animations -->
        <link rel="stylesheet" href="{{ url('guest/plugins/animate.css/animate.min.css') }}">
        <!-- Slick Carousel -->
        <link rel="stylesheet" href="{{ url('guest/plugins/slick-carousel/slick/slick.css') }}">
        <!-- NoUiSlider - Input Slider -->
        <link rel="stylesheet" href="{{ url('guest/plugins/nouislider/distribute/nouislider.min.css') }}">
        <!-- Site -->
        <link rel="stylesheet" href="{{ url('guest/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
        <link rel="stylesheet" href="{{ url('guest/plugins/select2/dist/css/select2.min.css') }}">
        <link rel="stylesheet" href="{{ url('guest/css/app_1.min.css') }}">
        <link rel="stylesheet" href="{{ url('guest/css/app_2.min.css') }}">
        <link rel="stylesheet" href="{{ url('guest/css/custom.css') }}">

        <!-- Page Loader JS -->
        <script src="{{ url('guest/js/page-loader.min.js') }}" async></script>
        @yield('css')
    </head>

    <body>
        <!-- Start page loader -->
        <div id="page-loader">
            <div class="page-loader__spinner"></div>
        </div>
        <!-- End page loader -->

        <header id="header-alt">
            <div class="header__top">
                <div class="container">
                    <ul class="top-nav">
                        <li class="dropdown top-nav__guest">
                        	@if(Auth::check())
                            	<a href="{{ url('dashboard') }}">{{ Auth::user()->name }}</a>
                            @else
                                <a href="{{ url('/register') }}">Register</a>
                            @endif
                        </li>

                        <li class="dropdown top-nav__guest">
                        	@if(Auth::check())
                            	<a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            @else
                                <a  href="{{ url('/login') }}">Login</a>
                            @endif
                        </li>

                        <li class="pull-right top-nav__icon">
                            <a href="https://www.facebook.com/pages/Milne-Brothers-Roofing/122606204463687"><i class="zmdi zmdi-facebook"></i></a>
                        </li>
                        <li class="pull-right top-nav__icon">
                            <a href="https://twitter.com/search?q=milne%20brothers%20roofing&src=typd"><i class="zmdi zmdi-twitter"></i></a>
                        </li>
                        <li class="pull-right top-nav__icon">
                            <a href="https://maps.google.com/?cid=7936184157434939732"><i class="zmdi zmdi-google"></i></a>
                        </li>

                        <li class="pull-right hidden-xs"><span><i class="zmdi zmdi-email"></i>{{ $location->email }}</span></li>
                        <li class="pull-right hidden-xs"><span><i class="zmdi zmdi-phone"></i><a href="tel:{{ $location->phone }}">{{ $location->phone }}</a></span></li>
                    </ul>
                </div>
            </div>

            <div class="header__main">
                <div class="container text-center">
                    <a class="logo" href="{{ url('/home') }}">
                        <img src="{{ url('guest/images/logos/mbr-transparent-bg.png') }}" class="img-responsive" alt="">
                    </a>

                    <div class="navigation-trigger visible-xs visible-sm" data-rmd-action="block-open" data-rmd-target=".navigation">
                        <i class="zmdi zmdi-menu"></i>
                    </div>

                    <div class="navigation-centered">
                        <a href="{{ url('/estimate') }}" class="btn btn-round">Request Estimate</a>
                    </div>

                    <ul class="navigation">
                        <li class="visible-xs visible-sm"><a class="navigation__close" data-rmd-action="navigation-close" href=""><i class="zmdi zmdi-long-arrow-right"></i></a></li>


                        <li><a href="{{ url('/home') }}">Home</a></li>

                        <li><a href="{{ url('/shingle') }}">Shingle Colors</a></li>

                        <li><a href="{{ url('/about') }}">About Us</a></li>

                        <li class="navigation__dropdown">
                            <a href="" class="prevent-default">More</a>

                            <ul class="navigation__drop-menu navigation__drop-menu--right">
								@if(Auth::check() && Auth::user()->hasRole('Admin'))
    								<li><a href="{{ url('dashboard') }}">Dashboard</a></li>
                                    <li><a href="{{ url('admin/jobs') }}">Jobs</a></li>
                                    <li><a href="{{ url('admin/bids') }}">Bids</a></li>
                                    @if(Auth::user()->hasRole('Owner'))
                                        <li><a href="{{ url('admin/payroll') }}">Payroll</a></li>
                                    @endif
                                    <hr>
                                @endif
                                <li><a href="{{ url('/contact') }}">Contact</a></li>
                                <li><a href="{{ url('/testimonials') }}">Testimonials</a></li>
                                <li><a href="{{ url('/about#outreach') }}">Community Outreach</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

        </header>

        <div class="container">
            <div class="pt-145">
                @include('errors.flash')
            </div>
        </div>
