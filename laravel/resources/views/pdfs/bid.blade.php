<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Milne Brothers Roofing | Quote</title>
        <meta name="Quote" content="Quote for Potential Client">
        <meta name="Jay Lara | DarkCherrySystems.com" content="">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <style>
            .well.well-primary{
                padding: 5px;
                background-color: #dce6f1;
                margin-bottom: 5px;
            }
            .h1, .h2, .h3, h1, h2, h3 {
                margin-top: 0px;
                margin-bottom: 5px;
            }
            p {
               margin: 0;
            }
            .mb-10{
                margin-bottom: 10px;
            }
            .mt-20{
                margin-top: 20px;
            }
            .mtb-20{
                margin-bottom: 20px;
                margin-top: 20px;
            }
            .mb-20{
                margin-bottom: 20px;
            }
            .pb-5{
                padding-bottom: 5px !important;
            }
            .pt-5{
                padding-top: 5px !important;
            }
            .pb-10{
                padding-bottom: 10px !important;
            }
            .pt-10{
                padding-top: 10px !important;
            }
            .extras{
                white-space: nowrap;
                text-align: right;
            }
            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                padding: 0px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }
            .no-mb{
                margin-bottom: 0px;
            }
            .well-basic{
                background-color: #fff;
                padding: 0px;
                margin-bottom: 5px;
            }
            .well-basic .table>thead>tr>th{
                background-color: #e7e7e7;
                padding: 5px;
            }
            .well-basic .table>tbody>tr>td{
                border-top: none;
                padding: 0 8px;
            }
            hr.page-break{
                border: none;
                page-break-after: always;
            }
            li > ol {
                counter-reset: list;
            }
            li > ol > li {
                list-style: none;
                position: relative;
            }
            li > ol > li:before {
                counter-increment: list;
                content: counter(list, lower-alpha) ". ";
                position: absolute;
                left: -1.4em;
            }
            .no-wrap{
                white-space: nowrap;
            }
            .text-uppercase{
                text-transform: uppercase;
            }
        </style>

    </head>
    <body>
        <div class="page-wrapper">
            @section('header')
                <div class="row">
                    <div class="col-xs-12">
                        <div class="well well-primary text-center">
                            <h2><strong>Milne Brothers Roofing LLC</strong></h2>

                            <div class="row">
                                <div class="col-xs-offset-1 col-xs-2">
                                    <h4><strong>Office</strong></h4>
                                    <h6>(801) 268-4496</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Brent</strong></h4>
                                    <h6>(801) 230-7460</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Anthony</strong></h4>
                                    <h6>(801) 558-6320</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Nathan</strong></h4>
                                    <h6>(801) 647-1137</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Fax</strong></h4>
                                    <h6>(801) 268-4496</h6>
                                </div>
                            </div>
                            <h4>5712 S. 800 W. Murray, UT 84123</h4>
                            <a href="www.milnebrothersroofing.com">www.milnebrothersroofing.com</a> | <a href="mailto:milnebrothersroofing@gmail.com">milnebrothersroofing@gmail.com</a>
                        </div>
                    </div>
                </div>
            @endsection

            @yield('header')

            <div class="row">
                <div class="col-xs-6">
                    <p>{{ Carbon\Carbon::now()->toFormattedDateString() }}</p>
                    <h4>
                        <strong>{{ $bid->person->name }}</strong><br>
                        <strong>{{ $bid->address }}</strong><br>
                        <strong><small>{{ $bid->city }}, {{ $bid->state }} {{ $bid->zip }}</small></strong>
                        <strong><small>{!! $bid->subdivision ? "<br/>Subdivision: ".$bid->subdivision : NULL !!}</small></strong>
                        <strong><small>{!! $bid->lot ? "<br/>Lot: ".$bid->lot : NULL !!}</small></strong>
                    </h4>
                </div>
                <div class="col-xs-6">
                    <h3 class="pull-right text-danger"><strong> In Business Since 1978!</strong></h3>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>The Price Includes</th>
                                <th width=150>Quantities</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($bid->material_list->categories as $category)
                                <tr>
                                    <td>{{ $category->name }} {{ $category->pivot->color ? '- '.str_unslug($category->pivot->color) : NULL }}</td>
                                    <td>{{ $category->pivot->qty }} {{ str_unslug($category->unit) }}</td>
                                </tr>
                            @empty
                                <tr>
                                    There are no products or services tied to this material list.
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h4 class="no-mb">
                        <strong>10-Year Limited Labor Warranty</strong> <strong class="pull-right text-uppercase">{{ $bid->type->name }} PRICE: 
                            <u>
                                {{ formatPrice($bid->amount) }}
                            </u>
                        </strong>
                    </h4>
                </div>
            </div>
            <div class="row mb-20">
                <div class="col-md-12">
                    <h4><small class="pull-right">w/ Cash Discount <strong>{{ formatPrice($bid ? $bid->amount - $bid->amount*.03 : NULL) }}</strong></small></h4>
                </div>
            </div>
            <div class="row mb-10">
                <div class="col-md-12">
                    <div class="well well-basic">
                        <table class="table no-mb">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan=3 >OPTIONAL ITEMS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="pt-5"><span class="pull-right"> [ If paid within 15 days of the Invoice Date w/ a check or cash ] <strong>3% Cash Discount 15</strong></span></td>
                                    <td class="pt-5" width=100><span class="text-center text-danger">({{ $bid ? formatPrice($bid->amount*.03) : NULL }})</span></td>
                                    <td class="pt-5" width=20><input type="checkbox" class="checkbox text-center"></td>
                                </tr>
                                @if($bid->is_pre_dry_in)
                                    <tr>
                                        <td><span class="pull-right"> [ Additional protection against slow wicking moisture ] <strong>Pre Dry-In</strong></span></td>
                                        <td width=100><span class="text-center">{{ $bid ? formatPrice($bid->square_count*8) : NULL }}</span></td>
                                        <td width=20><input type="checkbox" class="checkbox text-center"></td>
                                    </tr>
                                @endif
                                @if($bid->is_six_nail)
                                    <tr>
                                        <td><span class="pull-right"> [ Increases wind warranty to 130 MPH ] <strong>6 Nails/Shingle</strong></span></td>
                                        <td width=100><span class="text-center">{{ $bid ? formatPrice($bid->square_count*10) : NULL }}</span></td>
                                        <td width=20><input type="checkbox" class="checkbox text-center"></td>
                                    </tr>
                                @endif
                                @if($bid->is_owner_tear_off)
                                    <tr>
                                        <td><span class="pull-right"> [ Includes Dumpster and tool rental @ $250] <strong>Homeowner Tearoff</strong></span></td>
                                        <td width=100><span class="text-center text-danger">({{ $bid ? formatPrice($bid->labor_per_square_amount*.90) : NULL }})</span></td>
                                        <td width=20><input type="checkbox" class="checkbox text-center"></td>
                                    </tr>
                                @endif
                                <tr>
                                    <td class="pb-5"><span class="pull-right small no-wrap"><em>*Options are either added or subtracted from BID PRICE above.</em></span></td>
                                    <td class="pb-5"></td>
                                    <td class="pb-5"></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="row mb-10">
                <div class="col-md-12">
                    The only thing that is not included in this bid is the price to replace any bad plywood that we find under the shingles, if there
                    is any. The additional charge is (~$50/pc).
                </div>
            </div>
        </div>

        <hr class="page-break">

        <div class="page-wrapper">
            @yield('header')
        </div>
        
        <div class="row">
            <div class="col-md-12 mt-20">
                <p>
                    We are a small company and we strive to have <span class="text-danger"><strong><u>AT LEAST ONE OWNER ON EVERY JOB</u></strong></span>. <span class="text-primary"><strong>OUR
                    EXPERIENCE AND QUALITY SELLS ITSELF!</strong></span> You want your roof done right, which is why the
                    owners of Milne Brothers Roofing decided they will always work alongside their employees to
                    personally guarantee your 100% satisfaction. <u>We'll even match most of our competitors bids</u> so call or
                    email us TODAY!
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <ol>
                    <li class="mtb-20">
                        <p>
                            <strong>Respond to this bid by giving us a call or sending us an email.</strong> Nathan will then put you on our schedule. It takes us about 2 weeks to start your job; however, it usually only takes us 2 days to
                            complete your job once we start; one day to tear-off and dry-in your roof and the next day to shingle it. We don’t like your roof to be exposed to the elements any longer than it needs to. We also wait until we have two good days of weather. <em>(If you need your roof done sooner, in the case that your roof is leaking, please let us know and we will see what we can do to accommodate you).</em>
                        </p>
                    </li>
                    <li class="mb-20">
                        <p>
                            <strong>Choose a shingle color.</strong> We have three convenient ways to help you choose a color
                        </p>
                        <ol>
                            <li>
                                Visit our website: <a href="{{ url('/shingle') }}">{{ url('/shingle') }}</a>
                            </li>
                            <li>
                                Give us a few colors that you like and we can send you addresses of nearby houses we’ve already shingled with those colors.
                            </li>
                            <li>
                                Ask us for a recommendation. We almost always write down a couple colors that we think would look good on your roof.
                            </li>
                        </ol>
                    </li>
                    <li class="mb-20">
                        <p>
                            <strong>Pay us when we are all finished.</strong> We don’t require any pre-payments or contracts. <span class="text-danger"><strong>We now accept credit cards!
                            (Visa, MC & Discover)</strong></span>. <strong>If you trust us with your roof, we trust that you’ll pay us!</strong>
                        </p>
                    </li>
                    <li class="mb-20">
                        <p>
                            <strong>Fill out a brief survey (only 6 questions) to help us improve our company.</strong> <a href="{{ url('/testimonials') }}">{{ url('/testimonials') }}</a>
                        </p>
                    </li>
                    <li class="mb-20">
                        <p>
                            <strong>Nothing else.</strong> You should never have to call us again until you need a new roof in about 30 years. If you have any questions, don’t hesitate to ask. We want to make sure that you understand everything about our bid so that you can compare it accurately with our competition. Remember, your house is your most important asset and your roof protects it, so quality is very important. We GUARANTEE quality by working alongside our employees; so, you’ll have at least one owner of our company on your job, if not all three. We are fully licensed and insured!
                        </p>
                    </li>
                </ol>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <p>
                    If you have any questions, don’t hesitate to ask. We want to make sure that you understand everything about our bid so that you can compare it accurately with our competition. Remember, your house is your most important asset and your roof protects it, so quality is very important. <span class="text-danger"><strong>We GUARANTEE quality by working alongside our employees; so, you’ll have at least one owner of our company on your job, if not all three.</strong></span> We are fully licensed and insured!
                </p>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>