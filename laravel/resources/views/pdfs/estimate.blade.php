<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Milne Brothers Roofing | Bid/Repair Sheet</title>
        <meta name="Quote" content="Quote for Potential Client">
        <meta name="Jay Lara | DarkCherrySystems.com" content="">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <style>
            .well.well-primary{
                padding: 10px 5px 5px 5px;
                background-color: #dce6f1;
                margin-bottom: 5px;
            }
            .h1, .h2, .h3, h1, h2, h3 {
                margin-top: 0px;
                margin-bottom: 5px;
            }
            p {
               margin: 0;
            }
            .mtb-0{
                margin-bottom: 0;
                margin-top: 0;
            }
            .mtb-5{
                margin-bottom: 5px;
                margin-top: 5px;
            }
            .mb-0{
                margin-bottom: 0;
            }
            .mb-10{
                margin-bottom: 10px;
            }
            .mtb-20{
                margin-bottom: 20px;
                margin-top: 20px;
            }
            .mb-20{
                margin-bottom: 20px;
            }
            .pb-5{
                padding-bottom: 5px !important;
            }
            .pt-5{
                padding-top: 5px !important;
            }
            .pb-10{
                padding-bottom: 10px !important;
            }
            .pt-10{
                padding-top: 10px !important;
            }
            .row-title{
                padding-right: 5px !important;
                border: none !important;
            }
            .extras{
                white-space: nowrap;
                text-align: right;
            }
            .table>tbody>tr>td, 
            .table>tbody>tr>th, 
            .table>tfoot>tr>td, 
            .table>tfoot>tr>th, 
            .table>thead>tr>td, 
            .table>thead>tr>th {
                padding: 0px;
                line-height: 1.42857143;
                vertical-align: top;
                border-bottom: 1px solid #8c8a8a;
                border-top: none;
            }
            .three-column .table>tbody>tr>td, 
            .three-column .table>tbody>tr>th, 
            .three-column .table>tfoot>tr>td, 
            .three-column .table>tfoot>tr>th, 
            .three-column .table>thead>tr>td, 
            .three-column .table>thead>tr>th {
                border-right: 1px solid #8c8a8a;
            }
            .table.with-border>tbody>tr>td, 
            .table.with-border>tbody>tr>th, 
            .table.with-border>tfoot>tr>td, 
            .table.with-border>tfoot>tr>th, 
            .table.with-border>thead>tr>td, 
            .table.with-border>thead>tr>th {
                padding: 0px;
                line-height: 1.42857143;
                vertical-align: top;
                border-bottom: 1px solid #000;
                border-top: none;
            }
            .image-holder{
                margin-top: 20px;
                border: 1px solid #000;
            }
            .no-mb{
                margin-bottom: 0px;
            }
            .well-basic{
                background-color: #fff;
                padding: 0px;
                margin-bottom: 5px;
            }
            .well-basic .table>thead>tr>th{
                background-color: #e7e7e7;
                padding: 5px;
            }
            .well-basic .table>tbody>tr>td{
                border-top: none;
                padding: 0 8px;
            }
            li > ol {
                counter-reset: list;
            }
            li > ol > li {
                list-style: none;
                position: relative;
            }
            li > ol > li:before {
                counter-increment: list;
                content: counter(list, lower-alpha) ". ";
                position: absolute;
                left: -1.4em;
            }
            .no-wrap{
                white-space: nowrap;
            }
            .text-uppercase{
                text-transform: uppercase;
            }
            table.with-border{
                border-top: 1px solid #000;
                border-right: 1px solid #000;
                border-bottom: 1px solid #000;
            }
            table.with-border-dotted{
                border: 1px dotted #000;
            }
            .no-padding-left{
                padding-left: 0;
            }
            .no-padding-right{
                padding-right: 0;
            }
        </style>

    </head>
    <body>
        <div class="page-wrapper">
            @section('header')
                <div class="row">
                    <div class="col-xs-12">
                        <em>Go Ahead Date: _____________________________</em>
                        <span class="pull-right">
                            Print Date: {{ Carbon\Carbon::now()->toFormattedDateString() }} | <strong>Request Date:</strong> {{ $estimate->created_at->toFormattedDateString() }}
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h4 class="mtb-5"><strong>Milne Brothers Roofing LLC - Bid/Repair Sheet</strong></h4>
                        </div>
                    </div>
                </div>
            @endsection

            @yield('header')

            <div class="row">
                <div class="col-xs-5">
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td class="text-right row-title" width='50px'>Name:</td>
                                <td>{{ $estimate->person->name }}</td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Address:</td>
                                <td>{{ $estimate->project_address }}</td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">City:</td>
                                <td>{{ $estimate->project_city }}</td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Zip:</td>
                                <td>{{ $estimate->project_zip }}</td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Email:</td>
                                <td>{{ $estimate->person->email }}</td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Phone:</td>
                                <td>{{ $estimate->person->phone }}</td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Other:</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-7">
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td class="text-right row-title">House Colors:</td>
                                <td width="250px"></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Drip Color:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Current Shingle Color:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Recommended Colors:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Pitches:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Layers:</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

           
            <div class="row">
                <div class="col-xs-7 no-padding-right">
                    <table class="table mb-10">
                        <tbody>
                            <tr height='375' width='300' class='image-holder'>
                                <td colspan="8">
                                    @if(File::exists(public_path('estimate_images/'.$estimate->id.'/satellite.png')) && !$estimate->no_satellite_image)
                                        <img src="{{ url('estimates/satellite/'.$estimate->id) }}" alt="" class="center-block img-responsive mt-10">
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Valleys:</td>
                                <td width="75"></td>
                                <td class="text-right row-title">BEH:</td>
                                <td width="75"></td>
                                <td class="text-right row-title">BEG:</td>
                                <td width="75"></td>
                                <td class="text-right row-title">Rakes:</td>
                                <td width="75"></td>
                            </tr>
                        </tbody>
                    </table>
                    NOTES:
                    <table class="table with-border-dotted">
                        <tbody>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            @if($estimate->referred_by)
                                <tr>
                                    <td class="text-right row-title small">Referral:</td>
                                    <td class="small">{{ $estimate->referred_by }}</td>
                                </tr>
                            @endif
                            @if($estimate->additional_information)
                                <tr>
                                    <td nowrap class="text-right row-title small">More Info:</td>
                                    <td class="small">{{ $estimate->additional_information }}</td>
                                </tr>
                            @endif
                            <tr>
                                <td></td>
                                <td class="small">{{ $estimate->is_urgent ? "URGENT" : 'Not Urgent' }} | {{ str_replace(',', ', ', $estimate->type) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-5 no-padding-left three-column">
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td class="text-right row-title">Tear-Off Labor:</td>
                                <td width="50"></td>
                                <td width="50"></td>
                                <td width="50"></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Dumpster:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Plywood:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Pre Dry-In:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Tearoff Markup:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Redo Markup:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Pitch Markup 1:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Pitch Markup 2:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table with-border mb-0">
                        <tbody>
                            <tr>
                                <td class="text-right row-title">30 Year Archs:</td>
                                <td width="50"></td>
                                <td width="50"></td>
                                <td width="50"></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">___________' H&R</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">_________' Starters</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">______' Ice & Water</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Felt</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">_______' Drip Edge</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Vents</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Steps ___ ___' 4x4</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Pipes 1.5 - 3"</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">_________ Heat Flu</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td class="text-right row-title">Gables:</td>
                                <td width="50"></td>
                                <td width="50"></td>
                                <td width="50"></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Eyes:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Corner Return:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">DEV:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Clppd Gbls:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Bay Window:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Swamp Cooler:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Duct Removal:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Chimney:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Electric Vents:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Skylight:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Satellite:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">TPO:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">TPO Drip:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Gas Surcharge:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Total:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-right row-title">Credit Card Total:</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row mb-10">
                <div class="col-md-12">
                    <div class="well well-basic">
                        <table class="table no-mb">
                            <tbody>
                                <tr>
                                    <td class="pt-5" width=20><input type="checkbox" class="checkbox text-right"></td>
                                    <td class="pt-5"><span class="text-left">Meet Homeowner</span></td>

                                    <td class="pt-5" width=20><input type="checkbox" class="checkbox text-right"></td>
                                    <td class="pt-5"><span class="text-left">Call Bid</span></td>

                                    <td class="pt-5" width=20><input type="checkbox" class="checkbox text-right"></td>
                                    <td class="pt-5"><span class="text-left">Email Bid</span></td>
                                </tr>
                                <tr>
                                    <td class="pt-5" width=20><input type="checkbox" class="checkbox text-right"></td>
                                    <td class="pt-5"><span class="text-left">Follow-Up</span></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>