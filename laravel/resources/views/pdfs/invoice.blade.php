<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Milne Brothers Roofing | Quote</title>
        <meta name="Quote" content="Quote for Potential Client">
        <meta name="Jay Lara | DarkCherrySystems.com" content="">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <style>
            .well.well-primary{
                padding: 5px;
                background-color: #dce6f1;
                margin-bottom: 5px;
            }
            .h1, .h2, .h3, h1, h2, h3 {
                margin-top: 0px;
                margin-bottom: 5px;
            }
            p {
               margin: 0;
            }
            .mb-10{
                margin-bottom: 10px;
            }
            .mtb-20{
                margin-bottom: 20px;
                margin-top: 20px;
            }
            .mb-20{
                margin-bottom: 20px;
            }
            .pb-5{
                padding-bottom: 5px !important;
            }
            .pt-5{
                padding-top: 5px !important;
            }
            .pb-10{
                padding-bottom: 10px !important;
            }
            .pt-10{
                padding-top: 10px !important;
            }
            .extras{
                white-space: nowrap;
                text-align: right;
            }
            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                padding: 0px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }
            .no-mb{
                margin-bottom: 0px;
            }
            .well-basic{
                background-color: #fff;
                padding: 0px;
                margin-bottom: 5px;
            }
            .well-basic .table>thead>tr>th{
                background-color: #e7e7e7;
                padding: 5px;
            }
            .well-basic .table>tbody>tr>td{
                border-top: none;
                padding: 0 8px;
            }
            hr.page-break{
                border: none;
                page-break-after: always;
            }
            li > ol {
                counter-reset: list;
            }
            li > ol > li {
                list-style: none;
                position: relative;
            }
            li > ol > li:before {
                counter-increment: list;
                content: counter(list, lower-alpha) ". ";
                position: absolute;
                left: -1.4em;
            }
            .no-wrap{
                white-space: nowrap;
            }
            .text-uppercase{
                text-transform: uppercase;
            }
        </style>

    </head>
    <body>
        <div class="page-wrapper">
            @section('header')
                <div class="row">
                    <div class="col-xs-12">
                        <div class="well well-primary text-center">
                            <h2><strong>Milne Brothers Roofing LLC</strong></h2>

                            <div class="row">
                                <div class="col-xs-offset-1 col-xs-2">
                                    <h4><strong>Office</strong></h4>
                                    <h6>(801) 268-4496</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Brent</strong></h4>
                                    <h6>(801) 230-7460</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Anthony</strong></h4>
                                    <h6>(801) 558-6320</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Nathan</strong></h4>
                                    <h6>(801) 647-1137</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Fax</strong></h4>
                                    <h6>(801) 268-4496</h6>
                                </div>
                            </div>
                            <h4>5712 S. 800 W. Murray, UT 84123</h4>
                            <a href="www.milnebrothersroofing.com">www.milnebrothersroofing.com</a> | <a href="mailto:milnebrothersroofing@gmail.com">milnebrothersroofing@gmail.com</a>
                        </div>
                    </div>
                </div>
            @endsection

            @yield('header')

            <div class="row">
                <div class="col-xs-6">
                    <p>{{ Carbon\Carbon::now()->toFormattedDateString() }}</p>
                    <h4>
                        <strong>{{ $job->person->name }}</strong><br>
                        <strong>{{ $job->person->address }}</strong><br>
                        <strong><small>{{ $job->person->city }}, {{ $job->person->state }} {{ $job->person->zip }}</small></strong>
                        <strong><small>{!! $job->subdivision ? "<br/>Subdivision: ".$job->subdivision : NULL !!}</small></strong>
                        <strong><small>{!! $job->lot ? "<br/>Lot: ".$job->lot : NULL !!}</small></strong>
                    </h4>
                </div>
                <div class="col-xs-6">
                    <h3 class="pull-right text-danger"><strong> Invoice</strong></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>The price includes</th>
                                <th width=100>Quantities</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $category_price = [];
                                $square_count = [];
                                $labor_per_square_amount = [];
                            @endphp
                            @forelse($job->material_list->categories as $category)
                                <tr>
                                    @if($category->product)
                                        <td>{{ $category->products_through_pivot->first()->name }} {{ $category->pivot->color ? '- '.str_unslug($category->pivot->color) : NULL }}</td>
                                    @else
                                        <td>{{ $category->name }} {{ $category->pivot->color ? '- '.str_unslug($category->pivot->color) : NULL }}</td>
                                    @endif
                                    <td>{{ $category->pivot->qty }} {{ str_unslug($category->unit) }}</td>
                                </tr>
                                @php
                                    $category_price[] = $category->pivot->category_price;
                                    if($category->unit == 'square'){
                                        $square_count[] = $category->pivot->qty;
                                    }
                                    if($category->unit == 'labor/square'){
                                        $labor_per_square_amount[] = $category->pivot->category_price;
                                    }
                                @endphp
                            @empty
                                <tr>
                                    There are no products or services tied to this material list.
                                </tr>
                            @endforelse
                            @php
                                $category_price = (int) array_sum($category_price);
                                $job_price = $category_price*$job->tax->amount/100+$category_price;
                            @endphp
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h4 class="no-mb">
                        <strong>10-Year Limited Labor Warranty</strong> <strong class="pull-right text-uppercase">{{ $job->type->name }} PRICE: 
                            <u>
                                {{ formatPrice($job_price) }}
                            </u>
                        </strong>
                    </h4>
                </div>
            </div>
            <div class="row mb-20">
                <div class="col-md-12">
                    <h4><small class="pull-right">w/ Cash Discount <strong>{{ formatPrice($job_price - $job_price*.03) }}</strong></small></h4>
                </div>
            </div>
            <div class="row mb-10">
                <div class="col-md-12">
                    <div class="well well-basic">
                        <table class="table no-mb">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan=3 >OPTIONAL ITEMS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="pt-5"><span class="pull-right"> [ If paid within 15 days of the Invoice Date w/ a check or cash ] <strong>3% Cash Discount 15</strong></span></td>
                                    <td class="pt-5" width=100><span class="text-center text-danger">({{ $job ? formatPrice($job->invoice_amount*.03) : NULL }})</span></td>
                                    <td class="pt-5" width=20><input type="checkbox" class="checkbox text-center"></td>
                                </tr>
                                @if($job->is_pre_dry_in && $job->square_count)
                                    <tr>
                                        <td><span class="pull-right"> [ Additional protection against slow wicking moisture ] <strong>Pre Dry-In</strong></span></td>
                                        <td width=100><span class="text-center">{{ $job ? formatPrice($job->square_count*8) : NULL }}</span></td>
                                        <td width=20><input type="checkbox" class="checkbox text-center"></td>
                                    </tr>
                                @endif
                                @if($job->is_six_nail && $job->square_count)
                                    <tr>
                                        <td><span class="pull-right"> [ Increases wind warranty to 130 MPH ] <strong>6 Nails/Shingle</strong></span></td>
                                        <td width=100><span class="text-center">{{ $job ? formatPrice($job->square_count*10) : NULL }}</span></td>
                                        <td width=20><input type="checkbox" class="checkbox text-center"></td>
                                    </tr>
                                @endif
                                @if($job->is_owner_tear_off && $job->labor_per_square_amount)
                                    <tr>
                                        <td><span class="pull-right"> [ Includes Dumpster and tool rental @ $250] <strong>Homeowner Tearoff</strong></span></td>
                                        <td width=100><span class="text-center text-danger">({{ $job ? formatPrice($job->labor_per_square_amount*.90) : NULL }})</span></td>
                                        <td width=20><input type="checkbox" class="checkbox text-center"></td>
                                    </tr>
                                @endif
                                <tr>
                                    <td class="pb-5"><span class="pull-right small no-wrap"><em>*Options are either added or subtracted from INVOICE PRICE above.</em></span></td>
                                    <td class="pb-5"></td>
                                    <td class="pb-5"></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>