<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Milne Brothers Roofing | Job Sheet</title>
        <meta name="Quote" content="Job Sheet">
        <meta name="Jay Lara | DarkCherrySystems.com" content="">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <style>
            .well.well-primary{
                padding: 5px;
                background-color: #e7e7e7;
                margin-bottom: 5px;
            }
            .h1, .h2, .h3, h1, h2, h3 {
                margin-top: 0px;
                margin-bottom: 5px;
            }
            p {
               margin: 0;
            }
            .mb-10{
                margin-bottom: 10px;
            }
            .mtb-20{
                margin-bottom: 20px;
                margin-top: 20px;
            }
            .mb-20{
                margin-bottom: 20px;
            }
            .pb-5{
                padding-bottom: 5px !important;
            }
            .pt-5{
                padding-top: 5px !important;
            }
            .pb-10{
                padding-bottom: 10px !important;
            }
            .pr-10{
                padding-right: 10px !important;
            }
            .pt-10{
                padding-top: 10px !important;
            }
            .extras{
                white-space: nowrap;
                text-align: right;
            }
            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                padding: 0px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }
            .table>tbody>tr>td{
                padding: 5px;
            }
            .no-mb{
                margin-bottom: 0px;
            }
            .well-basic{
                background-color: #fff;
                padding: 0px;
                margin-bottom: 5px;
            }
            .well-basic .table>thead>tr>th{
                background-color: #e7e7e7;
                padding: 5px;
            }
            .well-basic.note .table>tbody>tr>td{
                border-top: none;
                padding: 0 8px;
            }
            .well-basic .table>tbody>tr>td{
                border-top: 1px solid #ddd;
                padding: 3px 3px;
            }
            hr.page-break{
                border: none;
                page-break-after: always;
            }
            li > ol {
                counter-reset: list;
            }
            li > ol > li {
                list-style: none;
                position: relative;
            }
            li > ol > li:before {
                counter-increment: list;
                content: counter(list, lower-alpha) ". ";
                position: absolute;
                left: -1.4em;
            }
            .no-wrap{
                white-space: nowrap;
            }
            .table>tfoot>tr>td.no-border{
                border:none;
            }
            td.thick-top-border{
                border-top: solid 2px #ddd;
            }
            td.dark-thick-top-border{
                border-top: solid 2px #cacaca;
            }
            .date-spacing{
                margin-left: 18px;
                margin-right: 18px;
            }
            .bg-white{
                background-color: #fff;
                padding-top: 3px;
                padding-bottom: 3px;
                border: 1px solid #ddd;
            }
            .table>tbody>tr>td.cell-left{
                border-left: 1px solid #ddd;
            }
            .table>tbody>tr>td.cell{
                border-left: 1px solid #ddd;
                border-right: 1px solid #ddd;
                border-top: 1px solid #ddd;
            }
        </style>

    </head>
    <body>
        <div class="page-wrapper">
            @section('header')
                <div class="row">
                    <div class="col-xs-12">
                        <div class="well well-primary">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h4><strong>Milne Brothers Roofing LLC</strong></h4>
                                    <p>Purchase Material:<strong> {{ $job->purchase_order }}</strong></p>
                                    <p>
                                        Job Type: <strong>{{ $job->type->name }}</strong>
                                    </p>
                                    <p>Pitch{{ count($job->pitches) != 1 ? 'es' : '' }}:
                                        <strong>
                                            @foreach($job->pitches as $pitch)
                                                {{ $loop->first ? "" : ", " }}
                                                {{ $pitch->type->name }}
                                            @endforeach
                                        </strong>
                                    </p>

                                    <h4>
                                        <strong>{{ $job->person->name }}</strong><br>
                                        <strong>{{ $job->person->address }}</strong><br>
                                        <strong><small>{{ $job->person->city }}, {{ $job->person->state }} {{ $job->person->zip }}</small></strong><br/>
                                        <strong><small>{!! $job->subdivision ? "<br/>Subdivision: ".$job->subdivision : NULL !!}</small></strong>
                                        <strong><small>{!! $job->lot ? "<br/>Lot: ".$job->lot : NULL !!}</small></strong>
                                        <strong>{{ $job->person->phone }}</strong>
                                    </h4>
                                </div>
                                <div class="col-md-6 text-right">
                                    <h1 class="text-danger">Job Sheet</h1>
                                    <div>{!! $job->accepted_at ? '<strong>Accepted On: </strong>'.Carbon\Carbon::parse($job->accepted_at)->format('l M. j, Y') : NULL !!}</div>
                                    <div>{!! $job->ordered_at ? '<strong>Ordered On: </strong>'.Carbon\Carbon::parse($job->ordered_at)->format('l M. j, Y') : NULL !!}</div>
                                    <div>{!! $job->delivery_at ? '<strong>Delivery On: </strong>'.Carbon\Carbon::parse($job->delivery_at)->toDayDateTimeString() : NULL !!}</div>
                                    <div>{!! $job->start_at ? '<strong>Starts On: </strong>'.Carbon\Carbon::parse($job->start_at)->format('l M. j, Y') : NULL !!}</div>
                                    <div>{!! $job->end_at ? '<strong>Ends On: </strong>'.Carbon\Carbon::parse($job->end_at)->format('l M. j, Y') : NULL !!}</div>
                                    <div>
                                        <strong>Completed On: </strong>
                                        <span class="bg-white">
                                            <span class="date-spacing">&nbsp;</span>/<span class="date-spacing">&nbsp;</span>/<span class="date-spacing">&nbsp;</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endsection

            @yield('header')

            <div class="row">
                <div class="col-xs-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Materials Needed</th>
                                <th width=150>Bid Quantity</th>
                                <th withh=300 class="text-right">Actual Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($job->material_list->categories as $category)
                                <tr>
                                    @if($category->product)
                                        <td>{{ $category->products_through_pivot->first()->name }} {{ $category->pivot->color ? '- '.str_unslug($category->pivot->color) : NULL }}</td>
                                    @else
                                        <td>{{ $category->name }} {{ $category->pivot->color ? '- '.str_unslug($category->pivot->color) : NULL }}</td>
                                    @endif
                                    <td><span class="qty">{{ $category->pivot->qty }}</span> {{ str_unslug($category->unit) }}</td>
                                    <td class="cell-left"></td>
                                </tr>
                            @empty
                                <tr>
                                    There are no products or services tied to this material list.
                                </tr>
                            @endforelse
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row mb-10">
                <div class="col-md-12">
                    <div class="well well-basic note">
                        <table class="table no-mb">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan=3 >Additional Information</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($job->instructions)
                                    <tr>
                                        <td class="pt-5">Supplier Instructions</td>
                                        <td class="pt-5">{{ $job->instructions }}</td>
                                        <td class="pt-5 text-right">{{ Carbon\Carbon::parse($job->created_at)->toFormattedDateString() }}</td>
                                    </tr>
                                @endif
                                @forelse($job->notes as $note)
                                    <tr>
                                        <td class="pt-5">{{ $note->user->name }}</td>
                                        <td class="pt-5">{{ $note->content }}</td>
                                        <td class="pt-5 text-right">{{ Carbon\Carbon::parse($note->created_at)->toFormattedDateString() }}</td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="row mb-10">
                <div class="col-md-12">
                    <div class="well well-basic">
                        <table class="table no-mb">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan=6 >Safety Review</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Safety Supervisor</td>
                                    <td class="cell" width="140px">&nbsp;</td>
                                    <td>Worker 3</td>
                                    <td class="cell" width="140px">&nbsp;</td>
                                    <td>Worker 5</td>
                                    <td class="cell" width="140px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Worker 2</td>
                                    <td class="cell" width="140px">&nbsp;</td>
                                    <td>Worker 4</td>
                                    <td class="cell" width="140px">&nbsp;</td>
                                    <td>Worker 6</td>
                                    <td class="cell" width="140px">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table no-mb">
                            <tbody>
                                <tr>
                                    <td width="210px">{{ Form::checkbox('agree') }}  <span>&nbsp;&nbsp;Clean-Up</span></td>
                                    <td width="210px">{{ Form::checkbox('agree') }}  <span>&nbsp;&nbsp;Framing Condition</span> </td>
                                    <td width="210px"><span>&nbsp;&nbsp;Start Time</span> <span class="date-spacing">&nbsp;</span> : <span class="date-spacing">&nbsp;</span></td>
                                    <td width="210px"><span>&nbsp;&nbsp;End Time</span> <span class="date-spacing">&nbsp;</span> : <span class="date-spacing">&nbsp;</span></td>
                                </tr>
                                <tr>
                                    <td width="210px">{{ Form::checkbox('agree') }}  <span>&nbsp;&nbsp;Electrical Cords</span></td>
                                    <td width="210px">{{ Form::checkbox('agree') }}  <span>&nbsp;&nbsp;Ladder</span> </td>
                                    <td width="210px"><span>&nbsp;&nbsp;Start Time</span> <span class="date-spacing">&nbsp;</span> : <span class="date-spacing">&nbsp;</span></td>
                                    <td width="210px"><span>&nbsp;&nbsp;End Time</span> <span class="date-spacing">&nbsp;</span> : <span class="date-spacing">&nbsp;</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row mb-10">
                <div class="col-md-12">
                    <div class="well well-basic">
                        <table class="table no-mb">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan=6 >Method Of Protection</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="210px">{{ Form::checkbox('agree') }}  <span>&nbsp;&nbsp;Ropes Harness</span></td>
                                    <td width="210px">{{ Form::checkbox('agree') }}  <span>&nbsp;&nbsp;Slide Guides</span> </td>
                                    <td width="210px">{{ Form::checkbox('agree') }}  <span>&nbsp;&nbsp;Ground Inspection</span></td>
                                </tr>
                                <tr>
                                    <td width="210px"><span>Roof Height:</span></td>
                                    <td width="210px"><span>Possible Hazards:</span> </td>
                                    <td width="210px"></td>
                                </tr>
                                <tr>
                                    <td class="thick-top-border" width="210px">&nbsp;</td>
                                    <td class="thick-top-border" width="210px">&nbsp;</td>
                                    <td class="thick-top-border" width="210px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="thick-top-border" width="210px">&nbsp;</td>
                                    <td class="thick-top-border" width="210px">&nbsp;</td>
                                    <td class="thick-top-border" width="210px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="thick-top-border" width="210px">&nbsp;</td>
                                    <td class="thick-top-border" width="210px">&nbsp;</td>
                                    <td class="thick-top-border" width="210px">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    </body>
</html>