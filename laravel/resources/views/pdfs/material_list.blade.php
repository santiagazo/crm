<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Milne Brothers Roofing | Material Sheet</title>
        <meta name="Sheet" content="Material for Supplier">
        <meta name="Jay Lara | DarkCherrySystems.com" content="">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <style>
            .well.well-primary{
                padding: 5px;
                background-color: #e7e7e7;
                margin-bottom: 5px;
            }
            .h1, .h2, .h3, h1, h2, h3 {
                margin-top: 0px;
                margin-bottom: 5px;
            }
            p {
               margin: 0;
            }
            .mb-10{
                margin-bottom: 10px;
            }
            .mtb-20{
                margin-bottom: 20px;
                margin-top: 20px;
            }
            .mb-20{
                margin-bottom: 20px;
            }
            .pb-5{
                padding-bottom: 5px !important;
            }
            .pt-5{
                padding-top: 5px !important;
            }
            .pb-10{
                padding-bottom: 10px !important;
            }
            .pr-10{
                padding-right: 10px !important;
            }
            .pt-10{
                padding-top: 10px !important;
            }
            .extras{
                white-space: nowrap;
                text-align: right;
            }
            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                padding: 0px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }
            .no-mb{
                margin-bottom: 0px;
            }
            .well-basic{
                background-color: #fff;
                padding: 0px;
                margin-bottom: 5px;
            }
            .well-basic .table>thead>tr>th{
                background-color: #e7e7e7;
                padding: 5px;
            }
            .well-basic .table>tbody>tr>td{
                border-top: none;
                padding: 0 8px;
            }
            hr.page-break{
                border: none;
                page-break-after: always;
            }
            li > ol {
                counter-reset: list;
            }
            li > ol > li {
                list-style: none;
                position: relative;
            }
            li > ol > li:before {
                counter-increment: list;
                content: counter(list, lower-alpha) ". ";
                position: absolute;
                left: -1.4em;
            }
            .no-wrap{
                white-space: nowrap;
            }
            .table>tfoot>tr>td.no-border{
                border:none;
            }
            td.thick-top-border{
                border-top: solid 2px #ddd;
            }
            td.dark-thick-top-border{
                border-top: solid 2px #cacaca;
            }
        </style>

    </head>
    <body>
        <div class="page-wrapper">
            @section('header')
                <div class="row">
                    <div class="col-xs-12">
                        <div class="well well-primary text-center">
                            <h2><strong>Milne Brothers Roofing LLC</strong></h2>

                            <div class="row">
                                <div class="col-xs-offset-1 col-xs-2">
                                    <h4><strong>Office</strong></h4>
                                    <h6>(801) 268-4496</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Brent</strong></h4>
                                    <h6>(801) 230-7460</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Anthony</strong></h4>
                                    <h6>(801) 558-6320</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Nathan</strong></h4>
                                    <h6>(801) 647-1137</h6>
                                </div>
                                <div class="col-xs-2">
                                    <h4><strong>Fax</strong></h4>
                                    <h6>(801) 268-4496</h6>
                                </div>
                            </div>
                            <h4>5712 S. 800 W. Murray, UT 84123</h4>
                            <a href="www.milnebrothersroofing.com">www.milnebrothersroofing.com</a> | <a href="mailto:milnebrothersroofing@gmail.com">milnebrothersroofing@gmail.com</a>
                        </div>
                    </div>
                </div>
            @endsection

            @yield('header')

            <div class="row">
                <div class="col-xs-6">
                    <p>Order Date: <strong>{{ Carbon\Carbon::now()->toDayDateTimeString() }}</strong></p>
                    <p>Delivery On: <strong>{!! $job->delivery_at ? Carbon\Carbon::parse($job->delivery_at)->toDayDateTimeString() : "Please Contact Us For Date & Time" !!}</strong></p>
                    <h4>
                        <strong>{{ $job->person->name }}</strong><br>
                        <strong>{{ $job->person->address }}</strong><br>
                        <strong><small>{{ $job->person->city }}, {{ $job->person->state }} {{ $job->person->zip }}</small></strong>
                        <strong><small>{!! $job->subdivision ? "<br/>Subdivision: ".$job->subdivision : NULL !!}</small></strong>
                        <strong><small>{!! $job->lot ? "<br/>Lot: ".$job->lot : NULL !!}</small></strong>
                    </h4>
                </div>
                <div class="col-md-6 text-right">
                    <h1 class="text-danger">Material Sheet</h1>
                    <p>Purchase Order:<strong> {{ $job->purchase_order }}</strong></p>
                    <p>
                        Job Type: <strong>{{ $job->type->name }}</strong>
                    </p>
                    <p>Pitch{{ count($job->pitches) != 1 ? 'es' : '' }}:
                        <strong>
                            @foreach($job->pitches as $pitch)
                                {{ $loop->first ? "" : ", " }}
                                {{ $pitch->type->name }}
                            @endforeach
                        </strong>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Materials Needed</th>
                                <th width=150>Quantities</th>
                                <th width=75>Price/Unit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($job->material_list->categories->where('pivot.product_id', '!=', NULL) as $category)

                                <tr>
                                    <td>{{ $category->products_through_pivot->first()->name }} {{ $category->pivot->color ? '- '.str_unslug($category->pivot->color) : NULL }}</td>
                                    <td><span class="qty">{{ $category->pivot->qty }}</span> {{ str_unslug($category->unit) }}</td>
                                    <td class="pr-10 text-right price">{{ $category->products->first() ? $category->products->first()->supplier_price : 'No Price Set' }}</td>
                                </tr>
                            @empty
                                <tr>
                                    There are no products or services tied to this material list.
                                </tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="no-border"></td>
                                <td class="no-border">Sub-Total</td>
                                <td class="pr-10 text-right no-border" id="sub_total"></td>
                            </tr>
                            <tr>
                                <td class="no-border"></td>
                                <td class="thick-top-border">Tax</td>
                                <td class="pr-10 text-right thick-top-border" id="tax">{{ $job->material_list->company->tax_amount }}%</td>
                            </tr>
                            <tr>
                                <td class="no-border"></td>
                                <td class="dark-thick-top-border">Total</td>
                                <td class="pr-10 text-right dark-thick-top-border" id="total"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            @if($job->instructions)
                <div class="row mb-10">
                    <div class="col-md-12">
                        <div class="well well-basic">
                            <table class="table no-mb">
                                <thead>
                                    <tr>
                                        <th class="text-center" colspan=3 >Additional Information</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td class="pt-5" width="148px">Special Instructions - </td>
                                            <td class="pt-5">{{ $job->instructions }}</td>
                                        </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            @endif

        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script>
            $(function(){
                var sub_total = 0;
                var tax = ({{ $job->material_list->company->tax_amount }}/100);

                $.each($("tbody tr"), function(i, row){
                    var price = $(row).find('.price').text();
                    var qty = $(row).find('.qty').text();
                    var line_total = qty*price;
                    sub_total += parseFloat(line_total.toFixed(2));
                });

                $('#sub_total').html(sub_total.toFixed(2));
                var taxed_sub_total = sub_total*tax;
                $('#total').html((taxed_sub_total+sub_total).toFixed(2));
            });
        </script>
    </body>
</html>