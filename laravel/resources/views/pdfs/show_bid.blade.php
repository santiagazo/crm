@extends('layouts.auth.main')

@section('css')
    <style>
        .accept-options li{
            margin-bottom: 5px;
        }
        .form-control.auto-height{
            height: auto;
        }
    </style>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8 col-md-10 mt-40 mb-20">
                <a href="javascript:void(0)" class="btn btn-primary pull-right" id="email_bid_btn">Email to {{ $bid->person->name }}</a>
                @if($bid->sent_at)
                    <a href="{{ url('/admin/bids/show/'.$bid->id) }}" class="btn btn-primary pull-right">View Bid</a>
                @endif
                <a href="{{ url('admin/material_lists/edit/'.$bid->material_list->id.'/bid') }}" class="btn btn-primary pull-right">Edit Material List</a>
                <a href="{{ url('admin/bids/edit/'.$bid->id) }}" class="btn btn-primary pull-right">Edit Bid</a>
            </div>

            <div class="col-lg-offset-2 col-lg-8 col-md-10" id='email_bid' style="display: none">
                <div class="box box-primary">
                    <div class="box-body">
                        {!! Form::open( ['url' => 'admin/pdfs/bids/email/'.$bid->id] ) !!}

                            {!! Form::hidden('to[]', $bid->person->email, ['id' => 'to']) !!}
                            
                            <!--Text form input-->
                            <div class="col-md-12">
                                <div class='form-group'>
                                    {!! Form::label('subject', 'Subject') !!}
                                    {!! Form::text('subject', 'Your Roofing Quote is Ready from Milne Brothers Roofing', ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-control form-group auto-height">
                                    <p>{{ $bid->person->name }},</p>
                                    <p>Attached you will find your price quote for the "{{ str_unslug($bid->type->name) }}" of your roof.</p>
                                    <p>You can electronically accept or decline this quote by selecting an option below.</p>
                                    <ul class="list-unstyled accept-options">
                                        <li>
                                            <a href="{{ url('quote/'.$bid->id.'/'.$bid->material_list->id.'/'.$bid->person->id.'/accept') }}" class="btn btn-sm btn-success">
                                                I accept the quote. Please schedule me at your earliest convenience.
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('quote/'.$bid->id.'/'.$bid->material_list->id.'/'.$bid->person->id.'/conditionally') }}" class="btn btn-sm btn-info">
                                                I accept the quote but have questions. Please contact me.
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('quote/'.$bid->id.'/'.$bid->material_list->id.'/'.$bid->person->id.'/decline') }}" class="btn btn-sm btn-default">
                                                I do not accept the quote.
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!--Textarea form input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('content', 'Personalized Message') !!}
                                    {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => '3']) !!}
                                </div>
                            </div>

                            <!--Submit form input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit('Send with Bid Attached', ['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-20">
            <div class="col-lg-offset-2 col-lg-8 col-md-10">
                <iframe src="{{ url('admin/pdf/bids/'.$bid->id) }}" width="100%" height="600px" frameborder="0"></iframe>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>

        $(function(){
            $('body').on('click', '#email_bid_btn', function(){
                $('#email_bid').slideToggle();
            });
        });

    </script>

@stop