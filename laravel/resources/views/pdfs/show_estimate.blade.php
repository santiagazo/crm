@extends('layouts.auth.main')

@section('title')
@endsection

@section('css')
    <style>
        .accept-options li{
            margin-bottom: 5px;
        }
        .form-control.auto-height{
            height: auto;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: #585858;
        }
    </style>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8 col-md-10 mt-40 mb-20">
                <a href="javascript:void(0)" class="btn btn-primary pull-right" id="email_estimate_btn">Email to a Recipient</a>
                <a href="{{ url('admin/pdf/estimates/'.$estimate->id) }}" class="btn-primary btn pull-right">Print</a>
                <a href="{{ url('admin/estimates/edit/'.$estimate->id) }}" class="btn btn-primary pull-right">Edit Bid Sheet</a>
            </div>

            <div class="col-lg-offset-2 col-lg-8 col-md-10" id='email_estimate' style="display: none">
                <div class="box box-primary">
                    <div class="box-body">
                        {!! Form::open( ['url' => 'admin/pdfs/estimates/email/'.$estimate->id] ) !!}

                            <!--Text form input-->
                            <div class="col-md-12">
                                <div class='form-group'>
                                    {!! Form::label('to[]', 'To') !!}
                                    {!! Form::select('to[]', [], NULL, ['class' => 'form-control estimate_select2', 'multiple' => true]) !!}
                                </div>
                            </div>

                            <!--Text form input-->
                            <div class="col-md-12">
                                <div class='form-group'>
                                    {!! Form::label('subject', 'Subject') !!}
                                    {!! Form::text('subject', 'New Bid Sheet', ['class' => 'form-control']) !!}
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-control form-group auto-height">
                                    <p>
                                        <p>Attached you will find the bid sheet.</p>
                                    </p>
                                </div>
                            </div>

                            <!--Textarea form input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('content', 'Personalized Message') !!}
                                    {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => '3']) !!}
                                </div>
                            </div>

                            <!--Submit form input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit('Send with Material Sheet Attached', ['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-20">
            <div class="col-lg-offset-2 col-lg-8 col-md-10">
                <iframe src="{{ url('admin/pdf/estimates/'.$estimate->id) }}" width="100%" height="600px" frameborder="0"></iframe>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>

        $(function(){
            $('body').on('click', '#email_estimate_btn', function(){
                $('#email_estimate').slideToggle();
            });

            $('.estimate_select2').select2({
            	tags: true
            })
        });

    </script>

@stop