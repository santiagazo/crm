@extends('layouts.auth.main')

@section('title')
@endsection

@section('css')
    <style>
        .accept-options li{
            margin-bottom: 5px;
        }
        .form-control.auto-height{
            height: auto;
        }
    </style>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8 col-md-10 mt-40 mb-20">
                <a href="javascript:void(0)" class="btn btn-primary pull-right" id="email_invoice_btn">Email to {{ $job->person->name }}</a>
                <a href="{{ url('admin/jobs/edit/'.$job->id) }}" class="btn btn-primary pull-right">Edit Job</a>
                <a href="{{ url('admin/material_lists/edit/'.$job->material_list->id) }}" class="btn btn-primary pull-right">Edit Material</a>
            </div>

            <div class="col-lg-offset-2 col-lg-8 col-md-10" id='email_invoice' style="display: none">
                <div class="box box-primary">
                    <div class="box-body">
                        {!! Form::open( ['url' => 'admin/pdfs/invoices/email/'.$job->id] ) !!}

                            {!! Form::hidden('to[]', $job->person->email, ['id' => 'to']) !!}

                            <!--Text form input-->
                            <div class="col-md-12">
                                <div class='form-group'>
                                    {!! Form::label('subject', 'Subject') !!}
                                    {!! Form::text('subject', 'Your Roofing Invoice is Ready from Milne Brothers Roofing', ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-control form-group auto-height">
                                    <p>{{ $job->person->name }},</p>
                                    <p>Attached you will find your invoice for the "{{ str_unslug($job->type->name) }}" of your roof. Please remit payment promptly.</p>
                                </div>
                            </div>

                            <!--Textarea form input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('content', 'Personalized Message') !!}
                                    {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => '3']) !!}
                                </div>
                            </div>

                            <!--Submit form input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit('Send with Invoice Attached', ['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-20">
            <div class="col-lg-offset-2 col-lg-8 col-md-10">
                <iframe src="{{ url('admin/pdf/invoices/'.$job->id) }}" width="100%" height="600px" frameborder="0"></iframe>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>

        $(function(){
            $('body').on('click', '#email_invoice_btn', function(){
                $('#email_invoice').slideToggle();
            });
        });

    </script>

@stop