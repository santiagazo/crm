@extends('layouts.auth.main')

@section('title')
@endsection

@section('css')
    <style>
        .accept-options li{
            margin-bottom: 5px;
        }
        .form-control.auto-height{
            height: auto;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: #585858;
        }
    </style>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8 col-md-10 mt-40 mb-20">
                <a href="javascript:void(0)" class="btn btn-primary pull-right" id="email_bid_btn">Email to Workers</a>
                <a href="{{ url('admin/jobs/show/'.$job->id) }}" class="btn btn-info pull-right">View Job</a>
                <a href="{{ url('admin/material_lists/edit/'.$job->material_list->id.'/job') }}" class="btn btn-default pull-right">Edit Material List</a>
                <a href="{{ url('admin/jobs/edit/'.$job->id) }}" class="btn btn-default pull-right">Edit Job</a>
            </div>

            <div class="col-lg-offset-2 col-lg-8 col-md-10" id='email_bid' style="display: none">
                <div class="box box-primary">
                    <div class="box-body">
                        {!! Form::open( ['url' => 'admin/pdfs/jobs/email/'.$job->id] ) !!}

                            <!--Text form input-->
                            <div class="col-md-12">
                                <div class='form-group'>
                                    {!! Form::label('to[]', 'To') !!}
                                    {!! Form::select('to[]', [], NULL, ['class' => 'form-control workers_select2', 'multiple' => true]) !!}
                                </div>
                            </div>

                            <!--Text form input-->
                            <div class="col-md-12">
                                <div class='form-group'>
                                    {!! Form::label('subject', 'Subject') !!}
                                    {!! Form::text('subject', 'New Job Sheet', ['class' => 'form-control']) !!}
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-control form-group auto-height">
                                    <p>
                                        Please see attached job sheet.
                                    </p>
                                </div>
                            </div>

                            <!--Textarea form input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('content', 'Personalized Message') !!}
                                    {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => '3']) !!}
                                </div>
                            </div>

                            <!--Submit form input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit('Send with Job Sheet Attached', ['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-20">
            <div class="col-lg-offset-2 col-lg-8 col-md-10">
                <iframe src="{{ url('admin/pdf/jobs/'.$job->id) }}" width="100%" height="600px" frameborder="0"></iframe>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>

        $(function(){
            $('body').on('click', '#email_bid_btn', function(){
                $('#email_bid').slideToggle();
            });

            $('.workers_select2').select2({
                tags: true
            });
        });

    </script>

@stop