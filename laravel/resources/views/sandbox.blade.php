<html>
  <head>
    <link rel="stylesheet" href="{{ url('auth/plugins/literally-canvas/_static/better.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('auth/plugins/literally-canvas/_static/pygments.css') }}" type="text/css" />
    <script type="text/javascript" src="{{ url('auth/plugins/literally-canvas/_static/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ url('auth/plugins/literally-canvas/_static/underscore.js') }}"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Finger+Paint" type="text/css" />
    <link rel="stylesheet" href="{{ url('auth/plugins/literally-canvas/_static/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('auth/plugins/literally-canvas/_static/literallycanvas.css') }}" type="text/css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/react/0.13.3/react-with-addons.js" type="text/javascript"></script>
    <script src="{{ url('auth/plugins/literally-canvas/_static/fastclick.js') }}" type="text/javascript"></script>
    <script src="{{ url('auth/plugins/literally-canvas/_static/literallycanvas.js') }}" type="text/javascript"></script>
    <script src="{{ url('auth/plugins/literally-canvas/_static/docs.js') }}" type="text/javascript"></script>
  </head>
  <body>


    <div class="literally backgrounds"></div>
    <button class="save-lc"></button>
    <script>
      $(document).ready(function() {
        var backgroundImage = new Image()
        backgroundImage.src = "{{ url('auth/plugins/literally-canvas/_static/sample_image.png') }}";

        var lc = LC.init(
            document.getElementsByClassName('literally backgrounds')[0],
            {
              backgroundShapes: [
                LC.createShape(
                  'Image', {x: 0, y: 0, image: backgroundImage, scale: 0})
              ]
            });
        // the background image is not included in the shape list that is
        // saved/loaded here
        var localStorageKey = 'drawing-with-background'
        if (localStorage.getItem(localStorageKey)) {
          lc.loadSnapshotJSON(localStorage.getItem(localStorageKey));
        }
        lc.on('drawingChange', function() {
          localStorage.setItem(localStorageKey, lc.getSnapshotJSON());
        });

        $('body').on('click', '.save-lc', function(){
          var snapshot = lc.getSnapshot();
          console.log(snapshot);
        });

      });
    </script>


  </body>
</html>