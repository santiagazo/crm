@extends('layouts.site.main')

@section('title')
About
@endsection

@section('css')
	<link href="{{ url('guest/plugins/lightgallery/dist/css/lightgallery.min.css') }}" rel="stylesheet">

	<style>
    	.grid-widget__info{
            text-align: center;
        }
        .grid-widget__info h3{
			color: #fff;
        }
		.hover .grid-widget__info {
            position: absolute;
            bottom: -1px;
            left: 0;
            width: 100%;
            height: 100%;
            color: #fff;
            padding: 0;
            background-color: rgba(0,0,0,.8);
            border-radius: 0 0 3px 3px;
        }
        .hover .grid-widget__footer,
        .hover .grid-widget__title{
            padding: 10px;
            height: 25%;
        }
        .grid-widget__body{
            padding: 20px;
            height: 50%;
            display: none;
        }
        .grid-widget__footer{
            padding: 12px 0 0 0;
            display: none;
        }
        .grid-widget__footer>div a{
            color: #fff;
            font-size: 20px;
            padding: 5px 5px;
        }
        .hover .grid-widget__body,
        .hover .grid-widget__footer{
            display: block;
        }
        .lg-sub-html a{
            color: #4daaff;
        }
        @media only screen and (min-width: 480px) and (max-width: 1200px) {
            .hover .grid-widget__footer{
                display: none !important;
            }
        } 
        @media only screen and (min-width: 480px) and (max-width: 771px) {
            .hover .grid-widget__body{
                display: none !important;
            }
        } 
        .custom-visible-xs{
            display: none !important;
        }  
        @media only screen and (max-width: 480px) {
            .custom-visible-xs{
                display: block !important;
                font-size: 14px
            }   
        }
        .grid-widget .text-white{
            color: #fff !important;
        }
	</style>
@stop

@section('content')
<div class="modal fade learn-more-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <img src="" class="center-block img-responsive modal-image" alt="">
                <br>
                <div class="modal-body-insert"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<section>
    <div class="container">
        <div class="row pb-20">
            <div class="col-md-12">
                <h1 class="text-center">
                    Meet The Team
                    <small class="text-center custom-visible-xs">Rotate device to see all options.</small>
                </h1>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="grid-widget grid-widget--listings">
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide30">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/brent.jpg') }}" alt="">

                            <div class="grid-widget__info">
								<div class="grid-widget__title">
                                    <h3>Brent Milne</h3>
                                    <small>Co-Owner</small>
                                    <div class="visible-sm-block visible-xs-block visible-md-block">
                                        <a href="javascript:void(0)" class="learn-more text-white">Learn More</a>
                                    </div>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>Original Owner of MBR in 1978</li>
                                        <li>Roofing Experience: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::parse(1978)) }} Years</li>
                                        <li>Still installs on a daily basis with employees!</li>
                                    </ul>
                                </div>
                                <div class="grid-widget__footer">
                                	<div>
                                    	<a href="tel:8012684496">Call</a>
                                        <a href="mailto:mailto:mbr@milnebrothersroofing.com">Email</a>
                                    </div>
                                    <div>
                                        <a href="javascript:void(0)" class="learn-more">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide31">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/anthony.jpg') }}" alt="">

                            <div class="grid-widget__info">
								<div class="grid-widget__title">
                                    <h3>Anthony Milne</h3>
                                    <small>Co-Owner</small>
                                    <div class="visible-sm-block visible-xs-block visible-md-block">
                                        <a href="javascript:void(0)" class="learn-more text-white">Learn More</a>
                                    </div>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>2nd Generation Owner since 2006</li>
                                        <li>Time in the Roofing Industry: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(1993, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>BYU Graduate: Accounting &amp; MAcc</li>
                                        <li>Installs daily with employees!</li>
                                    </ul>
                                </div>
                                <div class="grid-widget__footer">
                                	<div>
                                    	<a href="tel:8012684496">Call</a>
                                        <a href="mailto:mbr@milnebrothersroofing.com">Email</a>
                                    </div>
                                    <div>
                                        <a href="javascript:void(0)" class="learn-more">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide32">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/nathan.jpg') }}" alt="">

                            <div class="grid-widget__info">
								<div class="grid-widget__title">
                                    <h3>Nathan Milne</h3>
                                    <small>Co-Owner</small>
                                    <div class="visible-sm-block visible-xs-block visible-md-block">
                                        <a href="javascript:void(0)" class="learn-more text-white">Learn More</a>
                                    </div>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>2nd Generation Owner since 2006</li>
                                        <li>Time in the Roofing Industry: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(1995, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>BYU Graduate: Business Administration, Entrepreneur Emphasis</li>
                                        <li>Installs daily with employees!</li>
                                    </ul>
                                </div>
                                <div class="grid-widget__footer">
                                	<div>
                                    	<a href="tel:8012684496">Call</a>
                                        <a href="mailto:mbr@milnebrothersroofing.com">Email</a>
                                    </div>
                                    <div>
                                        <a href="javascript:void(0)" class="learn-more">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide32">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/tiffany.jpg') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3>Tiffany (Milne) Nelson</h3>
                                    <small>Secretary</small>
                                    <div class="visible-sm-block visible-xs-block visible-md-block">
                                        <a href="javascript:void(0)" class="learn-more text-white">Learn More</a>
                                    </div>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>Time with MBR: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2009, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>Time in the Roofing Industry: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2009, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>Anthony & Nathan's Sister</li>
                                    </ul>
                                </div>
                                <div class="grid-widget__footer">
                                    <div>
                                        <a href="tel:4803091250">Call</a>
                                        <a href="mailto:mbr@milnebrothersroofing.com">Email</a>
                                    </div>
                                    <div>
                                        <a href="javascript:void(0)" class="learn-more">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide33">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/agustin.jpg') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3>Agustin Perez</h3>
                                    <small>Master Roofer</small>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>Time with MBR: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(1997, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>Time In Roofing Industry: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(1997, 1, 1, 0, 0, 0)) }} Years</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide34">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/irineo.jpg') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3>Irineo Osorio</h3>
                                    <small>Master Roofer</small>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>Time with MBR: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2013, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>Time In Roofing Industry: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2005, 1, 1, 0, 0, 0)) }} Years</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide35">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/eli.jpg') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3>Eli Osorio</h3>
                                    <small>Master Roofer</small>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>Time with MBR: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2013, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>Time In Roofing Industry: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2007, 1, 1, 0, 0, 0)) }} Years</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide32">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/austin.jpg') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3>Austin Milne</h3>
                                    <small>Master Roofer</small>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>Time with MBR: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2014, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>Time In Roofing Industry: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2014, 1, 1, 0, 0, 0)) }} Years</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide32">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/ryan.jpg') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3>Ryan Sommercorn</h3>
                                    <small>Roofer</small>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>Time with MBR: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2016, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>Time In Roofing Industry: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2016, 1, 1, 0, 0, 0)) }} Years</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide36">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/ismael.jpg') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3>Ismael Hernandez</h3>
                                    <small>Roofer</small>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>Time with MBR: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2016, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>Time In Roofing Industry: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2016, 1, 1, 0, 0, 0)) }} Years</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide31">
                        <div class="grid-widget__item hoverable" href="" tabindex="-1">
                            <img src="{{ url('auth/images/jaime.jpg') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3>Jaime Perez</h3>
                                    <small>Roofer</small>
                                </div>
                                <div class="grid-widget__body hidden-sm hidden-md">
                                    <ul class="list-unstyled">
                                        <li>Time with MBR: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2017, 1, 1, 0, 0, 0)) }} Years</li>
                                        <li>Time In Roofing Industry: {{ Carbon\Carbon::now()->diffInYears(Carbon\Carbon::create(2016, 1, 1, 0, 0, 0)) }} Years</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row pt-40 pb-20" id=outreach>
            <div class="col-md-12">
                <h1 class="text-center">
                    Community Outreach
                </h1>
            </div>
        </div>


        <div class="row pb-40">
            <div class="col-md-offset-2 col-md-8">
                <p>
                    As the owners of our company, each of us feel that we've been given more than we need in our lives. We believe in giving back and it's
                    been our pleasure to participate in serving those around our greater community when we can. Below are some of the projects we've enjoyed.
                    We've listed them in hopes that you'll enjoy them too.
                </p>
            </div>
        </div>


        <div class="row pb-20">
            <div class="col-md-12">
                <div class="col-md-3 pb-30" id="habitatForHumanity">
                    <div class="grid-widget__item" data-src="{{ url('guest/images/roofs/HabitatforHumanity.png') }}" data-sub-html='Tooele Project Completed 2016'>
                        <img src="{{ url('guest/images/roofs/HabitatforHumanity2.png') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3><a class="font-white">Habitat For Humanity</a></h3>
                            </div>
                        </div>
                    </div>
                    <span data-src="{{ url('guest/images/roofs/HabitatforHumanity2.png') }}" data-sub-html="Tooele Project Completed 2016" style="display:none">
                        <img src="{{ url('guest/images/roofs/HabitatforHumanity2.png') }}" alt="">
                    </span>
                    <a href="https://youtu.be/tYJonL7-4iQ" style="display:none">
                        <img src="{{ url('guest/images/roofs/optimized/ScoutHouse.jpg') }}" alt="">
                    </a>
                    <span data-src="{{ url('guest/images/roofs/habitat-for-humanity/hfh3.jpg') }}" data-sub-html="" style="display:none">
                        <img src="{{ url('guest/images/roofs/habitat-for-humanity/hfh3.jpg') }}" alt="">
                    </span>
                </div>
                <div class="col-md-3 pb-30" id="eagleScoutProject">
                    <div class="grid-widget__item" data-src="{{ url('guest/images/roofs/optimized/ScoutHouse1.jpg') }}" data-sub-html="Scout House 3 Completed 2012">
                        <img src="{{ url('guest/images/roofs/optimized/ScoutHouse1.png') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3><a class="font-white">Scout House Projects</a></h3>
                            </div>
                        </div>
                    </div>
                    <span data-src="{{ url('guest/images/roofs/scout-house/sh7.jpg') }}" data-sub-html="Scout House 3 Completed 2012" style="display:none">
                        <img src="{{ url('guest/images/roofs/scout-house/sh7.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/scout-house/sh8.jpg') }}" data-sub-html="Scout House 3 Completed 2012" style="display:none">
                        <img src="{{ url('guest/images/roofs/scout-house/sh8.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/scout-house/sh9.jpg') }}" data-sub-html="Scout House 3 Completed 2012" style="display:none">
                        <img src="{{ url('guest/images/roofs/scout-house/sh9.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/scout-house/sh5.jpg') }}" data-sub-html="Scout House 3 Completed 2012" style="display:none">
                        <img src="{{ url('guest/images/roofs/scout-house/sh5.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/scout-house/sh4.jpg') }}" data-sub-html="Scout House 2 Completed 2008" style="display:none">
                        <img src="{{ url('guest/images/roofs/scout-house/sh4.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/scout-house/sh6.jpg') }}" data-sub-html="Scout House 2 Completed 2008" style="display:none">
                        <img src="{{ url('guest/images/roofs/scout-house/sh6.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/scout-house/sh2.png') }}" data-sub-html="Scout House 2 Completed 2008" style="display:none">
                        <img src="{{ url('guest/images/roofs/scout-house/sh2.png') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/scout-house/sh2.jpg') }}" data-sub-html="Scout House 1 Completed 2006" style="display:none">
                        <img src="{{ url('guest/images/roofs/scout-house/sh2.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/scout-house/sh1.jpg') }}" data-sub-html="Scout House 1 Completed 2006" style="display:none">
                        <img src="{{ url('guest/images/roofs/scout-house/sh1.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/scout-house/sh1.png') }}" data-sub-html="Scout House 1 Completed 2006" style="display:none">
                        <img src="{{ url('guest/images/roofs/scout-house/sh1.png') }}" alt="">
                    </span>
                </div>
                <div class="col-md-3 pb-30" id="homesForTroops">
                    <div class="grid-widget__item" data-src="{{ url('guest/images/roofs/homes-for-troops/hft1.png') }}" data-sub-html="See more here: <a href='https://www.hfotusa.org/building-homes/veterans/jacobs'>https://www.hfotusa.org/building-homes/veterans/jacobs</a>">
                        <img src="{{ url('guest/images/roofs/homes-for-troops/hft1.png') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3><a class="font-white">Homes For Troops</a></h3>
                            </div>
                        </div>
                    </div>
                    <span data-src="{{ url('guest/images/roofs/homes-for-troops/hft1.jpg') }}" data-sub-html="See more here: <a href='https://www.hfotusa.org/building-homes/veterans/jacobs'>https://www.hfotusa.org/building-homes/veterans/jacobs</a>" style="display:none">
                        <img src="{{ url('guest/images/roofs/homes-for-troops/hft1.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/homes-for-troops/hft2.png') }}" data-sub-html="See more here: <a href='https://www.hfotusa.org/building-homes/veterans/jacobs'>https://www.hfotusa.org/building-homes/veterans/jacobs</a>" style="display:none">
                        <img src="{{ url('guest/images/roofs/homes-for-troops/hft2.png') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/homes-for-troops/hft3.png') }}" data-sub-html="See more here: <a href='https://www.hfotusa.org/building-homes/veterans/jacobs'>https://www.hfotusa.org/building-homes/veterans/jacobs</a>" style="display:none">
                        <img src="{{ url('guest/images/roofs/homes-for-troops/hft3.png') }}" alt="">
                    </span>
                </div>
                <div class="col-md-3 pb-30" id="heberValleyCabin">
                    <div class="grid-widget__item" data-src="{{ url('guest/images/roofs/HeberValleyCabin.jpg') }}" data-sub-html='See more here <a href="https://www.hebervalleycamp.org/">https://www.hebervalleycamp.org/</a>''>
                        <img src="{{ url('guest/images/roofs/HeberValleyCabin.png') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3><a class="font-white">Heber Valley Camp</a></h3>
                            </div>
                        </div>
                    </div>
                    <span data-src="{{ url('guest/images/roofs/HeberValleyCabin1.jpg') }}" data-sub-html="Provided free labor to shingle 39 cabins over a 4 year period." style="display:none">
                        <img src="{{ url('guest/images/roofs/HeberValleyCabin1.jpg') }}" alt="">
                    </span>
                </div>
                <div class="col-md-3 pb-30" id="extremeHomeMakeover">
                    <div class="grid-widget__item" data-src="{{ url('guest/images/roofs/optimized/extremeMakeoverFace.jpg') }}" data-sub-html="Us showing off the Extreme Home Makeover logos.">
                        <img src="{{ url('guest/images/roofs/optimized/extremeMakeoverFace.jpg') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3><a class="font-white">Extreme Home Makeover</a></h3>
                            </div>
                        </div>
                    </div>
                    <span data-src="{{ url('guest/images/roofs/extreme-home-makeover/ehm2.jpg') }}" data-sub-html="" style="display:none">
                        <img src="{{ url('guest/images/roofs/extreme-home-makeover/ehm2.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/extreme-home-makeover/ehm3.jpg') }}" data-sub-html="" style="display:none">
                        <img src="{{ url('guest/images/roofs/extreme-home-makeover/ehm3.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/extreme-home-makeover/ehm4.jpg') }}" data-sub-html="" style="display:none">
                        <img src="{{ url('guest/images/roofs/extreme-home-makeover/ehm4.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/extreme-home-makeover/ehm5.jpg') }}" data-sub-html="" style="display:none">
                        <img src="{{ url('guest/images/roofs/extreme-home-makeover/ehm5.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/extreme-home-makeover/ehm6.jpg') }}" data-sub-html="" style="display:none">
                        <img src="{{ url('guest/images/roofs/extreme-home-makeover/ehm6.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/extreme-home-makeover/ehm7.jpg') }}" data-sub-html="" style="display:none">
                        <img src="{{ url('guest/images/roofs/extreme-home-makeover/ehm7.jpg') }}" alt="">
                    </span>
                    <span data-src="{{ url('guest/images/roofs/extreme-home-makeover/ehm8.jpg') }}" data-sub-html="Us showing off the Extreme Home Makeover logos." style="display:none">
                        <img src="{{ url('guest/images/roofs/extreme-home-makeover/ehm8.jpg') }}" alt="">
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
    <script src="{{ url('guest/plugins/lightgallery/dist/js/lightgallery-all.min.js') }}"></script>

	<script>
		$(function(){
            $( ".grid-widget__item.hoverable" ).hover(
              function() {
                $( this ).addClass( "hover" );
              }, function() {
                $( this ).removeClass( "hover" );
              }
            );

            $('body').on('click', '.learn-more', function(){
                var header = $(this).closest('.grid-widget__info').find('.grid-widget__title').html();
                var body = $(this).closest('.grid-widget__info').find('.grid-widget__body').html();
                var image = $(this).closest('.grid-widget__item').find('img').attr('src');

                console.log(header);
                console.log(body);
                console.log(image);

                $('.modal-header').html(header);
                $('.modal-body-insert').html(body);
                $('.modal-image').attr('src', image);
                $('.learn-more-modal').modal('show');
            });;

            $('#habitatForHumanity, #eagleScoutProject, #homesForTroops, #heberValleyCabin, #extremeHomeMakeover').lightGallery({
                gallery: true,
                download: false,
                onSliderLoad  : function (el) {
                  el.lightGallery();
                }
            });

        });

	</script>

@stop
