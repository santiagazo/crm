@extends('layouts.site.main')

@section('title')
Contact
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>

        .contact__form {
            background-color: #0058A8;
        }

        .contact {
            margin: 0px 0 50px;
        }

        .contact__form .btn,
        .contact__form .btn:focus,
        .contact__form .btn:hover {
            color: #0058A8;
            box-shadow: none;
        }

        .rmd-contact-list {
            font-size: 16px;
        }

        .rmd-contact-list__title {
            font-size: 16px;
        }

    </style>
@stop

@section('content')

    <section class="section">
            <div class="container">

                <div class="contact">
                    <div class="contact__map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12108.956946417215!2d-111.9195523492432!3d40.64665650741625!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6e22fd7187fe0954!2sMilne+Brothers+Roofing!5e0!3m2!1sen!2sus!4v1510158869471" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>

                    <div class="card contact__inner clearfix">
                        <div class="col-sm-6">
                            <div class="contact__info">
                                <ul class="rmd-contact-list">
                                    <li class="rmd-contact-list__title"><i class="zmdi zmdi-home"></i>Milne Brothers Roofing</li>
                                    <li><i class="zmdi zmdi-pin"></i>5712 S 800 W, <br>Murray, UT 84123</li>
                                    <li><i class="zmdi zmdi-phone"></i>(801) 268-4496</li>
                                    <li><i class="zmdi zmdi-email"></i>mbr@milnebrothersroofing.com</li>
                                </ul>

                            </div>
                        </div>

                        <div class="col-sm-6">
                            {!! Form::open( ['url' => 'contact', 'class' => 'contact__form']) !!}
                                <div class="form-group form-group--light form-group--float">
                                    <input type="text" name="name" class="form-control">
                                    <label>Name</label>
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group form-group--light form-group--float">
                                    <input type="text" name="email" class="form-control">
                                    <label>Email Address</label>
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group form-group--light form-group--float">
                                    <input type="text" name="phone" class="form-control">
                                    <label>Contact Number</label>
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group form-group--light form-group--float">
                                    <textarea name="message" class="form-control textarea-autoheight"></textarea>
                                    <label>Message</label>
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="m-t-30">
                                    <button type="submit" class="btn brn-sm btn-default btn-static">Send</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>

@endsection

@section('js')

    <script>

        $(function(){

        });

    </script>

@stop