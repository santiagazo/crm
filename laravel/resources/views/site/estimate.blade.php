@extends('layouts.site.main')

@section('title')
Estimate
@endsection

@section('css')
    <link rel="stylesheet" href="">

    <style>
        .checkbox, .radio {
            margin-bottom: 5px;
        }
        .card {
            box-shadow: 0 1px 1px rgba(0,0,0,0.2);
            margin-bottom: 15px;
        }
        .label-header h2{
            margin: 0;
            font-weight: 700;
            font-size: 17px;
            color: #0058A8;
        }
        .form-group {
            margin-bottom: 10px;
        }
        .btn input{
            white-space: normal
        }
        small{
            color: #0058A8;
        }
        .card{
            padding-bottom: 10px;
        }
        .other_text{
            display: none;
        }
        .mr-5{
            margin-right: 5px;
        }
        .mb-5{
            margin-bottom: 5px;
        }
        .mb-0{
            margin-bottom: 0px;
        }
        .checkbox input:checked+.input-helper:before {
            background-color: #29b6f6;
        }
        .checkbox input[type=checkbox]:checked+.input-helper:before, 
        .checkbox input[type=radio]:checked+.input-helper:before, 
        .radio input[type=checkbox]:checked+.input-helper:before, 
        .radio input[type=radio]:checked+.input-helper:before {
            border-color: #29b6f6;
        }
        .form-control{
            background-color: #f5f5f5;
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
@stop

@section('content')
<section class="section">
    <div class="container">

            {!! Form::open( ['url' => 'estimate', 'id' => 'estimate_form'] ) !!}
                <div class="card">
                    <div class="col-md-12">
                        <div class="row pt-20">
                            <div class="col-md-12">
                                <div class="label-header">
                                    <h2>Type of Estimate Request</h2>
                                    <small>Choose all that apply. <em>Currently we don't install Tile, Cedar Shake, or EPDM Roofing.</em></small>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 'tear_off') !!}
                                                    <i class="input-helper"></i>
                                                    Tear Off
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 'repair') !!}
                                                    <i class="input-helper"></i>
                                                    Repair
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 're_roof') !!}
                                                    <i class="input-helper"></i>
                                                    Re-Roof (Redo)
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 'new_construction') !!}
                                                    <i class="input-helper"></i>
                                                    New Construction
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 'warranty') !!}
                                                    <i class="input-helper"></i>
                                                    Warranty
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 'roof_inspection') !!}
                                                    <i class="input-helper"></i>
                                                    Roof Inspection
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 'flat_roofing_system') !!}
                                                    <i class="input-helper"></i>
                                                    Flat Roofing System
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 'new_gutters') !!}
                                                    <i class="input-helper"></i>
                                                    New Gutters
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 'swamp_removal') !!}
                                                    <i class="input-helper"></i>
                                                    Swamp Removal
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 'tie_in_addition') !!}
                                                    <i class="input-helper"></i>
                                                    Tie-In Addition
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            <div class="checkbox">
                                                <label>
                                                    {!! Form::checkbox('request_type[]', 'other:', null, ['id' => 'other_estimate']) !!}
                                                    <i class="input-helper"></i>
                                                    Other
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="form-group mb-5">
                                            {!! Form::text('other_text', null, ['class' => 'form-control other_text']) !!}
                                            <i class="form-group__bar other_text"></i>
                                            <label class="other_text">Items Needing Repair</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    {{-- <div class="col-md-6 pt-20">
                        <div class="label-header">
                            <h2>Current Or Desired Roof Membrane</h2>
                            <small>Select one. <em>Currently we don't install Tile, Cedar Shake, or EPDM Roofing.</em></small>
                        </div>
                        <div class="form-group">
                            {!! Form::select('roof_type', [
                                    "asphalt" => "Asphalt",
                                    "membrane" => "Membrane",
                                    "metal" => "Metal",
                                    "tar_gravel" => "Tar & Gravel",
                                    "tile" => "Tile",
                                    "dont_know" => "Don't Know"
                                ], 'asphalt', ['class' => 'roof_type_select']) !!}
                        </div>
                    </div>

                    <div class="col-md-6 pt-20">
                        <div class="label-header">
                            <h2>Roof Pitch</h2>
                            <small>Select One</small>
                        </div>
                        <div class="form-group">
                            {!! Form::select('roof_pitch', [
                                        "flat" => "Practically Flat (membrane/gravel)",
                                        "easily_walkable" => "Easily walkable (2/12 to 6/12)",
                                        "bearly_walkable" =>  "Barely walkable (7/12 to 8/12)",
                                        "steep" => "Steep (9/12 and greater)",
                                        "dont_know" => "Don’t Know"
                                    ],
                                    "dont_know",
                                    ['class' => 'roof_pitch_select']) !!}
                        </div>
                    </div> --}}


                    <div class="col-md-6 pt-20">
                        <div class="label-header">
                            <h2>Urgency</h2>
                            <small>Select one. We are not setup for emergency or same-day repairs/estimates.</small>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group pt-10">
                                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                                        <label class="btn active">
                                            {{ Form::radio('urgency', 'not_urgent', true) }}
                                            Not Urgent <br>
                                            (When Convenient)
                                        </label>
                                        <label class="btn">
                                            {{ Form::radio('urgency', 'urgent') }}
                                            1-3 Days (Immediate) <br>
                                            (Roof is Leaking)
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 pt-20">
                        <div class="label-header">
                            <h2>How Did You Hear About Us?</h2>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                                        <label class="btn active">
                                            {{ Form::radio('advertisment', 'referral', true, ['class' => 'advertisment']) }}
                                            Referral
                                        </label>
                                        <label class="btn">
                                            {{ Form::radio('advertisment', 'internet', null, ['class' => 'advertisment']) }}
                                            Internet Search
                                        </label>
                                        <label class="btn">
                                            {{ Form::radio('advertisment', 'advertisment', null, ['class' => 'advertisment']) }}
                                            Advertising
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group referred_by">
                                    {!! Form::text('referred_by', null, ['class' => 'form-control', 'autocomplete' => 'new-password' ]) !!}
                                    <i class="form-group__bar"></i>
                                    <label>Referred By</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="col-md-12 pt-20">
                        <div class="label-header">
                            <h2>Personal Information</h2>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group mb-0">
                                    {!! Form::text('first_name', null, ['class' => 'form-control', 'autocomplete' => 'given-name']) !!}
                                    <i class="form-group__bar"></i>
                                    <label>First Name</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mb-0">
                                    {!! Form::text('last_name', null, ['class' => 'form-control', 'autocomplete' => 'family-name']) !!}
                                    <i class="form-group__bar"></i>
                                    <label>Last Name</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mb-0">
                                    {!! Form::text('email', null, ['class' => 'form-control', 'autocomplete' => 'email']) !!}
                                    <i class="form-group__bar"></i>
                                    <label>Email</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mb-0">
                                    {!! Form::tel('phone', null, ['class' => 'form-control', 'autocomplete' => 'tel']) !!}
                                    {{-- 'data-inputmask' => '"mask": "(999) 999-9999"', 'data-mask' --}}
                                    <i class="form-group__bar"></i>
                                    <label>Phone Number</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <small><strong>Project Address</strong></small>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-0">
                                    {!! Form::text('project_address', null, ['class' => 'form-control billing_same_as_project']) !!}
                                    <i class="form-group__bar"></i>
                                    <label>Address</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-0">
                                    {!! Form::select('project_city', array_merge(['' => 'Select City'], $taxed_cities->toArray()), NULL, ['class' => 'form-control city_select2 billing_same_as_project']) !!}
                                    <i class="form-group__bar"></i>
                                    <label>City</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-0">
                                    {!! Form::text('project_zip', null, ['class' => 'form-control billing_same_as_project', 'pattern' => "\d*"]) !!}
                                    <i class="form-group__bar"></i>
                                    <label>Zip</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <small><strong>Billing Address</strong></small>
                                <div class="form-group mb-0">
                                    <div class="checkbox">
                                        <label>
                                            {!! Form::checkbox('billing_same_as_project', null, true, ['id' => 'billing_same_as_project']) !!}
                                            <i class="input-helper"></i>
                                            Billing Address is Same as Project Address
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="billing_same_holder" style="display: none">
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        {!! Form::text('address', null, ['class' => 'form-control']) !!}
                                        <i class="form-group__bar"></i>
                                        <label>Address</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        {!! Form::text('city', null, ['class' => 'form-control']) !!}
                                        <i class="form-group__bar"></i>
                                        <label>City</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-0">
                                        {!! Form::text('zip', null, ['class' => 'form-control']) !!}
                                        <i class="form-group__bar"></i>
                                        <label>Zip</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="col-md-12 pt-20">
                        <div class="label-header">
                            <h2>Additional Information</h2>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::textarea('additional_information', null, ['class' => 'form-control textarea-autoheight', 'rows' => 3]) !!}
                                    <i class="form-group__bar"></i>
                                    <label>Roof conditions, best ways to contact, severity of leak, etc.</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        {{ Form::hidden('upload_images', false, ['id' => 'upload_images']) }}
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary pull-right']) !!}
                        {!! Form::submit('Upload Images', ['class' => 'btn btn-primary upload-images-btn pull-right mr-5']) !!}
                    </div>
                </div>
            {!! Form::close() !!}

        </div>
</section>
@endsection

@section('js')
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>
        function str_unslug(str) {
            var frags = str.split('_');

            for (i=0; i<frags.length; i++) {
                frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
            }

            return frags.join(' ');
        }

        $(function(){
            $('[data-mask]').inputmask();

            $('.city_select2').select2({
                placeholder: "Select City"
            });

            $('.roof_type_select').select2({
                minimumResultsForSearch: -1
            });

            $('.roof_pitch_select').select2({
                minimumResultsForSearch: -1
            });

            var select2FocusCount = 0;
            $('.roof_type_select').next('.select2').find('.select2-selection').one('focus', select2Focus).on('blur', function (e) {
                if(select2FocusCount >= 1){
                    $(this).closest('.col-md-6').next().find('input').focus(); // focus the next element when the select2 is closed
                    select2FocusCount = 0;
                }
                select2FocusCount++
            });

            $('.roof_pitch_select').next('.select2').find('.select2-selection').one('focus', select2Focus).on('blur', function (e) {
                if(select2FocusCount >= 1){
                    $(this).closest('.col-md-4').next().find('input').focus(); // focus the next element when the select2 is closed
                    select2FocusCount = 0;
                }
                select2FocusCount++
            });

            $('.city_select2').next('.select2').find('.select2-selection').one('focus', select2Focus).on('blur', function (e) {
                if(select2FocusCount >= 1){
                    $(this).closest('.col-md-4').next().find('input').focus(); // focus the next element when the select2 is closed
                    select2FocusCount = 0;
                }
                select2FocusCount++
            });

            function select2Focus() {
                $(this).closest('.select2').prev('select').select2('open');
            }

            $('body').on('change', '#other_estimate', function(){
                $('.other_text').toggle();
            });

            $('body').on('change', '.billing_same_as_project', function(){
                if($('#billing_same_as_project:checked')){
                    $('[name=address]').val($('[name=project_address]').val());
                    $('[name=city]').val(str_unslug($('[name=project_city]').val()));
                    $('[name=zip]').val($('[name=project_zip]').val());
                }
            });

            $('body').on('change', '#billing_same_as_project', function(){
                $('.billing_same_holder').slideToggle();
                if($('#billing_same_as_project').not(':checked').length){
                    $('[name=address]').val('');
                    $('[name=city]').val('');
                    $('[name=zip]').val('');
                }else{
                    $('[name=address]').val($('[name=project_address]').val());
                    $('[name=city]').val(str_unslug($('[name=project_city]').val()));
                    $('[name=zip]').val($('[name=project_zip]').val());
                }
            });

            $('body').on('change', '.advertisment', function(){
                if($(this).val() == 'referral'){
                    $('.referred_by').fadeIn();
                }else{
                    $('.referred_by').fadeOut();
                }
            });

            $('body').on('click', '.upload-images-btn', function(e){
                e.preventDefault();
                $('#upload_images').val(true);
                $('form#estimate_form').submit();
            });

        });

    </script>

@stop