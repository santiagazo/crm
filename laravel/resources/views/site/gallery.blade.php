@extends('layouts.site.main')

@section('title')
Gallery
@endsection

@section('css')
    <link href="{{ url('guest/plugins/lightgallery/dist/css/lightgallery.css') }}" rel="stylesheet">

    <style>
        .grid-widget__info{
            text-align: center;
        }
        .grid-widget__info h3{
            color: #fff;
        }
        .hover .grid-widget__info {
            position: absolute;
            bottom: -1px;
            left: 0;
            width: 100%;
            height: 100%;
            color: #fff;
            padding: 0;
            background-color: rgba(0,0,0,.8);
            border-radius: 0 0 3px 3px;
        }

        .grid-widget__info {
            padding: 15px 5px 14px;
        }

        .hover .grid-widget__footer,
        .hover .grid-widget__title{
            padding: 10px;
            height: 25%;
        }
        .grid-widget__body{
            padding: 20px;
            height: 50%;
            display: none;
        }
        .grid-widget__footer{
            padding: 12px 0 0 0;
            display: none;
        }
        .grid-widget__footer>div a{
            color: #fff;
            font-size: 20px;
            padding: 5px 5px;
        }
        .hover .grid-widget__body,
        .hover .grid-widget__footer{
            display: block;
        }
        a.font-white{
            color: #fff;
        }
        .lg-outer .lg-item:before {
            -webkit-animation-name: none !important;
            animation-name: none !important;
        }

        .bg-grey{
            background-color: #edeff1;
        }
        .custom-visible-xs{
            display: none !important;
        }  
        @media only screen and (max-width: 480px) {
            .custom-visible-xs{
                display: inline-block !important;
            }   
        }
    </style>
@stop

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card__header card__header--minimal">
                        <h2>View Our Shingle Gallery</h2>
                        <small class="custom-visible-xs">Rotate device to see all options.</small>
                    </div>

                    <div class="grid-widget grid-widget--listings">
                        <div class="col-xs-3" id="autumn-brown">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/autumn-brown.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/autumn-brown.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Autumn Brown</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/AutumnBrown.JPG') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/AutumnBrown.JPG') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="black-walnut">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/black-walnut.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/black-walnut.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Black Walnut</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/BlackWalnut.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/BlackWalnut.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/BlackWalnut2.JPG') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/BlackWalnut2.JPG') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/BlackWalnut3.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/BlackWalnut3.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/BlackWalnut4.JPG') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/BlackWalnut4.JPG') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/BlackWalnut5.JPG') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/BlackWalnut5.JPG') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/BlackWalnut6.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/BlackWalnut6.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/BlackWalnut7.JPG') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/BlackWalnut7.JPG') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="desert-sand">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/desert-sand.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/desert-sand.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Desert Sand</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/DesertSand.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/DesertSand.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/DesertSand1.JPG') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/DesertSand1.JPG') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/DesertSand2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/DesertSand2.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/DesertSand3.JPG') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/DesertSand3.JPG') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="glacier-white">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/glacier-white.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/glacier-white.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Glacier White</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3" id="harvest-gold">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/harvest-gold.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/harvest-gold.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Harvest Gold</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold1.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold1.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold2.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold3.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold3.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold7.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold7.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold8.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold8.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold9.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold9.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold10.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold10.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold11.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold11.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold12.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold12.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/HarvestGold13.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/HarvestGold13.jpg') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="mountain-slate">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/mountain-slate.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/mountain-slate.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Mountain Slate</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/MountainSlate3.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/MountainSlate3.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/MountainSlate5.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/MountainSlate5.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/MountainSlate2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/MountainSlate2.jpg') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="natural-timber">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/natural-timber.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/natural-timber.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Natural Timber</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3" id="olde-english">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/olde-english.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/olde-english.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Olde English</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/OldEnglish.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/OldEnglish.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/OldEnglish2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/OldEnglish2.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/OldEnglish9.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/OldEnglish9.jpg') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="painted-desert">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/painted-desert.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/painted-desert.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Painted Desert</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/PaintedDesert.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/PaintedDesert.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/PaintedDesert1.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/PaintedDesert1.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/PaintedDesert2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/PaintedDesert2.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/PaintedDesert3.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/PaintedDesert3.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/PaintedDesert4.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/PaintedDesert4.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/PaintedDesert5.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/PaintedDesert5.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/PaintedDesert6.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/PaintedDesert6.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/PaintedDesert7.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/PaintedDesert7.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/PaintedDesert8.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/PaintedDesert8.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/PaintedDesert9.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/PaintedDesert9.jpg') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="rustic-brown">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-brown.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-brown.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Rustic Brown</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBrown1.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBrown1.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBrown2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBrown2.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBrown3.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBrown3.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBrown4.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBrown4.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBrown7.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBrown7.jpg') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="rustic-black">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-black.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-black.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Rustic Black</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBlack.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBlack.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBlack2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBlack2.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBlack3.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBlack3.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBlack4.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBlack4.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBlack5.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBlack5.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticBlack6.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticBlack6.jpg') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="rustic-cedar">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-cedar.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-cedar.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Rustic Cedar</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3" id="rustic-evergreen">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-evergreen.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-evergreen.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Rustic Evergreen</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticEvergreen.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticEvergreen.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticEvergreen1.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticEvergreen1.jpg') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="rustic-redwood">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-redwood.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-redwood.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Rustic Redwood</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticRedwood1.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticRedwood1.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticRedwood2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticRedwood2.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticRedwood3.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticRedwood3.jpg') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="rustic-slate">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-slate.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/rustic-slate.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Rustic Slate</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticSlate.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticSlate.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticSlate1.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticSlate1.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticSlate2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticSlate2.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticSlate3.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticSlate3.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticSlate4.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticSlate4.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticSlate5.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticSlate5.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/RusticSlate6.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/RusticSlate6.jpg') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="thunderstorm-grey">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/thunderstorm-grey.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/thunderstorm-grey.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Thunderstorm Grey</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/ThunderstormGrey.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/ThunderstormGrey.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/ThunderstormGrey2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/ThunderstormGrey2.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/ThunderstormGrey3.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/ThunderstormGrey3.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/ThunderstormGrey4.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/ThunderstormGrey4.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/ThunderstormGrey5.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/ThunderstormGrey5.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/ThunderstormGrey6.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/ThunderstormGrey6.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/ThunderstormGrey7.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/ThunderstormGrey7.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/ThunderstormGrey8.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/ThunderstormGrey8.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/ThunderstormGrey9.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/ThunderstormGrey9.jpg') }}" alt="">
                            </span>
                        </div>
                        <div class="col-xs-3" id="weathered-wood">
                            <div class="grid-widget__item" data-src="{{ url('guest/images/shingles/thumbnails/optimized/weathered-wood.jpg') }}">
                                <img src="{{ url('guest/images/shingles/thumbnails/optimized/weathered-wood.png') }}" alt="">

                                <div class="grid-widget__info">
                                    <div class="grid-widget__title">
                                        <h3><a class="font-white">Weathered Wood</a></h3>
                                    </div>
                                </div>
                            </div>
                            <span data-src="{{ url('guest/images/roofs/optimized/WeatheredWood.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/WeatheredWood.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/WeatheredWood1.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/WeatheredWood1.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/WeatheredWood2.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/WeatheredWood2.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/WeatheredWood3.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/WeatheredWood3.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/WeatheredWood4.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/WeatheredWood4.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/WeatheredWood5.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/WeatheredWood5.jpg') }}" alt="">
                            </span>
                            <span data-src="{{ url('guest/images/roofs/optimized/WeatheredWood6.jpg') }}" style="display:none">
                                <img src="{{ url('guest/images/roofs/optimized/WeatheredWood6.jpg') }}" alt="">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-grey pb-40">
    <div class="container">

        <div class="row pt-40 pb-20">
            <div class="col-md-12">
                <h1 class="text-center">
                    More Pictures
                </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"> 
                <ul class="grid-widget grid-widget--listings list-unstyled">
                    <div class="col-xs-4" id="repairs">
                        <div class="grid-widget__item" data-src="{{ url('guest/images/roofs/optimized/repairs/chimneyBefore.jpg') }}">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/chimneyBefore.jpg') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3><a class="font-white">Repairs</a></h3>
                                </div>
                            </div>
                        </div>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/chimneyAfter.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/chimneyAfter.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/4217b.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/4217b.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/4217a.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/4217a.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/4220b.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/4220b.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/4220a.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/4220a.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/4862b.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/4862b.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/4862a.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/4862a.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/4982b.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/4982b.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/4982a.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/4982a.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/5497b.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/5497b.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/5497a.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/5497a.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/5952b.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/5952b.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/5952a.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/5952a.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/dyrb.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/dyrb.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/dyra.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/dyra.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/hf1b.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/hf1b.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/hf1a.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/hf1a.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/hfb.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/hfb.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/hfa.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/hfa.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/jmb.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/jmb.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/jma.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/jma.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/klb.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/klb.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/kla.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/kla.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/pf1b.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/pf1b.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/pf1a.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/pf1a.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/pfb.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/pfb.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/pfa.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/pfa.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/rb1.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/rb1.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/ra1.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/ra1.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/rb.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/rb.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/ra.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/ra.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/tob.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/tob.jpg') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/repairs/toa.jpg') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/repairs/toa.jpg') }}" alt="">
                        </span>
                    </div>
                    <div class="col-xs-4" id="tpo">
                        <div class="grid-widget__item" data-src="{{ url('guest/images/roofs/optimized/IMG_5233.JPG') }}">
                            <img src="{{ url('guest/images/roofs/optimized/IMG_5233.JPG') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3><a class="font-white">TPO</a></h3>
                                </div>
                            </div>
                        </div>
                        <span data-src="{{ url('guest/images/roofs/optimized/IMG_5233.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/IMG_5233.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo1.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo1.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo2.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo2.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo3.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo3.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo4.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo4.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo5.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo5.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo6.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo6.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo7.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo7.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo8.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo8.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo9.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo9.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo10.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo10.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo11.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo11.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo12.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo12.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo13.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo13.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo14.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo14.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo15.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo15.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo16.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo16.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpo17.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpo17.JPG') }}" alt="">
                        </span>
                        <span data-src="{{ url('guest/images/roofs/optimized/tpo/tpocolors.JPG') }}" style="display:none">
                            <img src="{{ url('guest/images/roofs/optimized/tpo/tpocolors.JPG') }}" alt="">
                        </span>
                    </div>
                    <div class="col-xs-4" id="high-end-shingles">
                        <div class="grid-widget__item" data-src="{{ url('guest/images/roofs/optimized/highEndShingle.jpg') }}">
                            <img src="{{ url('guest/images/roofs/optimized/highEndShingle.jpg') }}" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h3><a class="font-white">High-End Shingles</a></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>
            </div>

    </div>
</section>
@endsection

@section('js')
        <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
        <script src="{{ url('guest/plugins/lightgallery/dist/js/lightgallery.min.js') }}"></script>
        <script src="{{ url('guest/plugins/lightgallery/dist/js/lg-fullscreen.js') }}"></script>
        <script src="{{ url('guest/plugins/lightgallery/dist/js/lg-thumbnail.js') }}"></script>
        <script src="{{ url('guest/plugins/lightgallery/lib/jquery.mousewheel.min.js') }}"></script>

    <script>
        $(function(){
            $('#autumn-brown, #black-walnut, #desert-sand, #glacier-white, #harvest-gold, #mountain-slate, #natural-timber, #olde-english, #painted-desert, #rustic-brown, #rustic-black, #rustic-cedar, #rustic-evergreen, #rustic-redwood, #rustic-slate, #thunderstorm-grey, #weathered-wood').lightGallery({
                gallery: true,
                download: false,
                onSliderLoad  : function (el) {
                  el.lightGallery();
                }
            });

            $('#repairs, #tpo, #high-end-shingles').lightGallery({
                gallery: true,
                download: false,
                onSliderLoad  : function (el) {
                  el.lightGallery();
                }
            });
        });

    </script>

@stop
