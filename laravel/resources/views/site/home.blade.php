@extends('layouts.site.main')

@section('title')
Home
@endsection

@section('css')

<link rel='stylesheet' href='{{ url('plugins/masterslider/slider-templates/masterslider/style/masterslider.css') }}' type='text/css' media='all' />
<link rel='stylesheet' href='{{ url('plugins/masterslider/slider-templates/masterslider/skins/default/style.css') }}' type='text/css' media='all' />
<link rel='stylesheet' href='{{ url('plugins/masterslider/slider-templates/partialview/style/ms-partialview.css') }}' type='text/css' media='all' />

<style>
    .ms-layer.bottom-banner{
        background-color: rgba(0,0,0,.4);
        padding: 25px;
        width: 100%;
        text-align: center;
    }
    .ms-layer.bottom-banner li{
        font-size: 14px;
    }
    .bottom-banner.video-title li{
        line-height: 20px
    }
    .ms-layer.bulleted-list{
        list-style-type: disc;
    }
    .ms-layer.bulleted-list li{
        display: list-item;
        text-align: -webkit-match-parent;
        padding: 10px 0;
        font-size: 2em;
        line-height: 1.2;
        color: #323232;
    }
    .border-dark{
        border: #e7e7e7 solid 1px;
        background-color: #fff;
    }
    .btn-outline-dafault{
        color: #ccc;
        background-color: transparent;
        border: 2px solid #ccc;
    }
    .single-image-slider,
    .multiple-image-slider{
        padding-bottom: 30px;
    }
    h1.h1-white{
        color: #fff;
        font-size: 4.5em !important;
    }
    .card:not([class*=mdc-bg-]) {
        background: #edeff1;
    }
    .partners-section .grid-widget__item {
        border-color: #edeff1;
    }
    .video-fine-print{
        color: #fff;
    }
    @media screen and (max-width: 991px){
        .video-title{
            font-size: 14px !important;
        }
        .video-fine-print{
            font-size: 10px !important;
        }
        h1.h1-white{
            color: #fff;
            font-size: 20px !important;
        }
        .btn-lg{
            padding: 8px 13px;
            font-size: 13px;
            line-height: 1.42857143;
            border-radius: 2px;
        }
    }
    .grid-widget__info{
        text-align: center;
    }
    .grid-widget__info h3{
        color: #fff;
    }
    .grid-widget__info {
        position: absolute;
        top: -1px;
        left: 0;
        width: 100%;
        height: 55px !important;
        color: #fff;
        padding: 1px 12px;
        height: 35px;
        background-color: rgba(0,0,0,.4);
        border-radius: 0 0 3px 3px;
    }
    .hover .grid-widget__info {
        position: absolute;
        top: -1px;
        left: 0;
        width: 100%;
        height: 100% !important;
        color: #fff;
        padding: 0;
        background-color: rgba(0,0,0,.8);
        border-radius: 0 0 3px 3px;
    }
    .hover .grid-widget__footer{
        padding: 10px;
        height: 25%;
    }
    .grid-widget__body{
        padding: 10px;
        height: 50%;
        display: none;
    }
    .grid-widget__footer{
        padding: 12px 0 0 0;
        display: none;
    }
    .grid-widget__footer>div a{
        color: #fff;
        font-size: 20px;
        padding: 5px 5px;
    }
    .hover .grid-widget__body,
    .hover .grid-widget__footer{
        display: block;
    }
    .custom-visible-xs{
        display: none !important;
    }  
    @media only screen and (max-width: 480px) {
        .custom-visible-xs{
            display: block !important;
            font-size: 14px;
        }   
    }
</style>

@stop

@section('content')

<section>

<div class="container-fluid">
  	<div class="row">
  		<div class="col-md-12 padding-top-20">
            <div class="single-image-slider">
                <!-- masterslider -->
                <div class="master-slider ms-skin-default" id="singleMasterSlider">
                    <div class="ms-slide" data-delay=10>
                        <img src="{{ url('plugins/masterslider/style/blank.gif') }}" data-src="{{ url('guest/images/carousel/blue-roofing-bg.png') }}" alt="Milne Brothers Roofing"/>
                        <h1 class="ms-layer video-title h1-white" style="left:130px; top:100px "
                            data-effect="left(150)"
                            data-duration="3500"
                            data-ease="easeOutExpo"
                            data-delay="50"
                        >Why Milne Brothers Roofing?</h1>
                        <img src="{{ url('guest/images/logos/mbr-transparent-bg-lg-dark.png') }}"
                            class="ms-layer img-reponsive hidden-xs hidden-sm visible-md visible-lg"
                            style="left:250px; top:250px"
                            width=500
                            height=250
                            class="ms-layer"
                            data-effect="left(150)"
                            data-duration="3500"
                            data-ease="easeOutExpo"
                            data-delay="50"
                            alt="">
                        <div class="ms-layer video-title" style="right:130px; top:235px;"
                            data-effect="right(500)"
                            data-duration="5000"
                            data-ease="easeOutExpo"
                            data-delay="400"
                        >An Owner On Every Job*</div>

                        <div class="ms-layer video-title" style="right:130px; top:305px;"
                            data-effect="right(500)"
                            data-duration="3500"
                            data-ease="easeOutExpo"
                            data-delay="1000"
                        >In Business Since 1978</div>

                        <div class="ms-layer video-title" style="right:130px; top:375px;"
                            data-effect="right(500)"
                            data-duration="3500"
                            data-ease="easeOutExpo"
                            data-delay="1600"
                        >Limited Lifetime Labor Warranty</div>

                        <div class="ms-layer video-title" style="right:130px; top:500px;"
                            data-effect="right(500)"
                            data-duration="3500"
                            data-ease="easeOutExpo"
                            data-delay="2200">
                            <a class="btn btn-default btn-lg btn-outline-dafault" href="{{ url('/estimate') }}">Request Estimate</a>
                            <a class="btn btn-default btn-lg btn-outline-dafault" href="{{ url('/about') }}">About Us</a>
                        </div>
                        <div class="ms-layer video-fine-print" style="right:130px; bottom:50px;"
                            data-effect="right(500)"
                            data-duration="3500"
                            data-ease="easeOutExpo"
                            data-delay="2200"
                        >*Excludes some new construction jobs</div>
                    </div>
                </div>
                <!-- end of masterslider -->
            </div>
        </div>
    </div>
    <div class="row mb-40">
        <div class="col-md-12">
            <h1 class="text-center">Our Services</h1>

            <div class="slick-slider">
                <div class="col-md-4" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide35">
                    <div class="grid-widget__item hoverable" href="" tabindex="-1">
                        <img src="{{ url('guest/images/carousel/newConstruction.png') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3>New Construction</h3>
                            </div>
                            <div class="grid-widget__body hidden-sm">
                                <ul class="list-unstyled">
                                    <li>Specialize in lower pitched roofs (7/12 or less)</li>
                                    <li>Asphalt or TPO products only (no shake or tile)</li>
                                    <li>Job completed in 1 day</li>
                                    <li>Limited Lifetime Labor Warranty</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide35">
                    <div class="grid-widget__item hoverable" href="" tabindex="-1">
                        <img src="{{ url('guest/images/carousel/tearingOff.png') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3>Tearoff & Replace</h3>
                            </div>
                            <div class="grid-widget__body hidden-sm">
                                <ul class="list-unstyled">
                                    <li>Job completed in 1-2 days</li>
                                    <li>An Owner on Every Job!</li>
                                    <li>10-Year Labor Warranty</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide35">
                    <div class="grid-widget__item hoverable" href="" tabindex="-1">
                        <img src="{{ url('guest/images/carousel/oldEnglishPewter.png') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3>Recover / Redo / Re-Roof</h3>
                            </div>
                            <div class="grid-widget__body hidden-sm">
                                <ul class="list-unstyled">
                                    <li>Job completed in 1 day</li>
                                    <li>5-Year Labor Warranty</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide35">
                    <div class="grid-widget__item hoverable" href="" tabindex="-1">
                        <img src="{{ url('guest/images/carousel/swampCooler.png') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3>Swamp Removal</h3>
                            </div>
                            <div class="grid-widget__body hidden-sm">
                                <ul class="list-unstyled">
                                    <li>Remove Swamp AND Duct (HVAC only remove Swamp)</li>
                                    <li>Eliminate Eye Sore</li>
                                    <li>Eliminate Leak Possibility</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide35">
                    <div class="grid-widget__item hoverable" href="" tabindex="-1">
                        <img src="{{ url('guest/images/carousel/gutters.png') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3>Gutters</h3>
                            </div>
                            <div class="grid-widget__body hidden-sm">
                                <ul class="list-unstyled">
                                    <li>We have partnered with RGS</li>
                                    <li>We do repair or full replacement of current system</li>
                                    <li>We do complete installs of new systems too!</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide35">
                    <div class="grid-widget__item hoverable" href="" tabindex="-1">
                        <img src="{{ url('guest/images/carousel/repair.png') }}" alt="">

                        <div class="grid-widget__info">
                            <div class="grid-widget__title">
                                <h3>Repairs</h3>
                            </div>
                            <div class="grid-widget__body hidden-sm">
                                <ul class="list-unstyled">
                                    <li>3 Owners do 90% of the repairs personally</li>
                                    <li>Starting Repair Price: $125</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-grey">
    <div class="container-fluid">
        <div class="row mt-20">
            <div class="col-md-12">
                <h1 class="text-center">Our Partners<small class="text-center custom-visible-xs">Rotate device to see all options.</small></h1>
                
                <div class="card mt-40">
                    <div class="grid-widget grid-widget--listings partners-section">
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://www.mcarthurhomes.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/McAurther.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://hallmarkhomesutah.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/hallmark-homes.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://www.randyyoungconstruction.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/randy-young.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://www.reliancehomes.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/reliance.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://tailorbuilthomes.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/taylor-built.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://buildingdynamics.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/building-dynamics.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://stonewoodhomesutah.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/stonewoodhomes.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://www.vestahomesutah.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/vesta-homes.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://www.canyoncovehomes.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/canyon-cove-homes.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://sunviewutah.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/sun-view-homes.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://www.cornercanyonconstruction.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/corner-canyon-construction.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://utahoasishomes.com/index.php" target="_blank">
                                <img src="{{ url('guest/images/logos/utah-oasis-homes.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://www.fivepointrestoration.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/five-point.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="https://wasatch-front.pauldavis.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/paul-davis.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://bwprentals.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/boardwalk.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://petersonhomes-utah.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/peterson-homes.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://www.redfishbuilders.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/red-fish-builders.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="https://tsimfg.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/tassie-siding.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="https://www.paccoastsupply.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/pacific-supply.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="https://www.abcsupply.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/abc-supply-co.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="http://www.utahgutter.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/rain-gutter-specialties.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <a class="grid-widget__item" href="https://www.travisroofingsupply.com/" target="_blank">
                                <img src="{{ url('guest/images/logos/travis-roofing-supply.png') }}" class="border-dark" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</section>

@endsection


@section('js')


	<script src="{{ url('plugins/masterslider/slider-templates/masterslider/masterslider.min.js') }}"></script>

	<script>
	$(document).ready(function(){

        $( ".grid-widget__item.hoverable" ).hover(
          function() {
            $( this ).addClass( "hover" );
          }, function() {
            $( this ).removeClass( "hover" );
          }
        );

        var singleSlider = new MasterSlider();

        singleSlider.setup('singleMasterSlider' , {
            width: 1920,
            height: 800,
            space:10,
            loop:false,
            view:'fadeWave',
            layout:'fullwidth',
            autoplay:false,
            mouse:false,
            grabCursor:false,
            swipe:false,
        });


        $('.slick-slider').slick({
            centerMode: true,
            centerPadding: '10px',
            infinite: true,
            dots: true,
            speed: 750, // slide transition duration
            arrows: false,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000, // slide duration
            slidesToShow: 3,
            dotsClass: 'slick-dots slick-dots-light',
            responsive: [
                {
                  breakpoint: 1920,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                  }
                },
                {
                  breakpoint: 991,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  }
                },
            ]
        });


    });

	</script>

@stop







