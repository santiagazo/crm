@extends('layouts.site.main')

@section('title')
Estimate Images
@endsection

@section('css')
    <link rel="stylesheet" href="{{ url('plugins/dropzone/dist/dropzone.css') }}">

    <style>
        
        .middle-of-page{
            margin: 250px auto;
        }

    </style>
@stop

@section('content')
<section class="section">
    <div class="container">
        <div class="row middle-of-page">
            <div class="col-md-12">
                <div id="images_bucket" class="dropzone"></div>
                <a href="{{ url('/') }}" class="btn btn-primary mt-20 pull-right">Finish</a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
    <script src="{{ url('plugins/dropzone/dist/dropzone.js') }}"></script>

    <script>
        $(function(){
            
            // DropZone Start ------------------------------------------
                Dropzone.autoDiscover = false;

                function initDropzone(){

                    var imageDropzone = new Dropzone("#images_bucket", {
                        url: "{{ url('media/'.$estimate_id) }}",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        params: {
                            id: {{ $estimate_id }},
                            type: 'estimate'
                        },
                        addRemoveLinks: true,
                        uploadMultiple: false,
                        maxFilesize: 5, //MB
                        dictDefaultMessage: "Drag and Drop or Click Here to Upload up to 20 Images.",
                    });
                    imageDropzone.on("uploadprogress", function(file) {
                        // console.log(file);
                    });
                    imageDropzone.on("sending", function(file) {
                        // console.log(file);
                    });
                    imageDropzone.on("success", function(file, response){
                        // console.log('success');
                        // console.log(response);
                        // console.log(file);
                        
                        jsFlash('success', 'Your image ('+response.title+') has been uploaded successfully.');
                    });
                    imageDropzone.on("error", function(file, response){
                        console.log('error');
                        // console.log(response.message);
                        // console.log(file);
                        // console.log(file.size);

                        var messages = "";

                        if(file.size >= 10000000){
                            messages += "<li>Please make sure your image size is under 10MB</li>";
                        }

                        if(response.message){
                             messages += "<li>"+response.message+"</li>";
                        }

                        if(response.file){
                            $.each(response.file, function(index, error){
                                messages += '<li>'+error+' ('+file.name+')</li>';
                            });
                        }

                        jsFlash('error', messages)
                    });

                }

                initDropzone();

                // Media Delete Start ----------------------------------
                
        });
    </script>
@stop