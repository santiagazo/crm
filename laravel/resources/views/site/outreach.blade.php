@extends('layouts.site.main')

@section('title')

@endsection

@section('css')
	<link rel="stylesheet" href="">

	<style>
		.grid-widget__item>img {
    width: 90%;
}

.grid-widget__info {
    width: 90%;
}
	</style>
@stop

@section('content')




<div class="content-wrapper">
    <div class="header__recommended">
        <div class="listings-grid">
            <div class="listings-grid__item">
                <a href="listing-detail.html">
                    <div class="listings-grid__main">
                        <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="">
                        <div class="listings-grid__price">$1,175,000</div>
                    </div>

                    <div class="listings-grid__body">
                        <small>21 Shop St, San Francisco</small>
                        <h5>Integer tempor luctus maximus</h5>
                    </div>

                    <ul class="listings-grid__attrs">
                        <li><i class="listings-grid__icon listings-grid__icon--bed"></i> 03</li>
                        <li><i class="listings-grid__icon listings-grid__icon--bath"></i> 02</li>
                        <li><i class="listings-grid__icon listings-grid__icon--parking"></i> 02</li>
                    </ul>
                </a>

                <div class="actions listings-grid__favorite">
                    <div class="actions__toggle">
                        <input type="checkbox">
                        <i class="zmdi zmdi-favorite-outline"></i>
                        <i class="zmdi zmdi-favorite"></i>
                    </div>
                </div>
            </div>

            <div class="listings-grid__item">
                <a href="listing-detail.html">
                    <div class="listings-grid__main">
                        <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="">
                        <div class="listings-grid__price">$1,200,000</div>
                    </div>

                    <div class="listings-grid__body">
                        <small>Beverly Hills, CA 90210</small>
                        <h5>Duis sollicitudin ante bibendum</h5>
                    </div>

                    <ul class="listings-grid__attrs">
                        <li><i class="listings-grid__icon listings-grid__icon--bed"></i> 03</li>
                        <li><i class="listings-grid__icon listings-grid__icon--bath"></i> 03</li>
                        <li><i class="listings-grid__icon listings-grid__icon--parking"></i> 01</li>
                    </ul>
                </a>

                <div class="actions listings-grid__favorite">
                    <div class="actions__toggle">
                        <input type="checkbox">
                        <i class="zmdi zmdi-favorite-outline"></i>
                        <i class="zmdi zmdi-favorite"></i>
                    </div>
                </div>
            </div>

            <div class="listings-grid__item">
                <a href="listing-detail.html">
                    <div class="listings-grid__main">
                        <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="">
                        <div class="listings-grid__price">$910,000</div>
                    </div>

                    <div class="listings-grid__body">
                        <small>132 04th St, San Francisco</small>
                        <h5>Fusce quis libero nonorcious</h5>
                    </div>

                    <ul class="listings-grid__attrs">
                        <li><i class="listings-grid__icon listings-grid__icon--bed"></i> 02</li>
                        <li><i class="listings-grid__icon listings-grid__icon--bath"></i> 01</li>
                        <li><i class="listings-grid__icon listings-grid__icon--parking"></i> 01</li>
                    </ul>
                </a>

                <div class="actions listings-grid__favorite">
                    <div class="actions__toggle">
                        <input type="checkbox">
                        <i class="zmdi zmdi-favorite-outline"></i>
                        <i class="zmdi zmdi-favorite"></i>
                    </div>
                </div>
            </div>

            <div class="listings-grid__item">
                <a href="listing-detail.html">
                    <div class="listings-grid__main">
                        <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="">
                        <div class="listings-grid__price">$2,542,000</div>
                    </div>

                    <div class="listings-grid__body">
                        <small>132 Lockslee, San Francisco</small>
                        <h5>Pellentesque habitant</h5>
                    </div>

                    <ul class="listings-grid__attrs">
                        <li><i class="listings-grid__icon listings-grid__icon--bed"></i> 05</li>
                        <li><i class="listings-grid__icon listings-grid__icon--bath"></i> 03</li>
                        <li><i class="listings-grid__icon listings-grid__icon--parking"></i> 03</li>
                    </ul>
                </a>

                <div class="actions listings-grid__favorite">
                    <div class="actions__toggle">
                        <input type="checkbox">
                        <i class="zmdi zmdi-favorite-outline"></i>
                        <i class="zmdi zmdi-favorite"></i>
                    </div>
                </div>
            </div>

            <div class="listings-grid__item">
                <a href="listing-detail.html">
                    <div class="listings-grid__main">
                        <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="">
                        <div class="listings-grid__price">$823,000</div>
                    </div>

                    <div class="listings-grid__body">
                        <small>San Francisco, CA 900212 </small>
                        <h5>Maecenas sed purus lorem aliquet cursus</h5>
                    </div>

                    <ul class="listings-grid__attrs">
                        <li><i class="listings-grid__icon listings-grid__icon--bed"></i> 01</li>
                        <li><i class="listings-grid__icon listings-grid__icon--bath"></i> 01</li>
                        <li><i class="listings-grid__icon listings-grid__icon--parking"></i> 0</li>
                    </ul>
                </a>

                <div class="actions listings-grid__favorite">
                    <div class="actions__toggle">
                        <input type="checkbox">
                        <i class="zmdi zmdi-favorite-outline"></i>
                        <i class="zmdi zmdi-favorite"></i>
                    </div>
                </div>
            </div>

            <div class="listings-grid__item">
                <a href="listing-detail.html">
                    <div class="listings-grid__main">
                        <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="">
                        <div class="listings-grid__price">$1,120,000</div>
                    </div>

                    <div class="listings-grid__body">
                        <small>21120 Broadway St, San Fransisco</small>
                        <h5>Maecenas sed purus at lorem</h5>
                    </div>

                    <ul class="listings-grid__attrs">
                        <li><i class="listings-grid__icon listings-grid__icon--bed"></i> 03</li>
                        <li><i class="listings-grid__icon listings-grid__icon--bath"></i> 02</li>
                        <li><i class="listings-grid__icon listings-grid__icon--parking"></i> 02</li>
                    </ul>
                </a>

                <div class="actions listings-grid__favorite">
                    <div class="actions__toggle">
                        <input type="checkbox">
                        <i class="zmdi zmdi-favorite-outline"></i>
                        <i class="zmdi zmdi-favorite"></i>
                    </div>
                </div>
            </div>

            <div class="listings-grid__item">
                <a href="listing-detail.html">
                    <div class="listings-grid__main">
                        <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="">
                        <div class="listings-grid__price">$3,000,000</div>
                    </div>

                    <div class="listings-grid__body">
                        <small>San Francisco, CA 937202</small>
                        <h5>Nullam finibus libero at hendrerit</h5>
                    </div>

                    <ul class="listings-grid__attrs">
                        <li><i class="listings-grid__icon listings-grid__icon--bed"></i> 06</li>
                        <li><i class="listings-grid__icon listings-grid__icon--bath"></i> 05</li>
                        <li><i class="listings-grid__icon listings-grid__icon--parking"></i> 02</li>
                    </ul>
                </a>

                <div class="actions listings-grid__favorite">
                    <div class="actions__toggle">
                        <input type="checkbox">
                        <i class="zmdi zmdi-favorite-outline"></i>
                        <i class="zmdi zmdi-favorite"></i>
                    </div>
                </div>
            </div>

            <div class="listings-grid__item">
                <a href="listing-detail.html">
                    <div class="listings-grid__main">
                        <img src="https://placeholdit.imgix.net/~text?&w=400&h=266" alt="">
                        <div class="listings-grid__price">$1,175,000</div>
                    </div>

                    <div class="listings-grid__body">
                        <small>4313 Beverly Hills, CA 90210</small>
                        <h5>1358 Ligula Street, Unit 12</h5>
                    </div>

                    <ul class="listings-grid__attrs">
                        <li><i class="listings-grid__icon listings-grid__icon--bed"></i> 03</li>
                        <li><i class="listings-grid__icon listings-grid__icon--bath"></i> 02</li>
                        <li><i class="listings-grid__icon listings-grid__icon--parking"></i> 02</li>
                    </ul>
                </a>

                <div class="listings-grid__favorite">
                    <div class="actions__toggle">
                        <input type="checkbox">
                        <i class="zmdi zmdi-favorite-outline"></i>
                        <i class="zmdi zmdi-favorite"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="row">
	<div class="col-md-12">
        <div class="card">
            <div class="grid-widget grid-widget--listings">
                <div class="col-xs-4">
                    <a class="grid-widget__item" href="">
                        <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="">

                        <div class="grid-widget__info">
                            <h4>San Francisco, CA</h4>
                            <small>1560 Listings</small>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a class="grid-widget__item" href="">
                        <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="">

                        <div class="grid-widget__info">
                            <h4>San Jose, CA</h4>
                            <small>1232 Listings</small>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a class="grid-widget__item" href="">
                        <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="">

                        <div class="grid-widget__info">
                            <h4>Denver, CO</h4>
                            <small>1103 Listings</small>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a class="grid-widget__item" href="">
                        <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="">

                        <div class="grid-widget__info">
                            <h4>San Diego, CA</h4>
                            <small>1100 Listings</small>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a class="grid-widget__item" href="">
                        <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="">

                        <div class="grid-widget__info">
                            <h4>Dallas, TX</h4>
                            <small>988 Listings</small>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a class="grid-widget__item" href="">
                        <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="">

                        <div class="grid-widget__info">
                            <h4>Dallas, TX</h4>
                            <small>921 Listings</small>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a class="grid-widget__item" href="">
                        <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="">

                        <div class="grid-widget__info">
                            <h4>Los Angeles, CA</h4>
                            <small>888 Listings</small>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a class="grid-widget__item" href="">
                        <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="">

                        <div class="grid-widget__info">
                            <h4>Nashville, TN</h4>
                            <small>743 Listings</small>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a class="grid-widget__item" href="">
                        <img src="https://placeholdit.imgix.net/~text?&w=500&h=500" alt="">

                        <div class="grid-widget__info">
                            <h4>Palm Bay, FL</h4>
                            <small>655 Listings</small>
                        </div>
                    </a>
                </div>
            </div>

            <a class="view-more" href="grid-listings.html">
                View all locations <i class="zmdi zmdi-long-arrow-right"></i>
            </a>
        </div>
    </div>

</div>

</div>

@endsection

@section('js')

	<script>

		$(function(){

		});

	</script>

@stop