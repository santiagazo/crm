@extends('layouts.site.main')

@section('title')
Shingle Colors
@endsection

@section('css')
<style>
ul, li {
	list-style: none;
}
h1 {
	font-size: 150%;
	font-weight: bold;
	text-align: center;
}
.my-slider {
	height: 250px;
	margin: 0 auto;
	/*width: 900px;*/
	width: 100%;
}
.my-slider ul {
	height: 450px;
	overflow: hidden;
}
.my-slider li {
	float: left;
}

</style>

@endsection

@section('content')

<div class="content-wrapper">
    <div class="my-slider js-imageslider">
        <ul class="my-slider-list">
            <li class="my-slider-item">
            	<img src="{{ url('images/logos/pacific-supply.png') }}" width="500" alt="">
            </li>
            <li class="my-slider-item">
            	<img src="{{ url('images/logos/rsf.png') }}" width="500" alt="">
            </li>
            <li class="my-slider-item">
            	<img src="{{ url('images/logos/sun-view-homes.png') }}" width="500" alt="">
            </li>
            <li class="my-slider-item">
            	<img src="{{ url('images/logos/tamko.png') }}" width="500" alt="">
            </li>
            <li class="my-slider-item">
            	<img src="{{ url('images/logos/tassie-fabrication.png') }}" width="500" alt="">
            </li>
        </ul>
    </div>
</div>




@endsection

@section('js')
<script src="{{ url('plugins/image-slider/jquery.imageslider.js') }}"></script>
<script>
    $(function() {
    	$('.js-imageslider').imageslider({
    		slideContainer: '.my-slider-list',
    		slideItems: '.my-slider-item',
    		slideDistance: 1,
    		slideDuration: 1,
    		slideEasing: 'linear',
    		resizable: true,
    		reverse: false,
    		pause: true
    	});
    });
</script>

@stop

