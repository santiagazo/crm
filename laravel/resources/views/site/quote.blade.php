@extends('layouts.site.main')

@section('title')
Your Response
@endsection

@section('css')

<style>
    ol>li{
        font-size: 16px;
    }
    .contact__form {
        background-color: #0058A8;
    }

    .contact {
        margin: 0px 0 50px;
    }

    .contact__form .btn,
    .contact__form .btn:focus,
    .contact__form .btn:hover {
        color: #0058A8;
        box-shadow: none;
    }

    .rmd-contact-list {
        font-size: 16px;
    }

    .rmd-contact-list__title {
        font-size: 16px;
    }

    .mtb-fit{
        margin: 175px auto;
    }
    .form-group--light .checkbox input:checked+.input-helper:before, 
    .form-group--light .radio input:checked+.input-helper:before {
        background-color: transparent;
    }
</style>

@stop

@section('content')
<section>
    <div class="container">
        <div class="mtb-fit">
            <div class="row">
                <div class="col-md-12">
                    <div class="card contact__inner clearfix">

                        @if($response == 'accept')
                            <div class="col-sm-6">
                                <div class="contact__info">
                                    <ul class="rmd-contact-list">
                                        <li class="rmd-contact-list__title">Congrats on the soon-to-be new roof! <br/> Here's the run down on what will happen next...</li>
                                        <li>Fill out the form so that we know it's you that is accepting the job.</li>
                                        <li>Wait for our call. We'll get a hold of you to let you know our window for your roof just as soon as we can.</li>
                                        <li>Enjoy a brand new roof with the satisfaction of a job well done!</li>
                                    </ul>

                                </div>
                            </div>
                        @elseif($response == 'decline')
                            <div class="col-sm-6">
                                <div class="contact__info">
                                    <ul class="rmd-contact-list">
                                        <li class="rmd-contact-list__title">Bummer, we didn't get your business. Thanks for letting us know.</li>
                                        <li>
                                            We help the vast majority of the people we quote, but occasionally one or two go some where else. We always want to know why.
                                        </li>
                                        <li>
                                            If you would be so kind and fill out the form to the right, it would help us know that <em>you</em> really are <em>you</em> and maybe even what we missed
                                            that left us out of the running for your business. Thanks so much!
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @else
                            <div class="col-sm-6">
                                <div class="contact__info">
                                    <ul class="rmd-contact-list">
                                        <li class="rmd-contact-list__title">You like the quote! Great! <br/> Here's the run down on what will happen next...</li>
                                        <li>Fill out the form so that we know it's you that likes the quote. Let us know any questions you have!</li>
                                        <li>Wait for our call. We'll get a hold of you to answer any questions you submit just as soon as we can.</li>
                                        <li>When you're ready, enjoy a brand new roof with the satisfaction of a job well done!</li>
                                    </ul>
                                </div>
                            </div>
                        @endif

                        <div class="col-sm-6">
                            {!! Form::open( ['url' => 'quote', 'class' => 'contact__form']) !!}

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-group--light form-group--float">
                                            <input type="text" name="name" class="form-control">
                                            <label>Name</label>
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-group--light form-group--float">
                                            <input type="text" name="email" class="form-control">
                                            <label>Email Address</label>
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-group--light form-group--float">
                                            <input type="text" name="phone" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                            <label>Contact Number</label>
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group form-group--light form-group--float">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        {{ Form::checkbox('is_cash_discount', true, null, ['class' => 'form-control']) }}
                                                        <i class="input-helper"></i>
                                                        Add Cash Discount
                                                    </label>
                                                </div>
                                            </div>
                                            @if($bid->is_pre_dry_in)
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            {{ Form::checkbox('is_pre_dry_in', true, null, ['class' => 'form-control']) }}
                                                            <i class="input-helper"></i>
                                                            Add Pre Dry-In
                                                        </label>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            @if($bid->is_six_nail)
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            {{ Form::checkbox('is_six_nail', true, null, ['class' => 'form-control']) }}
                                                            <i class="input-helper"></i>
                                                            Add Six Nail Shingle
                                                        </label>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($bid->is_owner_tear_off)
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            {{ Form::checkbox('is_owner_tear_off', true, null, ['class' => 'form-control']) }}
                                                            <i class="input-helper"></i>
                                                            Add Owner Tear Off
                                                        </label>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-group--light form-group--float">
                                            <textarea name="message" class="form-control textarea-autoheight"></textarea>
                                            <label>Message (Optional)</label>
                                            <i class="form-group__bar"></i>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                {!! Form::hidden('bid_id', $bid_id) !!}
                                {!! Form::hidden('material_list_id', $material_list_id) !!}
                                {!! Form::hidden('person_id', $person_id) !!}
                                {!! Form::hidden('status', $response) !!}

                                <div class="m-t-30">
                                    <button type="submit" class="btn brn-sm btn-default btn-static">Submit</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')

<script src="{{ url('auth/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ url('auth/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ url('auth/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
    $(function(){
        $('[data-mask]').inputmask();
    });
</script>
@stop
