@extends('layouts.site.main')

@section('title')
Home
@endsection

@section('css')

<link rel='stylesheet' href='{{ url('plugins/masterslider/slider-templates/masterslider/style/masterslider.css') }}' type='text/css' media='all' />
<link rel='stylesheet' href='{{ url('plugins/masterslider/slider-templates/masterslider/skins/default/style.css') }}' type='text/css' media='all' />
<link rel='stylesheet' href='{{ url('plugins/masterslider/slider-templates/partialview/style/ms-partialview.css') }}' type='text/css' media='all' />

<style>
    .grid-widget__info{
        text-align: center;
    }
    .grid-widget__info h3{
        color: #fff;
    }
    .hover .grid-widget__info {
        position: absolute;
        bottom: -1px;
        left: 0;
        width: 100%;
        height: 100%;
        color: #fff;
        padding: 0;
        background-color: rgba(0,0,0,.8);
        border-radius: 0 0 3px 3px;
    }
    .hover .grid-widget__footer,
    .hover .grid-widget__title{
        padding: 10px;
        height: 25%;
    }
    .grid-widget__body{
        padding: 20px;
        height: 50%;
        display: none;
    }
    .grid-widget__footer{
        padding: 12px 0 0 0;
        display: none;
    }
    .grid-widget__footer>div a{
        color: #fff;
        font-size: 20px;
        padding: 5px 5px;
    }
    .hover .grid-widget__body,
    .hover .grid-widget__footer{
        display: block;
    }
    .ms-layer.bottom-banner{
        background-color: #323232;
        padding: 25px;
        width: 100%;
        text-align: center;
    }
    .ms-layer.bulleted-list{
        list-style-type: disc;
    }
    .ms-layer.bulleted-list li{
        display: list-item;
        text-align: -webkit-match-parent;
        padding: 10px 0;
        font-size: 2em;
        line-height: 1.2;
        color: #323232;
    }
    .roof-colors{
        height: 500px;
        width: 100%;
        background-color: #fff;
        background-repeat: no-repeat;
        background-position: top center;
    }
    .roof-colors ul{
        margin: 50px 0;
    }
    .roof-colors li{
        color: #fff;
        padding: 10px 0;
        font-size: 18px;
        line-height: 1.1;
    }
	.dark-out{
        height: 500px;
        width: 100%;
        background-color: rgba(000, 000, 000, .2);
    }
    .roof-text-area{
        padding: 25px 25px;
        height: 350px;
    }
    .banner{
        background-color: #323232;
        padding: 15px 0;
        text-align: center;
    }
    .banner h1{
        color: #fff;
    }
    .border-dark{
        border: #e7e7e7 solid 1px;
        background-color: #fff;
    }

    .tab-pane{
        padding: 20px;
    }

    .card.border-blue {
        position: relative;
        box-shadow: 0 1px 4px #0058A8;
        margin-bottom: 25px;
        border-radius: 2px;
        display: block;
        border-left: 1px solid #0058A8;
    }

    .nav-tabs.nav-justified>li.active {
        border-right: 1px solid #0058A8;
        border-top: 1px solid #0058A8;
        background-color: #fff;
    }

    .nav-tabs.nav-justified>li>a {
        border-bottom: 1px solid #0058A8;
        background-color: #edeff1;
    }
    .nav-tabs.nav-justified>li {
        border-top: 1px solid #0058A8;
        border-right: 1px solid #0058A8;
        background-color: #edeff1;
    }
    .nav-tabs.nav-justified>.active>a,
    .nav-tabs.nav-justified>.active>a:focus,
    .nav-tabs.nav-justified>.active>a:hover {
        border-bottom-color: transparent;
        background-color: #fff;
    }

    .panel-group {
         margin-bottom: 0px;
    }

    a.font-white{
        color: #fff;
    }

    @media (min-width: 480px){
        .grid-widget__item {
            border-color: #edeff1;
        }
    }

    .grid-widget__item.manufacture-images{
        border-color: #edeff1;
    }

    .responsiveTabs{
        max-width:800px;
        margin:auto;
    }

    .card .panel-title a {
        color: #0058A8;
    }

    h1.font-white{
        color: #fff;
    }

</style>

@stop

@section('content')

<section>
    	<div class="row">

            <div class="grid-widget grid-widget--listings">
                <div class="col-xs-6" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide30">
                    <div class="grid-widget__item" href="" tabindex="-1">
                        <a href="{{ url('gallery') }}">
                            <img src="{{ url('guest/images/shingles/anthonyshouse.png') }}" class="img-responsive" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h1 class="font-white">Milne Brothers Gallery</h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6" style="" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide31">
                    <div class="grid-widget__item" href="" tabindex="-1">
                        <a href="https://www.tamko.com/colors" target="_blank">
                            <img src="{{ url('guest/images/shingles/tamko.png') }}" class="img-responsive" alt="">

                            <div class="grid-widget__info">
                                <div class="grid-widget__title">
                                    <h1 class="font-white">TAMKO Shingle Colors</h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
        </div>
    </div>

    <div class="row pt-40 pb-20">
        <div class="col-md-12">
            <h1 class="text-center">
                Why TAMKO?
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-offset-2 col-md-8">
                <div class="card border-blue responsiveTabs">
                    <ul id="myTab" class="nav nav-tabs nav-justified" style="margin-bottom: 15px;">
                        <li class="active"><a href="#sealant" data-toggle="tab">Sealant Strip</a></li>
                        <li><a href="#installation" data-toggle="tab">Installation</a></li>
                        <li><a href="#warranty" data-toggle="tab">Warranty</a></li>
                        <li><a href="#color" data-toggle="tab">Color Selection</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content" >
                        <div class="tab-pane fade in active" id='sealant'>
                            <p>TAMKO shingles have a proprietary sealant strip that seals better than any other brand, which is why we install with the standard 4 nail/shingle application.  You get a 110MPH wind warranty with 4 nails/shingle, and 130MPH wind warranty with a 6 nails/shingle application (wind warranty applies once the shingles have had a chance to seal, which might not happen until the Spring if your roof was installed during the Fall or Winter).</p>
                        </div>
                        <div class="tab-pane fade" id='installation'>
                            <p>TAMKO shingles are the easiest brand to work with in extreme temperatures during the summer and the winter.  They don't mark up as much in the summer heat and they are less brittle in the winter cold.</p>
                        </div>
                        <div class="tab-pane fade" id="warranty">
                            <p>TAMKO offers a 15- to 20-year non-prorated Full Start Period, Labor and Material (labor does not include removal of shingles).  Most "Lifetime" Shingle Warranties are only Full Start for the first 10 years.  It Pays to Compare.</p>
                        </div>
                        <div class="tab-pane fade" id="color">
                            <p>TAMKO offers more color choices than any other brand, 17 color options!  See all the colors they have to offer. See the colors <a href="{{ url('/gallery') }}">here.</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    <div class="bg-grey mt-20 pb-40">
        <div class="container">
            <div class="row pt-40">
                <div class="col-md-12">
                    <h1 class="text-center">
                        Other Shingle Manufacturers
                    </h1>
                </div>
            </div>
            <div class="row mt-20">
                <div class="col-md-12">
                    <div class="card bg-grey">
                        <div class="grid-widget grid-widget--listings">

                            <div class="col-xs-3">
                                <a class="grid-widget__item manufacture-images" href=" https://www.owenscorning.com/roofing/shingles/oakridge" target="_blank">
                                    <img src="{{ url('guest/images/logos/owencorning.png') }}" class="border-dark" alt="">
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <a class="grid-widget__item" href="http://www.certainteed.com/residential-roofing/products/landmark/" target="_blank">
                                    <img src="{{ url('guest/images/logos/certainteed.png') }}" class="border-dark" alt="">
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <a class="grid-widget__item" href="https://malarkeyroofing.com/shingle-color-selector?product=9&category=2" target="_blank">
                                    <img src="{{ url('guest/images/logos/malarkey.png') }}" class="border-dark" alt="">
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <a class="grid-widget__item"
                                href="http://www.gaf.com/Roofing/Residential/Products/Shingles/Timberline/Natural_Shadow#"
                                target="_blank">
                                    <img src="{{ url('guest/images/logos/gaf.png') }}" class="border-dark" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>

@endsection


@section('js')

	<script src="{{ url('plugins/masterslider/slider-templates/masterslider/masterslider.min.js') }}"></script>
    <script src="{{ url('guest/plugins/bootstrap-responsive-tabs/responsive-tabs.js') }}"></script>
@stop







