@extends('layouts.site.main')

@section('title')
Testimonials
@endsection

@section('css')
	<!-- rateYo - Ratings -->
    <link rel="stylesheet" href="{{ url('plugins/rateYo/src/jquery.rateyo.css') }}">
@stop

@section('content')
<section class="section">
    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <div class="card agent-reviews">

                    <div class="card__body">
                        <div class="card__sub">
                            <h4>Our Overall Rankings</h4>
                            <div class="list-group list-group--bordered list-group--condensed list-group--striped">
                                <div class="media list-group-item">
                                    <div class="pull-right">
                                        <div class="rmd-rate" data-rate-value="{{ $qualityOfWorkmanship }}" data-rate-readonly="true"></div>
                                    </div>
                                    <div class="media-body">
                                        Quality of Workmanship
                                    </div>
                                </div>
                                <div class="media list-group-item">
                                    <div class="pull-right">
                                        <div class="rmd-rate" data-rate-value="{{ $cleanliness }}" data-rate-readonly="true"></div>
                                    </div>
                                    <div class="media-body">
                                        Cleanliness
                                    </div>
                                </div>
                                <div class="media list-group-item">
                                    <div class="pull-right">
                                        <div class="rmd-rate" data-rate-value="{{ $professionalism }}" data-rate-readonly="true"></div>
                                    </div>
                                    <div class="media-body">
                                        Professionalism
                                    </div>
                                </div>
                                <div class="media list-group-item">
                                    <div class="pull-right">
                                        <div class="rmd-rate" data-rate-value="{{ $jobCompletedOnTime }}" data-rate-readonly="true"></div>
                                    </div>
                                    <div class="media-body">
                                        Job Completed On Time
                                    </div>
                                </div>
                                <div class="media list-group-item">
                                    <div class="pull-right">
                                        <div class="rmd-rate" data-rate-value="{{ $price }}" data-rate-readonly="true"></div>
                                    </div>
                                    <div class="media-body">
                                        Price
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card__sub">
                            <h4>{{ $testimonialsCount }} Customer Reviews</h4>

                            @foreach($testimonials as $testimonial)
                                <div class="agent-reviews__item">
                                    <div class="text-strong">By {{ $testimonial->name }} on {{ $testimonial->created_at->diffForHumans() }}
                                        @if($testimonial->person_id)
                                            <span class="pull-right text-success"><i class="zmdi zmdi-check-square"></i> Verified Customer</span>
                                        @endif
                                    </div>
                                    <div class="rmd-rate" data-rate-value="{{ $testimonial->stars }}" data-rate-readonly="true"></div>

                                    <p>{{ $testimonial->comment }}</p>
                                </div>
                            @endforeach

                        </div>

                        <div class="paginator">
                            {{ $testimonials->links() }}
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4" id="agent-question">
                {!! Form::open( ['url' => 'testimonials', 'class' => 'card'] ) !!}
                    <div class="card__header">
                        <h2>Give Us a Review</h2>
                        <small>Our quality of workmanship says a lot about us. Let other's know what you think.</small>
                    </div>

                    <div class="card__body m-t-10">
                        <div class="form-group form-group--float">
                            <input type="text" name='name' class="form-control">
                            <i class="form-group__bar"></i>
                            <label>Name</label>
                        </div>
                        <div class="form-group form-group--float">
                            <input type="text" name='email' class="form-control">
                            <i class="form-group__bar"></i>
                            <label>Email Address</label>
                        </div>
                        <div class="form-group form-group--float">
                            <input type="text" name='phone' class="form-control">
                            <i class="form-group__bar"></i>
                            <label>Contact Number</label>
                        </div>
                        <div class="form-group form-group--float">
                            <textarea class="form-control textarea-autoheight" name='review'></textarea>
                            <i class="form-group__bar"></i>
                            <label>Review</label>
                        </div>

                        <div class="pb-20">
                            <label for="quality_Of_Workmanship">Quality of Workmanship</label>
                            <div id="quality_Of_Workmanship" data-for="quality_Of_Workmanship"></div>
                        </div>
                        <div class="pb-20">
                            <label for="cleanliness">Cleanliness</label>
                            <div id="cleanliness" data-for="cleanliness"></div>
                        </div>
                        <div class="pb-20">
                            <label for="professionalism">Professionalism</label>
                            <div id="professionalism" data-for="professionalism"></div>
                        </div>
                        <div class="pb-20">
                            <label for="job_Completed_On_Time">Job Completed On Time</label>
                            <div id="job_Completed_On_Time" data-for="job_Completed_On_Time"></div>
                        </div>
                        <div class="pb-20">
                            <label for="price">Price</label>
                            <div id="price" data-for="price"></div>
                        </div>
                        {!! Form::hidden('ratings', NULL, ['id' => 'ratings']) !!}
                    </div>

                    <div class="card__footer">
                        <button class="btn btn-primary" id="submit-btn">Submit</button>
                        <button class="btn btn-sm btn-link">Reset</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')

    <!-- rateYo - Ratings -->
    <script src="{{ url('plugins/rateYo/src/jquery.rateyo.js') }}"></script>
    <script>

		$(function(){
            var ratings = [];

            $("#quality_Of_Workmanship, #professionalism, #cleanliness, #job_Completed_On_Time, #price").rateYo({
                starWidth: "20px",
                fullStar:true,
                rating: 0,
                ratedFill: "#fcd461",
                onSet: function (rating, rateYoInstance) {
                  var input = $(rateYoInstance.node).data('for');
                  ratings.push({name:input, number:rating});
                }
            });

            $('body').on('click', '#submit-btn', function(e){
                e.preventDefault();
                ratingsString = JSON.stringify(ratings);
                $('#ratings').val(ratingsString);

                console.log(ratingsString);
                $('form.card').submit();
            });

		});

	</script>

@stop
