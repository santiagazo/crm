<?php
/*
|--------------------------------------------------------------------------
| Authenticated Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => 'auth'], function()
{
    // User
    Route::get('/dashboard', 'UserController@dashboard');

    /*
    |-----------------------------------------------------------------------
    | Admin Routes
    |-----------------------------------------------------------------------
    */
    Route::group(['middleware' => 'admin'], function()
    {
        // Admin
        Route::get('admin/dashboard', 'AdminController@dashboard');

        // User
        Route::get('admin/users', 'UserController@index');
        Route::get('admin/users/create', 'UserController@create');
        Route::get('admin/users/edit/{id}', 'UserController@edit');
        Route::get('admin/users/delete/{id}', 'UserController@delete');
        Route::post('admin/users', 'UserController@store');
        Route::post('admin/users/{id}', 'UserController@update');

        // Estimates
        Route::get('admin/estimates/satellite/{id}', 'EstimatesController@satellite');
        Route::get('admin/estimates/show/{id}', 'EstimatesController@show');
        Route::get('admin/estimates/edit/{id}', 'EstimatesController@edit');
        Route::get('admin/estimates/create/{person_id?}', 'EstimatesController@create');
        Route::get('admin/estimates', 'EstimatesController@index');
        Route::post('admin/estimates', "EstimatesController@store");
        Route::post('admin/estimates/{id}', 'EstimatesController@update');
        Route::post('admin/estimates/satellite/{id}', 'EstimatesController@updateSatellite');
        Route::get('admin/estimates/satellite/delete/{id}', 'EstimatesController@deleteSatellite');

        // Bids
        Route::get('admin/bids/create/{person_id?}/{estimate_id?}', 'BidsController@create');
        Route::get('admin/bids/edit/{id}', 'BidsController@edit');
        Route::get('admin/bids/show/{id}', 'BidsController@show');
        Route::get('admin/bids/products/{q?}', 'BidsController@products');
        Route::get('admin/bids/delete/{id}', 'BidsController@delete');
        Route::post('admin/bids', 'BidsController@store');
        Route::post('admin/bids/{bid_id}', 'BidsController@update');
        Route::get('admin/bids/{purchase_order?}/{user_name?}/{customer_name?}/{job_address?}/{job_city?}/{subdivision?}/{lot?}', 'BidsController@index');

        // Jobs
        Route::get('admin/jobs/create/{bid_id?}', 'JobsController@create');
        Route::get('admin/jobs/edit/{id}', 'JobsController@edit');
        Route::get('admin/jobs/show/{id}', 'JobsController@show');
        Route::get('admin/jobs/products/{q?}', 'JobsController@products');
        Route::get('admin/jobs/delete/{id}', 'JobsController@delete');
        Route::post('admin/jobs', 'JobsController@store');
        Route::post('admin/jobs/{job_id}', 'JobsController@update');
        Route::get('admin/jobs/{purchase_order?}/{user_name?}/{customer_name?}/{job_address?}/{job_city?}/{subdivision?}/{lot?}', 'JobsController@index');

        // Material Lists
        Route::get('admin/material_lists/create/{materialable_id}/{materialable_type}', 'MaterialListsController@create');
        Route::post('admin/material_lists/bid', 'MaterialListsController@storeBid');
        Route::post('admin/material_lists/job', 'MaterialListsController@storeJob');
        Route::get('admin/material_lists/edit/{material_list_id}/{materialable_type}', 'MaterialListsController@edit');
        Route::post('admin/material_lists/bid/{material_list_id}', 'MaterialListsController@updateBid');
        Route::post('admin/material_lists/job/{material_list_id}', 'MaterialListsController@updateJob');

        // Products
        Route::get('admin/products', 'ProductsController@index');
        Route::get('admin/products/colors/{id}', 'ProductsController@colors');
        Route::get('admin/products/create', 'ProductsController@create');
        Route::get('admin/products/edit/{id}', 'ProductsController@edit');
        Route::get('admin/products/delete/{id}', 'ProductsController@delete');
        Route::get('admin/products/{company_id}/{name}/{house_price}/{supplier_price}/{unit}/{description}', 'ProductsController@index');
        Route::post('admin/products', 'ProductsController@store');
        Route::post('admin/products/{id}', 'ProductsController@update');

        // Company Product (Bulk)
        Route::get('admin/company/products/show/{company_id}', 'CompanyProductsController@show');
        Route::get('admin/company/products/edit/{company_id}', 'CompanyProductsController@edit');
        Route::post('admin/company/products/{company_id}', 'CompanyProductsController@update');

        // Product Categories
        Route::get('admin/categories', 'CategoriesController@index');
        Route::get('admin/categories/create', 'CategoriesController@create');
        Route::get('admin/categories/edit/{id}', 'CategoriesController@edit');
        Route::get('admin/categories/delete/{id}', 'CategoriesController@delete');
        Route::get('admin/categories/{company_id}/{name}/{house_price}/{supplier_price}/{unit}/{description}', 'CategoriesController@index');
        Route::post('admin/categories', 'CategoriesController@store');
        Route::post('admin/categories/{id}', 'CategoriesController@update');

        // Companies
        Route::get('admin/companies', 'CompaniesController@index');
        Route::get('admin/companies/create', 'CompaniesController@create');
        Route::get('admin/companies/show/{id}', 'CompaniesController@show');
        Route::get('admin/companies/edit/{id}', 'CompaniesController@edit');
        Route::get('admin/companies/delete/{id}', 'CompaniesController@delete');
        Route::get('admin/companies/{name}/{type_id}', 'CompaniesController@index');
        Route::post('admin/companies', 'CompaniesController@store');
        Route::post('admin/companies/{id}', 'CompaniesController@update');

        // People
        Route::get('admin/people', 'PeopleController@index');
        Route::get('admin/people/create', 'PeopleController@create');
        Route::get('admin/people/edit/{id?}', 'PeopleController@edit');
        Route::get('admin/people/show/{id?}', 'PeopleController@show');
        Route::get('admin/people/delete/{id}', 'PeopleController@delete');
        Route::get('admin/people/{name}/{address}/{city}/{state}/{zip}/{phone}/{email}', 'PeopleController@index');
        Route::post('admin/people', 'PeopleController@store');
        Route::post('admin/people/{id}', 'PeopleController@update');

        // Templates
        Route::get('admin/templates/show/{bid_id}', 'TemplatesController@show');
        Route::get('admin/templates/copy/{bid_id}', 'TemplatesController@copy');
        Route::get('admin/templates/delete/{bid_id}', 'TemplatesController@delete');
        Route::get('admin/templates/{bid_id?}', 'TemplatesController@index');


        // PDFs
        Route::get('admin/pdf/estimates/{estimate_id}', 'PdfController@estimates');
        Route::get('admin/pdf/bids/{job_id}', 'PdfController@bids');
        Route::get('admin/pdf/jobs/{job_id}', 'PdfController@jobs');
        Route::get('admin/pdf/suppliers/{job_id}', 'PdfController@suppliers');
        Route::get('admin/pdf/invoices/{job_id}', 'PdfController@invoices');
        Route::get('admin/pdf/bids/create/{job_id}', 'PdfController@createBid');
        Route::get('admin/pdf/jobs/create/{job_id}', 'PdfController@createJob');
        Route::get('admin/pdf/estimates/create/{estimate_id}', 'PdfController@createPaperEstimate');
        Route::get('admin/pdf/suppliers/create/{job_id}', 'PdfController@createSupplier');
        Route::get('admin/pdf/invoices/create/{job_id}', 'PdfController@createInvoice');
        Route::get('admin/pdf/bids/show/{job_id}', 'PdfController@show');
        Route::get('admin/pdf/jobs/show/{job_id}', 'PdfController@show');
        Route::get('admin/pdf/suppliers/show/{job_id}', 'PdfController@show');
        Route::get('admin/pdf/invoices/show/{job_id}', 'PdfController@show');
        Route::get('admin/pdf/estimates/show/{estimate_id}', 'PdfController@show');
        Route::post('admin/pdfs/bids/email/{job_id}', 'PdfController@email');
        Route::post('admin/pdfs/jobs/email/{job_id}', 'PdfController@email');
        Route::post('admin/pdfs/suppliers/email/{job_id}', 'PdfController@email');
        Route::post('admin/pdfs/invoices/email/{job_id}', 'PdfController@email');

        // Payroll
        Route::get('admin/payroll', 'PayrollController@index');

        // PDF Testing
        Route::get('admin/pdf/pdf/{job_id}', 'PdfController@pdf');
        Route::get('admin/pdf/html/{job_id}', 'PdfController@html');

        // Notes
        Route::post('admin/notes', 'NotesController@store_note');
        
        // Media
        Route::get('admin/media/delete/{mediable_id?}/{mediable_type?}', 'MediaController@delete');
        Route::post('admin/media/{mediable_id}', 'MediaController@store');

        // Taxes
        Route::get('admin/taxes', 'TaxesController@index');
    });



    /*
    |-----------------------------------------------------------------------
    | Owner Routes
    |-----------------------------------------------------------------------
    */
    Route::group(['middleware' => 'owner'], function()
    {


    });

    /*
    |-----------------------------------------------------------------------
    | Employee Routes
    |-----------------------------------------------------------------------
    */
    Route::group(['middleware' => 'employee'], function()
    {
        // Employee
        Route::get('employee/dashboard', 'EmployeeController@dashboard');        

    });


});


/*
|--------------------------------------------------------------------------
| Public Routes
|--------------------------------------------------------------------------
*/

Auth::routes();
Route::get('/outreach', 'SiteController@outreach');
Route::get('/warranty', 'SiteController@warranty');
Route::get('/shingle', 'SiteController@shingle');
Route::get('/gallery', 'SiteController@gallery');
Route::get('/testimonials', 'SiteController@testimonials');
Route::post('/testimonials', 'SiteController@storeTestimonials');
Route::get('/about', 'SiteController@about');
Route::get('/contact', 'SiteController@contact');
Route::post('/contact', 'SiteController@storeContact');
Route::get('/estimate', 'SiteController@estimate');
Route::post('/estimate', 'SiteController@storeEstimate');
Route::get('/estimate/images/{estimate_id}', 'SiteController@images');
Route::post('/media/{estimate_id}', 'MediaController@store');
Route::get('/quote/{bid_id}/{material_list_id}/{person_id}/{response}', 'SiteController@quote');
Route::post('/quote', 'SiteController@storeQuote');
Route::get('/home', 'SiteController@index');
Route::get('/sandbox', 'SandboxController@index');
Route::get('/sandbox/mail', 'SandboxController@mail');
Route::get('estimates/satellite/{id}', 'EstimatesController@satellite');
Route::get('/', 'SiteController@index');